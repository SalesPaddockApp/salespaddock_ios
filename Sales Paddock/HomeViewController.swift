//
//  HomeViewController.swift
//  Sales Paddock
//
//  Created by 3Embed on 08/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDataSource,  UITableViewDelegate {

    @IBOutlet weak var homeTableView: UITableView!
    let model: [[UIColor]] = generateRandomData()
    var storedOffsets = [Int: CGFloat]()
    
    let showNamesArray = ["Rahul 1", "Rahul 2", "Rahul 3", "Rahul 4", "Rahul 5", "Rahul 1", "Rahul 2", "Rahul 3", "Rahul 4", "Rahul 5"]
    let sectionNamesArray = [["name":"Rahul Sharma", "type":2], ["name":"Rahul Patil", "type":1], ["name":"Rahul Jain", "type":1], ["name":"Ritesh Patil", "type":1], ["name":"Sanket Patil", "type":1], ["name":"Rahul Sharma", "type":1], ["name":"Rahul Patil", "type":1], ["name":"Rahul Jain", "type":1], ["name":"Ritesh Patil", "type":1], ["name":"Sanket Patil", "type":1]]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        homeTableView.estimatedRowHeight = 270
        homeTableView.rowHeight = UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
        
    // MARK: - UITableViewDataSource protocol
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionNamesArray.count+1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let  footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        footerView.backgroundColor = UIColor(netHex: 0x666666)
        switch section {
        case 0:
            tableView.sectionFooterHeight = 0;
            footerView.backgroundColor = UIColor.clear
            return footerView
        default:
            tableView.sectionFooterHeight = 1;
            return footerView;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {        
        let index = ((indexPath as NSIndexPath).section, (indexPath as NSIndexPath).row)
        switch index {
        case (0, 0):
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.userNameTabelViewCell, for: indexPath) as? UserNameTableViewCell
            cell!.userNameLabel?.text = "Hi, Rahul"
            cell!.userNameLabel?.font = UIFont (name: "CircularAirPro-Bold", size: 36)
            return cell!
        case (_, 0):
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.userNameTabelViewCell, for: indexPath) as? UserNameTableViewCell
            cell!.userNameLabel?.text = sectionNamesArray[(indexPath as NSIndexPath).section-1]["name"] as? String
            cell!.userNameLabel?.font = UIFont (name: "CircularAirPro-Book", size: 16)
            return cell!
        default:
            let isHorseDetail = sectionNamesArray[(indexPath as NSIndexPath).section-1]["type"] as! Int
            if isHorseDetail==1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.horseCollectionViewAddTableViewCell, for: indexPath) as? HorseCollectionViewAddTableViewCell
                return cell!
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.showCollectionViewAddTableViewCell, for: indexPath) as? ShowCollectionViewAddTableViewCell
                return cell!
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch (indexPath as NSIndexPath).row {
        case 0:
            break
        default:
            let isHorseDetail = sectionNamesArray[(indexPath as NSIndexPath).section-1]["type"] as! Int
            if isHorseDetail==1 {
                guard let horseCollectionViewAddTableViewCell = cell as? HorseCollectionViewAddTableViewCell else { return }
                horseCollectionViewAddTableViewCell.setCollectionViewDataSourceDelegate(self, forRow: (indexPath as NSIndexPath).section)
                horseCollectionViewAddTableViewCell.collectionViewOffset = storedOffsets[(indexPath as NSIndexPath).section] ?? 0
            } else {
                guard let showCollectionViewAddTableViewCell = cell as? ShowCollectionViewAddTableViewCell else { return }
                showCollectionViewAddTableViewCell.setCollectionViewDataSourceDelegate(self, forRow: (indexPath as NSIndexPath).section)
                showCollectionViewAddTableViewCell.collectionViewOffset = storedOffsets[(indexPath as NSIndexPath).section] ?? 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch (indexPath as NSIndexPath).section {
        case 0:
            break
        default:
            let isHorseDetail = sectionNamesArray[(indexPath as NSIndexPath).section-1]["type"] as! Int
            if isHorseDetail==1 {
                guard let horseCollectionViewAddTableViewCell = cell as? HorseCollectionViewAddTableViewCell else { return }
                storedOffsets[(indexPath as NSIndexPath).section] = horseCollectionViewAddTableViewCell.collectionViewOffset
            } else {
                guard let showCollectionViewAddTableViewCell = cell as? ShowCollectionViewAddTableViewCell else { return }
                storedOffsets[(indexPath as NSIndexPath).section] = showCollectionViewAddTableViewCell.collectionViewOffset
            }
        }
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueIdentifier:String = segue.identifier!
        switch segueIdentifier {
        case "showDetailSegue":
            break
        case "123":
            break
        default:
            break
        }
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sectionNamesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        var kWhateverHeightYouWant = 0
        let isHorseDetail = sectionNamesArray[collectionView.tag-1]["type"] as! Int
        if isHorseDetail==1 {
            kWhateverHeightYouWant = 240
        } else {
            kWhateverHeightYouWant = 190
        }
        return CGSize(width: collectionView.bounds.size.width-20, height: CGFloat(kWhateverHeightYouWant))
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let isHorseDetail = sectionNamesArray[collectionView.tag-1]["type"] as! Int
        if isHorseDetail==1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.CellIdentifiers.horseCollectionViewCell, for: indexPath) as! HorseCollectionViewCell
            cell.horseBackgroundImageView.image =  UIImage(named: "pexels-photo-103543")
            //model[collectionView.tag][indexPath.item]
            cell.horseNameLabel.text = "Test"
            cell.horseDescriptionLabel.text = "Testing App"
            cell.sellPriceLabel.text = "$ 1000 Sell"
            cell.leasePriceLabel.text = "$ 2000 Lease"
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "showCollectionViewCell", for: indexPath) as! ShowCollectionViewCell
            cell.showBackgroundImageView.image = UIImage(named: "pexels-photo-55707")
            //model[collectionView.tag][indexPath.item]
            cell.showNameLabel.text = sectionNamesArray[(indexPath as NSIndexPath).row]["name"] as? String
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
        performSegue(withIdentifier: "showDetailSegue", sender: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
}
