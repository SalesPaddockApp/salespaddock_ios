//
//  ShowCollectionViewAddTableViewCell.swift
//  Sales Paddock
//
//  Created by 3Embed on 14/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class ShowCollectionViewAddTableViewCell: UITableViewCell {

    @IBOutlet fileprivate weak  var showCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

extension ShowCollectionViewAddTableViewCell {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow section: Int) {
        showCollectionView.delegate = dataSourceDelegate
        showCollectionView.dataSource = dataSourceDelegate
        showCollectionView.tag = section
        showCollectionView.setContentOffset(showCollectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        showCollectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set {
            showCollectionView.contentOffset.x = newValue
        }
        get {
            return showCollectionView.contentOffset.x
        }
    }
}

