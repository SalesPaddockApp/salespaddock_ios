//
//  AppDelegate.swift
//  Sales Paddock
//
//  Created by 3Embed on 05/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import CoreData
import NotificationCenter
import UserNotifications
import SocketIO
import FBSDKShareKit
import GooglePlaces
import GoogleMaps
import Fabric
import Crashlytics
import AVFoundation
import AudioToolbox
import Flurry_iOS_SDK
import FirebaseCore
import FirebaseMessaging
import FirebaseInstanceID
import GoogleSignIn
import Google

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let locationManager  = LocationCoordinateManager.sharedInstance
    var notificationView : UIView?
    var player = AVAudioPlayer()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if #available(iOS 10, *) {
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
                guard error == nil else {
                    return
                }
                if granted {
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }
        }
        else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
        
        Flurry.startSession("CHBCCRJRT82VZ58998BJ", with: FlurrySessionBuilder
            .init()
            .withCrashReporting(true)
            .withLogLevel(FlurryLogLevelAll))
        
        FIRApp.configure()
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        GMSServices.provideAPIKey(Constant.APIKeys.googleAPIKey)
        GMSPlacesClient.provideAPIKey(Constant.APIKeys.googleAPIKey)
        Fabric.with([Crashlytics.self])
        self.locationManager.startUpdatingLocation() // used for updating location.
        self.locationManager.updateLocation() // updating location remotelly to server.
        
        application.applicationIconBadgeNumber = 0
        
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        //        ChatSocketIOClient.sharedInstance.sendHeartbeatForUser(status: "0")
//        SocketIOManager.default.removeSubscriber()
//        SocketIOManager.default.closeConnection()
        application.applicationIconBadgeNumber = 0
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        self.locationManager.startUpdatingLocation() // used for updating location.
        self.locationManager.updateLocation() // updating location remotelly to server.
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        self.locationManager.startUpdatingLocation() // used for updating location.
        self.locationManager.updateLocation() // updating location remotelly to server.
        
        SocketIOManager.default.establishConnection()
        
        SocketIOManager.default.subscribeToChannel(channel: "Heartbeat", handler: {
            message in
            if(message[0]["err"] as! Int == 3){
                UserDefaults.standard.set(true, forKey: "isNetworkReachable")
                ChatSocketIOClient.sharedInstance.sendHeartbeatForUser(status: "1")
            }
        })
        SocketIOManager.default.subscribeToChannel(channel: "Message", handler: {
            sMessage in
            let vc = CreateNewChat()
            vc.gettingMessagefromServer(sMessage: sMessage[0] )
            
        })
        if Utility.getUserID() != ""{
            //            DispatchQueue.global(qos: .background).async {
            //                print("This is run on the background queue")
            let vc = CreateNewChat()
            vc.getofflineMessageService()
        }
        
        //        }
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        UserDefaults.standard.set(false, forKey: "isOnChatScreen")
        self.saveContext()
        application.applicationIconBadgeNumber = 0
    }
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.3Embed.Sales_Paddock" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "Sales_Paddock", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
//                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    //MARK : - Config PushNotification
    func configPushNotification() {
        
    }
    
    // Handle refresh notification token
    func tokenRefreshNotification(notification: NSNotification) {
        guard let pushToken = FIRInstanceID.instanceID().token() else { return }
//        print("InstanceID token: \(pushToken)")
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        if (pushToken.count>0) {
            connectToFcm()
            FIRMessaging.messaging().subscribe(toTopic: "/topics/topic")
        }
    }
    
    // Connect to FCM
    func connectToFcm() {
        FIRMessaging.messaging().connect { (error) in
            if (error != nil) {
//                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
//                print("Connected to FCM.")
            }
        }
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
//        print(error)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        guard let pushToken = FIRInstanceID.instanceID().token() else { return }
        //        let deviceTokenString = pushToken.reduce("", {$0 + String(format: "%02X", $1 as! CVarArg)})
        //        UserDefaults.standard.setValue(deviceTokenString, forKey: Constant.UD_NAME.PUSH_TOKEN)
        //        UserDefaults.standard.synchronize()
    }
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != UIUserNotificationType() {
            application.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        guard let dict = userInfo["payload"] else{
            return
        }
        guard let UserInfoDecode = convertToDictionary(text: dict as! String) else{
            return
        }
        
        if application.applicationState == UIApplicationState.active{
            let appDelegateObj = UIApplication.shared.delegate! as! AppDelegate
            let navControllr = appDelegateObj.window!.rootViewController as! UINavigationController
            
            if let str = UserInfoDecode["type"] as? String{
                if str == "admin alerts" || str == "rideAlert"{
                    var count : Int = 1
                    if (UserDefaults.standard.value(forKey: "Unread Notification Count") != nil){
                        count = UserDefaults.standard.value(forKey: "Unread Notification Count") as! Int + 1
                    }
                    UserDefaults.standard.set(count, forKey: "Unread Notification Count")
                    UserDefaults.standard.synchronize()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "badgeCountUpdate"), object: nil)
                }
            }
            else if !(navControllr.visibleViewController is ActiveChatViewController) && ((UserInfoDecode["userId"] != nil) ||  (UserInfoDecode["sellerId"] != nil)){
                self.toShowNewMessageNotification(userInfo: UserInfoDecode )
            }
            
        }
        else{
            self.receiveNewPushNotification(userInfo: UserInfoDecode )
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
//                print(error.localizedDescription)
            }
        }
        return nil
    }
    
}

extension AppDelegate : UNUserNotificationCenterDelegate{
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        self.receiveNewPushNotification(userInfo: response.notification.request.content.userInfo as! [String : Any])
    }
    
    
    
    func receiveNewPushNotification(userInfo:[String : Any]){
        
        //        NSLog("user info:%@", userInfo);
        if let str = userInfo["type"] as? String{
            
            if str == "admin alerts" || str == "rideAlert"{
                
                let appDelegateObj = UIApplication.shared.delegate! as! AppDelegate
                let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "NavViewController") as!  NavViewController
                vc.isComingFromPush = true
                // show the alert
                var controller = appDelegateObj.window!.rootViewController
                if (appDelegateObj.window!.rootViewController as? UINavigationController) != nil
                {
                    let navControllr = appDelegateObj.window!.rootViewController as! UINavigationController
                    controller = navControllr.visibleViewController
                }
                else
                {
                    controller = appDelegateObj.window!.rootViewController
                }
                controller?.present(vc, animated: true, completion: nil)
            }
        }
        if let userId = userInfo["userId"] as? String , let sellerId = userInfo["sellerId"] as? String{
            
            UserDefaults.standard.set(userId, forKey: "userIdFromPush");
            UserDefaults.standard.set(sellerId, forKey: "sellerIdFromPush")
            //            var isCommingFromPush = [String : Bool]()
            //            isCommingFromPush["push"] = true
            UserDefaults.standard.set(true, forKey: "fromPush")
        }
    }
    
    
    func toShowNewMessageNotification(userInfo: [String : Any]){
        
        self.notificationView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(-64), width: CGFloat(UIScreen.main.bounds.width), height: CGFloat(64)))
        UIView.animate(withDuration: 0.4, animations: {
            self.notificationView?.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(UIScreen.main.bounds.width), height: CGFloat(64))
            
        }, completion: {(finished) -> Void in
            _ = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.removeNotificationView), userInfo: nil, repeats: false)
        })
        self.notificationView?.backgroundColor = UIColor(red: 10/255, green: 180/255, blue: 157/255, alpha: 1.0)
        window?.addSubview(self.notificationView!)
        window?.bringSubview(toFront: self.notificationView!)
        let msgLabel = UILabel(frame: CGRect(x: CGFloat(71), y: CGFloat(18), width: CGFloat((self.notificationView?.frame.size.width)!), height: CGFloat(25)))
        msgLabel.textColor = UIColor.white
        msgLabel.text = "You have a new message"
        self.playSound()
        self.notificationView?.addSubview(msgLabel);
    }
    
    func playSound() {
        let url = Bundle.main.url(forResource: "notification", withExtension: "mp3")!
        do {
            player = try AVAudioPlayer(contentsOf: url)
            player.prepareToPlay()
            player.play()
            
        } catch let error {
//            print(error.localizedDescription)
        }
    }
    
    @objc func removeNotificationView(){
        
        UIView.animate(withDuration: 0.4, animations: {
            self.notificationView?.frame = CGRect(x: CGFloat(0), y: CGFloat(-64), width: CGFloat(UIScreen.main.bounds.width), height: CGFloat(64))
            
        }, completion: nil)
        //        self.notificationView?.removeFromSuperview()
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if url.scheme == Constant.Schemes.fbScheme {
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        }
        else if url.scheme == Constant.Schemes.googleScheme {
            return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        }
        return false
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
    }
}


