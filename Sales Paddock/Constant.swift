//
//  Constant.swift
//  Sales Paddock
//
//  Created by 3Embed on 12/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class Constant: NSObject {
    
    struct URLs {
        //        static let baseURL = "http://188.166.95.208:5006/"
        static let baseURL = "http://salespaddock-2060556237.us-west-2.elb.amazonaws.com:5005/"
//        static let baseURL = "http://188.166.95.208:5006/"//Dev URL
        
        static let socketBaseURL =  "http://www.salespaddockapp.com:9001/"
//                       live url = "http://www.salespaddockapp.com:9001/"
//        static let socketBaseURL = "http://188.166.95.208:9001/"//Dev URL
    }
    
    struct APIKeys {
        static let googleAPIKey = "AIzaSyA508QgVRgw0-WB8Qyw5VHzDfms4sZgTNs" // API key
        static let googleSKey = "AIzaSyAbqK5Myn70NgiSZiKtAFtmutMrBXwWMRc" // Server Key
    }
    
    struct Schemes {
        static let googleScheme = "com.googleusercontent.apps.213391538612-qh443hjt40o46ga76472uvss1p9d8092"
        static let fbScheme = "fb1901619233391493"
    }
    
    struct ServiceMethodNames {
        
        static let login = "api/login"
        static let getProductTypes = "prodType"
        static let getPreferences = "api/getPreference"
        static let register = "api/register"
        static let validateEmail = "validateEmail"
        static let viewProfile = "viewProf"
        static let viewProfileForChat = "viewProfileForChat"
        static let editProfile = "api/editProfile"
        static let getShowForUsers = "getShowForUsers"
        static let getPostDetails = "getPostDetails"
        static let addToRecentList = "addToRecentList"
        static let getShowDetails = "getShowDetails"
        static let createPost = "createPost"
        static let getAllPostsHost = "getAllPostsHost"
        static let getAllHostShow = "getShows"
        static let getSummaries = "getSummary"
        static let addToShow = "addToShow"
        static let getWishList = "getWishList"
        static let getCollections = "getCollections"
        static let createCollection = "createCollection"
        static let addPostsToCollection = "addPostsToCollection"
        static let getCollectionDetails = "getCollectionDetails"
        static let deleteCollection = "deleteCollection"
        static let editCollection = "editCollection"
        static let getPostwithOutcollecction = "getPostwithOutcollecction"
        static let addWishList = "addWish"
        static let removeWish = "removeWish"
        static let sendAlerts = "sendAlerts"
        static let updatePost = "updatePost"
        static let logout = "logout"
        static let forgetPassword = "forgetPassword"
        static let getNotification = "getMyNotifications"
        static let getAllMyListedPosts = "getAllMyListedPost"
        static let deletePostFromShow = "deletePostFromShow"
        static let getFAQ = "getFaq"
        static let getFilters = "getFilters"
        static let sendOTP = "sendOTP"
        static let validateNumber = "validateNumber"
        static let deletePost = "deletePost"
        static let getAppliedDescipline = "getAppliedDescipline"
        static let applyForDescipline = "applyDescipline"
        static let addOrUpdateFarm = "addOrUpdateFarm"
        static let getAllFarms = "getAllFarms"
        static let updateLocation = "updateLocation"
        static let getAroundYou = "getAroundYou"
        static let upcomingShows = "upcomingShows"
        static let activeShows = "activeShows"
        static let deleteFarm = "deleteFarm"
        static let getMessages = "getMessages"
        static let ackMessage = "ackMessage"
        static let deleteMessage = "deleteMessage"
        static let getSellers = "getSellers"
        static let searchPosts = "searchPosts"
    }
    
    struct UD_NAME {
        
        static let SESSION_TOKEN = "session_token"
        static let USER_ID = "user_id"
        static let USER_FNAME = "user_first_name"
        static let USER_LNAME = "user_last_name"
        static let PUSH_TOKEN = "push_token"
    }
    
    
    struct CellIdentifiers {
        
        static let userNameTabelViewCell = "userNameTableViewCell"
        static let showCollectionViewAddTableViewCell = "showCollectionViewAddTableViewCell"
        static let horseCollectionViewAddTableViewCell = "horseCollectionViewAddTableViewCell"
        
        static let showCoverImageTableViewCell = "showCoverImageTableViewCell"
        static let showDescriptionTableViewCell = "showDescriptionTableViewCell"
        
        static let horseCoverImageTableViewCell = "horseCoverImageTableViewCell"
        static let horseDescriptionTableViewCell = "horseDescriptionTableViewCell"
        static let horseOwnerNameTableViewCell = "horseOwnerNameTableViewCell"
        //static let horseOwnerDescriptionTableViewCell = "horseOwnerDescriptionTableViewCell"
        static let similarListingsTableViewCell = "similarListingsTableViewCell"
        
        
        static let horseOwnerAboutTableViewCell = "horseOwnerAboutTableViewCell"
        static let horseOwnerItemDescriptionTableViewCell = "horseOwnerItemDescriptionTableViewCell"
        static let verifyPhoneTableViewCell = "verifyPhoneTableViewCell"
        
        static let horseCollectionViewCell = "horseCollectionViewCell"
        static let showCollectionViewCell = "showCollectionViewCell"
        static let similarHorseCollectionViewCell = "similarHorseCollectionViewCell"
        
        //teja classes identifdiers
        
        static let breedTableViewCell = "breedTableViewCell"
        static let disciplineTableViewCell = "disciplineTableViewCell"
        static let listSelectBreedTableViewCell = "listSelectBreedTableViewCell"
        static let listYourAssetTableViewCell = "listYourAssetTableViewCell"
        static let yourListingViewControllerCell = "yourListingViewControllerCell"
        static let breedCategoryTableViewCell = "breedCategoryTableViewCell"
        static let uploadPhotoTableViewCell = "uploadPhotoTableViewCell"
        static let descriptionCollectionViewCell = "descriptionCollectionViewCell"
    }
    
    struct FontSizes {
        static let Large: CGFloat = 14.0
        static let Small: CGFloat = 10.0
    }
    
    struct AnimationDurations {
        static let Fast: TimeInterval = 1.0
        static let Medium: TimeInterval = 3.0
        static let Slow: TimeInterval = 5.0
    }
    
    struct Emojis {
        static let Happy = "😄"
        static let Sad = "😢"
    }
    
    struct SegueIdentifiers {
        static let Master = "MasterViewController"
        static let Detail = "DetailViewController"
    }
    
}
