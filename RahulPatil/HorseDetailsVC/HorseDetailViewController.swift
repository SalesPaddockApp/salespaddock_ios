//
//  HorseDetailViewController.swift
//  Sales Paddock
//
//  Created by 3Embed on 15/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Kingfisher
import AVKit
import AVFoundation

protocol HorseDetailViewControllerDelegate: class {
    func removePostFormCollection(postId : String)
}


class HorseDetailViewController: UIViewController, UITableViewDataSource,  UITableViewDelegate, UIGestureRecognizerDelegate  {
    
    @IBOutlet weak var favBtnOutlet: UIButton!
    @IBOutlet weak var horseTableView: UITableView!
    @IBOutlet weak var backButtonOutlet: UIButton!
    @IBOutlet weak var shareButtonOutlet: UIButton!
    @IBOutlet weak var makeFavOutlet: UIButton!
    @IBOutlet weak var heightConstraintOutlet: NSLayoutConstraint!
    
    @IBOutlet weak var navigationViewOutlet: UIView!
    @IBOutlet weak var startChattingViewConstraint: NSLayoutConstraint!
    
    //Collection added views
    @IBOutlet weak var viewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionImage: UIImageView!
    @IBOutlet weak var collectionName: UILabel!
    
    let model: [[UIColor]] = generateRandomData()
    var storedOffsets = [Int: CGFloat]()
    //    var postDetailsDictionary:[String: AnyObject] = [:]
    var arrayOfListDetails:[AnyObject] = []
    var dictOfResponse:[String: AnyObject] = [:]
    var comingFromProfile = false
    var imageData = [String:AnyObject]()
    var postId = String()
    var showId = String()
    var timer : Timer!
    var collectionData = CollectionsData(collectionsPrice: "", collectionsID: "", collectionsName: "", collectionsImageUrl: "", userImage: "", userId: "", ownerName: "", postId: "", coverPost: "")
    
     weak var delegate: HorseDetailViewControllerDelegate?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.hideNavigation()
        self.makeFavOutlet.isHidden = false
        horseTableView.estimatedRowHeight = 270
        horseTableView.rowHeight = UITableViewAutomaticDimension
       // horseTableView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0)
        self.navigationController!.interactivePopGestureRecognizer?.delegate = self
        self.horseTableView.autoresizingMask = .flexibleWidth
        getPostDetails()
        self.horseTableView.layoutIfNeeded()
        //        self.startChattingView.isHidden = true
        self.startChattingViewConstraint.constant = 0.0;
    }
  override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    //MARK:- Gesture Recognizer Delegate
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return (gestureRecognizer is UIScreenEdgePanGestureRecognizer)
    }
    
    //MARK:- Buttons Acion
    @IBAction func startChattingAction(_ sender: Any) {
        
        if (dictOfResponse["ownerDetails"] as! [String : String])["userId"] == Utility.getUserID(){
            let alert = UIAlertController.init(title: "Message", message: "You can't initiate chat with yourself", preferredStyle: .alert)
            let action = UIAlertAction.init(title: "OK", style: .default, handler: { (action: UIAlertAction) in
                return
            })
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            
        }else{
            
            let vc = CreateNewChat()
            vc.thisPostId = self.postId
            if let name = self.dictOfResponse["name"] as? String
            {
                vc.postName = name
            }
            else{
                vc.postName = (self.dictOfResponse["name"] as! [String])[0]
            }
            let firstImageDict = ((dictOfResponse["image"] as! [String:Any])["options"] as! [Any])[0] as! [String : Any]
            if(firstImageDict["type"] as! String == "2"){
                vc.postImageString = firstImageDict["thumbnailUrl"] as! String
            }else{
                vc.postImageString = firstImageDict["imageUrl"] as! String
            }
//            vc.postImageString = ((((dictOfResponse["image"] as! [String:Any])["options"] as! [Any])[0] as! [String:Any])["imageUrl"] as! String)
            let document = vc.cerateNewChatAndEntryInDatabase(chatDetails: dictOfResponse["ownerDetails"] as! [String : AnyObject])
            
            let chatStoryboard = UIStoryboard.init(name: "ChatStoryboard", bundle: Bundle.main)
            let activeVC = chatStoryboard.instantiateViewController(withIdentifier: "activeChatController") as! ActiveChatViewController
            
            activeVC.document = document
            activeVC.groupOrUserNameLabel.setTitle((dictOfResponse["ownerDetails"] as! [String : String])["name"], for: .normal)
            activeVC.receiverNameString = (dictOfResponse["ownerDetails"] as! [String : String])["name"]
            activeVC.groupOrUserImageURL = (dictOfResponse["ownerDetails"] as! [String : String])["profilePic"]
            activeVC.fromHome = true
            activeVC.postId = self.postId
            
            if let name = self.dictOfResponse["name"] as? String
            {
                activeVC.postName = name
            }
            else{
                activeVC.postName = (self.dictOfResponse["name"] as! [String])[0]
            }
            
            //        self.present(activeVC, animated: true, completion: nil)
            self.navigationController?.pushViewController(activeVC, animated: true)
        }
    }
    
    @IBAction func navigationLeftBtnAction(_ sender: AnyObject) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addToWishListButtonAction(_ sender: AnyObject) {
        self.addWishlist(sender: sender as Any)
//        print("old one")
    }
    
    @IBAction func wishlistAction(_ sender: Any) {
        self.addWishlist(sender: sender)
//        print("new one")
    }
    
    func addWishlist(sender: Any){
        let btn = sender as! UIButton
        if makeFavOutlet.isSelected {
            btn.isSelected = false
            makeFavOutlet.isSelected = false
            favBtnOutlet.isSelected = false
            removeWish()
            updateTimer()
        }
        else {
            btn.isSelected = true
            makeFavOutlet.isSelected = true
            favBtnOutlet.isSelected = true
            addToWistList()
            updateUIAndWebForAddToWishlist()
        }
    }
    
    
    @objc func viewProfileButtonAction(sender: UIButton) {
        
        if let dictOfOwnerDetails = dictOfResponse["ownerDetails"] as? [String: AnyObject] {
            if !dictOfOwnerDetails.isEmpty {
                
                performSegue(withIdentifier: "sellersProfilePageOutlet", sender: dictOfOwnerDetails)
            }
        }
    }
    
    @IBAction func shareButtonAction(_ sender: Any)
    {
        performSegue(withIdentifier: "Share Seague", sender: nil)
    }
    
    func updateUIAndWebForAddToWishlist(){
        timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: false)
        let index = IndexPath.init(row: 0, section: 0)
         let cell = self.horseTableView.cellForRow(at: index) as! HorseCoverImageTableViewCell
        cell.viewBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            cell.layoutIfNeeded()
        }) { (complimeted) in
//            print("done")
        }
    }
    
    @IBAction func collectionDetailsAction(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "Collections", bundle: nil)
        let collectionDetailsVC = storyboard.instantiateViewController(withIdentifier: "collectionDetailsController") as! CollectionDetailsViewController
        collectionDetailsVC.collectionName = self.collectionData.collectionsName
        collectionDetailsVC.collectionId = self.collectionData.collectionsID
        self.navigationController?.pushViewController(collectionDetailsVC, animated: true)
    }
    
    
    
    @objc func updateTimer(){
        if (timer == nil){
            return
        }
        timer.invalidate()
        let index = IndexPath.init(row: 0, section: 0)
        let cell = self.horseTableView.cellForRow(at: index) as! HorseCoverImageTableViewCell
        cell.viewBottomConstraint.constant = 50
        UIView.animate(withDuration: 0.2, animations: {
            cell.layoutIfNeeded()
        }) { (complimeted) in
//            print("done")
        }
    }
    
    // MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfListDetails.count + 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = indexPath.row
        let descNumber:Int = 1 + arrayOfListDetails.count
        switch index {
            
        case 0: // HORSE  Banner Image and Name
            
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.horseCoverImageTableViewCell, for: indexPath) as? HorseCoverImageTableViewCell
            
            let isWished = dictOfResponse["isWish"] as? String
            
            if isWished == "1" {
                // Already wished
                cell?.addToWishListButton.isSelected = true
            }
            else {
                // Yet to wish
                cell?.addToWishListButton.isSelected = false
            }
            // Add Target to Buttton
            cell?.addToWishListButton.addTarget(self,
                                                action:#selector(addToWishListButtonAction(_:)),
                                                for: .touchUpInside)
            
            // Name Of Horse
            
            if let nameStr = self.dictOfResponse["name"] as? String
            {
                cell?.horseNameLabel.text = nameStr
            }
            
            // Description of Horse
            if let descOfHorse = self.dictOfResponse["breed"] as? String{
                cell?.horseDescriptionOutlet.text = descOfHorse
            }
            
            //lease price
            if let pricingDetails = self.dictOfResponse["LeaseDetails"] as? [String : AnyObject]{
                if let price = pricingDetails["options"] as? String
                {
                    cell?.leasePriceLabelOutlet.text = "\(price)"
                }
            }
            
            // sell price
            if let pricingDetails = self.dictOfResponse["priceDetails"] as? [String : AnyObject]{
                if let price = pricingDetails["options"] as? String
                {
                    cell?.priceOutlet.text = "\(price)"
                }
            }
            
            // Array Of Images
            
            if let imageArray = self.imageData["options"] as? [AnyObject]
            {
                cell?.imageArray =  imageArray
                var xPosition:CGFloat
                if imageArray.count>=2{
                    xPosition = UIScreen.main.bounds.width * CGFloat(imageArray.count-1)
                    cell?.maximumOffSet = xPosition
                }
                cell?.addPhotosToScrollView(imageArray: imageArray)
                cell?.videoPlayDelegate = self
            }
            return cell!
            
        case descNumber: // OWNEWR DETAILS
            
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.horseOwnerNameTableViewCell, for: indexPath) as? HorseOwnerNameTableViewCell
            if (dictOfResponse["ownerDetails"] != nil) {
                cell?.loadCell(dictionary: dictOfResponse["ownerDetails"] as! [String: AnyObject])
            }
            
            cell?.viewProfileButton.addTarget(self,
                                              action: #selector(viewProfileButtonAction(sender:)),
                                              for: .touchUpInside)
            if let descOfHorse = self.dictOfResponse["description"] as? String
            {
                cell?.horseDEscriptionOutlet.text = descOfHorse
            }
            return cell!
            
        default:
            // Horse Details Dcit
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.horseDescriptionTableViewCell, for: indexPath) as? HorseDescriptionTableViewCell
            
            if arrayOfListDetails.count != 0 {
                cell?.loadCell(dictionary: arrayOfListDetails[indexPath.row - 1] as! [String : AnyObject])
            }
            
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK:- Scroll View Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView  == horseTableView
        {
            let offsetY: CGFloat = scrollView.contentOffset.y
            
            if(offsetY > 100)
            {
                backButtonOutlet.setImage(Image.init(named: "back_btn_off"), for: .normal)
                let alpha: CGFloat = min(1, 1 - ((64 + 100 - offsetY) / 100))
                navigationViewOutlet.backgroundColor = Color.white.withAlphaComponent(alpha)
                if(alpha == 1)
                {
                    backButtonOutlet.setImage(Image.init(named: "back_btn_off"), for: .normal)
                    shareButtonOutlet.setImage(Image.init(named: "share_btn_black_off"), for: .normal)
                    if (makeFavOutlet.isSelected == false){
                        makeFavOutlet.setImage(Image.init(named: "favorite_btn_black_off"), for: .normal)
                    }
                    
                }
            }
            else if (offsetY < 100)
            {
                backButtonOutlet.setImage(Image.init(named: "back_btn_wight_off"), for: .normal)
                shareButtonOutlet.setImage(#imageLiteral(resourceName: "share_btn_wight_off"), for: .normal)
                if (makeFavOutlet.isSelected == false){
                    makeFavOutlet.setImage(Image.init(named: "favorite_btn_wight_off"), for: .normal)
                }
                navigationViewOutlet.backgroundColor = Color.clear
            }
            else
            {
                backButtonOutlet.setImage(Image.init(named: "back_btn_wight_off"), for: .normal)
                shareButtonOutlet.setImage(#imageLiteral(resourceName: "share_btn_wight_off"), for: .normal)
                if (makeFavOutlet.isSelected == false){
                    makeFavOutlet.setImage(Image.init(named: "favorite_btn_wight_off"), for: .normal)
                }
            }
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let segueIdentifier:String = segue.identifier!
        
        switch segueIdentifier {
            
        case "sellersProfilePageOutlet":
            
            let controller = segue.destination as! ProfilePageVC
            controller.isMyProfile = "0"
            controller.profileData =  sender as! [String: AnyObject]
            
            break
            
        case "Share Seague":
            
            let controller = segue.destination as! ShareScreenViewController
            controller.horseData = self.dictOfResponse
            
            break
            
        default:
            break
        }
    }
    
    /*******************************************/
    // MARK: - WEB SERVICES
    
    func getPostDetails() {
        
        Helper.showPI()
        
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject,
                      "postId":postId  as AnyObject]
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getPostDetails,
                                 params: params,
                                 success: { (response) in
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    switch errFlag {
                                    case 0:
                                        let responseDict = (response["response"]).dictionaryObject
                                        if (responseDict != nil) {
                                            self.dictOfResponse = responseDict! as [String: AnyObject]
                                            self.arrayOfListDetails = responseDict?["postDetails"] as! [AnyObject]
                                            self.imageData = responseDict?["image"] as! [String : AnyObject]
                                            if(self.dictOfResponse["ownerDetails"] as! [String : String])["userId"] == Utility.getUserID(){
                                                self.startChattingViewConstraint.constant = 0.0
                                            } else {
                                                self.startChattingViewConstraint.constant = 60.0
                                            }
                                        }
                                        self.horseTableView.reloadData()
                                        if (!self.comingFromProfile){
                                            self.addToRecentList(postID: self.postId)
                                        }
                                        break
                                        
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Error", message: errMsg!)
                                        
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
    
    // Add To Recent List
    func addToRecentList(postID: String!) {
        
        let userDetail = dictOfResponse["ownerDetails"] as! [String: AnyObject]
        
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject,
                      "postId":postID as AnyObject,
                      "hostUserId":userDetail["userId"]! as AnyObject,
                      "showId":showId as AnyObject]
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.addToRecentList,
                                 params: params,
                                 success: { (response) in
                                    
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                        
                                    case 0:
                                        
                                        break
                                        
                                    default:
                                        
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
    
    func addToWistList() {
        
        let userDetail = dictOfResponse["ownerDetails"] as! [String: AnyObject]
        
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject,
                      "showId":showId as AnyObject,
                      "postId":postId as AnyObject,
                      "hostUserId":userDetail["userId"]! as AnyObject]
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.addWishList,
                                 params: params,
                                 success: { (response) in
                                    
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                    case 0:
                                        
                                        break
                                        
                                    default:
                                        
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    Helper.hidePI()
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
        
    }
    func removeWish() {
        
        let userDetail = dictOfResponse["ownerDetails"] as! [String: AnyObject]
        
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject,
                      "showId":self.showId as AnyObject,
                      "postId":self.postId as AnyObject,
                      "hostUserId":userDetail["userId"]! as AnyObject]
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.removeWish,
                                 params: params,
                                 success: { (response) in
                                    
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                    case 0:
                                        if self.delegate != nil{
                                            self.delegate?.removePostFormCollection(postId: self.postId)
                                        }
                                        break
                                        
                                    default:
                                        
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    Helper.hidePI()
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
}

extension HorseDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayOfListDetails.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.size.width)/2, height: (collectionView.bounds.size.width)/3)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.CellIdentifiers.similarHorseCollectionViewCell, for: indexPath) as! SimilarHorseCollectionViewCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}

extension HorseDetailViewController:videoPlayDelegate
{
    func playVideoWithURL(videoUrl: URL) {
        let player = AVPlayer(url: videoUrl as URL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func saveToCollections() {
        let storyboard = UIStoryboard.init(name: "Collections", bundle: nil)
        let createCollectionVC = storyboard.instantiateViewController(withIdentifier: "createCollectionController") as! CreateCollectionViewController
        if let imageArray = self.imageData["options"] as? [AnyObject]
        {
            if let data = imageArray[0] as? [String : Any]{
                if Int(data["type"] as! String) == 0{
                    createCollectionVC.collectionCoverImage = data["imageUrl"] as! String
                }else{
                    createCollectionVC.collectionCoverImage = data["thumbnailUrl"] as! String
                }
            }
        }
        createCollectionVC.postId = self.postId
        createCollectionVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        createCollectionVC.delegate = self
        self.updateTimer()
        self.present(createCollectionVC, animated: true, completion: nil)
    }
}

extension HorseDetailViewController: CreateCollectionViewControllerDelegate{
    
    func addedPost(collectionData: CollectionsData) {
        self.collectionImage.kf.setImage(with: URL(string:collectionData.collectionsImageUrl!),
                                              placeholder: #imageLiteral(resourceName: "horse_default_image"),
                                              options: [.transition(ImageTransition.fade(1))],
                                              progressBlock: { receivedSize, totalSize in
        },
                                              completionHandler: { image, error, cacheType, imageURL in
                                                self.collectionImage.kf.indicator?.stopAnimatingView()
        })
        self.collectionName.text = "Saved to \(collectionData.collectionsName!)"
        self.updateCollectionAdded()
        self.collectionData = collectionData
    }
    
    func updateCollectionAdded(){
        timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.updateTimerOfCollectionAdded), userInfo: nil, repeats: false)
        self.viewTopConstraint.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        }) { (complimeted) in
//            print("done")
        }
    }
    
    @objc func updateTimerOfCollectionAdded(){
        timer.invalidate()
        self.viewTopConstraint.constant = -84
        UIView.animate(withDuration: 0.2, animations: {
           self.view.layoutIfNeeded()
        }) { (complimeted) in
//            print("done")
        }
    }
}
