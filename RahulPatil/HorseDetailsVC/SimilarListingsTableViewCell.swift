//
//  SimilarListingsTableViewCell.swift
//  Sales Paddock
//
//  Created by 3Embed on 15/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class SimilarListingsTableViewCell: UITableViewCell {

    @IBOutlet weak var similarHorseCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension SimilarListingsTableViewCell {
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow section: Int) {
        similarHorseCollectionView.delegate = dataSourceDelegate
        similarHorseCollectionView.dataSource = dataSourceDelegate
        similarHorseCollectionView.tag = section
        similarHorseCollectionView.setContentOffset(similarHorseCollectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        similarHorseCollectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set {
            similarHorseCollectionView.contentOffset.x = newValue
        }
        get {
            return similarHorseCollectionView.contentOffset.x
        }
    }
}
