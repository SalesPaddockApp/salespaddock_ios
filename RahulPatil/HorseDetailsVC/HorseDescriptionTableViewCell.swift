//
//  HorseDescriptionTableViewCell.swift
//  Sales Paddock
//
//  Created by 3Embed on 15/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class HorseDescriptionTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func loadCell(dictionary: [String: AnyObject]) {
        
        itemNameLabel.text = dictionary["title"] as? String
        if let arrayOfSelected = dictionary["options"] as? [String]{
            if arrayOfSelected.count != 0 {
                if(arrayOfSelected.count >= 2){
                    let str = arrayOfSelected.joined(separator: ", ")
                    itemDescriptionLabel.text = str
                }
                else{
                    itemDescriptionLabel.text = arrayOfSelected[0]
                }
            }
            else {
                itemDescriptionLabel.text = "....."
            }
        }
        else if let arrayOfSelected = dictionary["options"] as? [[String:Any]]{
            var addressData = ""
            if !arrayOfSelected.isEmpty {
                
                let locationData = arrayOfSelected[0]
                if let farmName = locationData["farmName"] as? String{
                    addressData = "\(farmName) "

                }
                if let city = locationData["city"] as? String{
                    addressData = addressData+"\(city) "

                }
                if let state = locationData["state"] as? String {
                    addressData = addressData+"\(state)"
                }
                itemDescriptionLabel.text = addressData
            }
            else {
                itemDescriptionLabel.text = "....."
            }
        }
        itemNameLabel.sizeToFit()
        itemDescriptionLabel.sizeToFit()
    }
}
