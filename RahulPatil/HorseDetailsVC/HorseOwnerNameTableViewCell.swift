//
//  HorseOwnerNameTableViewCell.swift
//  Sales Paddock
//
//  Created by 3Embed on 17/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class HorseOwnerNameTableViewCell: UITableViewCell {
    
    @IBOutlet weak var horseOwnerProfileImageView: UIImageView!
    @IBOutlet weak var horseDEscriptionOutlet: UITextView!
    @IBOutlet weak var hostedBy: UILabel!
    @IBOutlet weak var viewProfileButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func loadCell(dictionary: [String: AnyObject]) {
        
        hostedBy.text = "\(dictionary["name"]!)"
        horseDEscriptionOutlet.text = dictionary["about"] as! String?
        
        let urlString = dictionary["profilePic"] as? String
        if urlString == nil {
        }
        else {
            let url = URL(string: urlString!)
            self.horseOwnerProfileImageView.kf.indicatorType = .activity
            self.horseOwnerProfileImageView.kf.indicator?.startAnimatingView()
            horseOwnerProfileImageView.kf.setImage(with: url,
                                                   placeholder: UIImage.init(named: "horse_photo_defalt_image"),
                                                   options: [.transition(ImageTransition.fade(1))],
                                                   progressBlock: { receivedSize, totalSize in
                },
                                                   completionHandler: { image, error, cacheType, imageURL in
                                                    //print(" Finished")
                                                    self.horseOwnerProfileImageView.kf.indicator?.stopAnimatingView()
            })
        }
    }
    
}
