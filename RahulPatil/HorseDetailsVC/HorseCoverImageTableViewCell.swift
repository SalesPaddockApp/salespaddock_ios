//
//  HorseCoverImageTableViewCell.swift
//  Sales Paddock
//
//  Created by 3Embed on 15/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Kingfisher
import AVFoundation
import AVKit
import youtube_ios_player_helper

protocol videoPlayDelegate {
    func playVideoWithURL(videoUrl : URL)
    func saveToCollections()
}

class HorseCoverImageTableViewCell: UITableViewCell {
    
    var maximumOffSet: CGFloat?
    @IBOutlet weak var leasePriceLabelOutlet: UILabel!
    
    @IBOutlet weak var horseDescriptionOutlet: UITextView!
    @IBOutlet weak var priceOutlet: UILabel!
    @IBOutlet weak var scrollViewOutlet: UIScrollView!
    var videoPlayDelegate:videoPlayDelegate! = nil
    var imageArray = [AnyObject]()
    @IBOutlet weak var horseNameLabel: UILabel!
    @IBOutlet weak var addToWishListButton: UIButton!
    @IBOutlet weak var viewBottomConstraint: NSLayoutConstraint!
    
    var timer: Timer!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.addTimerForAutoScroll()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @objc func onTimer(){
        if (self.maximumOffSet == nil){
            self.timer.invalidate()
            return
        }
        else if self.scrollViewOutlet.contentOffset.x<self.maximumOffSet!{
            scrollViewOutlet.setContentOffset(CGPoint(x: scrollViewOutlet.contentOffset.x+scrollViewOutlet.frame.size.width, y: 0), animated: true)
        } else {
            self.timer.invalidate()
        }
    }
    
    func addPhotosToScrollView(imageArray : [AnyObject]){
        
        for i in 0..<imageArray.count {
            let imageview = UIImageView()
            imageview.backgroundColor = Color.white
            let xPosition = self.scrollViewOutlet.frame.width * CGFloat(i)
            imageview.frame = CGRect(x: xPosition, y: 0, width: self.scrollViewOutlet.frame.width, height: self.scrollViewOutlet.frame.height)
            imageview.contentMode = .scaleToFill
            var imageUrl = ""
            guard let dataArray = imageArray[i] as? [String:AnyObject] else { return }
            if let type = dataArray["type"] as? String {
                if type == "0" {
                    imageUrl = dataArray["imageUrl"] as! String
                }
                else {
                    if let imageurl = dataArray["thumbnailUrl"] as? String, let videoURL = dataArray["imageUrl"] as? String {
                        imageUrl = imageurl
                        if (dataArray["type"] as! String) == "2" {
                            let ytplayerView = YTPlayerView(frame:  CGRect(x: 0, y: 0, width: self.scrollViewOutlet.frame.width, height: self.scrollViewOutlet.frame.height))
                            imageview.addSubview(ytplayerView)
                            imageview.tag = i
                            imageview.isUserInteractionEnabled = true
                            self.playVideo(withID: videoURL, withView : ytplayerView)
                        } else {
                            if let youtubeID = dataArray["youTubeId"] as? String {
                                let ytplayerView = YTPlayerView(frame:  CGRect(x: 0, y: 0, width: self.scrollViewOutlet.frame.width, height: self.scrollViewOutlet.frame.height))
                                imageview.addSubview(ytplayerView)
                                imageview.tag = i
                                imageview.isUserInteractionEnabled = true
                                self.playVideo(withID: youtubeID, withView : ytplayerView)
                            } else {
                                let button = UIButton(frame : CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: self.scrollViewOutlet.frame.height))
                                button.setImage(#imageLiteral(resourceName: "play_icon"), for: .normal)
                                button.addTarget(self, action: #selector(playButtonAction(_:)),for: .touchUpInside)
                                button.tag = i
                                imageview.addSubview(button)
                                imageview.isUserInteractionEnabled = true
                            }
                        }
                    }
                }
            }
            let url = URL(string: imageUrl)
            imageview.kf.indicatorType = .activity
            imageview.kf.indicator?.startAnimatingView()
            imageview.kf.setImage(with:url,
                                  placeholder: #imageLiteral(resourceName: "horse_photo_defalt_image"),options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
            },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    imageview.kf.indicator?.stopAnimatingView()
            })
            self.scrollViewOutlet.contentSize.width = scrollViewOutlet.frame.width * CGFloat(i + 1)
            self.scrollViewOutlet.addSubview(imageview)
        }
    }
    
    func addTimerForAutoScroll()
    {
        self.timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector:#selector(onTimer), userInfo: nil, repeats: true)
    }
    
    //MARK:- Buttons Action
    @objc func playButtonAction(_ button: UIButton)
    {
        let tag = Int(button.tag)
        if(self.videoPlayDelegate != nil)
        {
            guard let dataDict = imageArray[tag] as? [String: AnyObject] else { return }
            guard let urlStr = dataDict["imageUrl"] as? String else  { return }
            if let videoUrl = URL(string: urlStr) {
                self.videoPlayDelegate.playVideoWithURL(videoUrl: videoUrl)
            }
        }
    }
    
    func playVideo(withID videoID : String, withView ytView : YTPlayerView) {
        let dict = [
            "playsinline" : 1,
            "showinfo" : 0,
            "controls" : 1,
            "modestbranding": 0,
            "enablejsapi":1,
            "autohide":2,
            "rel":0,
            "origin" : "http://www.youtube.com",
            ] as [String : Any]
        let result = ytView.load(withVideoId: videoID, playerVars: dict)
        ytView.webView?.allowsInlineMediaPlayback = false
//        print("Result = \(result)")
        ytView.delegate = self
    }
    
    @IBAction func saveToCollectionAction(_ sender: Any) {
        if((self.videoPlayDelegate) != nil){
            self.videoPlayDelegate.saveToCollections()
        }
    }
    
}

extension HorseCoverImageTableViewCell : UIScrollViewDelegate{
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.timer.invalidate()
    }
}

extension HorseCoverImageTableViewCell : YTPlayerViewDelegate {
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        
    }
    
    func playerView(_ playerView: YTPlayerView, didPlayTime playTime: Float) {
        
    }
    
    func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
        
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo quality: YTPlaybackQuality) {
        
    }
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        playerView.webView?.allowsInlineMediaPlayback = false
    }
}

