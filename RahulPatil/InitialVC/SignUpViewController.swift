//
//  SignUpViewController.swift
//  Sales Paddock
//
//  Created by 3Embed on 06/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController,UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var nextBtnBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!

    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var firstNameCheckMarkImageView: UIImageView!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var lastNameCheckMarkImageView: UIImageView!
   
    @IBOutlet weak var nextBtn: UIButton!
    
    var activeTextField: UITextField!
    var pickedImage:UIImage? = nil
    var imageUrl:String? = ""
    
    var picker:UIImagePickerController?=UIImagePickerController()
    var present:UIPresentationController?=nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nextBtn.springAnimation()
        nextBtn.isEnabled = false;
        
        var profileImage = [String : String]()
        profileImage["image"] = "";
        UserDefaults.standard.setValue(profileImage, forKey:"profileImage")
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        self.navigationController!.setNavigationBarHidden(true, animated: false);
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.navigationController!.setNavigationBarHidden(true, animated: false);
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func navigationLeftBtnAction(_ sender: AnyObject) {
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func goNextBtnAction(_ sender: AnyObject) {
        
        let nxtBtn = sender as! UIButton
        if (nxtBtn.currentImage?.isEqual(UIImage(named: "next_btn_off")))!
        {
            return
        }
        let dictionary = ["fName":firstNameTextField.text! as String,
                          "lName":lastNameTextField.text! as String,
                        "profilePic":imageUrl! as String] as [String: String]
        
        performSegue(withIdentifier: "signUpToLogInSegue", sender: dictionary)
    }
    
    @IBAction func chooseImageBtnAction(_ sender: AnyObject) {
        self.view.endEditing(true)
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        picker?.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        // Present the controller
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
        }
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            self .present(picker!, animated: true, completion: nil)
        }
        else
        {
            openGallary()
        }
    }
    
    func openGallary()
    {
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(picker!, animated: true, completion: nil)
        }
        else
        {
        }
    }
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profileImageView.contentMode = .scaleAspectFit
            let resizedImage = Helper.resizeImage(pImage, CGSize(width: 100, height: 100))
            profileImageView.image = resizedImage
            pickedImage = resizedImage
            
            Helper.showPI(_message: "Uploading Image")
            
            let helperInstance = CloudinaryHelper.sharedInstance
            helperInstance.uploadImage(image:Helper.resizeImage(pickedImage!,CGSize.init(width: 400,height: 400)), onCompletion: { (success, cloudinaryUrl) in
                self.imageUrl = cloudinaryUrl!
                Helper.hidePI()
            })
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UITextFieldDelegates Methods
    func textFieldDidBeginEditing(_ textField: UITextField) {

        activeTextField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        activeTextField = nil
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {

        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {

        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var textFieldText:NSString = textField.text! as NSString
        textFieldText = textFieldText.replacingCharacters(in: range, with: string) as NSString
        if (textFieldText.length > 0)
        {
            if textField == firstNameTextField {
                firstNameCheckMarkImageView.isHidden = false
                nextBtn.setImage(UIImage.init(named:"next_btn_off"), for: .normal)
                nextBtn.isEnabled = false;
                if((lastNameTextField.text?.characters.count)!>0)
                {
                    nextBtn.setImage(UIImage.init(named:"next_btn_on"), for: .normal)
                    nextBtn.isEnabled = true;
                }
            } else {
                firstNameCheckMarkImageView.isHidden = false
                lastNameCheckMarkImageView.isHidden = false
                nextBtn.setImage(UIImage.init(named:"next_btn_on"), for: .normal)
                nextBtn.isEnabled = true;
                if((firstNameTextField.text?.characters.count)!==0)
                {
                    nextBtn.setImage(UIImage.init(named:"next_btn_off"), for: .normal)
                    nextBtn.isEnabled = false;
                    firstNameCheckMarkImageView.isHidden = true
                }
            }
        }
        else
        {
            nextBtn.setImage(UIImage.init(named:"next_btn_off"), for: .normal)
            nextBtn.isEnabled = false;
            if textField == firstNameTextField {
                firstNameCheckMarkImageView.isHidden = true
            } else {
                lastNameCheckMarkImageView.isHidden = true
            }
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == firstNameTextField)
        {
            lastNameTextField.becomeFirstResponder()
            return true
        }
        textField.resignFirstResponder()
        return true
    }

    @objc func keyboardWillShow(_ notification:Notification) {
        var userInfo = (notification as NSNotification).userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        if(activeTextField == nil)
        {
            activeTextField = firstNameTextField
        }
        
        var textFieldMaxY = activeTextField.frame.maxY
        var view = activeTextField.superview
        
        while view != self.view.superview {
            textFieldMaxY += view!.frame.minY
            view = view?.superview
        }
        let remainder = UIScreen.main.bounds.size.height - textFieldMaxY - keyboardFrame.size.height
        if remainder < 0 {
            let animationDurarion = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
            UIView.animate(withDuration: animationDurarion, animations: { () -> Void in
                self.mainScrollView.contentOffset.y = -remainder + 70
                self.view.layoutIfNeeded()
            })
        }
        self.nextBtnBottomConstraint.constant = keyboardFrame.size.height + 10
        mainScrollView.contentSize = CGSize(width: contentView.frame.size.width, height: contentView.frame.size.height + keyboardFrame.height + 20)
    }
    
        
    @objc func keyboardWillHide(_ notification:Notification) {
        var userInfo = (notification as NSNotification).userInfo!
        let animationDurarion = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
        UIView.animate(withDuration: animationDurarion, animations: { () -> Void in
            self.nextBtnBottomConstraint.constant = 30
            self.view.layoutIfNeeded()
        })
        mainScrollView.contentSize = contentView.frame.size
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueIdentifier:String = segue.identifier!
        switch segueIdentifier {
        case "signUpToLogInSegue":
            if let emailVController = segue.destination as? AddEmaillViewController
            {
                var userDetails = [String : String]()
                userDetails["fName"] = self.firstNameTextField.text
                userDetails["lName"] = self.lastNameTextField.text
                userDetails["profilePic"] = self.imageUrl
                emailVController.dictOfDetails = userDetails
            }
            break
        
        default:
            break
        }
    }
}
