//
//  LogInViewController.swift
//  Sales Paddock
//
//  Created by 3Embed on 07/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Firebase
import FirebaseInstanceID

class LogInViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var errorViewutlet: UIView!
    @IBOutlet weak var errorViewConstraintsOutlet: NSLayoutConstraint!
    @IBOutlet weak var nextBtnBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    @IBOutlet weak var viewMainTitleLabel: UILabel!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailCheckMarkImageView: UIImageView!
    @IBOutlet weak var passawordTextField: UITextField!
    @IBOutlet weak var showPasswordBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    
    var activeTextField: UITextField!
    var viewTitle:String = ""
    var dictOfDetails = [String: String]()
    var pickedImage:UIImage? = nil
    var pickedImageURL:String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewMainTitleLabel.text = viewTitle
        nextBtn.springAnimation()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        activeTextField = self.emailTextField
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.navigationController!.hideNavigation()
        self.nextBtn.isEnabled = false
        nextBtn.setImage(UIImage.init(named:"next_btn_off"), for: .normal)
        emailCheckMarkImageView.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.navigationController!.hideNavigation();
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - UITextFieldDelegates Methods
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTextField = nil
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var textFieldText:NSString = textField.text! as NSString
        textFieldText = textFieldText.replacingCharacters(in: range, with: string) as NSString
        
        let dummyTextField:UITextField = UITextField()
        dummyTextField.text = textFieldText as String
        
        if (textField == emailTextField){
            if ((textFieldText.length > 0) && dummyTextField.text!.isValidEmail())
            {
                emailCheckMarkImageView.isHidden = false
                nextBtn.isEnabled = true
                nextBtn.setImage(UIImage.init(named:"next_btn_on"), for: .normal)
            }
            else
            {
                emailCheckMarkImageView.isHidden = true
                nextBtn.isEnabled = false
                nextBtn.setImage(UIImage.init(named:"next_btn_off"), for: .normal)
            }
        }
        else
        {
            if (textField == passawordTextField)
            {
                emailCheckMarkImageView.isHidden = true
                if(emailTextField.text?.isValidEmail())!
                {
                    emailCheckMarkImageView.isHidden = false
                    nextBtn.isEnabled = true
                    nextBtn.setImage(UIImage.init(named:"next_btn_on"), for: .normal)
                }
            }
            else
            {
                nextBtn.isEnabled = false
                nextBtn.setImage(UIImage.init(named:"next_btn_off"), for: .normal)
                if(emailTextField.text?.isValidEmail())!
                {
                    emailCheckMarkImageView.isHidden = false
                }
            }
        }
        return true
    }
    

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
            
        case emailTextField:
            if textField == emailTextField
            {
                passawordTextField.becomeFirstResponder()
            }
            break
            
        case passawordTextField:
            
            if ((passawordTextField.text?.isEmpty) == false) {
                passawordTextField.resignFirstResponder()
            }
            else {
                passawordTextField.becomeFirstResponder()
            }
            break
            
        default:
            textField.becomeFirstResponder()
        }
        return true
    }
    
    @objc func keyboardWillShow(_ notification:Notification) {
        
        var userInfo = (notification as NSNotification).userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        if(activeTextField == nil)
        {
            activeTextField = emailTextField
        }
        var textFieldMaxY = activeTextField.frame.maxY
        var view = activeTextField.superview
        
        while view != self.view.superview {
            textFieldMaxY += view!.frame.minY
            view = view?.superview
        }
        
        let remainder = UIScreen.main.bounds.size.height - textFieldMaxY - keyboardFrame.size.height
        
        if remainder < 0 {
            
            let animationDurarion = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
            
            UIView.animate(withDuration: animationDurarion, animations: { () -> Void in
                self.mainScrollView.contentOffset.y = -remainder + 70
                self.view.layoutIfNeeded()
            })
        }
        
        self.nextBtnBottomConstraint.constant = keyboardFrame.size.height + 10
        mainScrollView.contentSize = CGSize(width: contentView.frame.size.width, height: contentView.frame.size.height + keyboardFrame.height + 20)
    }
    
    
    @objc func keyboardWillHide(_ notification:Notification) {
        var userInfo = (notification as NSNotification).userInfo!
        let animationDurarion = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
        UIView.animate(withDuration: animationDurarion, animations: { () -> Void in
            self.nextBtnBottomConstraint.constant = 30
            self.view.layoutIfNeeded()
        })
        mainScrollView.contentSize = contentView.frame.size
    }
    
    
    @IBAction func navigationLeftBtnAction(_ sender: AnyObject) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func forgotPasswordBtnAction(_ sender: AnyObject)
    {
        self.performSegue(withIdentifier: "forgetPwdSegue", sender: nil)
    }
    
    @IBAction func showPasswordBtnAction(_ sender: AnyObject) {
        
        if showPasswordBtn.isSelected {
            
            showPasswordBtn.isSelected = false
            passawordTextField.isSecureTextEntry = true
            
        }
        else {
            
            showPasswordBtn.isSelected = true
            passawordTextField.isSecureTextEntry = false
        }
    }
    
    
    func checkMandatoryField() -> Bool {
        
        if (emailTextField.text?.isEmpty)! {
            self.showAlert("Message", message: "Please enter email id.")
            return false
        }
        if (passawordTextField.text?.isEmpty)! {
            self.showAlert("Message", message: "Please enter Password.")
            return false
        }
        return true
    }
    
    @IBAction func goToNextBtnAction(_ sender: AnyObject) {
        
        self.view.endEditing(true)
        
        if checkMandatoryField() == false {
            return
        }
        
        Helper.showPI(_message: "Logging in...")
        
        guard let pushToken = FIRInstanceID.instanceID().token() else { return }
        let deviceid = UIDevice.current.identifierForVendor?.uuidString
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let systemVersion = UIDevice.current.systemVersion
        let model = UIDevice.current.model
        
        let params = ["email":emailTextField.text! as AnyObject,
                      "password":passawordTextField.text! as AnyObject,
                      "pushToken":pushToken as AnyObject,
                      "deviceId":deviceid! as AnyObject,
                      "appVersion":version! as AnyObject,
                      "model":model as AnyObject,
                      "manufacture":"Apple" as AnyObject,
                      "os":systemVersion as AnyObject]
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.login,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                        
                                    case 0:
                                        
                                        let responseDict = response["response"].dictionaryObject! as [String: AnyObject]
                                        
                                        let sessionToken = responseDict["token"] as! String
                                        Utility.setSessionToken(sessionToken: sessionToken)
                                        
                                        // Save User Data
                                        let userID = responseDict["userId"] as! String
                                        Utility.setUserID(userID: userID)
                                        
                                        let firstName = responseDict["fname"] as! String
                                        Utility.setUserFirstName(firstName: firstName)
                                        
                                        let lastName = responseDict["lname"] as! String
                                        Utility.setUserLastName(lastName: lastName)
                                        
                                        var profileImage = [String : String]()
                                        profileImage["image"] = responseDict["profilePic"] as? String
                                        UserDefaults.standard.setValue(profileImage, forKey:"profileImage")
                                        
                                        let vc = CreateNewChat()
                                        vc.getofflineMessageService()
                                        
                                        // Go to next level
                                        self.performSegue(withIdentifier: "logInToTabBarSegue", sender: nil)
                                        // Subscrib to group channel
                                        FIRMessaging.messaging().subscribe(toTopic: "adminMessage")
                                        break
                                        
                                    default:
                                        
                                        self.showAnimateView()
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
    
    @IBAction func showPasswordErrorViewAction(_ sender: Any)
    {
        self.showPasswordBtn.isSelected = false
        self.showPasswordBtnAction(self.showPasswordBtn)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueIdentifier:String = segue.identifier!
        switch segueIdentifier {
        case "logInToTabBarSegue":
            if let tabBarVC = segue.destination as? TabBarViewController {
            }
            break
        default:
            break
        }
    }
    
    func showAnimateView(){
        self.errorViewutlet.alpha = 0
        UIView.animate(withDuration: 0.4, animations:
            {
                self.errorViewConstraintsOutlet.constant = (self.view.frame.size.height-self.nextBtnBottomConstraint.constant)-70
                self.errorViewutlet.isHidden = false
                self.errorViewutlet.alpha = 1
                
                self.view.layoutIfNeeded()
        } , completion:{
            animate in
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                self.hideAnimateView()
            }
        })
    }
    
    func hideAnimateView(){
        self.errorViewutlet.alpha = 1
        UIView.animate(withDuration: 0.4, animations: {
            self.errorViewConstraintsOutlet.constant = self.view.frame.size.height+20
            self.errorViewutlet.isHidden = false;
            self.errorViewutlet.alpha = 0
            self.view.layoutIfNeeded()
        },completion:nil)
    }
}
