//
//  HelpViewController.swift
//  Sales Paddock
//
//  Created by 3Embed on 05/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Firebase

class HelpViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var terms: UITextView!
    
    let termsAndConditionsURL = "http://salespaddock-2060556237.us-west-2.elb.amazonaws.com/t&c.html";
    let privacyURL = "http://www.google.com";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTNCandPPBody()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController!.setNavigationBarHidden(true, animated: false);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController!.setNavigationBarHidden(false, animated: false);
    }
    
    func setupTNCandPPBody() {
        
        terms.delegate = self
        
        let termsString = "By signing up, I agree to Sales Paddock's Terms of Service, Payments Terms of Service, Guest Refund Policy, Payment Policy, and Host Guarantee Terms"
        
        let attributedString = NSMutableAttributedString(string: termsString)
        
//        let multipleAttributes = [
//            NSAttributedStringKey.foregroundColor.rawValue: UIColor(netHex:0x484848),
//            NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue] as! [NSAttributedStringKey : Any]
        
        let foundRange = attributedString.mutableString.range(of: "Terms of Service")
//        attributedString.setAttributes(multipleAttributes, range:foundRange)
        attributedString.addAttribute(NSAttributedStringKey.link, value: termsAndConditionsURL, range: foundRange)
        
        /*
         
         foundRange = attributedString.mutableString.range(of: "Payments Terms of Service")
         attributedString.setAttributes(multipleAttributes, range:foundRange)
         attributedString.addAttribute(NSLinkAttributeName, value: termsAndConditionsURL, range: foundRange)
         
         
         foundRange = attributedString.mutableString.range(of: "Guest Refund Policy")
         attributedString.setAttributes(multipleAttributes, range:foundRange)
         attributedString.addAttribute(NSLinkAttributeName, value: termsAndConditionsURL, range: foundRange)
         
         
         foundRange = attributedString.mutableString.range(of: "Payment Policy")
         attributedString.setAttributes(multipleAttributes, range:foundRange)
         attributedString.addAttribute(NSLinkAttributeName, value: termsAndConditionsURL, range: foundRange)
         
         
         foundRange = attributedString.mutableString.range(of: "Host Guarantee Terms")
         attributedString.setAttributes(multipleAttributes, range:foundRange)
         attributedString.addAttribute(NSLinkAttributeName, value: termsAndConditionsURL, range: foundRange)
         */
        
        terms.attributedText = attributedString
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func logInBtnAction(_ sender: AnyObject) {
        performSegue(withIdentifier: "helpToLogInSegue", sender: nil)
    }
    
    @IBAction func facebookLoginBtnAction(_ sender: AnyObject) {
    }
    
    @IBAction func createAccountBtnAction(_ sender: AnyObject) {
        performSegue(withIdentifier: "helpToSignUpSegue", sender: nil)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        let segueIdentifier:String = segue.identifier!
        switch segueIdentifier {
        case "helpToLogInSegue":
            if let logInVC = segue.destination as? LogInViewController {
                logInVC.viewTitle = "Log in"
            }
            break
        case "helpToSignUpSegue":
            break
        default:
            break
        }
    }
}
