//
//  SplashViewController.swift
//  Sales Paddock
//
//  Created by 3Embed on 05/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // start the timer
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(timerAction), userInfo: nil, repeats: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController!.hideNavigation();
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // Stop Timer
        timer.invalidate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func timerAction(sender: Timer) {
        
        // Check for Sezsion Presence
        let sessionToken = UserDefaults.standard.string(forKey: Constant.UD_NAME.SESSION_TOKEN)
        
        // If Session is Present go to Tab Bar Controller
        // On Start up
        if ((sessionToken) != nil) {
            performSegue(withIdentifier: "SplashToTabBarVC", sender: nil)
        }
        else {
            // Else to controlller Where We can Login or Registered
            performSegue(withIdentifier: "splashToHelpSegue", sender: nil)
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "splashToHelpSegue" {
            
        }
    }
}
