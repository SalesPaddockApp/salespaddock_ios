//
//  AddEmaillViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 09/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Firebase
import FirebaseInstanceID

class AddEmaillViewController: UIViewController {
    
    @IBOutlet weak var emailAlertConstraintOutlet: NSLayoutConstraint!
    @IBOutlet weak var pwdTandCLabelOutlet: UILabel!
    @IBOutlet weak var nextBtnOutlet: UIButton!
    var activeTextField: UITextField!
    var dictOfDetails:[String :String]!
    var remainder = 0
    
    @IBOutlet weak var emailIdErrorViewOutlet: UIView!
    @IBOutlet weak var errorViewOutlet: UIView!
    @IBOutlet weak var pwdTickImageOutlet: UIImageView!
    @IBOutlet weak var mainTableViewOutlet: UITableView!
    @IBOutlet weak var nextButtonOutletConstraint: NSLayoutConstraint!
    @IBOutlet weak var emailTickMarkIconOutlet: UIImageView!
    @IBOutlet weak var emailTextFieldOutlet: UITextField!
    @IBOutlet weak var passwordTextFieldOutlet: UITextField!
    @IBOutlet weak var errorMsgHeightOutlet: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        activeTextField = emailTextFieldOutlet
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.nextBtnOutlet.isEnabled = false
        nextBtnOutlet.setImage(UIImage.init(named:"next_btn_off"), for: .normal)
        pwdTickImageOutlet.isHidden = true
        emailTickMarkIconOutlet.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.navigationController!.hideNavigation();
    }
    
    @IBAction func backButtonAction(_ sender: Any)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showPasswordAction(_ sender: Any)
    {
        let showPwdBtn = sender as! UIButton
        if showPwdBtn.isSelected {
            
            showPwdBtn.isSelected = false
            passwordTextFieldOutlet.isSecureTextEntry = true
        }
        else {
            showPwdBtn.isSelected = true
            passwordTextFieldOutlet.isSecureTextEntry = false
            passwordTextFieldOutlet.becomeFirstResponder()
        }
    }
    
    @IBAction func nextBtnAction(_ sender: Any) {
        if validate(password: passwordTextFieldOutlet.text!)
        {
            signUp()
        }
        else
        {
            self.errorMsgHeightOutlet.constant = self.view.frame.size.height+20
            self.view.layoutIfNeeded()
            showAnimateView(isErrorForEmail: false)
        }
    }
    
    func showAnimateView(isErrorForEmail : Bool){
        self.errorViewOutlet.alpha = 0
        self.emailIdErrorViewOutlet.alpha = 0
        UIView.animate(withDuration: 0.4, animations: {
            if !isErrorForEmail{
                self.errorMsgHeightOutlet.constant = (self.view.frame.size.height-self.nextButtonOutletConstraint.constant)-100
                self.errorViewOutlet.isHidden = false
                self.errorViewOutlet.alpha = 1
            }
            else
            {
                self.emailAlertConstraintOutlet.constant = (self.view.frame.size.height-self.nextButtonOutletConstraint.constant)-100
                self.emailIdErrorViewOutlet.isHidden = false
                self.emailIdErrorViewOutlet.alpha = 1
                
            }
            self.view.layoutIfNeeded()
        } , completion:{
            animate in
            self.hideAnimateView(isErrorForEmail: isErrorForEmail)
        })
    }
    @IBAction func loginButtonAction(_ sender: Any) {
        
    }
    
    func hideAnimateView(isErrorForEmail : Bool){
        self.errorViewOutlet.alpha = 1
        self.emailIdErrorViewOutlet.alpha = 1
        UIView.animate(withDuration: 0.4, delay: 2, options: .curveEaseOut, animations: {
            if !isErrorForEmail
            {
                self.errorMsgHeightOutlet.constant = self.view.frame.size.height+20
                self.errorViewOutlet.isHidden = false;
                self.errorViewOutlet.alpha = 0
            }
            else
            {
                self.emailAlertConstraintOutlet.constant = self.view.frame.size.height+20
                self.emailIdErrorViewOutlet.isHidden = false;
                self.emailIdErrorViewOutlet.alpha = 0
            }
            self.view.layoutIfNeeded()
        },completion:nil)
    }
}

extension AddEmaillViewController : UITextFieldDelegate
{
    func validate(password: String) -> Bool
    {
        //        let regularExpression = "^(?=.{8,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(.*[^A-Za-z0-9].*)$"
        //"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[d$@$!%*?&#])[A-Za-z\dd$@$!%*?&#]{8,}"
        
        //        let passwordValidation = NSPredicate.init(format: "SELF MATCHES %@", regularExpression)
        //
        //        return passwordValidation.evaluate(with: password)
        if password.characters.count>7 {
            return true
        }
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTextField = nil
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == passwordTextFieldOutlet
        {
            passwordTextFieldOutlet.text = ""
            pwdTickImageOutlet.isHidden = true
            nextBtnOutlet.isEnabled = false
            nextBtnOutlet.setImage(UIImage.init(named:"next_btn_off"), for: .normal)
        }
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var textFieldText:NSString = textField.text! as NSString
        textFieldText = textFieldText.replacingCharacters(in: range, with: string) as NSString
        
        let dummyTextField:UITextField = UITextField()
        dummyTextField.text = textFieldText as String
        
        if (textField == emailTextFieldOutlet){
            if ((textFieldText.length > 0) && dummyTextField.text!.isValidEmail())
            {
                emailTickMarkIconOutlet.isHidden = false
                if (passwordTextFieldOutlet.text?.characters.count)!>7
                {
                    nextBtnOutlet.isEnabled = true
                    nextBtnOutlet.setImage(UIImage.init(named:"next_btn_on"), for: .normal)
                }
            }
            else
            {
                emailTickMarkIconOutlet.isHidden = true
                nextBtnOutlet.isEnabled = false
                nextBtnOutlet.setImage(UIImage.init(named:"next_btn_off"), for: .normal)
                if (passwordTextFieldOutlet.text?.characters.count)!>7
                {
                    pwdTickImageOutlet.isHidden = false
                }
            }
        }
        else
        {
            if (textField == passwordTextFieldOutlet && (dummyTextField.text?.characters.count)!>7)
            {
                pwdTickImageOutlet.isHidden = false
                emailTickMarkIconOutlet.isHidden = true
                if(emailTextFieldOutlet.text?.isValidEmail())!
                {
                    emailTickMarkIconOutlet.isHidden = false
                    nextBtnOutlet.isEnabled = true
                    nextBtnOutlet.setImage(UIImage.init(named:"next_btn_on"), for: .normal)
                }
            }
            else
            {
                pwdTickImageOutlet.isHidden = true
                nextBtnOutlet.isEnabled = false
                nextBtnOutlet.setImage(UIImage.init(named:"next_btn_off"), for: .normal)
                if(emailTextFieldOutlet.text?.isValidEmail())!
                {
                    emailTickMarkIconOutlet.isHidden = false
                }
            }
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
            
        case emailTextFieldOutlet:
            if emailTextFieldOutlet == textField
            {
                passwordTextFieldOutlet.becomeFirstResponder()
            }
            break
            
        case passwordTextFieldOutlet:
            self.nextBtnAction(nextBtnOutlet);
        default:
            textField.becomeFirstResponder()
        }
        return true
    }
    
    @objc func keyboardWillShow(_ notification:Notification) {
        
        var userInfo = (notification as NSNotification).userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        if(activeTextField == nil)
        {
            activeTextField = emailTextFieldOutlet
        }
        var textFieldMaxY = activeTextField.frame.maxY
        var view = activeTextField.superview
        
        while view != self.view.superview {
            textFieldMaxY += view!.frame.minY
            view = view?.superview
        }
        
        self.remainder = Int(UIScreen.main.bounds.size.height - textFieldMaxY - keyboardFrame.size.height)
        
        if self.remainder < 0 {
            
            let animationDurarion = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
            
            UIView.animate(withDuration: animationDurarion, animations: { () -> Void in
                self.mainTableViewOutlet.contentOffset.y = CGFloat(Int(self.mainTableViewOutlet.contentOffset.y) - Int(self.remainder) + 70)
                self.view.layoutIfNeeded()
            })
        }
        
        self.nextButtonOutletConstraint.constant = keyboardFrame.size.height + 10
        mainTableViewOutlet.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height + keyboardFrame.height + 20)
        self.view.layoutIfNeeded()
    }
    
    @objc func keyboardWillHide(_ notification:Notification) {
        var userInfo = (notification as NSNotification).userInfo!
        let animationDurarion = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
        UIView.animate(withDuration: animationDurarion, animations: { () -> Void in
            self.nextButtonOutletConstraint.constant = 30
            self.view.layoutIfNeeded()
        })
        mainTableViewOutlet.contentSize = self.view.frame.size
    }
    
    func signUp() {
        
        Helper.showPI(_message: "Signing in...")
        guard let pushToken = FIRInstanceID.instanceID().token() else { return }
        let fName = dictOfDetails["fName"]
        let lName = dictOfDetails["lName"]
        let deviceid = UIDevice.current.identifierForVendor?.uuidString
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let systemVersion = UIDevice.current.systemVersion
        let model = UIDevice.current.model
        
        
        let params = ["fName":fName! as AnyObject,
                      "lName":lName! as AnyObject,
                      "dob":"10/07/1990" as AnyObject,
                      "email":emailTextFieldOutlet.text! as AnyObject,
                      "password":passwordTextFieldOutlet.text! as AnyObject,
                      "gender":"0" as AnyObject,
                      "profilePic":dictOfDetails["profilePic"] as AnyObject,
                      "about":"I am new User" as AnyObject,
                      "pushToken":pushToken as AnyObject,
                      "deviceId":deviceid! as AnyObject,
                      "appVersion":version! as AnyObject,
                      "model":model as AnyObject,
                      "manufacture":"Apple" as AnyObject,
                      "os":systemVersion as AnyObject]
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.register,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                        
                                    case 0:
                                        
                                        // Save Session Data
                                        let responseDict = response["response"].dictionaryObject! as [String: AnyObject]
                                        
                                        let sessionToken = responseDict["token"] as! String
                                        Utility.setSessionToken(sessionToken: sessionToken)
                                        
                                        // Save User Data
                                        let userID = responseDict["userId"] as! String
                                        Utility.setUserID(userID: userID)
                                        
                                        Utility.setUserFirstName(firstName: fName!)
                                        Utility.setUserLastName(lastName: lName!)
                                        
                                        self.performSegue(withIdentifier: "disciplines segue", sender: nil)
                                        
                                        break
                                        
                                    default:
                                        self.view.endEditing(true)
                                        self.showAnimateView(isErrorForEmail: true)
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
}
