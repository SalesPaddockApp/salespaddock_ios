//
//  HorseCollectionViewCell.swift
//  Sales Paddock
//
//  Created by 3Embed on 10/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class HorseCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var horseBackgroundImageView: UIImageView!
    @IBOutlet weak var sellPriceLabel: UILabel!
    @IBOutlet weak var leasePriceLabel: UILabel!
    @IBOutlet weak var horseNameLabel: UILabel!
    @IBOutlet weak var horseDescriptionLabel: UILabel!
    @IBOutlet weak var horseOwnerImageView: UIImageView!    
}
