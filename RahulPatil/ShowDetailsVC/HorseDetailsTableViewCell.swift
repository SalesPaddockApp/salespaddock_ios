//
//  HorseDetailsTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 21/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

protocol OpenUserDetailPageDelegate {
    func openUsersProfile(currentIndex:Int)
}

class HorseDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet var userProfileButtonOutlet: UIButton!
    @IBOutlet var userImageOutlet: UIImageView!
    var OpenUserDetailPageDelegate:OpenUserDetailPageDelegate! = nil
    @IBOutlet weak var horseImageOutlet: UIImageView!
    @IBOutlet weak var horseNameOutlet: UILabel!
    var currentIndex = Int()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func loadCell(dictionary: [String: AnyObject]) {
        
        // Formate Horse name
        let arrayOfHorseName:[String] = (dictionary["name"] as? [String])!
        
        if !arrayOfHorseName.isEmpty {
            horseNameOutlet.text = arrayOfHorseName[0]
        }
        
        if (dictionary["showName"] != nil) {
            horseNameOutlet.text = dictionary["showName"]! as? String
        }
        if let imageArray = dictionary["image"] as? [AnyObject]
        {
            if !imageArray.isEmpty{
                let dataArray = imageArray[0] as! [String:AnyObject]
                var imageUrl = String()
                if(dataArray["type"] as! String) == "0"
                {
                    imageUrl = dataArray["imageUrl"] as! String
                }
                else
                {
                    imageUrl = dataArray["thumbnailUrl"] as! String
                }
                let url = URL(string: imageUrl)
                horseImageOutlet.contentMode = .scaleToFill
                self.horseImageOutlet.kf.indicatorType = .activity
                self.horseImageOutlet.kf.indicator?.startAnimatingView()
                horseImageOutlet.kf.setImage(with: url,
                                             placeholder: UIImage.init(named: "horse_default_image"),
                                             options: [.transition(ImageTransition.fade(1))],
                                             progressBlock: { receivedSize, totalSize in
                },
                                             completionHandler: { image, error, cacheType, imageURL in
                                                self.horseImageOutlet.kf.indicator?.stopAnimatingView()
                                                
                })
            }
        }
        let userdetails =  dictionary["ownerDetail"] as! [String : AnyObject]
        let urlStr = userdetails["profilePic"] as! String
        let url = URL(string:urlStr)
        self.userImageOutlet.kf.indicatorType = .activity
        self.userImageOutlet.kf.indicator?.startAnimatingView()
        self.userImageOutlet.kf.setImage(with: url,
                                         placeholder: #imageLiteral(resourceName: "profile_defalt_image"),
                                         options: [.transition(ImageTransition.fade(1))],
                                         progressBlock:nil,
                                         completionHandler: { image, error, cacheType, imageURL in
                                            self.userImageOutlet.kf.indicator?.stopAnimatingView()
        })
    }
    
    
    @IBAction func openUserProfileActn(_ sender: Any) {
        if (self.OpenUserDetailPageDelegate != nil){
            self.OpenUserDetailPageDelegate.openUsersProfile(currentIndex:self.currentIndex)
        }
        //
    }
}
