//
//  UserNameTableViewCell.swift
//  Sales Paddock
//
//  Created by 3Embed on 10/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class UserNameTableViewCell: UITableViewCell {

    @IBOutlet weak var userNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
