//
//  showDescriptionTableViewCell.swift
//  Sales Paddock
//
//  Created by 3Embed on 13/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class showDescriptionTableViewCell: UITableViewCell,UITextViewDelegate {
  
    @IBOutlet weak var showDescriptionLabel: UITextView!
    @IBOutlet weak var showHeldDateLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        return true
    }
}

