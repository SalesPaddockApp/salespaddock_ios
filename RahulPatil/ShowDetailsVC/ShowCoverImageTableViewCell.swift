//
//  ShowCoverImageTableViewCell.swift
//  Sales Paddock
//
//  Created by 3Embed on 14/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class ShowCoverImageTableViewCell: UITableViewCell {

    @IBOutlet weak var showCoverImageView: UIImageView!
    @IBOutlet weak var showNameLabel: UILabel!
    @IBOutlet weak var addToWishListButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
