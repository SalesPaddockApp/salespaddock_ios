//
//  ShowCollectionViewCell.swift
//  Sales Paddock
//
//  Created by 3Embed on 09/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class ShowCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var showBackgroundImageView: UIImageView!
    @IBOutlet weak var showNameLabel: UILabel!
    
}
