//
//  HorseCollectionViewAddTableViewCell.swift
//  Sales Paddock
//
//  Created by 3Embed on 14/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class HorseCollectionViewAddTableViewCell: UITableViewCell {
    
    @IBOutlet weak var horseNameOutlet: UILabel!
    @IBOutlet weak var horseImageView: UIImageView!
    @IBOutlet weak var leasePriceLabel: UILabel!
    @IBOutlet weak var sellPriceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func loadCell(dictionary: [String: AnyObject]) {
        
        let priceingArray = dictionary["priceDetails"] as? [AnyObject]
        if priceingArray == nil{
            
        }
        else{
            let leasePrice = priceingArray?[0] as! String
            let sellPrice = priceingArray?[1] as! String
            
            leasePriceLabel.text = String(format: "$%@ ",leasePrice)
            sellPriceLabel.text = String(format: "$%@ ",sellPrice)
        }
        
        // Formate Horse name
        let arrayOfHorseName:[String] = (dictionary["name"] as? [String])!
        
        if !arrayOfHorseName.isEmpty {
            horseNameOutlet.text = arrayOfHorseName[0]
        }
        }
}
