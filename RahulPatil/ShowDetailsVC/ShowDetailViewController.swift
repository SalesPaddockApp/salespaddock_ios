//
//  ShowDetailViewController.swift
//  Sales Paddock
//
//  Created by 3Embed on 13/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class ShowDetailViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var showDetailTableView: UITableView!
    

    var firstTimeFilterBtnContraint:CGFloat = 0.0
    
    var storedOffsets = [Int: CGFloat]()
    var arrayOfHorses = [AnyObject]()

    var postDictionary = [String: AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showDetailTableView.scrollsToTop = true;
        showDetailTableView.estimatedRowHeight = 270
        showDetailTableView.rowHeight = UITableViewAutomaticDimension
        showDetailTableView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0)
        
        getShowDetails()
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func navigationLeftBtnAction(_ sender: AnyObject) {
        _ = navigationController?.popViewController(animated: true)
    }

    // MARK: - UIScrollViewDelegate protocol

    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView  == showDetailTableView
        {
            let offsetY: CGFloat = scrollView.contentOffset.y
            if offsetY > 200 {
                let alpha: CGFloat = min(1, 1 - ((200 + 64 - offsetY) / 64))
                self.navigationController?.setNavigationBarHidden(true, animated: true)
                if(alpha == 1)
                {
                    self.navigationController?.setNavigationBarHidden(false, animated: true)

                }
            }
            else
            {
//                backButtonOutlet.setImage(#imageLiteral(resourceName: "back_btn_wight_off"), for: .normal)
            }
        }
    }

    // MARK: - UITableViewDataSource protocol
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrayOfHorses.count == 0 {
            return 2
        }
        else {
            return 3+arrayOfHorses.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch indexPath.row {
        case 0:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.showCoverImageTableViewCell, for: indexPath) as? ShowCoverImageTableViewCell

            if (postDictionary["showName"] != nil) {
                cell?.showNameLabel.text = postDictionary["showName"]! as? String
            }
            
            if postDictionary["imgPath"] != nil {
                
                let imageURl = postDictionary["imgPath"] as? String
                if (imageURl != nil) {

                    let url = URL(string: imageURl!)
                    
                    cell?.showCoverImageView.kf.indicatorType = .activity
                    cell?.showCoverImageView.kf.indicator?.startAnimatingView()
                    
                    cell?.showCoverImageView.kf.setImage(with: url,
                                                 placeholder: UIImage.init(named: "horse_default_image"),
                                                 options: [.transition(ImageTransition.fade(1))],
                                                 progressBlock: { receivedSize, totalSize in
                        },
                                                 completionHandler: { image, error, cacheType, imageURL in
                                                    cell?.showCoverImageView.kf.indicator?.stopAnimatingView()
                    })
                }
            }
            
            return cell!
            
        case 1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.showDescriptionTableViewCell, for: indexPath) as? showDescriptionTableViewCell
            
            cell?.showDescriptionLabel.text = "\(postDictionary["Description"]!)]"

            if let dateString = postDictionary["startDate"] as? String
            {
               cell?.showHeldDateLabel.text = dateString
            }
            
            return cell!

        case 2:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.userNameTabelViewCell, for: indexPath) as? UserNameTableViewCell
            return cell!

        default:
            
            let cell = tableView.dequeueReusableCell(withIdentifier:"HorseDetailsTableViewCell", for: indexPath) as? HorseDetailsTableViewCell
            
            cell?.loadCell(dictionary: arrayOfHorses[indexPath.row-3] as! [String: AnyObject])
           
            return cell!
        }
    }
    
    
    // MARK:  (Delegate)
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
            
        case 0: // Participating Shows
            
            break
            
        case 1: // Popular Horses
            
            break
            
        case 2: // Recently Viewd Header
            
            break
            
        default: // Recently Viewed Details
            performSegue(withIdentifier: "horseDetailSegue", sender: arrayOfHorses[indexPath.row - 3] as! [String: AnyObject])
            break
        }
    }

    // View Profile When clicked on Profile Image
    func viewProfileButtonAction(sender: UIButton) {
        
        let selectedIndex = sender.tag % 500
        let dict:[String: AnyObject] = (arrayOfHorses[selectedIndex] as? [String: AnyObject])!

        if !dict.isEmpty {
            performSegue(withIdentifier: "ShowDetailsToOwnerSegue", sender: dict["ownerDetail"] as! [String: AnyObject])
        }
    }
    
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let segueIdentifier:String = segue.identifier!
        
        switch segueIdentifier {
        
        case "horseDetailSegue":
            
            var dict = sender as! [String: AnyObject]
            let newdict = ["showId": postDictionary["_id"]!] as [String: AnyObject]
            for key in newdict.keys {
                dict[key] = newdict[key]
            }

            break
        
        case "ShowDetailsToOwnerSegue":
        
            let HODVC = segue.destination as! HorseOwnerDetailViewController
            HODVC.postDetails = sender as! [String: AnyObject]
            
            break
        
        default:
            break
        }
    }
    
    /*******************************************/
    // MARK: - WEBSERVICE
    
    func getShowDetails() {
        
        Helper.showPI()
        
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject,
                      "showId":postDictionary["_id"]! as AnyObject]
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getShowDetails,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                        
                                    case 0:
                                        
                                        let responseArray = (response["response"]).arrayObject
                                        if (responseArray != nil) {
                                           self.arrayOfHorses = responseArray! as [AnyObject]
                                        }
                                        self.showDetailTableView.reloadData()
                                        break
                                        
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Alert", message: errMsg!)
                                        
                                        break
                                    }
            },
                                 failure: { (Error) in
                                    Helper.hidePI()
                                    self.showAlert("Alert", message: Error.localizedDescription)
        })
        
    }
}
