//
//  HorseOwnerItemDescriptionTableViewCell.swift
//  Sales Paddock
//
//  Created by 3Embed on 15/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class HorseOwnerItemDescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func loadCell(dictionary: [String: AnyObject], indexPath: NSIndexPath) {
        
        
        switch indexPath.row {
        case 1:
            
            itemNameLabel.text = "Farm/Company Name"
            itemDescriptionLabel.text = dictionary["company"] as! String?
            
            break
            
        case 2:
            
            itemNameLabel.text = "City"
            itemDescriptionLabel.text = dictionary["location"] as! String?
            
            break
            
        case 3:
            
            itemNameLabel.text = "Work"
            itemDescriptionLabel.text = dictionary["work"] as! String?
            
            break
            
        case 5:
            
            itemNameLabel.text = "Email"
            itemDescriptionLabel.text = dictionary["email"] as! String?
            
            break
            
        default:
            
            itemNameLabel.text = ""
            itemDescriptionLabel.text = ""
            
            break
        }
    }
    
}
