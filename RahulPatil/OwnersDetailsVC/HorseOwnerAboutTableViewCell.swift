//
//  HorseOwnerAboutTableViewCell.swift
//  Sales Paddock
//
//  Created by 3Embed on 15/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class HorseOwnerAboutTableViewCell: UITableViewCell {

    @IBOutlet weak var horseOwnerProfileImageView: UIImageView!
    @IBOutlet weak var horseOwnerNameLabel: UILabel!
    @IBOutlet weak var horseOwnerAddressLabel: UILabel!
    @IBOutlet weak var horseOwnerAboutLabel: UILabel!
    @IBOutlet weak var seeMoreBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
