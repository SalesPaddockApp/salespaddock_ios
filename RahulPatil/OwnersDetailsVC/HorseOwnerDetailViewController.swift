//
//  HorseOwnerDetailViewController.swift
//  Sales Paddock
//
//  Created by 3Embed on 15/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class HorseOwnerDetailViewController: UIViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var backButtonOutlet: UIButton!
    @IBOutlet weak var navigationViewOutlet: UIView!
    @IBOutlet weak var horseOwnerTableView: UITableView!
    
    var postDetails = [String: AnyObject]()
    var dictOfResponse:[String: AnyObject] = [:]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        horseOwnerTableView.estimatedRowHeight = 270
        horseOwnerTableView.rowHeight = UITableViewAutomaticDimension
        horseOwnerTableView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0)
        self.navigationController!.interactivePopGestureRecognizer?.delegate = self

        getProfileDetails()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func navigationLeftBtnAction(_ sender: AnyObject) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    // MARK: - UIGestureRecognizerDelegate

    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return (gestureRecognizer is UIScreenEdgePanGestureRecognizer)
    }
    // MARK: - UIScrollViewDelegates

    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView  == horseOwnerTableView
        {
            let offsetY: CGFloat = scrollView.contentOffset.y
            if offsetY > 50 {
                let alpha: CGFloat = min(1, 1 - ((50 + 64 - offsetY) / 64))
                navigationViewOutlet.backgroundColor = Color.white.withAlphaComponent(alpha)
                if(alpha == 1)
                {
                backButtonOutlet.setImage(Image.init(named: "back_btn_off"), for: .normal)
                }
            }
            else
            {
                backButtonOutlet.setImage(Image.init(named: "back_btn_off"), for: .normal)
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView  == horseOwnerTableView
        {
            let offsetY: CGFloat = scrollView.contentOffset.y
            if offsetY > 50 {
                let alpha: CGFloat = min(1, 1 - ((50 + 64 - offsetY) / 64))
                navigationViewOutlet.backgroundColor = Color.white.withAlphaComponent(alpha)
                if(alpha == 1)
                {
                backButtonOutlet.setImage(Image.init(named: "back_btn_wight_off"), for: .normal)
                }
            }
            else
            {
                backButtonOutlet.setImage(Image.init(named: "back_btn_off"), for: .normal)
            }
        }
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let index = (indexPath as NSIndexPath).row
        switch index {
        case (0):
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.horseOwnerAboutTableViewCell, for: indexPath) as? HorseOwnerAboutTableViewCell
            
            if (dictOfResponse.isEmpty == false) {
                
                let fullName = String(format: "%@ %@",(dictOfResponse["fName"]! as?  String)! , (dictOfResponse["lName"]! as?  String)!)
                
                cell?.horseOwnerAboutLabel.text = dictOfResponse["about"]! as? String
                cell?.horseOwnerNameLabel.text = fullName
                cell?.horseOwnerAddressLabel.text = dictOfResponse["address"] as! String?
                
                // Horse Image
                let urlStringHorse = dictOfResponse["profilePic"] as? String
                if urlStringHorse == nil {
                }
                else {
                    let url = URL(string: urlStringHorse!)
                    cell?.horseOwnerProfileImageView.kf.indicatorType = .activity
                    cell?.horseOwnerProfileImageView.kf.indicator?.startAnimatingView()
                    cell?.horseOwnerProfileImageView.kf.setImage(with: url,
                                                                 placeholder: UIImage.init(named: "profile_defalt_image"),
                                                                 options: [.transition(ImageTransition.fade(1))],
                                                                 progressBlock: { receivedSize, totalSize in
                        },
                                                                 completionHandler: { image, error, cacheType, imageURL in
                                                                    cell?.horseOwnerProfileImageView.kf.indicator?.stopAnimatingView()
                    })
                }
            }
            return cell!
        case (4):
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.verifyPhoneTableViewCell, for: indexPath) as? VerifyPhoneTableViewCell
            if (dictOfResponse.isEmpty == false) {
                cell?.phoneNumberLabel.text = dictOfResponse["phone"] as! String?
            }
            
            return cell!
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.horseOwnerItemDescriptionTableViewCell, for: indexPath) as? HorseOwnerItemDescriptionTableViewCell
            
            if (dictOfResponse.isEmpty == false) {
                cell?.loadCell(dictionary: dictOfResponse, indexPath: indexPath as NSIndexPath)
            }
            
            return cell!
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: IndexPath) {
        switch (indexPath as NSIndexPath).row {
        case (0):
            break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: IndexPath) {
        switch (indexPath as NSIndexPath).row {
        case 0:
            break
        default:
            break
        }
    }
    
    func updateUI(dictionary: [String: AnyObject]) {
        
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueIdentifier:String = segue.identifier!
        switch segueIdentifier {
        case "showDetailSegue":
            break
        case "123":
            break
        default:
            break
        }
    }
    
    /*******************************************/
    // MARK: - WEBSERVICE
    
    func getProfileDetails() {
        
        Helper.showPI()
        
        let params = ["userId":postDetails["userId"]! as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject]
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.viewProfile,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                        
                                    case 0:
                                        
                                        self.dictOfResponse = response["response"].dictionaryObject! as [String : AnyObject]
                                        self.horseOwnerTableView.reloadData()
                                        
                                        break
                                        
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Error", message: errMsg!)
                                        
                                        break
                                    }
            },
                                 failure: { (Error) in
                                    self.showAlert("Error", message: Error.localizedDescription)
                                    
                                    
        })
        
    }
}
