//
//  ShareScreenTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 14/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class ShareScreenTableViewCell: UITableViewCell {

    @IBOutlet weak var shareImageOutlet: UIImageView!
    @IBOutlet weak var shareLabelOutlet: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
