//
//  PostImageView.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 31/01/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class PostImageView: UIView {

    @IBOutlet weak var imageViewOutlet: UIImageView!
    
    @IBOutlet weak var crossButton: UIButton!
    
}
