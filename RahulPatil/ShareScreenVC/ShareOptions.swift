//
//  ShareOptions.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 30/01/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ShareOptions: NSObject {
    var shareOptionIcon : UIImage?
    var shareOptionName : String?
    
    init(shareOptionIcon : UIImage, shareOptionName : String) {
        self.shareOptionIcon = shareOptionIcon
        self.shareOptionName = shareOptionName
    }
}
