//
//  ShareScreenViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 14/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Social
import Kingfisher
import MessageUI
import Photos
import FBSDKShareKit


class ShareScreenViewController: UIViewController {
    
    struct Constants{
        static let appStoreLink = "https://itunes.apple.com/us/app/sales-paddock/id1164576952?mt=8"
    }
    
    @IBOutlet weak var titleNameOutlet: UILabel!
    @IBOutlet weak var tableViewOutlet: UITableView!
    @IBOutlet weak var imageViewOutlet: UIImageView!
    var localImageURL = URL(string: "")
    var horseData = [String : AnyObject]()
    var appStoreLink = Constants.appStoreLink
    var appLinkFromAppStore = String()
    var shareOptions:[ShareOptions] = {
        let facebookShare = ShareOptions(shareOptionIcon : #imageLiteral(resourceName: "facebook_icon"),
                                         shareOptionName : "Facebook Share")
        
        let messengerShare = ShareOptions(shareOptionIcon : #imageLiteral(resourceName: "sms_icon"),
                                          shareOptionName : "Message Share")
        
        let twitterShare = ShareOptions(shareOptionIcon : #imageLiteral(resourceName: "twitter_icon"),
                                        shareOptionName : "Twitter Share")
        
        let instaShare = ShareOptions(shareOptionIcon : #imageLiteral(resourceName: "instagram_icon"),
                                      shareOptionName : "Instagram Share")
        
        let emailShare = ShareOptions(shareOptionIcon : #imageLiteral(resourceName: "email_icon"),
                                      shareOptionName : "Email Share")
        
        return [facebookShare,messengerShare,twitterShare,instaShare,emailShare]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let nameOfHorse = horseData["name"] as? String
        {
            self.titleNameOutlet.text = "\(nameOfHorse)"
            appLinkFromAppStore = "\(nameOfHorse)"
        }
        
        if let dataDict = horseData["image"] as? [String:AnyObject]{
            if let imageArray = dataDict["options"] as? [AnyObject]{
                if let dataArray = imageArray[0] as? [String:AnyObject]{
                    var imageUrl = ""
                    if(dataArray["type"] as! String) == "0"
                    {
                        imageUrl = dataArray["imageUrl"] as! String
                    }
                    else
                    {
                        imageUrl = dataArray["thumbnailUrl"] as! String
                    }
                    localImageURL = URL(string: imageUrl)
                    self.imageViewOutlet.kf.indicatorType = .activity
                    self.imageViewOutlet.kf.indicator?.startAnimatingView()
                    imageViewOutlet.kf.setImage(with:self.localImageURL,
                                                placeholder: UIImage.init(named:"horse_photo_defalt_image"),options: [.transition(ImageTransition.fade(1))],
                                                progressBlock: { receivedSize, totalSize in
                    },
                                                completionHandler: { image, error, cacheType, imageURL in
                                                    self.imageViewOutlet.kf.indicator?.stopAnimatingView()
                    })
                }
            }
        }
        
        if let description = horseData["description"] as? String
        {
            appLinkFromAppStore += " \(description)"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // ShareScreenTableViewCell
    }
    
    @IBAction func CloseViewAction(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ShareScreenViewController : UITableViewDelegate, UITableViewDataSource, FBSDKSharingDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shareOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShareScreenTableViewCell") as! ShareScreenTableViewCell
        let shareimageObject = shareOptions[indexPath.row]
        cell.shareImageOutlet.image = shareimageObject.shareOptionIcon
        cell.shareLabelOutlet.text = shareimageObject.shareOptionName
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 0:
            self.openFacebookShare()
            break
            
        case 1:
            self.sendSMSText(phoneNumber:"")
            break
            
        case 2:
            self.openTwitter()
            break
            
        case 3:
            self.postImageToInstagram(image: self.imageViewOutlet.image!)
            break
            
        case 4:
            self.presentMailController()
            break
            
        default:
            break
        }
    }
    
    func openFacebookShare(){
            let content = FBSDKSharePhotoContent()
            let fbsharePhoto = FBSDKSharePhoto(image: self.imageViewOutlet.image!, userGenerated:false)
            content.contentURL = URL(string:"https://www.salespaddock.com")
            content.photos = [fbsharePhoto!]
            let dialog = FBSDKShareDialog()
            dialog.shareContent = content
            dialog.delegate = self
            dialog.mode = FBSDKShareDialogMode.native
            if !dialog.canShow() {
                dialog.mode = .feedBrowser
            }
            dialog.show()
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        
    }
    
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        
        
    }
    func openTwitter(){
        if let vc = SLComposeViewController(forServiceType: SLServiceTypeTwitter) {
            vc.setInitialText(appLinkFromAppStore)
            vc.add(self.imageViewOutlet.image!)
            vc.add(URL(string:appStoreLink))
            present(vc, animated: true)
        }else{
            self.showAlert("Alert", message: "Twitter is not available!")
        }
    }
    
    func sendSMSText(phoneNumber: String) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = appLinkFromAppStore
            if let image = self.imageViewOutlet.image{
                if let imageData: Data = UIImagePNGRepresentation(image){
                    controller.addAttachmentData(imageData, typeIdentifier: "image/png", filename: "\(titleNameOutlet.text!).png")
                    controller.messageComposeDelegate = self
                    self.present(controller, animated: true, completion: nil)
                }
            }
        }
        else{
            self.showAlert("Alert", message: "Unable to send message!")
        }
    }
    
    func presentMailController(){
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            let imageData: Data = UIImagePNGRepresentation(self.imageViewOutlet.image!)!
            mail.addAttachmentData(imageData, mimeType: "image/png", fileName: "\(titleNameOutlet.text!).png")
            mail.setSubject("\(titleNameOutlet.text!)")
            mail.setMessageBody("<p> \(appLinkFromAppStore+" \(appStoreLink)" ) </p>", isHTML: true)
            present(mail, animated: true)
        } else {
            self.showAlert("Error", message: "Mail cannot be sent")
            
        }
    }
    
    func postImageToInstagram(image: UIImage) {
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(image:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @objc func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        if error != nil {
        }
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        let fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        if let lastAsset = fetchResult.firstObject {
            let localIdentifier = lastAsset.localIdentifier
            let u = "instagram://library?LocalIdentifier=" + localIdentifier
            let url = NSURL(string: u)!
            if UIApplication.shared.canOpenURL(url as URL) {
                UIApplication.shared.openURL(NSURL(string: u)! as URL)
            } else {
                let alertController = UIAlertController(title: "Error", message: "Instagram is not installed", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
}

extension ShareScreenViewController : MFMessageComposeViewControllerDelegate, UIDocumentInteractionControllerDelegate, MFMailComposeViewControllerDelegate
{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
