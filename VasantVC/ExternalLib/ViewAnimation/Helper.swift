//
//  Helper.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 19/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit

class Helper: NSObject,NVActivityIndicatorViewable {
    
    //MARK: - Progress Ideicator
    
    // Show with Default Message
    class func showPI() {
        
        let activityData = ActivityData(size: CGSize(width: 30,height: 30),
                                        message: "Loading...",
                                        type: NVActivityIndicatorType(rawValue: 12),
                                        color: UIColor.white,
                                        padding: nil,
                                        displayTimeThreshold: nil,
                                        minimumDisplayTime: nil)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    // Show with Your Message
    class func showPI(_message:  String) {
        
        let activityData = ActivityData(size: CGSize(width: 30,height: 30),
                                        message: _message,
                                        type: NVActivityIndicatorType(rawValue: 12),
                                        color: UIColor.white,
                                        padding: nil,
                                        displayTimeThreshold: nil,
                                        minimumDisplayTime: nil)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    // Hide
    class func hidePI() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    //MARK: - Generic Methods
    class func isValidEmail(emailText: String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailText)
        
    }
    
    // For Resizing Image
    class func resizeImage(_ image: UIImage,_ targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
