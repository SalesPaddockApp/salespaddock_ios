//
//  Utility.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 28/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class Utility: NSObject {
    
    /*****************************************************/
    //MARK: -                     Setter Methods          
    /*****************************************************/
    // Set Sesstion Token
    class func setSessionToken(sessionToken: String!) {
        
        var token = ""
        if (sessionToken.characters.count != 0) {
            token = sessionToken
        }
        UserDefaults.standard.set(token, forKey: Constant.UD_NAME.SESSION_TOKEN)
        UserDefaults.standard.synchronize()
    }
    
    class func removeSessionToken() {
        
        UserDefaults.standard.removeObject(forKey: Constant.UD_NAME.SESSION_TOKEN)
        UserDefaults.standard.synchronize()
    }
    
    
    
    // Set UserID
    class func setUserID(userID: String!) {
        
        var ID = ""
        if (userID.characters.count != 0) {
            ID = userID
        }
        UserDefaults.standard.set(ID, forKey: Constant.UD_NAME.USER_ID)
        UserDefaults.standard.synchronize()
    }
    
    // Set First Name
    class func setUserFirstName(firstName: String!) {
        
        var fName = ""
        if (firstName.characters.count != 0) {
            fName = firstName
        }
        UserDefaults.standard.set(fName, forKey: Constant.UD_NAME.USER_FNAME)
        UserDefaults.standard.synchronize()
    }
    
    // Set Last Name
    class func setUserLastName(lastName: String!) {
        
        var lName = ""
        if (lastName.characters.count != 0) {
            lName = lastName
        }
        UserDefaults.standard.set(lName, forKey: Constant.UD_NAME.USER_LNAME)
        UserDefaults.standard.synchronize()
    }
    
    
    
    /*****************************************************/
    //MARK: -            (Getter Methods)
    /*****************************************************/
    
    // Get Sesstion Token
    class func getSessionToken() -> String {
        
        var sessionToken = UserDefaults.standard.value(forKey: Constant.UD_NAME.SESSION_TOKEN)
        if (sessionToken == nil) {
            sessionToken = ""
        }
        return sessionToken as! String
    }
    
    // Get UserID
    class func getUserID() -> String {
        
        var userID = UserDefaults.standard.value(forKey: Constant.UD_NAME.USER_ID)
        if (userID == nil) {
            userID = ""
        }
        return userID as! String
    }
    // Get First
    class func getUserFirstName() -> String {
        
        var fName = UserDefaults.standard.value(forKey: Constant.UD_NAME.USER_FNAME)
        if (fName == nil) {
            fName = ""
        }
        return fName as! String
    }
    // Get Last
    class func getUserLastName() -> String {
        
        var lName = UserDefaults.standard.value(forKey: Constant.UD_NAME.USER_LNAME)
        if (lName == nil) {
            lName = ""
        }
        return lName as! String
    }
}



