//
//  ViewAnimation.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 19/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit
//import NVActivityIndicatorView

extension UITableView {
    
    func animateFromBottom() {
        
        self.reloadData()
        let cells = self.visibleCells
        let tableHeight: CGFloat = self.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        var index = 0
        
        for cellObj in cells {
            let cell: UITableViewCell = cellObj as UITableViewCell
            
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
                }, completion: nil)
            index += 1
        }
    }
}


extension UIView {
    
        func addGradientWithColor(color: UIColor) {
            let gradient = CAGradientLayer()
            gradient.frame = self.bounds
            gradient.colors = [UIColor.clear.cgColor, color.cgColor]
            
            self.layer.insertSublayer(gradient, at: 0)
        }
    
    func dismissAlert() {
        
        let bounds = self.bounds
        let smallFrame = self.frame.insetBy(dx: self.frame.size.width / 4, dy: self.frame.size.height / 4)
        let finalFrame = smallFrame.offsetBy(dx: 0, dy: bounds.size.height)
        
        UIView.animateKeyframes(withDuration: 4, delay: 0, options: .calculationModeCubic, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.5) {
                self.frame = smallFrame
            }
            UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5) {
                self.frame = finalFrame
            }
            }, completion: nil)
        
    }
    
    
//    func rotate360Degrees(duration: CFTimeInterval = 1.0, completionDelegate: AnyObject? = nil) {
//        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
//        rotateAnimation.fromValue = 0.0
//        rotateAnimation.toValue = CGFloat(M_PI * 2.0)
//        rotateAnimation.duration = duration
//        
//        if let delegate: AnyObject = completionDelegate {
//            rotateAnimation.delegate = delegate
//        }
//        self.layer.addAnimation(rotateAnimation, forKey: nil)
//    }
//    
//    func tableViewLoader() {
//        let tableViewLoader = UIImageView(image: UIImage(named: "profile_defalt_image"))
//        
//        // Set position to center
//        tableViewLoader.frame.origin.x = (self.frame.size.width - tableViewLoader.frame.size.width) / 2
//        tableViewLoader.frame.origin.y = (self.frame.size.height - tableViewLoader.frame.size.height - 96) / 2
//        self.insertSubview(tableViewLoader, atIndex: 0)
//    }
    
//    func animateLoader() {
//        if self.isRotating == false {
//            self.tableViewLoader.rotate360Degrees(duration: 1.5, completionDelegate: self)
//            self.isRotating = true
//        }
//    }
//    
//    override func animationDidStop(anim: CAAnimation!, finished flag: Bool) {
//        if self.shouldStopRotating == false {
//            self.tableViewLoader.rotate360Degrees(completionDelegate: self)
//        } else {
//            self.reset()
//        }
//    }
//    
//    func reset() {
//        self.isRotating = false
//        self.shouldStopRotating = false
//    }
    
}

extension UIViewController {
    
    func showAlert(_ title: String, message: String) {
        
        // create the alert
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            // do something like... 
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithDismissal(_ title: String, message: String) {
        
        // create the alert
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            // do something like...
            self.dismiss(animated: true, completion: nil)
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
}

extension UINavigationController {
    
    func showNavigation() {
       self.setNavigationBarHidden(false, animated: false);
    }
    
    func hideNavigation() {
        self.setNavigationBarHidden(true, animated: false);
    }

    func white() {
        
        self.navigationBar.barTintColor = UIColor.white
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor ( red: 0.1333, green: 0.1333, blue: 0.1333, alpha: 1.0 )]
    }
    
    func green() {
        
        self.navigationBar.barTintColor = UIColor ( red: 0.1569, green: 0.651, blue: 0.5412, alpha: 1.0 )
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
}

extension UINavigationItem {
    
    func addLeftSpace() {
        
        let floatVersion = (UIDevice.current.systemVersion as NSString).floatValue
        if (floatVersion >= 7.0) {
            let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpace.width = -16.0
            self.leftBarButtonItems = [negativeSpace, self.leftBarButtonItem!]
            
        }
        else {
            self.leftBarButtonItems = [self.leftBarButtonItem!]
        }
    }
    
    func addRightSpace() {
        
        let floatVersion = (UIDevice.current.systemVersion as NSString).floatValue
        if (floatVersion >= 7.0) {
            let negativeSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpace.width = -16.0
            self.rightBarButtonItems = [negativeSpace, self.rightBarButtonItem!]
            
        }
        else {
            self.rightBarButtonItems = [self.rightBarButtonItem!]
        }
    }
}
