//
//  ReachabilityShowView.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 06/10/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class ReachabilityShowView: UIView {
    
    static var share:ReachabilityShowView? = nil
    var isShown:Bool = false
    
    static var instance: ReachabilityShowView {
        
        if (share == nil) {
            share = UINib(nibName: "ReachabilityShowView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? ReachabilityShowView
        }
        return share!
    }
    
    func showNetworkError() {
        if isShown == false {
            isShown = true
            let window = UIApplication.shared.keyWindow
            self.frame = (window?.frame)!
            window?.addSubview(self)
        }
    }
    
    func hideNetworkError() {
        if isShown == true {
            isShown = false
            self.removeFromSuperview()
        }
    }
}
