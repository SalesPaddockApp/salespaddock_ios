//
//  ReachabilityCheck.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 01/10/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Alamofire

//@objc protocol ReachabilityCheckDelegate {
//    @objc optional func didNetworkReachabilityChange(reachable: Bool)
//}

class ReachabilityCheck: NSObject {
    
    var reachability: Reachability?
    var showViewObj:ReachabilityShowView? = nil
    var isNetworkAvailable:Bool = true
    
//    var delegate: ReachabilityCheckDelegate? = nil
    
    
    private static var obj: ReachabilityCheck? = nil
    static var shared: ReachabilityCheck {
        if obj == nil {
            obj = ReachabilityCheck()
        }
        return obj!
    }
    override init() {
//        showViewObj = ReachabilityShowView.instance
    }
    
    func startMonitoring() {
        
        // Start reachability without a hostname intially
        setupReachability(nil, useClosures: true)
        startNotifier()
        
        // After 5 seconds, stop and re-start reachability, this time using a hostname
        let dispatchTime = DispatchTime.now() + DispatchTimeInterval.seconds(5)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
            //self.stopNotifier()
            self.setupReachability("google.com", useClosures: true)
            self.startNotifier()
            
            let dispatchTime = DispatchTime.now() + DispatchTimeInterval.seconds(5)
            DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
                //self.stopNotifier()
                self.setupReachability("invalidhost", useClosures: true)
                self.startNotifier()            }
            
        }
    }
    
    func setupReachability(_ hostName: String?, useClosures: Bool) {
        
        let reachability = hostName == nil ? Reachability() : Reachability(hostname: hostName!)
        self.reachability = reachability
        
        if useClosures {
            reachability?.whenReachable = { reachability in
                DispatchQueue.main.async {
                    self.updateLabelColourWhenReachable(reachability)
                }
            }
            reachability?.whenUnreachable = { reachability in
                DispatchQueue.main.async {
                    self.updateLabelColourWhenNotReachable(reachability)
                }
            }
        } else {
            NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(_:)), name: ReachabilityChangedNotification, object: reachability)
        }
    }
    
    func startNotifier() {
        do {
            try reachability?.startNotifier()
        }
        catch {
            return
        }
    }
    
    func stopNotifier() {
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: ReachabilityChangedNotification, object: nil)
        reachability = nil
    }
    
    func updateLabelColourWhenReachable(_ reachability: Reachability) {
        
        if reachability.isReachableViaWiFi {
            // Connected through Wifi
        }
        else {
            
        }
        isNetworkAvailable = true
        //showViewObj?.hideNetworkError()
//        UserDefaults.standard.set(<#T##value: Bool##Bool#>, forKey: <#T##String#>)
//        delegate?.didNetworkReachabilityChange!(reachable: isNetworkAvailable)
    }
    
    func updateLabelColourWhenNotReachable(_ reachability: Reachability) {
        isNetworkAvailable = false
        //showViewObj?.showNetworkError()
        
        UserDefaults.standard.set(false, forKey: "isNetworkReachable")
        
//        delegate?.didNetworkReachabilityChange!(reachable: isNetworkAvailable)
    }
    
    
    @objc func reachabilityChanged(_ note: Notification) {
        let reachability = note.object as! Reachability
        
        if reachability.isReachable {
            updateLabelColourWhenReachable(reachability)
        }
        else {
            updateLabelColourWhenNotReachable(reachability)
        }
    }
    
    deinit {
        stopNotifier()
    }
}
