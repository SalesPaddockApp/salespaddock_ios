//
//  GraphView.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 16/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit

class GraphView: UIView {
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var lowerLimitButton: UIButton!
    @IBOutlet weak var upperLimitButton: UIButton!
    @IBOutlet weak var showRangeLabel: UILabel!
    
    @IBOutlet weak var trailingOfGraph: NSLayoutConstraint!
    @IBOutlet weak var leadingOfGraph: NSLayoutConstraint!
    
    var valueOfLowerLimit: CGFloat = 0.0
    var valueOfUpperLimit: CGFloat = 0.0
    
    static func loadViewFromNib(_ frame: CGRect) -> GraphView {
        
        let view = Bundle(for: self).loadNibNamed("GraphView", owner: nil, options: nil)?.first as! GraphView
        view.frame = frame
        return view
    }

    func addGraphView(_ onView: UIView, lowerLimit: CGFloat, upperLimit: CGFloat) {
        onView.addSubview(self)
        
        valueOfLowerLimit = lowerLimit
        valueOfUpperLimit = upperLimit
        
        calculateRange()
    }
    
    @IBAction func lowerPanGestureAction(_ panGesture: UIPanGestureRecognizer) {
        
        let touchlocation = panGesture.location(in: lowerLimitButton.superview)
        if (touchlocation.x < 20) {
            leadingOfGraph.constant = 20
        }
        else {
            leadingOfGraph.constant = touchlocation.x
        }
        self.layoutIfNeeded()
        calculateRange()
        
        
        /*********************************************************/
        //        if panGesture.state == UIGestureRecognizerState.Began {
        //            //add something you want to happen when the Label Panning has started
        //        }
        //
        //        if panGesture.state == UIGestureRecognizerState.Ended {
        //            //add something you want to happen when the Label Panning has ended
        //        }
        //
        //        if panGesture.state == UIGestureRecognizerState.Changed {
        //            //add something you want to happen when the Label Panning has been change ( during the moving/panning )
        //        }
        //        else {
        //            // or something when its not moving
        //        }
    }
    
    @IBAction func upperPanGestureAction(_ panGesture: UIPanGestureRecognizer) {
        
        let touchlocation = panGesture.location(in: upperLimitButton.superview)
        let trailingSpace = (upperLimitButton.superview?.frame.width)! - touchlocation.x
        
        if (trailingSpace < 20) {
            trailingOfGraph.constant = 20
        }
        else {
            trailingOfGraph.constant = trailingSpace
        }
        self.layoutIfNeeded()
        calculateRange()
        
        /*********************************************************/
        //        if panGesture.state == UIGestureRecognizerState.Began {
        //            //add something you want to happen when the Label Panning has started
        //        }
        //
        //        if panGesture.state == UIGestureRecognizerState.Ended {
        //            //add something you want to happen when the Label Panning has ended
        //        }
        //
        //
        //        if panGesture.state == UIGestureRecognizerState.Changed {
        //            //add something you want to happen when the Label Panning has been change ( during the moving/panning )
        //        }
        //        else {
        //            // or something when its not moving
        //        }
    }
    
    
    func calculateRange() {
        
        let lowLimit: CGFloat = leadingOfGraph.constant - 20
        let upLimit: CGFloat = trailingOfGraph.constant - 20
        let widthOfProgress: CGFloat = view.frame.width - 20 * 2
        let interval: CGFloat = (valueOfUpperLimit - valueOfLowerLimit) / widthOfProgress
        
        let calcultedLowerLimit = valueOfLowerLimit + lowLimit * interval
        let calcultedUpperLimit = valueOfUpperLimit - upLimit * interval
        
        showRangeLabel.text = String(format: "$%.0f - $%.0f",calcultedLowerLimit,calcultedUpperLimit)
    }
}
