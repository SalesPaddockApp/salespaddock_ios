//
//  MenuHeaderTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 14/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit

class MenuHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var viewOrEditLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        userNameLabel.text = String(format: "Hi, %@.", Utility.getUserFirstName())
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
