//
//  MenuTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 14/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var menuListLabel: UILabel!
    @IBOutlet weak var menuListImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadCell(_ dictionary: Dictionary<String,String>) {
        
    }
}
