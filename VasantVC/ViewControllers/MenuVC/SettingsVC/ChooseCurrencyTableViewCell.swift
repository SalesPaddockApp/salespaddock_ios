//
//  ChooseCurrencyTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 15/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit

class ChooseCurrencyTableViewCell: UITableViewCell {

    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var checkMarkButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /**
     Update Cell Ui with Dictionary
     
     - parameter dictionary: Dictionary
     - parameter :
     */
    func loadCell(_ dictionary: Dictionary<String,String> = [:]) {
        
        countryLabel.text = String(format: "%@ | %@", arguments:[dictionary["code"]!,dictionary["currency"]!])
    }
}
