//
//  SettingsTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 15/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var settingsSubtitleLabel: UILabel!
    @IBOutlet weak var settingsTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
