//
//  ChooseCurrencyViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 15/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit

protocol ChooseCurrencyViewControllerDelegate {
    func didSelectCountry(_ controller:ChooseCurrencyViewController,dictionary:[String: String])
}

class ChooseCurrencyViewController: UIViewController {

    var delegate:ChooseCurrencyViewControllerDelegate! = nil
    var arrayOfTitle = [[String: String]]()
    var selectedIndexPath: IndexPath = IndexPath(row: 0, section: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        self.navigationItem.addLeftSpace()
//        self.navigationController?.green()
        getCountryCode()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     Get County code and Name of Country and Make array
     */
    func getCountryCode() {
        
        for code in Locale.isoRegionCodes as [String] {
            
            let id = Locale.identifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            
            
            let countryName = (Locale(identifier: "en_UK") as NSLocale).displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            let locale = Locale(identifier: id)
            let currencySymbol = (locale as NSLocale).object(forKey: NSLocale.Key.currencySymbol) as! String
            
            
            let dictionary: [String: String] = ["name" : countryName,"code" : code, "currency" : currencySymbol]
            
            arrayOfTitle.append(dictionary)
        }

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        let chooseCurrencyCellObj = (tableView.dequeueReusableCell(withIdentifier: "ChooseCurrencyTableViewCellIdentifier") as? ChooseCurrencyTableViewCell)!
        
        chooseCurrencyCellObj.loadCell(arrayOfTitle[(indexPath as NSIndexPath).row])
        if (selectedIndexPath == indexPath) {
            chooseCurrencyCellObj.checkMarkButton.isSelected = true
        }
        else {
            chooseCurrencyCellObj.checkMarkButton.isSelected = false
        }
        return chooseCurrencyCellObj
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        
 
        if (selectedIndexPath != IndexPath()) {
            let chooseCurrencyCellObj = tableView.cellForRow(at: selectedIndexPath) as! ChooseCurrencyTableViewCell
            chooseCurrencyCellObj.checkMarkButton.isSelected = false
        }
        
        let chooseCurrencyCellObj: ChooseCurrencyTableViewCell = tableView.cellForRow(at: indexPath) as! ChooseCurrencyTableViewCell
        chooseCurrencyCellObj.checkMarkButton.isSelected = true
        selectedIndexPath = indexPath
    }

    @IBAction func backButtonAction(_ sender: AnyObject) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    @IBAction func saveButtonAction(_ sender: AnyObject) {
        
        if (selectedIndexPath == IndexPath()){
            
        }
        else {
            self.dismiss(animated: true, completion: nil)
            if ((delegate) != nil) {
                delegate?.didSelectCountry(self, dictionary: (arrayOfTitle[(selectedIndexPath as NSIndexPath).row] as [String : String]))
            }
        }
    }
}
