//
//  SettingsViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 15/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit
import MessageUI
import Firebase
import GoogleSignIn

class SettingsViewController: UIViewController,ChooseCurrencyViewControllerDelegate, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var settingsTableView: UITableView!
    
    let arrayOfTitle = ["Send App Feedback","Version 1.0","Logout"]
    var preventAnimation = Set<IndexPath>()
    var dictOfSelectedCountry:[String: String] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBottonAction(_ sender: AnyObject) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    
    // MARK: - TableView
    // MARK:  (Datasource)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let settingsTableViewCellObj: SettingsTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCellIdentifier") as? SettingsTableViewCell)!
        
        settingsTableViewCellObj.settingsTitleLabel.text = arrayOfTitle[(indexPath as NSIndexPath).row]
        
        if ((indexPath as NSIndexPath).row == 0) {
            
            if (dictOfSelectedCountry.isEmpty) {
                settingsTableViewCellObj.settingsSubtitleLabel.isHidden = true
            }
            else {
                settingsTableViewCellObj.settingsSubtitleLabel.isHidden = false
                settingsTableViewCellObj.settingsSubtitleLabel.text = String(format: "%@ | %@",dictOfSelectedCountry["code"]!, dictOfSelectedCountry["currency"]!)
            }
            settingsTableViewCellObj.selectionStyle = .default
        }
        else {
            settingsTableViewCellObj.settingsSubtitleLabel.isHidden = true
            settingsTableViewCellObj.selectionStyle = .none
            
            if ((indexPath as NSIndexPath).row == 1)
            {
                let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                settingsTableViewCellObj.settingsTitleLabel.text = "Version \(version!)"
            }
        }
        return settingsTableViewCellObj
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.frame = CGRect(x: 20, y: cell.frame.origin.y, width: cell.frame.size.width, height: cell.frame.size.height)
        
        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.6, options: UIViewAnimationOptions.curveEaseIn, animations: {
            cell.frame = CGRect(x: 0, y: cell.frame.origin.y, width: cell.frame.size.width, height: cell.frame.size.height)
        }, completion: { finished in
            
        })
    }
    
    
    
    //    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    //        cell.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
    //        UIView.animateWithDuration(0.25, animations: {
    //            cell.layer.transform = CATransform3DMakeScale(1,1,1)
    //        })
    //
    //    }
    
    //    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    //        //
    //        if !preventAnimation.contains(indexPath) {
    //            preventAnimation.insert(indexPath)
    //            let rotationTransform = CATransform3DTranslate(CATransform3DIdentity, 0, +600, 0)
    //            cell.layer.transform = rotationTransform
    //            UIView.animateWithDuration(0.6, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: .CurveEaseOut, animations: {
    //                cell.layer.transform = CATransform3DIdentity
    //                }, completion: nil)
    //        }
    //    }
    
    // MARK:  (Delegate)
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch ((indexPath as NSIndexPath).row) {
            
        case 0: // send app feedback
            self.presentMailController()
            break
            
        case 2: // Logout
            self.logout()
            break
            
        default :
            break
        }
    }
    
    func presentMailController(){
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["salespaddock@gmail.com"])
            mail.setSubject("App Feedback")
            present(mail, animated: true)
        } else {
            self.showAlert("Error", message: "Mail cannot be sent")
            
        }
    }
    
    func logout(){
        
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let userId = UserDefaults.standard.object(forKey: "user_id") as! String!
        let params:[String : AnyObject] = ["token":token! as AnyObject,"userId":userId! as AnyObject]
        Helper.showPI()
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.logout, params: params as [String : AnyObject]?
            , success:
            { (response) in
                
                Helper.hidePI()
                
                let errFlag = response["errCode"].numberValue
                switch errFlag {
                case 0:
                    self.deleteUserData()
                    self.goToSplashVC()
                    break
                default:
                    let errMsg = response["Message"].string
                    break
                }
        }, failure: { (Error) in
            Helper.hidePI()
        })
    }
    
    
    
    /// TO delete all user related data and unsubscribe message channel and logout from googlelogin
    func deleteUserData(){
        
        UserDefaults.standard.removeObject(forKey: Constant.UD_NAME.SESSION_TOKEN)
        UserDefaults.standard.removeObject(forKey: "user_id")
        UserDefaults.standard.removeObject(forKey: "selectedFilters")
        let timestamp : Int = 0
        UserDefaults.standard.set(timestamp, forKey: "timeStamp")
        self.deleteAllDocuments()
        // Unsubscrib to group channel
        FIRMessaging.messaging().unsubscribe(fromTopic: "adminMessage")
        GIDSignIn.sharedInstance().signOut()
    }
    
    /// TO delete chat document
    func deleteAllDocuments(){
        
        var result : CBLQueryEnumerator
        let database = CBObject.sharedInstance.database
        let query = database?.createAllDocumentsQuery()
        query?.allDocsMode =  CBLAllDocsMode.allDocs
        query?.descending = true
        do{
            result = try query!.run()
            for i in 0..<result.count{
                let cbe = CouchbaseEvevnts()
                cbe.deleteDocument(document: (database?.document(withID: (result.row(at: i)).documentID!)!)!)
            }
        }
        catch{
        }
        let batchCountArray = [String]()
        UserDefaults.standard.set(batchCountArray, forKey: "batchCount")
        NotificationCenter.default.post(name:Notification.Name(rawValue:"badgeCount"),
                                        object: nil,
                                        userInfo: ["count":batchCountArray.count])
    }
    
    // MARK: - Choose Country Delegate Method
    func didSelectCountry(_ controller: ChooseCurrencyViewController, dictionary: [String : String]) {
        dictOfSelectedCountry = dictionary
        settingsTableView.reloadData()
    }
    // MARK: -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "SettingsToCurrencyVC") {
            
            let nav = segue.destination as! ChooseCurrencyViewController
            nav.delegate = self
        }
    }
    // MARK -
    func goToSplashVC() {
        performSegue(withIdentifier: "LogoutToSplashSeque", sender: nil)
    }
}

extension SettingsViewController : MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
