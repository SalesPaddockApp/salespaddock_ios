//
//  MenuViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 14/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit
import MessageUI

class MenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var isHostingFirstTime:Bool = true
    
    @IBOutlet weak var menuTableView: UITableView!
    
    var arrayOfMenuTittle: Array<String> = ["Saved",
                                            "Settings",
                                            "Quick Tips",
                                            "Switch to Seller",
                                            "Customer Support"]
    
    var arrayOfMenuImages: Array<UIImage> = [UIImage(named: "wishlists_icon")!,
                                             UIImage(named: "settings_icon")!,
                                             UIImage(named: "help_icon")!,
                                             UIImage(named: "switch_icon")!,
                                             UIImage(named: "feedback_icon")!,
                                             ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        menuTableView.estimatedRowHeight = 80
        menuTableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //menuTableView.animateFromBottom()
        self.menuTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - UITableDatasourse
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfMenuTittle.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let menuHeaderTableViewCellObj: MenuHeaderTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "MenuHeaderTableViewCellIdentifier") as? MenuHeaderTableViewCell)!
        
        let menuTitleTableViewCellObj: MenuTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCellIdentifier") as? MenuTableViewCell)!
        
        
        switch ((indexPath as NSIndexPath).row) {
            
        case 0:
            menuHeaderTableViewCellObj.selectionStyle = UITableViewCellSelectionStyle.none
            return menuHeaderTableViewCellObj
            
        default :
            menuTitleTableViewCellObj.selectionStyle = UITableViewCellSelectionStyle.none
            menuTitleTableViewCellObj.menuListLabel.text = arrayOfMenuTittle[(indexPath as NSIndexPath).row - 1]
            menuTitleTableViewCellObj.menuListImageView.image = arrayOfMenuImages[(indexPath as NSIndexPath).row - 1]
            return menuTitleTableViewCellObj
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        switch ((indexPath as NSIndexPath).row) {
            
        case 0: // Edit Profile
            performSegue(withIdentifier: "MenuToEditVCSeque", sender: nil)

            break
            
        case 1: // WishList
//            performSegue(withIdentifier: "MenuToWishListVC", sender: nil)
            performSegue(withIdentifier: "collectionsSegue", sender: nil)
            break
            
        case 2: // Settings
            performSegue(withIdentifier: "ManuToSettingsVC", sender: nil)
            break
            
        case 3: // FAQs
            performSegue(withIdentifier: "FAQPageSegue", sender: nil)
            break
            
        case 4: // Switch To Host
            performSegue(withIdentifier: "SwitchToHostVC", sender: nil)
            break
            
        case 5: // Give us Feedback
            self.presentMailController()
            break
       
        default :

            break
        }
    }
    
    func presentMailController(){
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["salespaddock@gmail.com"])
            mail.setSubject("App Feedback")
            present(mail, animated: true)
        } else {
            self.showAlert("Error", message: "Mail cannot be sent")
            
        }
    }
}

extension MenuViewController : MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
