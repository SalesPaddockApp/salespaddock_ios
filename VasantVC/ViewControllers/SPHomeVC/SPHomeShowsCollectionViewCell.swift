//
//  SPHomeShowsCollectionViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 29/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftyJSON

class SPHomeShowsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var showsImageView: UIImageView!
    @IBOutlet weak var showsName: UILabel!
    
    func updateUI(dictionary: [String: JSON]) {
        showsName.text = dictionary["showName"]?.stringValue
        
        let url = URL(string: (dictionary["image"]?.stringValue)!)
        
        showsImageView.kf.indicatorType = .activity
        showsImageView.kf.indicator?.startAnimatingView()
        
        showsImageView.kf.setImage(with: url,
                                  placeholder: #imageLiteral(resourceName: "horse_default_image"),
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
            },
                                  completionHandler: { image, error, cacheType, imageURL in

                                    self.showsImageView.kf.indicator?.stopAnimatingView()
        })
    }
}
