//
//  SPHomeViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 29/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import SwiftyJSON

class SPHomeViewController: UIViewController,SPHomePartShowsTableViewCellDelegate {
    
    @IBOutlet weak var SPHomeTableView: UITableView!
    @IBOutlet weak var leaseLabelOutlet: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    
    var showData:[JSON] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        SPHomeTableView.estimatedRowHeight = 260.0
        SPHomeTableView.rowHeight = UITableViewAutomaticDimension
        
        userNameLabel.text = String(format: "Hi, %@.", Utility.getUserFirstName())
        
        var batchCountArray = [String]()
        if (UserDefaults.standard.value(forKey: "batchCount") != nil) {
            batchCountArray = UserDefaults.standard.value(forKey: "batchCount") as! [String]
        }
        if batchCountArray.count == 0{
            self.tabBarController?.tabBar.items?[1].badgeValue = nil
        }else{
            self.tabBarController?.tabBar.items?[1].badgeValue = "\(batchCountArray.count)"

        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getShowsList()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - TableView
    // MARK:  (Datasource)
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return showData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        
//        var cell = UITableViewCell()
        
//        switch indexPath.row {
//            
//        case 0: // Participating Shows
        
            let participatingCell = (tableView.dequeueReusableCell(withIdentifier: "SPHomePartShowsTableViewCellIdentifier") as? SPHomePartShowsTableViewCell)!
        
        let data = (showData[indexPath.row]).dictionaryValue
        participatingCell.delegate = self
        
        
        participatingCell.showTypeTitleOutlet?.text = data["key"]!.stringValue
        
        participatingCell.arrayOfShows = (data["data"]?.arrayValue)!

//        participatingCell.loadPartShows(array: (data["data"]?.arrayValue)! as [AnyObject])

        participatingCell.showsCollectionView?.reloadData()
//        case 1: // Popular Horses
//            
//            let popularHorsesCell = (tableView.dequeueReusableCell(withIdentifier: "SPHomePopularHorsesTableViewCellIdentifier") as? SPHomePopularHorsesTableViewCell)!
//            
//            popularHorsesCell.loadPopularHorses(array: arrayOfPopularHorses)
//            popularHorsesCell.delegate = self
//            popularHorsesCell.selectionStyle = .none
//            cell = popularHorsesCell
//            break
//            
//        case 2: // Recently Viewd Header
//            
//            let recentlyHeaderCell = (tableView.dequeueReusableCell(withIdentifier: "SPHomeHeaderTableViewCellIdentifier") as? SPHomeHeaderTableViewCell)!
//            
//            recentlyHeaderCell.selectionStyle = .none
//            cell = recentlyHeaderCell
//            break
//            
//        default: // Recently Viewed Details
//            
//            let recentlyViewedHorsesCell = (tableView.dequeueReusableCell(withIdentifier: "SPHomeRecentViewedTableViewCellIdentifier") as? SPHomeRecentViewedTableViewCell)!
//            
//            
////            recentlyViewedHorsesCell.RVProfileButton.tag = (indexPath.row - 3) + 500
////            recentlyViewedHorsesCell.RVProfileButton.addTarget(self,
////                                                               action: #selector(profileButtonAction(sender:)),
////                                                               for: .touchUpInside)
//            
//            recentlyViewedHorsesCell.loadRecentViewed(dictionary: arrayOfRecentViewed[indexPath.row - 3] as! [String: AnyObject])
//            recentlyViewedHorsesCell.selectionStyle = .none
//            cell = recentlyViewedHorsesCell
//            break
//        }
        return participatingCell
    }
    
    // MARK:  (Delegate)
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
            
        case 0: // Participating Shows
            
            break
            
        default: // Recently Viewed Details
            
            break
        }
    }
    
    // MARK: - Custom Delegate
    // MARK: - SPHomePartShowsTableViewCellDelegate
    func didSelectShow(dictionary: [String : AnyObject], selectedIndex: Int) {
        goToShowDetails(dictionary: dictionary)
    }
    
    //MARK: -
    
    // Go to Show details View
    func goToShowDetails(dictionary: [String: AnyObject]) {
        performSegue(withIdentifier: "HomeToShowDetailsSegue", sender: dictionary)
    }
    
    // MARK: -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier! {
            
        case "HomeToShowDetailsSegue":
            
            let SDVC = segue.destination as! ShowDetailVC
            SDVC.postDictionary = sender as! [String:AnyObject]
            
            break
            
        default:
            
            break
        }
    }
    
    
    /*******************************************/
    // MARK: - WEBSERVICE
    
    func getShowsList() {
        
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject]
        
        
        Helper.showPI()
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getShowForUsers,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                    case 0:
                                        
                                        self.showData = response["response"].arrayValue as [JSON]
                                        self.SPHomeTableView.reloadData()
                                        break
                                        
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Error", message: errMsg!)
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
    
    /*
    
    @IBAction func prepare(forUnwindSegue: UIStoryboardSegue)
    {
        
    }
    
    override func unwind(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
        let segue = UnwindScaleSegue(identifier: unwindSegue.identifier, source: unwindSegue.source, destination: unwindSegue.destination)
        segue.perform()
    }
 */
 
}
