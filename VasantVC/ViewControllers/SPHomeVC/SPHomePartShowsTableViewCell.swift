//
//  SPHomePartShowsTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 29/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol SPHomePartShowsTableViewCellDelegate {
    // Gelegate gets called when selected Cell
    func didSelectShow(dictionary: [String: AnyObject], selectedIndex: Int)
}

class SPHomePartShowsTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate {

    @IBOutlet weak var showTypeTitleOutlet: UILabel!
    @IBOutlet weak var showsCollectionView: UICollectionView?
    
    var arrayOfShows = [JSON]()
    var delegate:SPHomePartShowsTableViewCellDelegate! = nil
    let collectionLayout = UICollectionViewFlowLayout()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func loadPartShows(array: [JSON]) {
        
        arrayOfShows = array
    
        showsCollectionView?.dataSource = self
        showsCollectionView?.delegate = self
        
        showsCollectionView?.reloadData()
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayOfShows.count
    }
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageWidth: Float = Float(self.showsCollectionView!.frame.width) - 35 //480 + 50
        // width + space
        let currentOffset: Float = Float(scrollView.contentOffset.x)
        let targetOffset: Float = Float(targetContentOffset.pointee.x)
        var newTargetOffset: Float = 0
        if targetOffset > currentOffset {
            newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth
        }
        else {
            newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth
        }
        if newTargetOffset < 0 {
            newTargetOffset = 0
        }
        else if (newTargetOffset > Float(scrollView.contentSize.width)){
            newTargetOffset = Float(Float(scrollView.contentSize.width))
        }
        
        targetContentOffset.pointee.x = CGFloat(currentOffset)
        scrollView.setContentOffset(CGPoint(x: CGFloat(newTargetOffset), y: scrollView.contentOffset.y), animated: true)
    }
    
    /*
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if scrollView == self.showsCollectionView
        {
            targetContentOffset.memory = scrollView.contentOffset

            // set acceleration to 0.0
            let pageWidth: Float = Float(self.showsCollectionView!.bounds.size.width)
            let minSpace = 10
            var cellToSwipe = (scrollView.contentOffset.x) / (CGFloat(pageWidth) + CGFloat(minSpace)) + 0.5
            // cell width + min spacing for lines
            if cellToSwipe < 0 {
                cellToSwipe = 0
            }
            else if Int(cellToSwipe) >= self.arrayOfShows.count
            {
                let a = self.arrayOfShows.count - 1
                cellToSwipe = CGFloat(a)
            }
            
            self.showsCollectionView?.scrollToItem(at: IndexPath(row: Int(cellToSwipe), section: 0), at: .left, animated: true)
        }
    }*/
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        var frameOfCell = self.frame
        frameOfCell.size.width -= 25*2 // Padding
        frameOfCell.size.height = self.frame.size.height-105
        
        return frameOfCell.size
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cellObj = collectionView.dequeueReusableCell(withReuseIdentifier: "SPHomeShowsCollectionViewCellIdentifier", for: indexPath as IndexPath) as! SPHomeShowsCollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cellObj.updateUI(dictionary: arrayOfShows[indexPath.row].dictionaryValue)
        return cellObj
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        if (delegate != nil) {
            if let data = arrayOfShows[indexPath.row].dictionaryObject{
            delegate.didSelectShow(dictionary: data as [String : AnyObject], selectedIndex:indexPath.row)
            }
        }
    }
}
