//
//  SPHomeHorsesCollectionViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 29/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class SPHomeHorsesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var horseImageView: UIImageView!
    @IBOutlet weak var leasePriceLabel: UILabel!
    @IBOutlet weak var sellPriceLabel: UILabel!
    @IBOutlet weak var nameOftheHorseOutlet: UILabel!
    func loadCell(dictionary: [String: AnyObject]) {
        
        
        let priceingArray = dictionary["priceDetails"] as? [AnyObject]
            let leasePrice = priceingArray?[1] as! String
//            if(leasePrice.characters.count>0)
//            {
                leasePriceLabel.text = String(format: "$%@",leasePrice)
//                leaseLabelOutlet.text = "Lease"
//
//            }
//            else
//            {
//                leasePriceLabel.text = ""
//                leaseLabelOutlet.text = ""
//            }
            let sellPrice = priceingArray?[0] as! String
//            if(sellPrice.characters.count>0)
//            {
                sellPriceLabel.text = String(format: "$%@",sellPrice)
//                selllabelOutlet.text = "Sell"
//            }
//            else
//            {
//                sellPriceLabel.text = ""
//                selllabelOutlet.text = ""
//            }
        
        let arrayOfHorseName:[String] = (dictionary["name"] as? [String])!
        
        if !arrayOfHorseName.isEmpty {
            nameOftheHorseOutlet.text = arrayOfHorseName[0]
        }

        let imageArray  = dictionary["image"] as! [AnyObject]
        if !imageArray.isEmpty{
            let data = imageArray[0] as! [String: AnyObject]
            var url = URL(string: "")
            if data["type"] as! String == "0"
            {
                url = URL(string:data["imageUrl"] as! String)
            }
            else{
                url = URL(string:data["thumbnailUrl"] as! String)
            }
            horseImageView.kf.indicatorType = .activity
            horseImageView.kf.indicator?.startAnimatingView()
            horseImageView.kf.setImage(with: url,
                                       placeholder:#imageLiteral(resourceName: "horse_default_image"),
                                       options: [.transition(ImageTransition.fade(1))],
                                       progressBlock: { receivedSize, totalSize in
                },
                                       completionHandler: { image, error, cacheType, imageURL in
                                        self.horseImageView.kf.indicator?.stopAnimatingView()
            })
        }
    }
}
