//
//  CollectionDetailsViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 27/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class CollectionDetailsViewController: UIViewController {

    @IBOutlet weak var postsCollectionVIew: UICollectionView!
    @IBOutlet weak var viewTitle: UILabel!
    @IBOutlet weak var emptyView: UIView!
    
    var collectionName : String!
    var collectionId: String!
    
    var collectionDataObj = CollectionDetailsModel()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewTitle.text = self.collectionName
        self.getCollectionDetails()
        self.emptyView.isHidden = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- Buttons Action
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func editAction(_ sender: Any) {
        let actionSheet = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        let editAction = UIAlertAction.init(title: "Edit collection", style: .default) { (action) in
//            self.performSegue(withIdentifier: "editCollectionSegue", sender: nil)
            let storyBoard: UIStoryboard = UIStoryboard(name: "Collections", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "editCollectionController") as! EditCollectionViewController
             vc.CollectionDetailsModelObj = self.collectionDataObj
            let navigationController = self.navigationController
            let transition = CATransition()
            transition.duration = 0.5
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.type = kCATransitionMoveIn
            transition.subtype = kCATransitionFromTop
            navigationController?.view.layer.add(transition, forKey: nil)
            vc.delegate = self
            navigationController?.pushViewController(vc, animated: false)
        }
        let addAction = UIAlertAction.init(title: "Add to collection", style: .default) { (action) in
            self.performSegue(withIdentifier: "addToCollectionSegue", sender: nil)
        }
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        actionSheet.addAction(editAction)
        actionSheet.addAction(addAction)
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    @IBAction func addToCollectionAction(_ sender: Any) {
        self.performSegue(withIdentifier: "addToCollectionSegue", sender: nil)
    }
    
    
    
    //MARK:- Navigate
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editCollectionSegue"{
            let editCollectionVC = segue.destination as! EditCollectionViewController
            editCollectionVC.CollectionDetailsModelObj = self.collectionDataObj
        }else if segue.identifier == "addToCollectionSegue"{
            let addToCollectionVC = segue.destination as! AddToCollectionViewController
            addToCollectionVC.collectionName = self.collectionDataObj.collectionName
            addToCollectionVC.collectionId = self.collectionDataObj.collectionId
            addToCollectionVC.collectionImage = self.collectionDataObj.CollectionCoverImage
            addToCollectionVC.collectionImagePostId = self.collectionDataObj.coverPost
            addToCollectionVC.delegate = self
        }
    }
    
    //MARK:- Web Services
    func getCollectionDetails(){
        Helper.showPI()
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject,
                      "collectionId" : self.collectionId as AnyObject]
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getCollectionDetails,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    switch errFlag {
                                        
                                    case 0:
                                        if let responseDict = (response["responce"]).dictionaryObject
                                        {
                                            self.collectionDataObj.setDataInModel(collectionData: responseDict)
                                        }else if let responseArray = (response["responce"]).arrayObject{
                                            if responseArray.count > 0{
                                                self.collectionDataObj.setDataInModel(collectionData: responseArray[0])
                                            }
                                        }
                                  self.postsCollectionVIew.reloadData()
                                        break
                                        
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Error", message: errMsg!)
                                        
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    Helper.hidePI()
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
    
}

extension CollectionDetailsViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.collectionDataObj.postDetails.count <= 0{
            self.emptyView.isHidden = false
            self.postsCollectionVIew.isHidden = true
        }else{
            self.emptyView.isHidden = true
            self.postsCollectionVIew.isHidden = false
            self.postsCollectionVIew.reloadData()
        }
        return self.collectionDataObj.postDetails.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionDetailsCell", for: indexPath) as! CollectionDetailsCollectionViewCell
        cell.setData(collectionData: self.collectionDataObj.postDetails[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize.init(width: self.view.frame.size.width / 3 - 1, height: self.view.frame.size.width / 3 - 1)
        let wid = self.view.frame.size.width/2-20
        let higt = wid * 0.678
        return CGSize(width: wid, height: higt)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "horeseDetailController") as! HorseDetailViewController
        let horseDataObj = self.collectionDataObj.postDetails[indexPath.row]
        controller.postId = horseDataObj.postId!
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension CollectionDetailsViewController : AddToCollectionViewControllerDelegate{
    
    func addedPostInCollection(collectionsDetail: [CollectionDetail]) {
        var collectionDetailArray = self.collectionDataObj.postDetails
        collectionDetailArray.append(contentsOf: collectionsDetail)
        self.collectionDataObj.postDetails = collectionDetailArray
        self.postsCollectionVIew.reloadData()
    }
}

extension CollectionDetailsViewController : EditCollectionViewControllerDelegate{
    func updateCollection(name: String, image: String) {
        self.collectionDataObj.collectionName = name
        self.viewTitle.text = name
        self.collectionDataObj.CollectionCoverImage = image
    }
}

extension CollectionDetailsViewController : HorseDetailViewControllerDelegate{
    
    func removePostFormCollection(postId: String) {
        var collectionDeails = [CollectionDetail]()
        for detail in self.collectionDataObj.postDetails{
            if detail.postId != postId{
                collectionDeails.append(detail)
            }
        }
        self.collectionDataObj.postDetails = collectionDeails
        self.postsCollectionVIew.reloadData()
    }
}
