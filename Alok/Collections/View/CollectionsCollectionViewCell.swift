//
//  CollectionsCollectionViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 23/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class CollectionsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var collectionName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(collectionData: CollectionsData){
        
        self.coverImage.kf.setImage(with: URL(string:collectionData.collectionsImageUrl!),
                                         placeholder: UIImage.init(named: "horse_default_image"),
                                         options: [.transition(ImageTransition.fade(1))],
                                         progressBlock: { receivedSize, totalSize in
        },
                                         completionHandler: { image, error, cacheType, imageURL in
                                            self.coverImage.kf.indicator?.stopAnimatingView()
        })
        self.collectionName.text = collectionData.collectionsName
    }
}
