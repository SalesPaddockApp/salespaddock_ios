//
//  SelecteCollectionsCollectionViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 27/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class SelecteCollectionsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var collectionCoverImage: UIImageView!
    @IBOutlet weak var collectionSelectImage: UIImageView!
    @IBOutlet weak var CollectionVideoImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setData(collectionData: CollectionsData){
        if collectionData.isSelected{
            self.collectionSelectImage.image = #imageLiteral(resourceName: "check_on")
        }else{
            self.collectionSelectImage.image = #imageLiteral(resourceName: "check_off")
        }
        if collectionData.isVideo{
            self.CollectionVideoImage.image = #imageLiteral(resourceName: "horse_default_image")
        }else{
            self.CollectionVideoImage.isHidden = true
        }
        self.collectionCoverImage.kf.setImage(with: URL(string:collectionData.collectionsImageUrl!),
                                    placeholder: #imageLiteral(resourceName: "horse_default_image"),
                                    options: [.transition(ImageTransition.fade(1))],
                                    progressBlock: { receivedSize, totalSize in
        },
                                    completionHandler: { image, error, cacheType, imageURL in
                                        self.collectionCoverImage.kf.indicator?.stopAnimatingView()
        })
    }
}
