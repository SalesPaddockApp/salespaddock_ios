//
//  CollectionDetailsCollectionViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 27/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class CollectionDetailsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var videoImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setData(collectionData: CollectionDetail){
        
        self.coverImage.kf.setImage(with: URL(string:collectionData.collectionImage!),
                                              placeholder: #imageLiteral(resourceName: "horse_default_image"),
                                              options: [.transition(ImageTransition.fade(1))],
                                              progressBlock: { receivedSize, totalSize in
        },
                                              completionHandler: { image, error, cacheType, imageURL in
                                                self.coverImage.kf.indicator?.stopAnimatingView()
        })
        if collectionData.isVideo{
            self.videoImage.isHidden = false
        }else{
            self.videoImage.isHidden = true
        }
    }
}
