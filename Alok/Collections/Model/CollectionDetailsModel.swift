//
//  CollectionDetailsModel.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 27/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class CollectionDetailsModel: NSObject {
    
    var collectionId : String?
    var collectionName : String?
    var CollectionCoverImage : String?
    var coverPost : String?
    var postDetails : [CollectionDetail]
    
    override init() {
        self.collectionName = ""
        self.collectionId = ""
        self.CollectionCoverImage = ""
        self.coverPost = ""
        self.postDetails = [CollectionDetail]()
    }
    
    func setDataInModel(collectionData: Any){

        self.collectionName = ""
        self.collectionId = ""
        self.CollectionCoverImage = ""
        self.postDetails = [CollectionDetail]()
        let colDetailsObj = CollectionDetail(postId: "", collectionName: "", collectionImage: "", isVideo: false)
        if let data = collectionData as? [String : Any]{
            if let colId = data["collectionId"] as? String{
                self.collectionId = colId
            }
            if let colName = data["name"] as? String{
                self.collectionName = colName
            }
            if let colImage = data["image"] as? String{
                self.CollectionCoverImage = colImage
            }
            if let coverPostId = data["coverPost"] as? String{
                self.coverPost = coverPostId
            }
            if let colDetails = data["postDetails"] as? [Any]{
                self.postDetails = colDetailsObj.setDataInModel(collectionData: colDetails)
            }
        }
    }
}
