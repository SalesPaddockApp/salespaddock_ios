//
//  CollectionDetail.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 27/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class CollectionDetail: NSObject {
    var collectionName : String?
    var collectionImage : String?
    var isVideo : Bool = false
    var postId : String?
    
    init(postId : String, collectionName : String, collectionImage : String, isVideo : Bool) {
        self.postId = postId
        self.collectionName = collectionName
        self.collectionImage = collectionImage
        self.isVideo = isVideo
    }
    
    func setDataInModel(collectionData : [Any]) ->[CollectionDetail]{
        var collectionArray = [CollectionDetail]()
        for data in collectionData{
            if let dict = data as? [String : Any]{
                var collectionName = ""
                var collectionImage = ""
                var isVideo : Bool = false
                var postId = ""
                if let name = dict["name"] as? String{
                    collectionName = name
                }
                if let id = dict["_id"] as? String{
                    postId = id
                }
                if let image = dict["image"] as? String{
                    collectionImage = image
                }
                else if let imageDict = dict["image"] as? [String : String]
                {
                    if let type = imageDict["type"]{
                        if type == "0"{
                            if let image = imageDict["imageUrl"]{
                                collectionImage = image
                                isVideo = false
                            }
                        }else{
                            if let image = imageDict["thumbnailUrl"]{
                                collectionImage = image
                                isVideo = true
                            }
                        }
                    }
                }
                let collectionDetailsObj = CollectionDetail(postId: postId, collectionName: collectionName, collectionImage: collectionImage, isVideo: isVideo)
                collectionArray.append(collectionDetailsObj)
            }
        }
        return collectionArray
    }
    
    func createCollectionDetailModelArrayFrom(collectionDataArray: [CollectionsData]) ->[CollectionDetail]{
        var collectionArray = [CollectionDetail]()
        var collectionName = ""
        var collectionImage = ""
        var isVideo : Bool = false
        var postId = ""
        for collectionDetail in collectionDataArray{
            if collectionDetail.isSelected{
                collectionName = collectionDetail.collectionsName!
                collectionImage = collectionDetail.collectionsImageUrl!
                postId = collectionDetail.postId!
                isVideo = collectionDetail.isVideo
                let collectionDetailsObj = CollectionDetail(postId: postId, collectionName: collectionName, collectionImage: collectionImage, isVideo: isVideo)
                collectionArray.append(collectionDetailsObj)
            }
        }
        return collectionArray
    }
    
}
