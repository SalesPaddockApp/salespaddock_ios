//
//  CollectionsData.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 23/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class CollectionsData: NSObject {
    var collectionsPrice : String?
    var collectionsImageUrl : String?
    var collectionsID : String?
    var collectionsName :String?
    var userImage : URL?
    var userId : String?
    var ownerName : Any?
    var isSelected : Bool = false
    var isVideo : Bool = false
    var postId : String?
    var coverPost : String?
    
    init(collectionsPrice : String, collectionsID : String, collectionsName : String, collectionsImageUrl : String, userImage : String, userId : String, ownerName : Any, postId : String, coverPost : String) {
        self.collectionsPrice = collectionsPrice
        self.collectionsImageUrl = collectionsImageUrl//URL(string:collectionsImageUrl)
        self.collectionsID = collectionsID
        self.collectionsName = collectionsName
        if let url = URL(string:userImage){
            self.userImage = url
        }
        self.ownerName = ownerName
        self.userId = userId
        self.postId = postId
        self.coverPost = coverPost
    }
    
    /// It will fetch data from response object and convert it to the CollectionsData Object.
    ///
    /// - Parameter collectionsData: response data
    /// - Returns: array of CollectionsData Object
    
    func fetchDataFromAPIForCollections(collectionData : [Any]) -> [CollectionsData]{
        var collectionsDataArray = [CollectionsData]()
        for collectionsDetails in collectionData
        {
            if let collectionsDict = collectionsDetails as? [String: Any]{
                var collectionsPrice = ""
                var id = ""
                var collectionsTempName = ""
//                var imageUrl = ""
                var userImage = ""
                var userIda = ""
                var postId = ""
                var coverPost = ""
                var collectionsObject : CollectionsData
                if let priceDetails = collectionsDict["priceDetails"] as? String{
                    collectionsPrice = priceDetails
                }
                if let collectionsId = collectionsDict["collectionId"] as? String{
                    id = collectionsId
                }
                if let userPicUrl = collectionsDict["userImage"] as? String{
                    userImage = userPicUrl
                }
                if let name = collectionsDict["name"] as? String{
                    collectionsTempName = name
                }
                if let userIdentity = collectionsDict["userId"] as? String{
                    userIda = userIdentity
                }

                if let image = collectionsDict["image"] as? String{
                    self.collectionsImageUrl = image
                }
                else if let imageArray = collectionsDict["image"] as? [Any]
                {
                    if !imageArray.isEmpty{
                        if let imageObj = imageArray[0] as? [String:String]{
                            if imageObj["type"] == "0"{
                                self.collectionsImageUrl = imageObj["imageUrl"]!
                            }
                            else{
                                self.collectionsImageUrl = imageObj["thumbnailUrl"]!
                            }
                        }
                    }
                }else if let imageDict = collectionsDict["image"] as? [String : String]
                {
                    if let type = imageDict["type"]{
                        if type == "0"{
                            if let image = imageDict["imageUrl"]{
                                self.collectionsImageUrl = image
                                isVideo = false
                            }
                        }else{
                            if let image = imageDict["thumbnailUrl"]{
                                self.collectionsImageUrl = image
                                isVideo = true
                            }
                        }
                    }
                }
                if let postIdOfPost = collectionsDict["postId"] as? String{
                    postId = postIdOfPost
                }else if let postIdOfPost = collectionsDict["_id"] as? String{
                    postId = postIdOfPost
                }
                if let coverPostId = collectionsDict["coverPost"] as? String{
                    coverPost = coverPostId
                }
                
                if (collectionsDict["userName"] != nil)
                {
                    collectionsObject = CollectionsData(collectionsPrice: collectionsPrice, collectionsID: id, collectionsName: collectionsTempName, collectionsImageUrl: collectionsImageUrl!, userImage : userImage, userId: userIda, ownerName: collectionsDict["userName"]!, postId : postId, coverPost : coverPost)
                }
                else
                {
                    collectionsObject = CollectionsData(collectionsPrice: collectionsPrice, collectionsID: id, collectionsName: collectionsTempName, collectionsImageUrl: collectionsImageUrl!, userImage : userImage, userId: userIda, ownerName: [], postId : postId, coverPost : coverPost)
                }
                collectionsDataArray.append(collectionsObject)
            }
        }
        return collectionsDataArray
    }
    
    func setSelectedCollectionData(collectionData : CollectionsData) ->CollectionsData{
        if collectionData.isSelected{
            collectionData.isSelected = false
        }else{
            collectionData.isSelected = true
        }
        return collectionData
    }
}

