//
//  CreateFromSavedViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 26/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class CreateFromSavedViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var savedCollectionsCollectionView: UICollectionView!
    
    var collectionName : String!
    var coverPost : String?
    let collectionDataObj = CollectionsData.init(collectionsPrice: "", collectionsID: "", collectionsName: "", collectionsImageUrl: "", userImage: "", userId: "", ownerName: "", postId: "", coverPost: "")
    var collectionsDataArray = [CollectionsData]()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getCollectionLists()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Buttons Action
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func doneAction(_ sender: Any) {
        self.createCollection()
    }
    
    //MARK:- Web Services
    func getCollectionLists() {
        Helper.showPI()
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject]
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getWishList,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    switch errFlag {
                                        
                                    case 0:
                                        if let responseArray = (response["response"]["result"]).arrayObject
                                        {
                                           self.collectionsDataArray = self.collectionDataObj.fetchDataFromAPIForCollections(collectionData: responseArray)
                                        }
                                        self.savedCollectionsCollectionView.reloadData()
                                        break
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Error", message: errMsg!)
                                        
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    Helper.hidePI()
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
    
    func createCollection() {
        
        let postIds : [String] = self.getPostIds()
        if postIds.count <= 0{
            return
        }
        let image = self.getImage()
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject,
                      "collectionId" : "" as AnyObject,
                      "postIds" : postIds as AnyObject,
                      "name" : self.collectionName as AnyObject,
                      "image" : image as AnyObject,
                      "coverPost": self.coverPost as AnyObject]
        Helper.showPI()
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.addPostsToCollection,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    switch errFlag {
                                        
                                    case 0:
                                        self.goToCollectionVC()
                                        break
                                        
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Error", message: errMsg!)
                                        
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    Helper.hidePI()
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
    
    func goToCollectionVC(){
        if var viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            viewControllers = viewControllers.reversed()
            for currentViewController in viewControllers {
                if currentViewController .isKind(of: CollectionsViewController.self) {
                    self.navigationController?.popToViewController(currentViewController, animated: true)
                    break
                }
            }
        }
    }
    
    func getPostIds() ->[String]{
        var postIds : [String] = []
        for data in self.collectionsDataArray{
            let collectionData = data as CollectionsData
            if collectionData.isSelected{
                postIds.append(collectionData.postId!)
            }
        }
        return postIds
    }
    
    func getImage() ->String{
        var image : String = ""
        for data in self.collectionsDataArray{
            let collectionData = data as CollectionsData
            if collectionData.isSelected{
                image = collectionData.collectionsImageUrl!
                self.coverPost = collectionData.postId
                return image
            }
        }
        return image
    }
    
}

extension CreateFromSavedViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectionsDataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "savedCollectionCell", for: indexPath) as! SelecteCollectionsCollectionViewCell
        cell.setData(collectionData: self.collectionsDataArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var collectionData = self.collectionsDataArray[indexPath.row]
        collectionData = self.collectionDataObj.setSelectedCollectionData(collectionData: collectionData)
        self.collectionsDataArray[indexPath.row] = collectionData
        self.savedCollectionsCollectionView.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize.init(width: self.view.frame.size.width / 3 - 1, height: self.view.frame.size.width / 3 - 1)
        let wid = self.view.frame.size.width/2-20
        let higt = wid * 0.678
        return CGSize(width: wid, height: higt)
    }
    
}
