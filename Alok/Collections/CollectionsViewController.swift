//
//  CollectionsViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 22/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class CollectionsViewController: UIViewController {

    enum TableViewType : String {
        case AllTableView
        case CollectionsTableView
    }
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var allTableView: UITableView!
    @IBOutlet weak var collectionsCollectionView: UICollectionView!
    @IBOutlet weak var viewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var allButtonOutlet: UIButton!
    @IBOutlet weak var collectionsButtonOutlet: UIButton!
    
    let horseDataObj = HorseData(postPrice: "", postID: "", postName: "", postImageUrl: "", userImage: "", userId: "", ownerName: "")
    let collectionDataObj = CollectionsData.init(collectionsPrice: "", collectionsID: "", collectionsName: "", collectionsImageUrl: "", userImage: "", userId: "", ownerName: "", postId: "", coverPost : "")
    var horseDataArray = [HorseData]()
    var collectionsDataArray = [CollectionsData]()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.allButtonOutlet.isSelected = true
        self.collectionsButtonOutlet.isSelected = false
        self.allTableView.estimatedRowHeight = 270
        self.allTableView.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getCollectionLists()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //MARK:- Buttons Action
    
    @IBAction func allAction(_ sender: Any) {
        self.changeScrollViewFrame(.AllTableView)
    }
    
    @IBAction func collectionsAction(_ sender: Any) {
        self.changeScrollViewFrame(.CollectionsTableView)
    }
    
    @IBAction func addNewCollectionsAction(_ sender: Any) {
        // push view controller but animate modally
        let storyBoard: UIStoryboard = UIStoryboard(name: "Collections", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "newCollectionController") as! CreateNameForSavedViewController
        let navigationController = self.navigationController
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromTop
        navigationController?.view.layer.add(transition, forKey: nil)
        navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func backButtonAction(_ sender: Any)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    

    /// TO change Scroll view frame on button selection
    ///
    /// - Parameter index: Button selected 0 for All and 1 For Collections
    func changeScrollViewFrame(_ tableViewType: TableViewType){
        var frame = self.mainScrollView.bounds
        switch tableViewType {
        case .AllTableView:
            frame.origin.x = 0.0
            break
        case .CollectionsTableView:
            frame.origin.x = self.view.frame.size.width
            break
        }
        self.mainScrollView.scrollRectToVisible(frame, animated: true)
    }
    
    
    //MARK:- Web Service
//    func getWishLists() {
//        Helper.showPI()
//        let params = ["userId":Utility.getUserID() as AnyObject,
//                      "token" :Utility.getSessionToken() as AnyObject]
//        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getWishList,
//                                 params: params,
//                                 success: { (response) in
//
//                                    Helper.hidePI()
//                                    let errFlag = response["errCode"].numberValue
//                                    switch errFlag {
//
//                                    case 0:
//                                        if let responseArray = (response["response"]["result"]).arrayObject
//                                        {
//                                            self.horseDataArray = self.horseDataObj.fetchDataFromAPIForPost(postData: responseArray)
//                                        }
//                                        self.allTableView.reloadData()
//                                        break
//
//                                    default:
//
//                                        let errMsg = response["Message"].string
//                                        self.showAlert("Error", message: errMsg!)
//
//                                        break
//                                    }
//        },
//                                 failure: { (Error) in
//                                    Helper.hidePI()
//                                    self.showAlert("Error", message: Error.localizedDescription)
//        })
//    }
    
    func getCollectionLists() {
        Helper.showPI()
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject]
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getCollections,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    switch errFlag {
                                        
                                    case 0:
                                        if let responseDict = (response["response"]).dictionaryObject
                                        {
                                            if let data = responseDict["CollectionDetails"] as? [Any]{
                                                self.collectionsDataArray = self.collectionDataObj.fetchDataFromAPIForCollections(collectionData: data)
                                            }
                                            if let data = responseDict["wishListDetails"] as? [Any]{
                                                self.horseDataArray = self.horseDataObj.fetchDataFromAPIForPost(postData: data)
                                            }
                                        }
                                        self.collectionsCollectionView.reloadData()
                                        self.allTableView.reloadData()
                                        break
                                        
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Error", message: errMsg!)
                                        
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    Helper.hidePI()
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
}

extension CollectionsViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WishlistHorseTableViewCell", for: indexPath) as! WishlistHorseTableViewCell
        
        let postData = horseDataArray[indexPath.row]
        cell.horseImageOutlet.kf.indicatorType = .activity
        cell.horseImageOutlet.kf.indicator?.startAnimatingView()
        
        cell.horseImageOutlet.kf.setImage(with: postData.postImageUrl!,
                                          placeholder: UIImage.init(named: "horse_default_image"),
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
        },
                                          completionHandler: { image, error, cacheType, imageURL in
                                            cell.horseImageOutlet.kf.indicator?.stopAnimatingView()
        })
        cell.horseNameOutelt.text = postData.postName!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return horseDataArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        performSegue(withIdentifier: "horse Details Segue", sender: indexPath.row)
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "horeseDetailController") as! HorseDetailViewController
        let horseDataObj = self.horseDataArray[indexPath.row]
        controller.postId = horseDataObj.postID!
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "horse Details Segue"
        {
            let controller = segue.destination as! HorseDetailViewController
            if let rowNum = sender as? Int{
                let horseDataObj = self.horseDataArray[rowNum]
                controller.postId = horseDataObj.postID!
            }
        }else if segue.identifier == "collectionDetailsSegue"{
            let collectionDetailsVC = segue.destination as! CollectionDetailsViewController
            let index = sender as! Int
            let data = self.collectionsDataArray[index]
            collectionDetailsVC.collectionName = data.collectionsName
            collectionDetailsVC.collectionId = data.collectionsID
        }
    }
}

extension CollectionsViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectionsDataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! CollectionsCollectionViewCell
        cell.setData(collectionData: self.collectionsDataArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "collectionDetailsSegue", sender: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize.init(width: self.view.frame.size.width / 2 - 30, height: self.view.frame.size.width / 2 - 30)
        let wid = self.view.frame.size.width/2-20
        let higt = wid * 0.678
        return CGSize(width: wid, height: higt)
    }
}

extension CollectionsViewController : UIScrollViewDelegate{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.mainScrollView{
            let offset = scrollView.contentOffset
            self.viewLeadingConstraint.constant = scrollView.contentOffset.x / 2
            let frame = self.view.frame.size.width / 2
            if offset.x <= frame{
                self.allButtonOutlet.isSelected = true
                self.collectionsButtonOutlet.isSelected = false
            }else{
                self.allButtonOutlet.isSelected = false
                self.collectionsButtonOutlet.isSelected = true
            }
            scrollView.contentOffset = offset;
        }
    }
}

