//
//  CreateNameForSavedViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 27/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class CreateNameForSavedViewController: UIViewController {

    @IBOutlet weak var collectionNameTextField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nextButton.isEnabled = false
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.collectionNameTextField.clearButtonMode = UITextFieldViewMode.whileEditing
        let deadlineTime = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            self.collectionNameTextField.becomeFirstResponder()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Buttons Action
    @IBAction func cancelAction(_ sender: Any) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionReveal
        transition.subtype = kCATransitionFromBottom
        navigationController?.view.layer.add(transition, forKey: nil)
        _ = navigationController?.popViewController(animated: false)
    }
    
    @IBAction func nextAction(_ sender: Any) {
         self.performSegue(withIdentifier: "toAddSavedCollectionSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddSavedCollectionSegue"{
            let createFromSavedVC = segue.destination as! CreateFromSavedViewController
            createFromSavedVC.collectionName = self.collectionNameTextField.text
        }
    }
    
}

extension CreateNameForSavedViewController : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.count == 1 && string == ""{
            self.nextButton.isEnabled = false
        }else if string.count >= 1{
            self.nextButton.isEnabled = true
        }
        return true
    }
}
