//
//  ChangeCoverViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 28/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

protocol ChangeCoverViewControllerDelegate: class {
    func selectedPost(collectionDetail : CollectionDetail)
}

class ChangeCoverViewController: UIViewController {

    @IBOutlet weak var collectionImageCollectionView: UICollectionView!
    
    var collectionName : String!
    var collectionId: String!
    
    var collectionDataObj = CollectionDetailsModel()
    weak var delegate: ChangeCoverViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getCollectionDetails()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MAKR:- Button Action
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Web Services
    func getCollectionDetails(){
        Helper.showPI()
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject,
                      "collectionId" : self.collectionId as AnyObject]
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getCollectionDetails,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    switch errFlag {
                                        
                                    case 0:
                                        if let responseDict = (response["responce"]).dictionaryObject
                                        {
                                            self.collectionDataObj.setDataInModel(collectionData: responseDict)
                                        }
                                        self.collectionImageCollectionView.reloadData()
                                        break
                                        
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Error", message: errMsg!)
                                        
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    Helper.hidePI()
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
    
}

extension ChangeCoverViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectionDataObj.postDetails.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionDetailsCell", for: indexPath) as! CollectionDetailsCollectionViewCell
        cell.setData(collectionData: self.collectionDataObj.postDetails[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.selectedPost(collectionDetail: self.collectionDataObj.postDetails[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize.init(width: self.view.frame.size.width / 3 - 1, height: self.view.frame.size.width / 3 - 1)
        let wid = self.view.frame.size.width/2-20
        let higt = wid * 0.678
        return CGSize(width: wid, height: higt)
    }
}
