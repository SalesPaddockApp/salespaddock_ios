//
//  AddToCollectionViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 28/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

protocol AddToCollectionViewControllerDelegate: class {
    func addedPostInCollection(collectionsDetail : [CollectionDetail])
}

class AddToCollectionViewController: UIViewController {

    @IBOutlet weak var savedPostCollectionView: UICollectionView!
    
    var collectionId : String?
    var collectionName: String?
    var collectionImage : String?
    var collectionImagePostId : String?
    let collectionDataObj = CollectionsData.init(collectionsPrice: "", collectionsID: "", collectionsName: "", collectionsImageUrl: "", userImage: "", userId: "", ownerName: "", postId: "", coverPost: "")
    var collectionsDataArray = [CollectionsData]()
    let collectionDetailObj = CollectionDetail(postId: "", collectionName: "", collectionImage: "", isVideo: false)
    weak var delegate : AddToCollectionViewControllerDelegate?
    
    //MARK:-View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getPostwithOutcollecction()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:-Buttons Action
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneAction(_ sender: Any) {
        self.addPostToCollection()
    }
    
    
    //MARK:- Web Services
    func getPostwithOutcollecction() {
        Helper.showPI()
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject,
                      "collectionId" : self.collectionId as AnyObject]
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getPostwithOutcollecction,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
//                                    print(response)
                                    switch errFlag {
                                    case 0:
                                        if let responseArray = (response["responce"]).arrayObject
                                        {
                                            self.collectionsDataArray = self.collectionDataObj.fetchDataFromAPIForCollections(collectionData: responseArray)
                                        }
                                        self.savedPostCollectionView.reloadData()
                                        break
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Error", message: errMsg!)
                                        
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    Helper.hidePI()
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
    
    func addPostToCollection() {
       
        let postIds = self.getPostIds()
        if postIds.count <= 0{
            self.dismiss(animated: true, completion: nil)
            return
        }
        if self.collectionImage == ""{
            self.getCollectionImage()
        }
        Helper.showPI()
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject,
                      "collectionId" : self.collectionId as AnyObject,
                      "postIds" : postIds as AnyObject,
                      "name" : self.collectionName as AnyObject,
                      "image" : self.collectionImage as AnyObject,
                      "coverPost": self.collectionImagePostId as AnyObject]
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.addPostsToCollection,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    switch errFlag {
                                        
                                    case 0:
                                        self.delegate?.addedPostInCollection(collectionsDetail: self.collectionDetailObj.createCollectionDetailModelArrayFrom(collectionDataArray: self.collectionsDataArray))
                                        self.dismiss(animated: true, completion: nil)
                                        break
                                        
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Error", message: errMsg!)
                                        
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    Helper.hidePI()
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
    
    func getPostIds() ->[String]{
        var postIds : [String] = []
        for data in self.collectionsDataArray{
            let collectionData = data as CollectionsData
            if collectionData.isSelected{
                postIds.append(collectionData.postId!)
            }
        }
        return postIds
    }
    
    func getCollectionImage(){
        for data in self.collectionsDataArray{
            let collectionData = data as CollectionsData
            if collectionData.isSelected{
                self.collectionImage = collectionData.collectionsImageUrl
                self.collectionImagePostId = collectionData.postId
                break;
            }
        }
    }
    
}

extension AddToCollectionViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectionsDataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "savedCollectionCell", for: indexPath) as! SelecteCollectionsCollectionViewCell
        cell.setData(collectionData: self.collectionsDataArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var collectionData = self.collectionsDataArray[indexPath.row]
        collectionData = self.collectionDataObj.setSelectedCollectionData(collectionData: collectionData)
        self.collectionsDataArray[indexPath.row] = collectionData
        self.savedPostCollectionView.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize.init(width: self.view.frame.size.width / 3 - 1, height: self.view.frame.size.width / 3 - 1)
        let wid = self.view.frame.size.width/2-20
        let higt = wid * 0.678
        return CGSize(width: wid, height: higt)
    }
    
}
