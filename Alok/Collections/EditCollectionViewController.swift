//
//  EditCollectionViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 28/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

protocol EditCollectionViewControllerDelegate: class {
    func updateCollection(name: String, image: String)
}

class EditCollectionViewController: UIViewController {

    @IBOutlet weak var collectionCoverImage: UIImageView!
    @IBOutlet weak var collectionNameTextField: UITextField!
    
    var CollectionDetailsModelObj = CollectionDetailsModel()
    var changedCollectionDetail = CollectionDetail(postId: "", collectionName: "", collectionImage: "", isVideo: false)
    
    weak var delegate : EditCollectionViewControllerDelegate?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionNameTextField.clearButtonMode = UITextFieldViewMode.whileEditing
        self.setUpView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpView(){
        self.collectionNameTextField.text = CollectionDetailsModelObj.collectionName
        self.collectionCoverImage.kf.setImage(with: URL(string:CollectionDetailsModelObj.CollectionCoverImage!),
                                              placeholder: #imageLiteral(resourceName: "horse_default_image"),
                                              options: [.transition(ImageTransition.fade(1))],
                                              progressBlock: { receivedSize, totalSize in
        },
                                              completionHandler: { image, error, cacheType, imageURL in
                                                self.collectionCoverImage.kf.indicator?.stopAnimatingView()
        })
    }
    
    //MARK:- Buttons Action
    
    @IBAction func backAction(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
       self.dismissViewConreoller()
    }
    
    func dismissViewConreoller(){
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionReveal
        transition.subtype = kCATransitionFromBottom
        navigationController?.view.layer.add(transition, forKey: nil)
        _ = navigationController?.popViewController(animated: false)
    }
    
    @IBAction func DoneAction(_ sender: Any) {
        if self.checkCollectionUpdate(){
            self.updateCollection()
        }else{
            self.dismissViewConreoller()
        }
    }
    
    @IBAction func changeCover(_ sender: Any) {
        self.performSegue(withIdentifier: "changeCoverSegue", sender: nil)
    }
    
    @IBAction func deleteCollectionAction(_ sender: Any) {
        let alertController = UIAlertController.init(title: "Delete collection?", message: "When you delete this collection the photo and videos will still be saved.", preferredStyle: .alert)
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .default, handler: nil)
        let deleteAction = UIAlertAction.init(title: "Delete", style: .default) { (action) in
            self.deleteCollection()
        }
        alertController.addAction(cancelAction)
        alertController.addAction(deleteAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func checkCollectionUpdate() ->Bool{
        if self.collectionNameTextField.text == self.CollectionDetailsModelObj.collectionName && self.changedCollectionDetail.collectionImage == ""{
            return false
        }else{
            return true
        }
    }
    
    //MARK:- Navigate
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "changeCoverSegue"{
            let changeCoverVC = segue.destination as! ChangeCoverViewController
            changeCoverVC.collectionId = self.CollectionDetailsModelObj.collectionId
            changeCoverVC.delegate = self
        }
    }
    
    //MARK:- Web Services
    func deleteCollection(){
        Helper.showPI()
        let params = ["token" :Utility.getSessionToken() as AnyObject,
                      "collectionId" : self.CollectionDetailsModelObj.collectionId as AnyObject]
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.deleteCollection,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    switch errFlag {
                                        
                                    case 0:
                                       self.goToCollectionVC()
                                        break
                                        
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Error", message: errMsg!)
                                        
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    Helper.hidePI()
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
    
    func goToCollectionVC(){
        if var viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            viewControllers = viewControllers.reversed()
            for currentViewController in viewControllers {
                if currentViewController .isKind(of: CollectionsViewController.self) {
                    self.navigationController?.popToViewController(currentViewController, animated: true)
                    break
                }
            }
        }
    }
    
    func updateCollection(){
        Helper.showPI()
        var coverImage = self.CollectionDetailsModelObj.CollectionCoverImage
        var coverPost = self.CollectionDetailsModelObj.coverPost
        if self.changedCollectionDetail.collectionImage != ""{
            coverImage = self.changedCollectionDetail.collectionImage
            coverPost = self.changedCollectionDetail.postId
        }
        let params = ["token" :Utility.getSessionToken() as AnyObject,
                      "collectionId" : self.CollectionDetailsModelObj.collectionId as AnyObject,
                      "name" : self.collectionNameTextField.text as AnyObject,
                      "image" : coverImage as AnyObject,
                      "coverPost": coverPost as AnyObject]
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.editCollection,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    switch errFlag {
                                    case 0:
                                        self.delegate?.updateCollection(name: self.collectionNameTextField.text!, image: coverImage!)
                                        self.dismissViewConreoller()
                                        break
                                        
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Error", message: errMsg!)
                                        
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    Helper.hidePI()
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
}

extension EditCollectionViewController : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text?.count == 0{
            return true
        }
        textField.resignFirstResponder()
        return true
    }
    
}

extension EditCollectionViewController : ChangeCoverViewControllerDelegate{
    
    func selectedPost(collectionDetail: CollectionDetail) {
        self.changedCollectionDetail = collectionDetail
        self.collectionCoverImage.kf.setImage(with: URL(string:collectionDetail.collectionImage!),
                                              placeholder: #imageLiteral(resourceName: "horse_default_image"),
                                              options: [.transition(ImageTransition.fade(1))],
                                              progressBlock: { receivedSize, totalSize in
        },
                                              completionHandler: { image, error, cacheType, imageURL in
                                                self.collectionCoverImage.kf.indicator?.stopAnimatingView()
        })
    }
}
