//
//  CreateCollectionViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 26/02/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

protocol CreateCollectionViewControllerDelegate: class {
    func addedPost(collectionData : CollectionsData)
}


class CreateCollectionViewController: UIViewController {
    @IBOutlet weak var backButtonOutlet: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addNewCollectionOutlet: UIButton!
    @IBOutlet weak var newCollectionView: UIView!
    @IBOutlet weak var newCollectionImage: UIImageView!
    @IBOutlet weak var newCollectionName: UITextField!
    @IBOutlet weak var allCollectionsView: UIView!
    @IBOutlet weak var collectionsCollectionView: UICollectionView!
    @IBOutlet weak var cancelButtonOutlet: UIButton!
    @IBOutlet weak var doneButtonOutlet: UIButton!
    
    var isAllCollectios : Bool = true
    var collectionCoverImage : String!
    var postId : String!
    let collectionDataObj = CollectionsData.init(collectionsPrice: "", collectionsID: "", collectionsName: "", collectionsImageUrl: "", userImage: "", userId: "", ownerName: "", postId: "", coverPost: "")
    var horseDataArray = [HorseData]()
    var collectionsDataArray = [CollectionsData]()
    weak var delegate: CreateCollectionViewControllerDelegate?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getCollectionLists()
        NotificationCenter.default.addObserver(self, selector: #selector(CreateCollectionViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CreateCollectionViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.backButtonOutlet.isHidden = true
        self.newCollectionView.isHidden = true
        self.doneButtonOutlet.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.newCollectionImage.kf.setImage(with: NSURL.init(string: self.collectionCoverImage)! as URL,
                                            placeholder: UIImage.init(named: "horse_default_image"),
                                            options: [.transition(ImageTransition.fade(1))],
                                            progressBlock: { receivedSize, totalSize in
        },
                                            completionHandler: { image, error, cacheType, imageURL in
                                                self.newCollectionImage.kf.indicator?.stopAnimatingView()
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func changeViewToCreateCollections(){
        self.addNewCollectionOutlet.isHidden = true
        if self.collectionsDataArray.count > 0{
            self.backButtonOutlet.isHidden = false
        }
        self.allCollectionsView.isHidden = true
        self.newCollectionView.isHidden = false
        self.titleLabel.text = "New Collection"
        self.newCollectionName.becomeFirstResponder()
        
    }
    
    func changeViewToAllCollectionsView(){
        self.addNewCollectionOutlet.isHidden = false
        self.backButtonOutlet.isHidden = true
        self.allCollectionsView.isHidden = false
        self.newCollectionView.isHidden = true
        self.titleLabel.text = "Save to"
        self.newCollectionName.resignFirstResponder()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    //MARK:- Buttons Action
    @IBAction func dismissControllerAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil    )
    }
    
    @IBAction func doneAction(_ sender: Any) {
        //        print("Done")
        let collectionData = CollectionsData(collectionsPrice: "", collectionsID: "", collectionsName: self.newCollectionName.text!, collectionsImageUrl: self.collectionCoverImage, userImage: "", userId: "", ownerName: "", postId: "", coverPost: "")
        self.addPostInCollection(collectionData: collectionData)
    }
    
    @IBAction func addAction(_ sender: Any) {
        self.changeViewToCreateCollections()
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.changeViewToAllCollectionsView()
    }
    
    //MARK:- Web Service
    func getCollectionLists() {
        Helper.showPI()
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject]
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getCollections,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    switch errFlag {
                                        
                                    case 0:
                                        if let responseDict = (response["response"]).dictionaryObject
                                        {
                                            if let data = responseDict["CollectionDetails"] as? [Any]{
                                                self.collectionsDataArray = self.collectionDataObj.fetchDataFromAPIForCollections(collectionData: data)
                                            }
                                        }
                                        if self.collectionsDataArray.count == 0{
                                            self.changeViewToCreateCollections()
                                        }else{
                                            self.collectionsCollectionView.reloadData()
                                        }
                                        break
                                        
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Error", message: errMsg!)
                                        
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    Helper.hidePI()
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
    
    //    func createNewCollection(){
    //        Helper.showPI()
    //        let params = ["userId":Utility.getUserID() as AnyObject,
    //                      "token" :Utility.getSessionToken() as AnyObject,
    //                      "name" :self.newCollectionName.text as AnyObject,
    //                      "image" :self.collectionCoverImage as AnyObject,
    //                      "postId" : self.postId as AnyObject]
    //        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.createCollection,
    //                                 params: params,
    //                                 success: { (response) in
    //
    //                                    Helper.hidePI()
    //                                    let errFlag = response["errCode"].numberValue
    //                                    switch errFlag {
    //
    //                                    case 0:
    //                                        print(response)
    //                                        self.createCollectionSuccessful()
    //                                        self.dismiss(animated: true, completion: nil)
    //                                        break
    //
    //                                    default:
    //
    //                                        let errMsg = response["Message"].string
    //                                        self.showAlert("Error", message: errMsg!)
    //
    //                                        break
    //                                    }
    //        },
    //                                 failure: { (Error) in
    //                                    Helper.hidePI()
    //                                    self.showAlert("Error", message: Error.localizedDescription)
    //        })
    //    }
    //
    //    func createCollectionSuccessful(){
    //        let collectionData = CollectionsData(collectionsPrice: "", collectionsID: "", collectionsName: self.newCollectionName.text!, collectionsImageUrl: self.collectionCoverImage, userImage: "", userId: "", ownerName: "", postId: self.postId)
    //        self.delegate?.addedPost(collectionData: collectionData)
    //    }
    
    func addPostInCollection(collectionData: CollectionsData){
        Helper.showPI()
        var collectionCoverImg = ""
        var coverPost = ""
        if collectionData.collectionsImageUrl != nil && collectionData.collectionsImageUrl! != ""{
            collectionCoverImg = collectionData.collectionsImageUrl!
        }else{
            collectionCoverImg = self.collectionCoverImage
        }
        if collectionData.coverPost != nil && collectionData.coverPost != ""{
            coverPost = collectionData.coverPost!
        }else{
            coverPost = self.postId
        }
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject,
                      "name" :collectionData.collectionsName as AnyObject,
                      "image" :collectionCoverImg as AnyObject,
                      "collectionId" : collectionData.collectionsID as AnyObject,
                      "postIds" : [self.postId] as AnyObject,
                      "coverPost": coverPost as AnyObject]
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.addPostsToCollection,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    switch errFlag {
                                        
                                    case 0:
                                        //                                        print(response)
                                        if let responseDict = response.dictionaryObject{
                                            if let collId = responseDict["CollectionId"] as? String{
                                                collectionData.collectionsID = collId
                                            }
                                        }
                                        collectionData.collectionsImageUrl = collectionCoverImg
                                        self.delegate?.addedPost(collectionData: collectionData)
                                        self.dismiss(animated: true, completion: nil)
                                        break
                                        
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Error", message: errMsg!)
                                        
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    Helper.hidePI()
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
}

extension CreateCollectionViewController : UICollectionViewDataSource, UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectionsDataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "allCollectionsCell", for: indexPath) as! AllCollectionsCollectionViewCell
        cell.setData(collectionData: self.collectionsDataArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.addPostInCollection(collectionData: self.collectionsDataArray[indexPath.row])
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}

extension CreateCollectionViewController : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.count == 1 && string == ""{
            self.cancelButtonOutlet.isHidden = false
            self.doneButtonOutlet.isHidden = true
        }else if string.count >= 1{
            self.cancelButtonOutlet.isHidden = true
            self.doneButtonOutlet.isHidden = false
        }
        if textField.text?.count == 42 && string != ""{
            self.makeShakeEffect()
            return false
        }
        return true
    }
    
    func makeShakeEffect(){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.newCollectionName.center.x - 10, y: self.newCollectionName.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.newCollectionName.center.x + 10, y: self.newCollectionName.center.y))
        self.newCollectionName.layer.add(animation, forKey: "position")
    }
}
