//
//  ChatViewControllerCell.swift
//  TabBarController
//
//  Created by Rahul Sharma on 29/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher

class ChatViewControllerCell: UITableViewCell {

    
    @IBOutlet weak var groupOrUserNameLabel: UILabel!
    @IBOutlet weak var lastMessagelable: UILabel!
    @IBOutlet weak var lastMessageTimeLable: UILabel!
    @IBOutlet weak var unreadMessageCountView: UIView!
    @IBOutlet weak var unreadMessageCountLabel: UILabel!
    @IBOutlet weak var groupOrUserImage: UIImageView!
    @IBOutlet weak var horseNameLabel: UILabel!
    @IBOutlet weak var chatInitialTimelable: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.unreadMessageCountView.layer.cornerRadius = self.unreadMessageCountView.frame.size.width / 2
        self.unreadMessageCountView.clipsToBounds = true
        self.unreadMessageCountView.bringSubview(toFront: unreadMessageCountLabel)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadCell(cellData: [String : AnyObject]){
        
//        groupOrUserNameLabel.text = "\(cellData["name"] as! String) (\(cellData["type"] as! String))"
        groupOrUserNameLabel.text = cellData["name"] as? String
        lastMessagelable.text = cellData["lastMessage"] as? String
        lastMessageTimeLable.text = self.toGetCellLastMessageTime(timeString: (cellData["time"] as? String)!)
        chatInitialTimelable.text = cellData["createdOn"] as? String
        horseNameLabel.text = cellData["postName"] as? String
        if(cellData["unread"] as! Int == 0){
            self.unreadMessageCountView.isHidden = true
        }
        else{
            self.unreadMessageCountView.isHidden = false
            self.unreadMessageCountLabel.text = "\(cellData["unread"] as! Int)"
        }
        if(cellData["image"] != nil){
            let url = URL(string: cellData["image"] as! String)
            
            self.groupOrUserImage.kf.indicatorType = .activity
            self.groupOrUserImage.kf.indicator?.startAnimatingView()
            groupOrUserImage.kf.setImage(with: url,
                                              placeholder: UIImage.init(named: "profile_defalt_image"),
                                              options: [.transition(ImageTransition.fade(1))],
                                              progressBlock: { receivedSize, totalSize in
//                                                print("\(receivedSize)/\(totalSize)")
            },
                                              completionHandler: { image, error, cacheType, imageURL in
                                                //print(" Finished")
                                                self.groupOrUserImage.kf.indicator?.stopAnimatingView()
            })
            
        }else{
            groupOrUserImage.image = UIImage(named:"chats_image_frame")
        }
        
        
        
        
        
    }
    
    func toGetCellLastMessageTime(timeString: String)->String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.init(abbreviation: "UTC") as TimeZone!
        let date = dateFormatter.date(from: timeString)
        dateFormatter.dateFormat = "EEE, MMM d, yyyy - h:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        
        let today = NSCalendar.current.isDateInToday(date!)
        if(today){
            
            dateFormatter.dateFormat = "hh:mm a"
            return dateFormatter.string(from: date!)
        }
        else if(NSCalendar.current.isDateInYesterday(date!)){
            
            return "YESTERDAY"
        }
        else{
            
            dateFormatter.dateStyle = .short
            let dateString = dateFormatter.string(from: date!)
            return dateString
            
        }
    }
    

}
