//
//  SettingViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 06/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import SocketIO
import Kingfisher

class SettingViewController: UIViewController {
    
    @IBOutlet weak var settingTableview: UITableView!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var lastSeenlabel: UILabel!
    var document : CBLDocument!
    var mediaInfo = [[String : AnyObject]]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.userProfileImage.layer.cornerRadius = self.userProfileImage.frame.size.width / 2
        self.userProfileImage.clipsToBounds = true
        self.settingTableview.rowHeight = UITableViewAutomaticDimension
        self.settingTableview.estimatedRowHeight = 120
        settingTableview.tableFooterView = UIView()
        self.getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.white()
//        self.getData()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = false
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getData(){

        let data = CouchbaseEvevnts.retriveData(document: self.document)
//        if(data["image"] == nil){
//            self.userProfileImage.image = UIImage(named:"chats_image_frame")
//        }else{
//            self.userProfileImage.image = UIImage(named: data["image"] as! String)
//        }
//        self.userProfileImage.image
        let url = URL(string: data["image"] as! String)
        
        self.userProfileImage.kf.indicatorType = .activity
        self.userProfileImage.kf.indicator?.startAnimatingView()
        
        self.userProfileImage.kf.setImage(with: url,
                                     placeholder: UIImage.init(named: "profile_defalt_image"),
                                     options: [.transition(ImageTransition.fade(1))],
                                     progressBlock: { receivedSize, totalSize in
                                        // print("\(receivedSize)/\(totalSize)")
        },
                                     completionHandler: { image, error, cacheType, imageURL in
                                        //print(" Finished")
                                        self.userProfileImage.kf.indicator?.stopAnimatingView()
        })

        self.userNameLabel.text = data["name"] as? String
        self.lastSeenlabel.text = "Last seen at \(self.getTime(time: data["time"] as! String))"
        
        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
            // do some task
            if data["message"] == nil{
                return
            }
            self.getMessageDataForAllMedia(message: data["message"] as! [[String : String]])
//            DispatchQueue.main.async(execute: {() -> Void in
//                // update some UI
//            })
        })
    }
    
    func getTime(time: String)->String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.init(abbreviation: "UTC") as TimeZone!
        let date = dateFormatter.date(from: time)
        dateFormatter.dateFormat = "MMM d, yyyy - h:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        let stringDate = dateFormatter.string(from: date!)
        return stringDate
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let vc = segue.destination as! AllMediaViewController
        vc.document = self.document
        vc.mediaInfo = self.mediaInfo
    }
    
    func getMessageDataForAllMedia(message : [[String : String]]){
        
        
        for i in 0..<message.count{
            switch message[i]["type"]! {
            case "1":
                var imageInfo = [String : AnyObject]()
//                imageInfo["image"] = FolderClass.getImage(path: message[i]["messageId"]! )
                imageInfo["messageId"] = message[i]["messageId"]! as AnyObject?
                imageInfo["type"] = "1" as AnyObject
                imageInfo["date"] = getCategorizedDate(date: message[i]["date"]! as String) as AnyObject
                mediaInfo.append(imageInfo)
                break
            case "2":
                var videoInfo = [String : AnyObject]()
                videoInfo["videoURL"] = message[i]["videoURL"] as AnyObject
                videoInfo["type"] = "2" as AnyObject
                videoInfo["date"] = getCategorizedDate(date: message[i]["date"]! as String) as AnyObject
                mediaInfo.append(videoInfo)
                break
                
            default: break
            }
        
        }
    }

    
    func getCategorizedDate(date : String)->String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.init(abbreviation: "UTC") as TimeZone!
        let mediaDate = dateFormatter.date(from: date)
//        let components = Calendar.current.dateComponents([.day], from: mediaDate!, to: Date())

//        let days = components.day
//        if days == 0{
//            return "Today"
//        }
//        else if days == 1{
//            return "Yesterday"
//        }
//        else{
//            dateFormatter.dateFormat = "MMMM"
//            
//            return dateFormatter.string(from: mediaDate!)
//        }
        let calender = Calendar.current
        if calender.isDateInToday(mediaDate!){
            return  "Today"
        }
        else if calender.isDateInYesterday(mediaDate!){
            return "Yesterday"
        }
        else{
            dateFormatter.dateFormat = "MMMM"
            return dateFormatter.string(from: mediaDate!)
        }
    
    }
    
}


extension SettingViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingCellTwo") as! SettingViewControllerCellTwo
            cell.headingLabel.text = "Status"
            cell.descriptionLabel.text = "This is your first status..."
            return cell
        }
        else if(indexPath.row == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingCellTwo") as! SettingViewControllerCellTwo
            cell.headingLabel.text = "Mobile"
            cell.descriptionLabel.text = "Under Construction"
            return cell
            
        }
        else if(indexPath.row == 2){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingCellOne") as! SettingViewControllerCellOne
//            cell.singelLabel.textColor = UIColor.init(red: 23, green: 179, blue: 156, alpha: 1)
            cell.singelLabel.textColor = UIColor.black
            cell.singelLabel.text = "View All Media"
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            return cell
        }
        else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingCellOne") as! SettingViewControllerCellOne
            cell.singelLabel.text = "Email chat"
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.row == 2){
            
            performSegue(withIdentifier: "toOpenAllMedia", sender: nil)
            
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
