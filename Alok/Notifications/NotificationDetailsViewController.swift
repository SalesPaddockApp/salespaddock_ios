//
//  NotificationDetailsViewController.swift
//  Sales Paddock
//
//  Created by Govind on 1/13/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class NotificationDetailsViewController: UIViewController {

    var notificationMsg = String()
    @IBOutlet var notificationMsgOutlet: UILabel!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Details"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.white()
        notificationMsgOutlet.text = notificationMsg
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.layoutIfNeeded()
    }
}
