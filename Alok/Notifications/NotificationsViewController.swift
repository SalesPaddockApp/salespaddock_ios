//
//  NotificationsViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 23/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class NotificationsViewController: UIViewController {
    
    var notificationsData = [[String : AnyObject]]()
    @IBOutlet weak var notificationtableView: UITableView!
    @IBOutlet weak var noNotificaionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notificationtableView.tableFooterView = UIView()
        notificationtableView.estimatedRowHeight = 80
        notificationtableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func resetTabBarCount(){
        UserDefaults.standard.set(0, forKey: "Unread Notification Count")
        UserDefaults.standard.synchronize()
        if let currentTabBar = self.tabBarController as? RAMAnimatedTabBarController
        {
            currentTabBar.setNotificationCountForUnreadMessages()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getMyNotificationService()
        self.resetTabBarCount()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    func getMyNotificationService(){
        
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject]
        Helper.showPI()
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getNotification,
                                 params: params,
                                 success: { (response) in
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                        
                                    case 0:
                                        
                                        self.notificationsData = response["response"].arrayObject as! [[String : AnyObject]]
                                        self.notificationtableView.reloadData()
                                        break
                                        
                                    default:
                                        
                                        self.notificationtableView.isHidden = true
                                        self.view.bringSubview(toFront: self.noNotificaionLabel)
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    
        })
        
    }
}

//MARK: Table view delegate and datasource
extension NotificationsViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return notificationsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationsCell") as! NotificationTableViewCell
        cell.horseDetailPressedDelegate = self
        cell.currentRowIndex = indexPath.row
        let num = self.notificationsData[indexPath.row]["isByAdmin"] as! Int
        switch num {
        case 0:
            cell.notificationLabel.text = self.notificationsData[indexPath.row]["message"] as? String
            if let postImage = self.notificationsData[indexPath.row]["userPic"] as? String{
                let postImageUrl = URL(string : postImage)
                
                cell.senderImageOutlet.kf.indicatorType = .activity
                cell.senderImageOutlet.kf.indicator?.startAnimatingView()
                
                cell.senderImageOutlet.kf.setImage(with: postImageUrl, placeholder: #imageLiteral(resourceName: "profile_defalt_image"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil,
                                                   completionHandler: { image, error, cacheType, imageURL in
                    cell.senderImageOutlet.kf.indicator?.stopAnimatingView()
                })
            } else {
                cell.senderImageOutlet.image = #imageLiteral(resourceName: "profile_defalt_image")
            }
            let dict = self.notificationsData[indexPath.row]
            if let dataArray = dict["postImage"] as? [String:AnyObject]
            {
                cell.postButtonOutlet.isEnabled = true
                var imageUrl = ""
                if(dataArray["type"] as! String) == "0"
                {
                    imageUrl = dataArray["imageUrl"] as! String
                }
                else
                {
                    imageUrl = dataArray["thumbnailUrl"] as! String
                }
                
                let url = URL(string:imageUrl)
                cell.postImageOutlet.kf.indicatorType = .activity
                cell.postImageOutlet.kf.indicator?.startAnimatingView()
                cell.postImageOutlet.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "horse_default_image"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil,
                                                 completionHandler: { image, error, cacheType, imageURL in
                    cell.postImageOutlet.kf.indicator?.stopAnimatingView()
                })
            }
            else
            {
                cell.postImageOutlet.image = nil
                cell.postButtonOutlet.isEnabled = false
            }
            return cell
            
        case 1:
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "NotificationMsgTableViewCell") as! NotificationMsgTableViewCell
            if let mainMsg = self.notificationsData[indexPath.row]["mainMessage"] as? String
            {
                cell1.notifcationLabelOutlet.text = mainMsg
            }
            return cell1
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "notification Detail Segue", sender: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "notification Detail Segue"
        {
            let senderRow = sender as! Int
            let controller = segue.destination as! NotificationDetailsViewController
            let dict = self.notificationsData[senderRow]
            controller.notificationMsg = dict["mainMessage"] as! String
        }
        else if segue.identifier == "Horse Detail Segue"{
            let controller = segue.destination as! HorseDetailViewController
            if let rowNum = sender as? Int{
                if let postId = self.notificationsData[rowNum]["postId"] as? String{
                    controller.postId = postId
                }
            }
        }
    }
}


extension NotificationsViewController : horseDetailPressedDelegate{
    
    func openHorseDetails(atRow: Int){
        self.performSegue(withIdentifier: "Horse Detail Segue", sender: atRow)
    }
    
    
}
