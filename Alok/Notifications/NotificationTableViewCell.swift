//
//  NotificationTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 23/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

protocol horseDetailPressedDelegate {
    func openHorseDetails(atRow: Int)
}

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var postButtonOutlet: UIButton!
    @IBOutlet var senderImageOutlet: UIImageView!
    @IBOutlet var postImageOutlet: UIImageView!
    @IBOutlet weak var notificationLabel: UILabel!
    var currentRowIndex :Int = 0
    var horseDetailPressedDelegate:horseDetailPressedDelegate! = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func horseDetailsAction(_ sender: Any)
    {
        if (horseDetailPressedDelegate != nil)
        {
            horseDetailPressedDelegate.openHorseDetails(atRow: currentRowIndex)
        }
    }

}
