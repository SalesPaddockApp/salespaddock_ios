//
//  CreateNewChat.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 12/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import Alamofire
import SwiftyJSON

class CreateNewChat: NSObject {
    
    let database = CBObject.sharedInstance.database
    var document : CBLDocument!
    var isCreated : Bool = false
    var documentIdArray = [CBLDocument]()
    var messageData = [[String : AnyObject]]()
    var postId : String!
    var postName : String!
    var thisPostId : String!
    var isSeller : String!
    var postImageString : String!
    
    func cerateNewChatAndEntryInDatabase(chatDetails : [String : AnyObject])->CBLDocument{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.init(abbreviation: "UTC") as TimeZone!
        let stringDate = dateFormatter.string(from: Date())
        let firstTime = self.checkingExistence(userId: chatDetails["userId"] as! String)
        if firstTime{
            if self.postId != self.thisPostId{
                self.updatePostDetails(document: document, userId: chatDetails["userId"] as! String, postId: self.thisPostId)
            }
            return document
        }else{
            return  CouchbaseEvevnts.createNewDocument(database: self.database!,
                                                       name: chatDetails["name"] as! String,
                                                       date : stringDate,
                                                       image: chatDetails["profilePic"] as! String,
                                                       userId: chatDetails["userId"] as! String,
                                                       postId: self.thisPostId,
                                                       postName: self.postName,
                                                       type: "Buyer",
                                                       postImage: self.postImageString)
            
        }
    }
    
    //
    /// To get all contact from database
    func checkingExistence(userId: String)->Bool{
        
        var result : CBLQueryEnumerator
        var documentIdArray = [CBLDocument]()
        do{
            let database = try CBLManager.sharedInstance().databaseNamed("mydb")
            let query = database.createAllDocumentsQuery()
            query.allDocsMode =  CBLAllDocsMode.allDocs
            query.descending = true
            do{
                result = try query.run()
                documentIdArray.removeAll()
                for i in 0..<result.count{
                    documentIdArray.append(database.document(withID: (result.row(at: i)).documentID!)!)
                }
            }
            catch{
//                print("Not get data")
            }
        }
        catch{
//            print("Not created")
        }
        
        for i in 0..<documentIdArray.count{
            
            let data = CouchbaseEvevnts.retriveData(document: documentIdArray[i])
            if data["to"] as! String == userId{
                self.document = documentIdArray[i]
                self.isCreated = data["isCreated"] as! Bool
                self.isSeller = data["type"] as! String
                self.postId = data["postId"] as! String
                return true
            }
            
        }
        return false
    }
    
    
    func gettingMessagefromServer(sMessage : [String : AnyObject]){
        
        if sMessage["deliver"] != nil {
            return
        }
        if(sMessage["err"]?.int8Value == 1){
            
            //            let alert = UIAlertController.init(title: "Message", message: "Horse has been deleted", preferredStyle: .alert)
            //            let alertAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
            //            alert.addAction(alertAction)
            //            let appDelegate = UIApplication.shared.delegate
            //            appDelegate?.window??.rootViewController?.present(alert, animated: true, completion: nil);
            return
        }
        var test : Bool
        if sMessage["userId"] as! String == Utility.getUserID(){
            test = self.checkingExistence(userId: sMessage["sellerId"] as! String)
            self.isSeller = "Buyer"
        }else{
            test = self.checkingExistence(userId: sMessage["userId"] as! String)
            self.isSeller = "Seller"
        }
        self.messageAckServiceForSocket(messageDict: [sMessage])
        let messageId = sMessage["id"] as! String
        if test{
            
            var message : JSQMessage
            var text : String
            let CBE = CouchbaseEvevnts()
            CBE.updateUnreadMessageCount(document: self.document)
            var messageFrom : String
            if self.isSeller == "Seller"{
                messageFrom = sMessage["sellerId"] as! String
            }else{
                messageFrom = sMessage["userId"] as! String
                
            }
            
            if self.postId != sMessage["postId"] as? String{
                var fromUser = String()
                if self.isSeller == "Seller"{
                    fromUser = sMessage["userId"] as! String
                }else{
                    fromUser = sMessage["sellerId"] as! String
                }
                OperationQueue.main.addOperation {() -> Void in
                    self.updatePostDetails(document: self.document, userId: fromUser, postId: sMessage["postId"] as! String)
                }
            }
            switch sMessage["type"] as! String{
            case "0":
                text = self.base64Decoded(bsae64String:sMessage["payload"] as! String)
                //                if messageFrom == Utility.getUserID(){
                //                    messageFrom = senderId()
                //                }
                message = JSQMessage(senderId: messageFrom, senderDisplayName: "server", date: Date() as Date, text: text, messageId: "\(messageId)")
                if(isCreated == true){
                    OperationQueue.main.addOperation {() -> Void in
                        
                        let vc = CouchbaseEvevnts()
                        vc.updateDocument(document: self.document, messageArray: message, postId: sMessage["postId"] as! String)
                    }
                }
                    
                else{
                    //                    OperationQueue.main.addOperation {() -> Void in
                    let vc = CouchbaseEvevnts()
                    self.isCreated = vc.cerateDocument(document: self.document, receivingUser: "Server", sendingUser: sMessage["from"] as! String, messageArray: message, postId: sMessage["postId"] as! String)
                    //                    }
                }
                
                OperationQueue.main.addOperation {() -> Void in
                    
                    let lastTime = self.getCurrentTimeInString()
                    CouchbaseEvevnts.updateDocumentForLastMessageAndTime(document: self.document, lastMessage: text, time: lastTime)
                    
                }
                if self.isSeller == "Seller"{
                    
                    NotificationCenter.default.post(name:Notification.Name(rawValue:"chatNotification"),
                                                    object: nil,
                                                    userInfo: ["message":message, "from":sMessage["userId"]!,
                                                               "postId":sMessage["postId"]!]
                    )
                }
                else{
                    
                    NotificationCenter.default.post(name:Notification.Name(rawValue:"chatNotification"),
                                                    object: nil,
                                                    userInfo: ["message":message, "from":sMessage["sellerId"]!,
                                                               "postId":sMessage["postId"]!])
                }
                //                }
                break
            case "1":
                
                let dataDecoded:NSData = NSData(base64Encoded: sMessage["payload"] as! String, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)!
                let decodedimage:UIImage = UIImage(data: dataDecoded as Data)!
                let photoItem = JSQPhotoMediaItem(image: decodedimage)
                if messageFrom == Utility.getUserID(){
                    messageFrom = senderId()
                }
                message = JSQMessage(senderId: sMessage["from"] as! String, senderDisplayName: "Server", date: Date() as Date, media: photoItem, messageId: "\(String(describing: sMessage["timestamp"]))")
                
                if(isCreated == true){
                    OperationQueue.main.addOperation {() -> Void in
                        
                        let vc = CouchbaseEvevnts()
                        vc.updateDocument(document: self.document, messageArray: message, postId: sMessage["postId"] as! String)
                    }
                }
                    
                else{
                    //                    OperationQueue.main.addOperation {() -> Void in
                    
                    let vc = CouchbaseEvevnts()
                    self.isCreated = vc.cerateDocument(document: self.document, receivingUser: "Server", sendingUser: sMessage["from"] as! String, messageArray: message, postId: sMessage["postId"] as! String)
                    //                    }
                }
                OperationQueue.main.addOperation {() -> Void in
                    
                    let lastTime = self.getCurrentTimeInString()
                    CouchbaseEvevnts.updateDocumentForLastMessageAndTime(document: self.document, lastMessage: "Image", time: lastTime)
                }
                
                break
            default:
                break
            }
        }
            
        else{
            //            let text = self.base64Decoded(bsae64String:sMessage["payload"] as! String)
            self.postId = sMessage["postId"] as! String
            if isSeller == "Seller"{
                var message = sMessage
                message["sender"] = self.postId as AnyObject?
                self.getProfileDetails(userId: sMessage["userId"] as! String,message: [message], currentPostId: self.postId)
            }else{
                var message = sMessage
                message["sender"] = self.postId as AnyObject?
                self.getProfileDetails(userId: sMessage["sellerId"] as! String,message: [message], currentPostId: self.postId)
            }
        }
    }
    
    func updatePostDetails(document: CBLDocument,userId: String, postId: String){
        
        var params = [String : AnyObject]()
        params["userId"] = userId as AnyObject
        params["token"] = Utility.getSessionToken() as AnyObject
        params["postId"] = postId as AnyObject
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.viewProfileForChat,
                                 params: params,
                                 success: { (response) in
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                        
                                    case 0:
                                        let userInfo = response["data"].dictionaryObject! as [String : AnyObject]
                                        let postName = userInfo["postName"] as! String!
                                        let postImage = (((userInfo["postImage"] as! [String : AnyObject])["options"] as! [[String : AnyObject]])[0] as! [String : String])["imageUrl"]!
                                        OperationQueue.main.addOperation {() -> Void in
                                        let vc = CouchbaseEvevnts()
                                        vc.updatePostDetailsInDB(document: document, postId: postId, postName: postName!, postImage:postImage)
                                        }
                        NotificationCenter.default.post(name:Notification.Name(rawValue:"chatPostNotification"),
                                                                        object: nil,
                                                                        userInfo: ["postName":postName!,
                                                                                   "postImage":postImage,
                                                                                   "postId":postId,
                                                                                    "from":userId]
                                        )
                                        
                                        break
                                        
                                    default:
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    
                                    
        })
        
    }
    
    
    func getofflineMessageService(){
        
        var timestamp : Int
        if UserDefaults.standard.value(forKey: "timeStamp") != nil{
            timestamp = UserDefaults.standard.value(forKey: "timeStamp") as! Int
        }
        else{
            timestamp = 0
        }
        var params = [String : AnyObject]()
        params["userId"] = Utility.getUserID() as AnyObject?
        params["timestamp"] = timestamp as AnyObject?
        timestamp += 1
        UserDefaults.standard.set(timestamp, forKey: "timeStamp")
        
        AFWrapper.requestPOSTURLForChat(serviceName: Constant.ServiceMethodNames.getMessages,
                                        params: params,
                                        success: { (response) in
                                            
                                            if response["data"].count == 0{
                                                return
                                            }
                                            self.updateDataBase(messageDict: response["data"].arrayObject as! [[String : AnyObject]])
                                            self.messageAckService(messageDict: response["data"].arrayObject as! [[String : AnyObject]])
                                            
        },
                                        failure: { (Error) in
                                            //print(Error)
                                            timestamp = 0;
                                            UserDefaults.standard.set(timestamp, forKey: "timeStamp")
        })
        
    }
    
    func updateDataBase(messageDict : [[String : AnyObject]]){
        
        self.getAllContact()
        self.reloadMessageData()
        var messageFrom : String
        for uIndex in 0..<messageDict.count{
            var alreadyInDatabase : Bool = false
            if messageDict[uIndex]["userId"] as! String == Utility.getUserID(){
                self.isSeller = "Buyer"
                messageFrom = messageDict[uIndex]["sellerId"] as! String
            }else{
                messageFrom = messageDict[uIndex]["userId"] as! String
                self.isSeller = "Seller"
            }
            for rIndex in 0..<messageData.count{
                
                if messageFrom == messageData[rIndex]["to"] as! String{
                    for i in 0..<(messageDict[uIndex]["message"] as! [[String : AnyObject]]).count{
                        
                        alreadyInDatabase = true
                        let messageId = (messageDict[uIndex]["message"] as! [[String : AnyObject]])[i]["id"] as! String
                        var message : JSQMessage
                        var text : String
                        let CBE = CouchbaseEvevnts()
                        
                        switch (messageDict[uIndex]["message"] as! [[String : AnyObject]])[i]["type"] as! String{
                        case "0":
                            
                            text = base64Decoded(bsae64String:(messageDict[uIndex]["message"] as! [[String : AnyObject]])[i]["payload"]! as! String)
                            
                            var messageSender = (messageDict[uIndex]["message"] as! [[String : AnyObject]])[i]["sender"]! as! String
                            if messageSender != Utility.getUserID(){
                                CBE.updateUnreadMessageCount(document: self.messageData[rIndex]["document"] as! CBLDocument)
                            }else{
                                messageSender = senderId()
                            }
                            
                            if messageDict[uIndex]["postId"] as! String != messageData[rIndex]["postId"] as! String{
                                
                                if messageSender != senderId(){
//                                    OperationQueue.main.addOperation {() -> Void in
                                        self.updatePostDetails(document: self.messageData[rIndex]["document"] as! CBLDocument, userId: messageSender, postId: messageDict[uIndex]["postId"] as! String)
//                                    }
                                }
                            }
                            
                            message = JSQMessage(senderId: messageSender, senderDisplayName: "server", date: Date() as Date, text: text, messageId: "\(messageId)")
                            
                            OperationQueue.main.addOperation {() -> Void in
                                
                                let vc = CouchbaseEvevnts()
                                vc.updateDocument(document: self.messageData[rIndex]["document"] as! CBLDocument, messageArray: message, postId: messageDict[uIndex]["postId"] as! String)
                            }
                            let lastTime = self.getCurrentTimeInString()
                            OperationQueue.main.addOperation {() -> Void in
                                
                                CouchbaseEvevnts.updateDocumentForLastMessageAndTime(document: self.messageData[rIndex]["document"] as! CBLDocument, lastMessage: text, time: lastTime)
                            }
                            NotificationCenter.default.post(name:Notification.Name(rawValue:"chatNotification"),
                                                            object: nil,
                                                            userInfo: ["message":message,
                                                                       "from":messageFrom,
                                                                       "postId":messageDict[uIndex]["postId"] as! String])
                            //                            }
                            break
                            
                        default:
                            break
                        }
                    }
                    break
                }
            }
            if !alreadyInDatabase{
                var text = [String]()
                var messageArray = [[String : AnyObject]]()
                for i in 0..<(messageDict[uIndex]["message"] as! [[String : AnyObject]]).count{
                    
                    text.append(self.base64Decoded(bsae64String:(messageDict[uIndex]["message"] as! [[String : AnyObject]])[i]["payload"] as! String))
                    messageArray.append((messageDict[uIndex]["message"] as! [[String : AnyObject]])[i])
                    //                    CouchbaseEvevnts.updateUnreadMessageCount(document:)
                }
                self.postId = messageDict[uIndex]["postId"] as! String
                self.getProfileDetails(userId: messageFrom, message: messageArray, currentPostId:messageDict[uIndex]["postId"] as! String)
            }
        }
        Helper.hidePI()
        
//        NSLog("before testing push")
        if UserDefaults.standard.value(forKey: "fromPush") != nil && (UserDefaults.standard.value(forKey: "fromPush") as! Bool == true){
            UserDefaults.standard.set(false, forKey: "fromPush")
            
            if (UserDefaults.standard.value(forKey: "userIdFromPush") != nil){
              
                let userIdFromPush = UserDefaults.standard.value(forKey: "userIdFromPush")
                let sellerIdFromPush = UserDefaults.standard.value(forKey: "sellerIdFromPush")
                
                let isUserOnChatScreen = UserDefaults.standard.bool(forKey: "isOnChatScreen")
                let activeUser = UserDefaults.standard.value(forKey: "activeUser")
                if(isUserOnChatScreen && (activeUser != nil)){
                    if(activeUser as! String == userIdFromPush as! String || activeUser as! String == sellerIdFromPush as! String){
                        return
                    }
                }
                for i in 0..<messageData.count{
                    if userIdFromPush as! String ==  messageData[i]["to"] as! String || sellerIdFromPush as! String ==  messageData[i]["to"] as! String{

                        let storyBoard = UIStoryboard.init(name: "ChatStoryboard", bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier:"activeChatController") as! ActiveChatViewController
                        vc.document = messageData[i]["document"] as! CBLDocument!
                        vc.groupOrUserNameLabel.setTitle( messageData[i]["name"]as? String, for: .normal)
                        vc.groupOrUserImageURL = messageData[i]["image"] as! String
                        vc.fromHome = true
                        let appDelegateObj = UIApplication.shared.delegate! as! AppDelegate
                        var controller = appDelegateObj.window!.rootViewController
                        if (appDelegateObj.window!.rootViewController as? UINavigationController) != nil
                        {
                            let navControllr = appDelegateObj.window!.rootViewController as! UINavigationController
                            controller = navControllr.visibleViewController
                        }
                        else
                        {
                            controller = appDelegateObj.window!.rootViewController
                        }
                        DispatchQueue.main.async {
                            controller?.navigationController?.pushViewController(vc, animated: true)
                        }
                        break
                    }
                }
                
            }
        }
        
    }
    
    
    func getProfileDetails(userId : String, message: [[String:AnyObject]], currentPostId:String) {
        
        var params = [String : AnyObject]()
        params["userId"] = userId as AnyObject
        params["token"] = Utility.getSessionToken() as AnyObject
        params["postId"] = currentPostId as AnyObject
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.viewProfileForChat,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    //                                    print("Horse Owner Profile Response : ",response)
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                        
                                    case 0:
                                        if(response["data"].dictionaryObject == nil){
                                            return
                                        }
                                        let userInfo = response["data"].dictionaryObject! as [String : AnyObject]
                                        self.postName = userInfo["postName"] as! String!
                                        if self.postName == ""{
                                            break;
                                        }
                                        self.createNewChatInDatabase(userId: userId, message: message, image: userInfo["profilePic"]! as! String,name: userInfo["fName"]! as! String, postImage: (((userInfo["postImage"] as! [String:Any])["options"] as! [Any])[0] as! [String:Any])["imageUrl"] as! String, currentPostId: currentPostId)
                                        break
                                        
                                    default:
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    Helper.hidePI()
        })
        
    }
    
    func createNewChatInDatabase(userId: String, message: [[String : AnyObject]], image: String, name: String, postImage:String, currentPostId:String){
        
        let database = CBObject.sharedInstance.database
        let newDocument = CouchbaseEvevnts.createNewDocument(database: database!, name: name, date: self.getCurrentTimeInString(), image: image, userId: userId, postId: currentPostId, postName: self.postName,type: isSeller, postImage: postImage)
        
        if message.count == 0{
            return
        }
     
        for i in 0..<message.count{
            var messageSender = message[i]["sender"] as! String
            let text = self.base64Decoded(bsae64String: message[i]["payload"] as! String)
//            let messageId = NSDate.timeIntervalSinceReferenceDate
            let messageId = message[0]["id"] as! String
            if messageSender == Utility.getUserID(){
                messageSender = senderId()
            }
            let newMessage = JSQMessage(senderId: messageSender, senderDisplayName: "server", date: Date() as Date, text: text, messageId: "\(messageId)")
            let vc = CouchbaseEvevnts()
            if i == 0{
                //                OperationQueue.main.addOperation {() -> Void in
                
                self.isCreated = vc.cerateDocument(document: newDocument, receivingUser: "Server", sendingUser: userId, messageArray: newMessage, postId: currentPostId)
                //                }
            }
            else{
                OperationQueue.main.addOperation {() -> Void in
                    
                    vc.updateDocument(document: newDocument, messageArray: newMessage, postId: currentPostId)
                }
            }
            let lastTime = self.getCurrentTimeInString()
            OperationQueue.main.addOperation {() -> Void in
                
                CouchbaseEvevnts.updateDocumentForLastMessageAndTime(document: newDocument, lastMessage: text, time: lastTime)
                //            }
                
                NotificationCenter.default.post(name:Notification.Name(rawValue:"chatNotification"),
                                                object: nil,
                                                userInfo: ["message":newMessage,
                                                           "from": userId,
                                                           "postId":currentPostId])
                if messageSender != Utility.getUserID() && UserDefaults.standard.value(forKey: "timeStamp") as! Int != 1{
                        let CBE = CouchbaseEvevnts()
                        CBE.updateUnreadMessageCount(document: newDocument)
                    
                }
            }
        }
        
        if UserDefaults.standard.value(forKey: "fromPush") != nil && (UserDefaults.standard.value(forKey: "fromPush") as! Bool == true){
            UserDefaults.standard.set(false, forKey: "fromPush")
            if (UserDefaults.standard.value(forKey: "userIdFromPush") != nil){
                
                let userIdFromPush = UserDefaults.standard.value(forKey: "userIdFromPush")
                if userIdFromPush as! String ==  userId{
                    
                    let appDelegate = UIApplication.shared.delegate
                    let storyBoard = UIStoryboard.init(name: "ChatStoryboard", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier:"activeChatController") as! ActiveChatViewController
                    vc.document = newDocument
                    vc.groupOrUserNameLabel.setTitle( name, for: .normal)
                    vc.groupOrUserImageURL = image
                    vc.fromHome = true
                    if let navController = appDelegate?.window??.rootViewController as? NavViewController
                    {
                        
                        if let ramanimtaedTabBarController = navController.childViewControllers.first as? RAMAnimatedTabBarController
                        {
                            DispatchQueue.main.async {
                                ramanimtaedTabBarController.navigationController?.pushViewController(vc, animated: true)
                                
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                navController.pushViewController(vc, animated: true)
                            }
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            appDelegate?.window??.rootViewController?.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    
    
    func getCurrentTimeInString()->String{
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.init(abbreviation: "UTC") as TimeZone!
        let stringDate = dateFormatter.string(from: date)
        return stringDate
    }
    
    
    func messageAckService(messageDict : [[String : AnyObject]]){
        
        var params = [String : AnyObject]()
        params["userId"] = Utility.getUserID() as AnyObject?
        var messageIdArray = [String]()
        for i in 0..<messageDict.count{
            for j in 0..<(messageDict[i]["message"] as! [[String : AnyObject]]).count{
                messageIdArray.append((messageDict[i]["message"] as! [[String : AnyObject]])[j]["id"] as! String)
            }
        }
        params["msgIds"] = messageIdArray as AnyObject?
        params["status"] = "1" as AnyObject
        
        
        AFWrapper.requestPOSTURLForChat(serviceName: Constant.ServiceMethodNames.ackMessage,
                                        params: params,
                                        success: { (response) in
                 
        },
                                        failure: { (Error) in
                                            self.messageAckService(messageDict: messageDict)
        })
        
    }
    
    func messageAckServiceForSocket(messageDict : [[String : AnyObject]]){
        
        var params = [String : AnyObject]()
        params["userId"] = Utility.getUserID() as AnyObject?
        var messageIdArray = [String]()
        
        messageIdArray.append(messageDict[0]["id"] as! String)
        params["msgIds"] = messageIdArray as AnyObject?
        params["status"] = "1" as AnyObject
        
        AFWrapper.requestPOSTURLForChat(serviceName: Constant.ServiceMethodNames.ackMessage,
                                 params: params,
                                 success: { (response) in

        },
                                 failure: { (Error) in
                                    self.messageAckServiceForSocket(messageDict: messageDict)
        })
    }
    
    func getAllContact(){
        
        var result : CBLQueryEnumerator
        do{
            let database = try CBLManager.sharedInstance().databaseNamed("mydb")
            let query = database.createAllDocumentsQuery()
            query.allDocsMode =  CBLAllDocsMode.allDocs
            query.descending = true
            do{
                result = try query.run()
                //                self.documentIdArray.removeAll()
                for i in 0..<result.count{
                    self.documentIdArray.append(database.document(withID: (result.row(at: i)).documentID!)!)
                }
            }
            catch{
//                print("Not get data")
            }
        }
        catch{
//            print("Not created")
        }
        self.reloadMessageData()
    }
    
    func reloadMessageData(){
        
        self.messageData.removeAll()
        for i in 0..<documentIdArray.count{
            let data = CouchbaseEvevnts.retriveData(document: documentIdArray[i])
            var DBData = [String : AnyObject]()
            DBData["name"] = data["name"] as! String? as AnyObject?
            DBData["lastMessage"] = data["lastMessage"] as! String? as AnyObject?
            DBData["time"] = data["time"] as! String? as AnyObject?
            DBData["image"] = data["image"] as AnyObject?
            DBData["document"] = documentIdArray[i]
            DBData["to"] = data["to"] as AnyObject
            DBData["postId"] = data["postId"] as AnyObject
            self.messageData.append(DBData as [String : AnyObject])
            
        }
    }
}


//MARK: To Encode and Decode into Base64
extension CreateNewChat{
    
    func base64Encoded(textMessage: String) -> String {
        let plainData = textMessage.data(using: String.Encoding.utf8)
        let base64String = plainData?.base64EncodedString(options: .lineLength64Characters)
        return base64String!
    }
    
    func base64Decoded(bsae64String: String) -> String {
        
        let decodedData:NSData = NSData(base64Encoded: bsae64String, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)!
        let decodedString = NSString(data: decodedData as Data, encoding: String.Encoding.utf8.rawValue)
        return decodedString! as String
    }
    func senderId() -> String {
        
        return (UIDevice.current.identifierForVendor?.uuidString)!
    }
}
