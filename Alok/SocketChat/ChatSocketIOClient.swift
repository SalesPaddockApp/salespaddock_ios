//
//  ChatSocketIOClient.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 12/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class ChatSocketIOClient: NSObject {

    static let sharedInstance = ChatSocketIOClient()
    
    override init() {
        super.init()
    }
    
    func sendHeartbeatForUser(status: String){
  
        var dict = [[String : String]]()
        var dic = [String : String]()
        dic["from"] = Utility.getUserID()
        dic["status"] = status
        dict.append(dic)

//        SocketIOManager.sharedInstance.publishToChannel(channel:"Heartbeat", message: dict as [[String : AnyObject]])
        SocketIOManager.default.publishToChannel(channel:"Heartbeat", message: dict as [[String : AnyObject]])
    }
    
    /// To send message to message channel
    ///
    /// - Parameters:
    ///   - message: message data
    ///   - fromUser: sending user
    ///   - toUser: receiving user
    ///   - docId: document D
    ///   - messageType: message type e.g.-text has type 0
    func sendMessage(message: String,fromUser: String, toUser: String, docId: String, messageType: String, postId: String, seller: Int, messageId: Double){
        
//        type,from,payload,toDocId,id,senderName
//        ['type', 'sellerId','userId', 'payload',  'toDocId', 'id','senderName','postId','isSeller']
        
        var dict = [String : String]()
        dict["type"] = messageType
        dict["payload"] = message
        dict["toDocId"] = docId
        dict["id"] = "\(messageId)"
        dict["senderName"] = fromUser
        dict["postId"] = postId
        dict["isSeller"] = "\(seller)"
        if seller == 1{
            dict["sellerId"] = fromUser
            dict["userId"] = toUser
        }else{
            
            dict["userId"] = fromUser
            dict["sellerId"] = toUser
        }
        
        var dictToPassData = [[String : String]]()
        dictToPassData.append(dict)
        SocketIOManager.default.publishToChannel(channel:"Message", message: dictToPassData as [[String : AnyObject]])
        
    }
    
    

    
}
