//
//  SocketIOManager.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 10/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import SocketIO

open class SocketIOManager: NSObject {
    
    var channelsNameArray = [String]()
    
//    static let sharedInstance = SocketIOManager()
//    let socket = SocketManager(socketURL: URL(string: Constant.URLs.socketBaseURL)!, config: [.log(true),.reconnects(true)]).defaultSocket

    open static let `default` = SocketIOManager()
    private let manager: SocketManager
    private var socket: SocketIOClient
    
    private override init() {
        manager = SocketManager(socketURL: URL(string: Constant.URLs.socketBaseURL)!, config: [.log(true),.reconnects(true)])
        socket = manager.defaultSocket
    }
    
    func establishConnection() {
        socket.connect()
    }
    
    
    func closeConnection() {
        socket.disconnect()
        channelsNameArray.removeAll()
    }

    
    func subscribeToChannel(channel: String,handler: @escaping(_ messageData : [[String : AnyObject]])->Void){
        if(!channelsNameArray.contains(channel))
        {
            channelsNameArray.append(channel)
        }
        else{
            return
        }
        socket.on(channel){(message, ack) in
            //completion handler
            handler(message as! [[String : AnyObject]])
//            print(message)
        }
    }
    
    func publishToChannel(channel: String, message: [[String : AnyObject]]){
        
        socket.emit(channel, with: message)
    }
    
    func removeSubscriber(){
        
        socket.removeAllHandlers()
    }
    
}
