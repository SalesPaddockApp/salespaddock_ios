//
//  CouchbaseEvevnts.swift
//  TabBarController
//
//  Created by Rahul Sharma on 30/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit
import JSQMessagesViewController


//protocol CouchbaseEvevntsDelegate {
//    func toUpdateBatchCount(count:Int)
//}

class CouchbaseEvevnts: NSObject {
    
    var database : CBLDatabase! = nil
    var docID : String!
    //    var cdbDelegate : CouchbaseEvevntsDelegate! = nil
    
    ///
    /// To create new database for new User
    /// -Returns : document ID
    ///
    class func createNewDocument(database : CBLDatabase, name: String,date: String,image: String, userId: String, postId: String, postName: String, type: String, postImage: String)->CBLDocument{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        let stringDate = dateFormatter.string(from: Date())
        
        let document = database.createDocument()
        var dict = [String : AnyObject]()
        dict["name"] = name as AnyObject?
        dict["time"] = date as AnyObject?
        dict["to"] = userId as AnyObject
        dict["from"] = Utility.getUserID() as AnyObject
        dict["image"] = image as AnyObject?
        dict["isCreated"] = false as AnyObject
        dict["createdOn"] = stringDate as AnyObject
        dict["postId"] = postId as AnyObject
        dict["postName"] = postName as AnyObject
        dict["type"] = type as AnyObject
        dict["unread"] = 0 as AnyObject
        dict["postImage"] = postImage as AnyObject
        do{
            try document.putProperties(dict)
            return document
        }
        catch{
            
//            print("Not saved")
            return document
        }
    }
    
    //
    /// To update last message and time in DB
    ///
    /// - Parameters:
    ///   - document: Document
    ///   - lastMessage: Last Message
    ///   - time: Last Message Time
    class func updateDocumentForLastMessageAndTime(document:CBLDocument,
                                                   lastMessage:String,
                                                   time:String){
        
        
        var dic = document.properties
        dic?["lastMessage"] = lastMessage
        dic?["time"] = time
        do{
            try document.putProperties(dic!)
        }
        catch{
//            print("Data not saved")
        }
    }
    
    
    ///  To update unread messages
    ///
    /// - Parameter document: document
    func updateUnreadMessageCount(document: CBLDocument){
        
        var dic = document.properties
        var messageCount : Int!
        messageCount = dic?["unread"] as! Int
        messageCount = messageCount + 1
        dic?["unread"] = messageCount
        do{
            try document.putProperties(dic!)
        }
        catch{
//            print("Data not saved")
        }
        
        var batchCountArray = [String]()
        if (UserDefaults.standard.value(forKey: "batchCount") != nil) {
            batchCountArray = UserDefaults.standard.value(forKey: "batchCount") as! [String]
        }
        let docId = document.documentID
        var isMatched:Bool = false
        for i in 0..<batchCountArray.count{
            if batchCountArray[i] == docId{
                isMatched = true
                break
            }
        }
        if batchCountArray.count == 0 || isMatched == false{
            batchCountArray.append(docId)
            UserDefaults.standard.set(batchCountArray, forKey: "batchCount")
            NotificationCenter.default.post(name:Notification.Name(rawValue:"badgeCount"),
                                            object: nil,
                                            userInfo: ["count":batchCountArray.count])
        }
        
    }
    
    func toRemoveUnreadMessage(document: CBLDocument){
        
        var dic = document.properties
        dic?["unread"] = 0
        do{
            try document.putProperties(dic!)
        }
        catch{
//            print("Data not saved")
        }
        
        
        var batchCountArray = [String]()
        if (UserDefaults.standard.value(forKey: "batchCount") != nil) {
            batchCountArray = UserDefaults.standard.value(forKey: "batchCount") as! [String]
        }
        let docId = document.documentID
        
        for i in 0..<batchCountArray.count{
            if batchCountArray[i] == docId{
                batchCountArray.remove(at: i)
                break
            }
        }
        
        NotificationCenter.default.post(name:Notification.Name(rawValue:"badgeCount"),
                                        object: nil,
                                        userInfo: ["count":batchCountArray.count])
        
        UserDefaults.standard.set(batchCountArray, forKey: "batchCount")
        
    }
    
    //
    /// To create new document for new conversation
    ///
    /// - Parameters:
    ///   - database: Database of new Conversion
    ///   - receivingUser: receiving User
    ///   - sendingUser: Sending User
    ///   - messageArray: Message array containing all message
    func cerateDocument(document : CBLDocument, receivingUser:String, sendingUser:String, messageArray:JSQMessage, postId: String)->Bool{
        
        let senderId = messageArray.senderId
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.init(abbreviation: "UTC") as TimeZone!
        let stringDate = dateFormatter.string(from: messageArray.date)
        var dic = [String : String]()
        var arrayDic = [[String : String]]()
        let messageId = messageArray.messageId
        
        dic = ["messageId" : messageId, "date" : stringDate, "text" : messageArray.text, "type" : "0", "senderId" : senderId, "isSend": "0"]
        
        arrayDic.append(dic)
        var dict = document.properties
        dict?["receivingUser"] = receivingUser
        dict?["sendingUser"] = sendingUser
        dict?["isCreated"] = true
        dict?["postId"] = postId
        dict?["message"] = arrayDic
        
        do{
            try document.putProperties(dict! as [String : AnyObject])
            docID = document.documentID
            NotificationCenter.default.post(name:Notification.Name(rawValue:"chatNotification"),
                                            object: nil,
                                            userInfo: ["message":messageArray, "from":sendingUser,
                                                       "postId":postId]
            )
            
            //            print("data creatde...............................................")
            
        }
        catch{
            
//            print("Document not saved")
        }
        return true
    }
    
    //
    /// To update database
    ///
    /// - Parameters:
    ///   - database: database
    ///   - documentId: Document Id
    ///   - messageArray: Message Array containing all message
    ///
    
    func updateDocument(document:CBLDocument, messageArray: JSQMessage, postId: String){
        
        let senderId = messageArray.senderId
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.init(abbreviation: "UTC") as TimeZone!
        
        let stringDate = dateFormatter.string(from: messageArray.date)
        let messageId = messageArray.messageId
        var dict = [String : String]()
        
        dict = ["messageId" : messageId, "date" : stringDate, "text" : messageArray.text, "type" : "0", "senderId" : senderId, "isSend": "0"]
        
        let allDict = document.properties
        
        var allData = allDict! as [String : AnyObject]
        var msgArray = allData["message"] as! [[String : AnyObject]]
        msgArray.append(dict as [String : AnyObject])
        allData["message"] = msgArray as AnyObject
        allData["postId"] = postId as AnyObject
        
        do{
            try document.putProperties(allData)
            //            print("data updated.......................")
        }
        catch{
            
//            print("Documents not updated")
        }
        
        
    }
    
    
    func updateSellerOrBuyer(document: CBLDocument, seller: String){
        
        var dic = document.properties
        dic?["type"] = seller
        do{
            try document.putProperties(dic!)
        }
        catch{
//            print("Data not saved")
        }
        
    }
    
    
    func updatePostDetailsInDB(document : CBLDocument, postId : String, postName: String, postImage: String){
        
        var dic = document.properties
        dic?["postId"] = postId
        dic?["postName"] = postName
        dic?["postImage"] = postImage
        do{
            try document.putProperties(dic!)
        }
        catch{
//            print("Data not saved")
        }
        
    }
    
    
    /// To delete single message
    ///
    /// - Parameters:
    ///   - document: document
    ///   - message: message to delete
    func deleteSingleMessage(document: CBLDocument, messageId : String){
        
        var allData = document.properties
        var messageData = allData?["message"] as! [[String : AnyObject]]
        for i in 0..<messageData.count{
            
            if (messageData[i]["messageId"] as! String == messageId){
                messageData.remove(at: i)
                break
            }
        }
        allData?["message"] = messageData
        do{
            try document.putProperties(allData!)
        }
        catch{
            
//            print("Documents not updated")
        }
    }
    
    //
    /// To delete database
    ///
    /// - Parameters:
    ///   - document: Database Document
    /// - Returns: Deleted successfuly or not
    
    func deleteDocument(document: CBLDocument){
        
        do{
            try document.delete()
            return
        }
        catch{
            
//            print("Document not deleted")
            return
        }
    }
    
    
    /// To delete all message for a user
    ///
    /// - Parameter document: document to delete messages
    class func deleteChatOnly(document : CBLDocument){
        
        var allData = document.properties
        var messageData = allData?["message"] as! [[String : AnyObject]]
        messageData.removeAll()
        allData?["message"] = messageData
        do{
            try document.putProperties(allData!)
        }
        catch{
//            print("Documents not updated")
        }
    }
    
    //
    /// To retrive data from DB
    ///
    /// - Parameter document: Document
    /// - Returns: Database content in dictionary
    class func retriveData(document : CBLDocument)->[String : AnyObject]{
        
        var messageArray = document.properties
        if((messageArray?["name"]) != nil){
            return messageArray! as [String : AnyObject]
        }
        else{
            
            return ["success": "false" as AnyObject]
        }
        
        
    }
    
    
}
