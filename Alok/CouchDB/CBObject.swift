//
//  CBObject.swift
//  TabBarController
//
//  Created by Rahul Sharma on 30/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit

class CBObject: NSObject {
    
    var database : CBLDatabase! = nil
    //    var manager : CBLManager! = nil
    static let sharedInstance = CBObject()
    
    override init() {
        //        self.manager = CBLManager.sharedInstance()
        do{
            self.database = try CBLManager.sharedInstance().databaseNamed("mydb")
            
        }
        catch{
//            print("db not created")
        }
    }
}
