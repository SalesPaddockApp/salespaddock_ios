//
//  PostImageViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 27/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Kingfisher
import youtube_ios_player_helper

protocol playVideoDelegate
{
    func playVideoActionPressed(videoUrl: URL)
}

class PostImageViewCell: UITableViewCell {
    
    var playVideoDelegate:playVideoDelegate! = nil
    var maximumOffSet: CGFloat?
    @IBOutlet weak var scrolViewOutlet: UIScrollView!
    @IBOutlet weak var horseNameLabel: UILabel!
    @IBOutlet weak var descriptionOutlet: UITextView!
    @IBOutlet weak var priceOutlet: UILabel!
    var imageArray = [AnyObject]()
    var timer: Timer!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addTimerForAutoScroll()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func addTimerForAutoScroll()
    {
        self.timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector:#selector(onTimer), userInfo: nil, repeats: true)
    }
    
    @objc func onTimer() {
        guard let max = self.maximumOffSet else { return }
        if self.scrolViewOutlet.contentOffset.x<max {
            scrolViewOutlet.setContentOffset(CGPoint(x: scrolViewOutlet.contentOffset.x+scrolViewOutlet.frame.size.width, y: 0), animated: true)
        } else {
            self.timer.invalidate()
        }
    }
    
    func addPhotosToScrollView(imageArray : [AnyObject]) {
        for i in 0..<imageArray.count {
            let imageview = UIImageView()
            var imageUrl = ""
            let dataArray = imageArray[i] as! [String:AnyObject]
            if(dataArray["type"] as! String) == "0"
            {
                imageUrl = dataArray["imageUrl"] as! String
            }
            else {
                if let imageurl = dataArray["thumbnailUrl"] as? String, let videoURL = dataArray["imageUrl"] as? String {
                    imageUrl = imageurl
                    if (dataArray["type"] as! String) == "2" {
                        let ytplayerView = YTPlayerView(frame:  CGRect(x: 0, y: 0, width: self.scrolViewOutlet.frame.width, height: self.scrolViewOutlet.frame.height))
                        imageview.addSubview(ytplayerView)
                        imageview.tag = i
                        imageview.isUserInteractionEnabled = true
                        self.playVideo(withID: videoURL, withView : ytplayerView)
                    } else {
                        if let youtubeID = dataArray["youTubeId"] as? String {
                            let ytplayerView = YTPlayerView(frame:  CGRect(x: 0, y: 0, width: self.scrolViewOutlet.frame.width, height: self.scrolViewOutlet.frame.height))
                            imageview.addSubview(ytplayerView)
                            imageview.tag = i
                            imageview.isUserInteractionEnabled = true
                            self.playVideo(withID: youtubeID, withView : ytplayerView)
                        } else {
                            let button = UIButton(frame : CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: self.scrolViewOutlet.frame.height))
                            button.setImage(#imageLiteral(resourceName: "play_icon"), for: .normal)
                            button.addTarget(self, action: #selector(playButtonAction(_:)),for: .touchUpInside)
                            button.tag = i
                            imageview.addSubview(button)
                            imageview.isUserInteractionEnabled = true
                        }
                    }
                }
            }
            let url = URL(string: imageUrl)
            
            imageview.kf.indicatorType = .activity
            imageview.kf.indicator?.startAnimatingView()
            
            imageview.kf.setImage(with:url,
                                  placeholder: UIImage.init(named:"horse_photo_defalt_image"),options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
            },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    imageview.kf.indicator?.stopAnimatingView()
            })
            imageview.contentMode = .scaleToFill
            let xPosition = UIScreen.main.bounds.size.width * CGFloat(i)
            imageview.frame = CGRect(x: xPosition, y: 0, width: UIScreen.main.bounds.size.width, height: self.scrolViewOutlet.frame.height)
            self.scrolViewOutlet.contentSize.width = UIScreen.main.bounds.size.width * CGFloat(i + 1)
            self.scrolViewOutlet.addSubview(imageview)
        }
    }
    
    func playVideo(withID videoID : String, withView ytView : YTPlayerView) {
        let dict = [
            "playsinline" : 1,
            "showinfo" : 0,
            "controls" : 1,
            "modestbranding": 0,
            "enablejsapi":1,
            "autohide":2,
            "rel":0,
            "origin" : "http://www.youtube.com",
            ] as [String : Any]
        let result = ytView.load(withVideoId: videoID, playerVars: dict)
        ytView.webView?.allowsInlineMediaPlayback = false
//        print("Result = \(result)")
        ytView.delegate = self
    }
    
    @objc func playButtonAction(_ button: UIButton)
    {
        let tag = Int(button.tag)
        if(self.playVideoDelegate != nil)
        {
            let dataDict = imageArray[tag] as! [String: AnyObject]
            let urlStr = dataDict["imageUrl"] as! String
            self.playVideoDelegate.playVideoActionPressed(videoUrl: URL(string: urlStr)!)
        }
    }
}

extension PostImageViewCell : UIScrollViewDelegate{
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.timer.invalidate()
    }
}

extension PostImageViewCell : YTPlayerViewDelegate {
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        
    }
    
    func playerView(_ playerView: YTPlayerView, didPlayTime playTime: Float) {
        
    }
    
    func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
        
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo quality: YTPlaybackQuality) {
        
    }
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        playerView.webView?.allowsInlineMediaPlayback = false
    }
}
