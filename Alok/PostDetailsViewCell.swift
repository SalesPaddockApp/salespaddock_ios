//
//  PostDetailsViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 27/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class PostDetailsViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
