//
//  ChatViewController.swift
//  TabBarController
//
//  Created by Rahul Sharma on 29/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher
import Alamofire
import SwiftyJSON
import JSQMessagesViewController

class ChatViewController: UIViewController{
    
    
    @IBOutlet weak var noChatImage: UIImageView!
    @IBOutlet weak var noChatLabel: UILabel!
    //    @IBOutlet weak var inboxLabelView: UIView!
    @IBOutlet weak var toCoverBarView: UIView!
    @IBOutlet weak var inboxNavigationView: UIView!
    @IBOutlet weak var inboxNavigationLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var chatTableView: UITableView!
    var searchActive : Bool = false
    var tableViewData = [[String : AnyObject]]()
    var index : Int!
    
    var filterdedData = [[String : AnyObject]]()
    var documentIdArray = [CBLDocument]()
    
    var lastMessage : String!
    var lastMessageTime : String!
    var isCommingFromPush:Bool = false
    var isAlreadyLoaded : Bool = false
    
    override func viewDidLoad(){
        
        super.viewDidLoad()
        //        self.view.bringSubview(toFront: inboxLabelView)
        self.view.bringSubview(toFront: self.inboxNavigationView)
        self.view.bringSubview(toFront: self.toCoverBarView)
        self.tableViewData.removeAll()
        self.getAllContact()
        self.rearrangeData()
        if tableViewData.count == 0{
            self.view.bringSubview(toFront: self.noChatImage)
            self.view.bringSubview(toFront: self.noChatLabel)
        }
        else{
            self.noChatImage.isHidden = true
            self.noChatLabel.isHidden = true
        }
        
        self.tabBarController?.tabBar.isHidden = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(catchNotification), name: NSNotification.Name(rawValue: "chatNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(catchPostNotification), name: NSNotification.Name(rawValue: "chatPostNotification"), object: nil)
        self.isAlreadyLoaded = true
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewDidAppear(true)
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isHidden = true
        if self.isAlreadyLoaded == false{
            self.tableViewData.removeAll()
            self.getAllContact()
            self.rearrangeData()
            
            
            if tableViewData.count == 0{
                self.view.bringSubview(toFront: self.noChatImage)
                self.view.bringSubview(toFront: self.noChatLabel)
            }
            else{
                self.noChatImage.isHidden = true
                self.noChatLabel.isHidden = true
            }
            
            NotificationCenter.default.addObserver(self, selector: #selector(catchNotification), name: NSNotification.Name(rawValue: "chatNotification"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(catchPostNotification), name: NSNotification.Name(rawValue: "chatPostNotification"), object: nil)
        }
        self.isAlreadyLoaded = false
        ChatSocketIOClient.sharedInstance.sendHeartbeatForUser(status: "1")
    }

    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "chatNotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "chatPostNotification"), object: nil)
        
    }

    @objc func catchNotification(notification:Notification) -> Void {
        
        let message = notification.userInfo
        self.noChatImage.isHidden = true
        self.noChatLabel.isHidden = true
        var isExist = false
        for i in 0..<tableViewData.count{
            if tableViewData[i]["to"] as! String == message?["from"] as! String{
                isExist = true
                var unreadMessageCount = tableViewData[i]["unread"] as! Int
                unreadMessageCount += 1
                tableViewData[i]["unread"] = unreadMessageCount as AnyObject?
                let messageData = message?["message"] as! JSQMessage
                tableViewData[i]["lastMessage"] = messageData.text as AnyObject?
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                dateFormatter.timeZone = NSTimeZone.init(abbreviation: "UTC") as TimeZone!
                let stringDate = dateFormatter.string(from: messageData.date)
                tableViewData[i]["time"] = stringDate as AnyObject?
            }
        }
        if !isExist{
            
            self.getAllContact()
        }
        self.rearrangeData()
        self.chatTableView.reloadData()
        
    }
    
    @objc func catchPostNotification(notification:Notification) -> Void{
        
        let message = notification.userInfo
        for i in 0..<tableViewData.count{
            if tableViewData[i]["to"] as! String == message?["from"] as! String{
                //postName
                tableViewData[i]["postName"] = message?["postName"] as AnyObject?
                tableViewData[i]["postId"] = message?["postId"] as AnyObject?
                
            }
        }
        self.chatTableView.reloadData()
        
    }
    
    func rearrangeData(){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.init(abbreviation: "UTC") as TimeZone!
        for i in 0..<tableViewData.count{
            tableViewData[i]["time"] = dateFormatter.date(from: tableViewData[i]["time"] as! String) as AnyObject
        }
        tableViewData.sort(by: { ($0["time"] as! Date).compare($1["time"] as! Date) == ComparisonResult.orderedDescending
        })
        for i in 0..<tableViewData.count{
            tableViewData[i]["time"] = dateFormatter.string(from: tableViewData[i]["time"] as! Date) as AnyObject
        }
        self.chatTableView.reloadData()
    }
    
    //
    /// To get all contact from database
    func getAllContact(){
        
        var result : CBLQueryEnumerator
        do{
            let database = try CBLManager.sharedInstance().databaseNamed("mydb")
            let query = database.createAllDocumentsQuery()
            query.allDocsMode =  CBLAllDocsMode.allDocs
            query.descending = true
            do{
                result = try query.run()
                self.documentIdArray.removeAll()
                for i in 0..<result.count{
                    self.documentIdArray.append(database.document(withID: (result.row(at: i)).documentID!)!)
                }
            }
            catch{
//                print("Not get data")
            }
        }
        catch{
//            print("Not created")
        }
        self.reloadTableView()
    }
    
    func reloadTableView(){
        
        self.tableViewData.removeAll()
        for i in 0..<documentIdArray.count{
            
            let data = CouchbaseEvevnts.retriveData(document: documentIdArray[i])
            var DBData = [String : AnyObject]()
            DBData["name"] = data["name"] as! String? as AnyObject?
            DBData["lastMessage"] = data["lastMessage"] as! String? as AnyObject?
            DBData["time"] = data["time"] as! String? as AnyObject?
            DBData["image"] = data["image"] as AnyObject?
            DBData["document"] = documentIdArray[i]
            DBData["to"] = data["to"] as AnyObject
            DBData["createdOn"] = data["createdOn"] as AnyObject
            DBData["postId"] = data["postId"] as AnyObject
            DBData["postName"] = data["postName"] as AnyObject
            DBData["type"] = data["type"] as AnyObject
            DBData["unread"] = data["unread"] as AnyObject
            self.tableViewData.append(DBData as [String : AnyObject])
            
        }
        
    }
    
    func getCurrentTimeInString()->String{
        
        let date = Date()
        //        print(date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.init(abbreviation: "UTC") as TimeZone!
        let stringDate = dateFormatter.string(from: date)
        return stringDate
    }
}



//MARK: Search Bar Delegate
extension ChatViewController : UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        searchActive = false;
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    //    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    //        searchActive = false;
    //    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        self.view.endEditing(true)
        self.resignFirstResponder()
        self.searchBar.text = ""
        searchBar.setShowsCancelButton(false, animated: true)
        self.chatTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchBar.setShowsCancelButton(true, animated: true)
        
        if(searchBar.text == nil || searchBar.text == ""){
            self.searchActive = false
            self.chatTableView.reloadData()
        }
        else{
            self.searchActive = true
            //            let lower = searchBar.text?.lowercased()
            let lower = searchBar.text
            filterdedData = tableViewData.filter({(($0["name"] as! String).range(of: lower!) != nil) })
            self.chatTableView.reloadData()
        }
    }
    
}

//MARK: Table View Delegate
extension ChatViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(searchActive) {
            return filterdedData.count
        }
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = chatTableView.dequeueReusableCell(withIdentifier: "ConversationTableViewCell") as! ChatViewControllerCell
        cell.groupOrUserImage.layer.cornerRadius = cell.groupOrUserImage.frame.size.width / 2
        cell.groupOrUserImage.clipsToBounds = true
        
        
        if(searchActive){
            cell.loadCell(cellData: self.filterdedData[indexPath.row])
        }
        else{
            cell.loadCell(cellData: self.tableViewData[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.index = indexPath.row
        performSegue(withIdentifier: "activeChatController", sender: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if(editingStyle == UITableViewCellEditingStyle.delete){
            
            if(searchActive){
                
                self.toDeleteEntry(indexPath:indexPath)
                
            }else{
                self.toDeleteEntry(indexPath:indexPath)
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "activeChatController"){
            
            let vc = segue.destination as! ActiveChatViewController
            
            if searchActive{
                vc.document = self.filterdedData[sender as! Int]["document"] as! CBLDocument
                vc.groupOrUserNameLabel.setTitle(self.filterdedData[sender as! Int]["name"] as? String, for: .normal)
                vc.receiverNameString = self.filterdedData[sender as! Int]["name"] as? String
                if(self.filterdedData[sender as! Int]["image"] != nil){
                    //                    vc.groupOrUserImage = UIImage(named: self.filterdedData[sender as! Int]["image"] as! String)!
                    vc.groupOrUserImageURL = self.filterdedData[sender as! Int]["image"] as! String
                }else{
                    vc.groupOrUserImage = UIImage(named:"chats_image_frame")
                }
            }
            else{
                vc.document = self.tableViewData[sender as! Int]["document"] as! CBLDocument
                vc.groupOrUserNameLabel.setTitle(self.tableViewData[sender as! Int]["name"] as? String, for: .normal)
                vc.receiverNameString = self.tableViewData[sender as! Int]["name"] as? String
                //                vc.groupOrUserImageButton.setImage(UIImage(named: (self.tableViewData[sender as! Int]["image"] as? String)!), for: .normal)
                if(self.tableViewData[sender as! Int]["image"] != nil){
                    //                    vc.groupOrUserImage = UIImage(named: self.tableViewData[sender as! Int]["image"] as! String)!
                    vc.groupOrUserImageURL = self.tableViewData[sender as! Int]["image"] as! String
                }else{
                    vc.groupOrUserImage = UIImage(named:"chats_image_frame")
                }
            }
        }
    }
    
}

//MARK: Scrole view delegaet
extension ChatViewController : UIScrollViewDelegate{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let contentOffset = scrollView.contentOffset.y
        //print(contentOffset)
        if(contentOffset > 101 && contentOffset <= 151){
            self.inboxNavigationView.isHidden = false
            self.inboxNavigationView.backgroundColor = UIColor.init(white: 1, alpha: contentOffset / 151)
        }
        else if(contentOffset > 151)
        {
            self.inboxNavigationView.isHidden = false
            self.inboxNavigationView.backgroundColor = UIColor.init(white: 1, alpha: 1)
        }
        else{
            self.inboxNavigationView.isHidden = true
        }
    }
    
}

//MARK: Web services
extension ChatViewController{
    
    func toDeleteEntry(indexPath: IndexPath){
        
        
        var params = [String : AnyObject]()
        params["isEntry"] = "1" as AnyObject?
        
        if searchActive{
            params["from"] = Utility.getUserID() as AnyObject?
            params["to"] = filterdedData[indexPath.row]["to"]  as AnyObject?
            params["postId"] = filterdedData[indexPath.row]["postId"]  as AnyObject?
        }else{
            params["from"] = Utility.getUserID()  as AnyObject?
            params["to"] = tableViewData[indexPath.row]["to"]  as AnyObject?
            params["postId"] = tableViewData[indexPath.row]["postId"]  as AnyObject?
        }
        
        Helper.showPI(_message: "Deleting ...")
        AFWrapper.requestPOSTURLForChat(serviceName: Constant.ServiceMethodNames.deleteMessage,
                                 params: params,
                                 success: { (response) in
                                    
                                    let vc = CouchbaseEvevnts()
                                    if(self.searchActive){
                                        vc.deleteDocument(document: self.filterdedData[indexPath.row]["document"] as! CBLDocument)
                                        self.filterdedData.remove(at: indexPath.row)
                                        
                                    }else{
                                        
                                        vc.deleteDocument(document: self.tableViewData[indexPath.row]["document"] as! CBLDocument)
                                        self.tableViewData.remove(at: indexPath.row)
                                        
                                    }
                                    self.chatTableView.deleteRows(at: [indexPath], with: .fade)
                                    self.chatTableView.reloadData()
                                    
        },
                                 failure: { (Error) in
//                                    print(Error)
        })
    }
    
}

//MARK: To Encode and Decode into Base64
extension ChatViewController{
    
    func base64Encoded(textMessage: String) -> String {
        
        let plainData = textMessage.data(using: String.Encoding.utf8)
        let base64String = plainData?.base64EncodedString(options: .lineLength64Characters)
        return base64String!
    }
    
    func base64Decoded(bsae64String: String) -> String {
        
        let decodedData:NSData = NSData(base64Encoded: bsae64String, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)!
        let decodedString = NSString(data: decodedData as Data, encoding: String.Encoding.utf8.rawValue)
        return decodedString! as String
    }
}

