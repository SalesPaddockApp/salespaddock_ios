//
//  PostDetailsViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 23/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Kingfisher
import AVKit
import AVFoundation
import youtube_ios_player_helper

class PostDetailsViewController: UIViewController {
    
    
    @IBOutlet weak var backButtonOutlet: UIButton!
    @IBOutlet weak var navViewOutlet: UIView!
    @IBOutlet weak var postDetailTableView: UITableView!
    var postId : String!
    var postName : String!
    var arrayOfListDetails = [[String : AnyObject]]()
    var ownerDetails = [String : String]()
    var postImage : String!
    var priceDetail = [String : String]()
    var responseDict = [String:AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getPostDetailsService()
        postDetailTableView.rowHeight = UITableViewAutomaticDimension
        postDetailTableView.estimatedRowHeight = 400
        postDetailTableView.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        //        self.navigationController?.isNavigationBarHidden = false
        //        self.navigationController?.white()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        //        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func getPostDetailsService() {
        
        Helper.showPI()
        
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject,
                      "postId":self.postId as AnyObject]
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.viewProfileForChat,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                        
                                    case 0:
                                        if (response["data"]).dictionaryObject == nil{
                                            self.showAlert("Error", message: "No data found, for this horse")
                                            break;
                                        }
                                        self.responseDict = (response["data"]).dictionaryObject! as [String : AnyObject]
                                        
                                        self.arrayOfListDetails = self.responseDict["post"] as! [[String : AnyObject]]
                                        self.postName = self.responseDict["postName"] as! String
                                        self.postImage = (((self.responseDict["postImage"] as! [String : AnyObject])["options"] as! [[String : AnyObject]])[0] as! [String : String])["imageUrl"]!
                                        self.priceDetail = self.responseDict["priceDetails"] as! [String : String]
                                        self.postDetailTableView.reloadData()
                                        
                                        break
                                        
                                        //                                    case 1:
                                        //                                        let errMsg = response["Message"].string
                                        //                                         self.showAlert("Error", message: errMsg!)
                                        //                                        break;
                                        
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Error", message: errMsg!)
                                        
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
    
    
}

extension PostDetailsViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.arrayOfListDetails.count == 0{
            return 0
        }
        return self.arrayOfListDetails.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "postImageCell") as! PostImageViewCell
            
            
            cell.priceOutlet.text = "\(priceDetail["options"]!)"
            cell.horseNameLabel.text = self.postName!
            let descriptionDetail = responseDict["description"] as! [String : String]
            cell.descriptionOutlet.text = descriptionDetail["options"]
            let imagess = responseDict["postImage"] as! [String: AnyObject]
            
            if let imageArray =  imagess["options"] as? [AnyObject]{
                
                cell.imageArray = imageArray
                var xPosition:CGFloat
                if imageArray.count>=2{
                    xPosition = UIScreen.main.bounds.width * CGFloat(imageArray.count-1)
                    cell.maximumOffSet = xPosition
                }
                cell.addPhotosToScrollView(imageArray: imageArray)
                cell.playVideoDelegate = self
            }
            return cell
        }
            
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "postDetailCell") as! PostDetailsViewCell
            cell.descriptionLabel.text = self.arrayOfListDetails[indexPath.row - 1]["title"] as? String
            let details = self.arrayOfListDetails[indexPath.row - 1]["selected"] as AnyObject
            if let detail : [String] = details as? [String]{
                cell.detailLabel.text = detail[0]
            }else{
                cell.detailLabel.text = (details as! [[String : String]])[0]["city"]!
            }
            return cell
        }
        
    }
    
    
}

extension PostDetailsViewController : playVideoDelegate, UIScrollViewDelegate {
    
    func playVideoActionPressed(videoUrl: URL) {
        let player = AVPlayer(url: videoUrl as URL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView  == postDetailTableView
        {
            let offsetY: CGFloat = scrollView.contentOffset.y
            
            if(offsetY > 100)
            {
                backButtonOutlet.setImage(Image.init(named: "back_btn_off"), for: .normal)
                let alpha: CGFloat = min(1, 1 - ((64 + 100 - offsetY) / 100))
                navViewOutlet.backgroundColor = Color.white.withAlphaComponent(alpha)
                if(alpha == 1)
                {
                    backButtonOutlet.setImage(Image.init(named: "back_btn_off"), for: .normal)
                }
            }
            else if (offsetY < 100)
            {
                backButtonOutlet.setImage(Image.init(named: "back_btn_wight_off"), for: .normal)
                navViewOutlet.backgroundColor = Color.clear
            }
            else
            {
                backButtonOutlet.setImage(Image.init(named: "back_btn_wight_off"), for: .normal)
            }
        }
    }
}
