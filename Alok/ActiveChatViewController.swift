//
//  ActiveChatViewController.swift
//  TabBarController
//
//  Created by Rahul Sharma on 29/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit
import Foundation
import JSQMessagesViewController
import AVKit
import Kingfisher

class ActiveChatViewController: JSQMessagesViewController/*,JSQMessagesViewAccessoryButtonDelegate*/ {
    
    @IBOutlet weak var groupOrUserNameLabel: UIButton!
    var receiverNameString : String?
    @IBOutlet weak var groupOrUserImageButton: UIButton!
    @IBOutlet weak var chatPersonImage: UIImageView!
    
    @IBOutlet weak var userNameButtonWidth: NSLayoutConstraint!
    
//    var postDetailsView : UIView!
//    var postDetailsSubView : UIView!
//    var postImage : UIImageView!
//    var postNameLabel : UILabel!
    var seperatotView : UIView!
    var groupOrUserImage : UIImage!
    var groupOrUserImageURL : String!
    
    let incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(with: UIColor.lightGray)
    let outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with: UIColor(red: 10/255, green: 180/255, blue: 157/255, alpha: 1.0))
    
    var messages = [JSQMessage]()
    var document : CBLDocument!
    
    let fromUser = Utility.getUserID()
    var toUser : String!
    
    var messageId : Double = 0.0
    var isCreated = false
    var isFirstTime : Bool!
    
    var fromHome : Bool = false
    var postId : String!
    var postName : String!
    var postImageStr : String!
    var isSeller : Bool = true
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.groupOrUserImageButton.layer.cornerRadius = self.groupOrUserImageButton.frame.size.width / 2
        self.groupOrUserImageButton.clipsToBounds = true
        reloadMessagesView()
        
        if(self.fromHome){
            Helper.hidePI()
        }
        JSQMessagesCollectionViewCell.registerMenuAction(#selector(delete(_:)))
        
        self.inputToolbar.contentView?.leftBarButtonItem = nil
        
        NotificationCenter.default.addObserver(self, selector: #selector(catchNotification), name: NSNotification.Name(rawValue: "chatNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(catchPostNotification), name: NSNotification.Name(rawValue: "chatPostNotification"), object: nil)
        //chatPostNotification
        automaticallyScrollsToMostRecentMessage = true
        if self.checkDevice(){
//            super.viewWillAppear(true)
        }
        //        OperationQueue.main.addOperation {() -> Void in
        self.setup()
        //        }
        
    }
    
    @objc func catchNotification(notification:Notification) -> Void {
        
        let message = notification.userInfo
        if self.toUser != message?["from"] as? String{
            return
        }
        self.postId = message?["postId"] as? String
        self.messages.append(message?["message"] as! JSQMessage)
        self.finishReceivingMessage()
    }
    
    @objc func catchPostNotification(notification:Notification) -> Void {
        let message = notification.userInfo as! [String : Any]
        if self.toUser != message["from"] as? String {
            return
        }
        if let pName = message["postName"] as? String, let pID = message["postId"] as? String, let imageOfPost = message["postImage"] as? String {
            self.postId = pID
            self.postName = pName
//            self.postNameLabel.text = pName
            self.horseNameLabel?.text = pName
//            self.postImage.layer.cornerRadius = 5
//            self.postImage.clipsToBounds = true
//            self.postImage.kf.indicatorType = .activity
//            self.postImage.kf.indicator?.startAnimatingView()
//            self.postImage.kf.setImage(with: URL(string:imageOfPost),
//                                       placeholder: UIImage.init(named: "horse_default_image"),
//                                       options: [.transition(ImageTransition.fade(1))],
//                                       progressBlock: { receivedSize, totalSize in
//
//            },
//                                       completionHandler: { image, error, cacheType, imageURL in
//                                        self.postImage.kf.indicator?.stopAnimatingView()
//            })
            self.horseImageView?.kf.indicatorType = .activity
            self.horseImageView?.kf.indicator?.startAnimatingView()
            self.horseImageView?.kf.setImage(with: URL(string: imageOfPost),
                                             placeholder: UIImage.init(named: "horse_default_image"),
                                             options: [.transition(ImageTransition.fade(1))],
                                             progressBlock: { receivedSize, totalSize in
            },
                                             completionHandler: { image, error, cacheType, imageURL in
                                                self.horseImageView?.kf.indicator?.stopAnimatingView()
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
//        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.clear
        self.navigationController?.white()
//        self.postDetailWindowView()
        self.setUpHorseDetailsView()
    }
    
    
//    func postDetailWindowView() {
//        let window: UIWindow? = UIApplication.shared.keyWindow
//        var yPosition = 64.0;
//        if !checkDevice(){
//            yPosition = 108.0
//        }
//        postDetailsView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(yPosition), width: CGFloat(self.view.frame.size.width), height: CGFloat(60)))
////        postDetailsView.backgroundColor = UIColor(red: CGFloat(247.0 / 255.0), green: CGFloat(248.0 / 255.0), blue: CGFloat(247.0 / 255.0), alpha: CGFloat(1.0))
//        postDetailsView.backgroundColor = UIColor.white
//        window?.addSubview(postDetailsView)
//        window?.bringSubview(toFront: postDetailsView)
//
//        self.seperatotView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.view.frame.size.width), height: CGFloat(1)))
//        self.seperatotView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
//        postDetailsView.addSubview(self.seperatotView)
//
//        let gotoChat = UIButton(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.view.frame.size.width), height: CGFloat(60)))
//        gotoChat.addTarget(self, action: #selector(self.gotoDetails), for: .touchUpInside)
//        gotoChat.setTitle("", for: .normal)
//        gotoChat.backgroundColor = UIColor.clear
//        self.postDetailsView.addSubview(gotoChat)
//
//        postDetailsSubView = UIView(frame: CGRect(x: CGFloat(25), y: CGFloat(4), width: CGFloat(self.view.frame.size.width - 50), height: CGFloat(54)))
//        self.postDetailsSubView.backgroundColor = UIColor.init(red: 244.0/255.0, green: 246.0/255.0, blue: 248.0/255.0, alpha: 1)
//        self.postDetailsSubView.layer.cornerRadius = 5.0
//        self.postDetailsView.addSubview(postDetailsSubView)
//        self.postDetailsView.bringSubview(toFront: gotoChat)
//        self.postImage = UIImageView(frame: CGRect(x: CGFloat(10), y: CGFloat(4), width: CGFloat(46), height: CGFloat(46)))
//        self.postImage.layer.cornerRadius = 5
//        self.postImage.clipsToBounds = true
//        if let image = self.postImageStr{
//            self.postImage.kf.indicatorType = .activity
//            self.postImage.kf.indicator?.startAnimatingView()
//            postImage.kf.setImage(with: URL(string: image),
//                                  placeholder: UIImage.init(named: "horse_default_image"),
//                                  options: [.transition(ImageTransition.fade(1))],
//                                  progressBlock: { receivedSize, totalSize in
//            },
//                                  completionHandler: { image, error, cacheType, imageURL in
//                                    self.postImage.kf.indicator?.stopAnimatingView()
//            })
//        }
//        else{
//            postImage.image = UIImage(named: "horse_default_image")
//        }
//        self.postDetailsSubView.addSubview(self.postImage);
//
//        self.postNameLabel = UILabel(frame: CGRect(x: CGFloat(71), y: CGFloat(14), width: CGFloat(self.postDetailsSubView.frame.size.width), height: CGFloat(25)))
//        self.postDetailsSubView.addSubview(self.postNameLabel);
//        self.postNameLabel.text = self.postName
//
//    }

    
    @objc func gotoDetails (){
        
        performSegue(withIdentifier: "postDetails", sender: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
        let CBE = CouchbaseEvevnts()
        CBE.toRemoveUnreadMessage(document: self.document)
//        self.postDetailsView?.removeFromSuperview()
//        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UserDefaults.standard.set(true, forKey: "isOnChatScreen")
        UserDefaults.standard.set(self.toUser, forKey: "activeUser")
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        UserDefaults.standard.set(false, forKey: "isOnChatScreen")
        //        self.navigationController?.isNavigationBarHidden = true
    }
    
    // Preferred status bar style lightContent to use on dark background.
    // Swift 3
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "chatNotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "chatPostNotification"), object: nil)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadMessagesView() {
        
        let messageData = CouchbaseEvevnts.retriveData(document: self.document)
        self.toUser = messageData["to"] as! String
        if self.postId != nil{
            if self.postId != messageData["postId"] as? String{
                OperationQueue.main.addOperation {() -> Void in
                    
                    let CNC = CreateNewChat()
                    CNC.updatePostDetails(document: self.document, userId: Utility.getUserID(), postId: self.postId)
                }
            }
            
        }else{
            self.postId = messageData["postId"] as! String
        }
        self.postName = messageData["postName"] as! String
        if let image = messageData["postImage"]{
            self.postImageStr = image as! String
        }
        
        if messageData["type"] as! String == "Seller"{
            self.isSeller = true
        }
        else{
            self.isSeller = false
        }
        if((messageData["message"]) != nil){
            self.isCreated = true
        }
        else{
            
            return
        }
        
        let totalData = messageData["message"] as! [[String : String]]
        var messageCount : Int = 0
        if(totalData.count > 30){
            self.showLoadEarlierMessagesHeader = true
            messageCount = totalData.count - 30
        }
        else{
            messageCount = 0
            self.showLoadEarlierMessagesHeader = false
        }
        self.getMessageView(totalData: totalData, messageCount: messageCount)
    }
    
    func getMessageView(totalData:[[String : String]],messageCount: Int){
        
        self.messages.removeAll()
        for i in messageCount..<totalData.count{
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateFormatter.timeZone = NSTimeZone.init(abbreviation: "UTC") as TimeZone!
            let date = dateFormatter.date(from: totalData[i]["date"]!)
            if(totalData[i]["type"] == "0"){
                
                let message = JSQMessage(senderId: totalData[i]["senderId"]!, senderDisplayName: senderDisplayName(), date: date!, text: totalData[i]["text"]!, messageId: "\(totalData[i]["messageId"]!)")
                self.messages.append(message)
            }
            
        }
        
        self.collectionView?.reloadData()
        OperationQueue.main.addOperation {() -> Void in
            
            let CBE = CouchbaseEvevnts()
            CBE.toRemoveUnreadMessage(document: self.document)
        }
    }
    
    func checkDevice()->Bool{
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136, 13334, 1920:
//                print("iPhone 5 or 5S or 5C")
//            case 1334:
//                print("iPhone 6/6S/7/8")
//            case 1920, 2208:
//                print("iPhone 6+/6S+/7+/8+")
                return true
            case 2436:
//                print("iPhone X")
                return false
            default:
//                print("unknown")
                return true
            }
        }
        return true
    }
    
    //MARK: -  Buttons Action
    @IBAction func UserNameTapAction(_ sender: Any) {
        
        //        performSegue(withIdentifier: "settingSegue", sender: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //        if(segue.identifier == "openImage"){
        //
        //            let imageClass = segue.destination as! OpenSelectedImage
        //            imageClass.dict = sender as! [String : AnyObject]
        //        }
        //        else
        if(segue.identifier == "postDetails"){
            
            let vc = segue.destination as! PostDetailsViewController
            vc.postId = self.postId
            vc.postName = self.postName
        }
        
    }
    
    @IBAction func groupOrUserImageButtonTap(_ sender: Any) {
        
        let clearChatAlert = UIAlertController.init(title: "Warning", message: "Your chat with this user will be deleted", preferredStyle: .alert)
        clearChatAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            self.becomeFirstResponder()
            self.toClearAllChat()
            
        }))
        clearChatAlert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { action in
            self.becomeFirstResponder()
        }))
        DispatchQueue.main.async {
            
            self.present(clearChatAlert, animated: true, completion: {
                
            })
        }
    }
    
    override func clearAllAction(_ sender: Any){
        let clearChatAlert = UIAlertController.init(title: "Warning", message: "Your chat with this user will be deleted", preferredStyle: .alert)
        clearChatAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            self.becomeFirstResponder()
            self.toClearAllChat()
            
        }))
        clearChatAlert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { action in
            self.becomeFirstResponder()
        }))
        DispatchQueue.main.async {
            
            self.present(clearChatAlert, animated: true, completion: {
                
            })
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        if fromHome {
            
            let mainStoryboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "RAMAnimatedTabBarController") as! RAMAnimatedTabBarController
            vc.selectedIndex = 1
            vc.setSelectIndex(from: 0, to: 1)
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func backAction(_ sender: Any) {
        if fromHome {
            
            let mainStoryboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "RAMAnimatedTabBarController") as! RAMAnimatedTabBarController
            vc.selectedIndex = 1
            vc.setSelectIndex(from: 0, to: 1)
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func postDetailsButton(_ sender: Any) {
        
    }
    
    func getCurrentTimeInString()->String{
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.init(abbreviation: "UTC") as TimeZone!
        let stringDate = dateFormatter.string(from: date)
        return stringDate
    }
    
    func setUpHorseDetailsView(){
        
        if let image = self.postImageStr{
            
            self.horseImageView?.kf.indicatorType = .activity
            self.horseImageView?.kf.indicator?.startAnimatingView()
            self.horseImageView?.kf.setImage(with: URL(string: image),
                                  placeholder: UIImage.init(named: "horse_default_image"),
                                  options: [.transition(ImageTransition.fade(1))],
                                  progressBlock: { receivedSize, totalSize in
            },
                                  completionHandler: { image, error, cacheType, imageURL in
                                    self.horseImageView?.kf.indicator?.stopAnimatingView()
            })
        }
        else{
            self.horseImageView?.image = UIImage(named: "horse_default_image")
        }
        self.horseNameLabel?.text = self.postName
    }
    
    override func horseDetailsAction(_ sender: Any) {
        performSegue(withIdentifier: "postDetails", sender: nil)
    }
}

//MARK: - Setup
extension ActiveChatViewController {
    
    func setup() {
        
//        self.chatPersonImage.layer.cornerRadius = self.chatPersonImage.frame.size.width / 2
//        self.chatPersonImage.clipsToBounds = true
//        self.chatPersonImage.kf.indicatorType = .activity
//        self.chatPersonImage.kf.indicator?.startAnimatingView()
//        self.chatPersonImage.kf.setImage(with: URL(string: self.groupOrUserImageURL! as String),
//                                         placeholder: UIImage.init(named: "profile_defalt_image"),
//                                         options: [.transition(ImageTransition.fade(1))],
//                                         progressBlock: { receivedSize, totalSize in
//                                            //    print("\(receivedSize)/\(totalSize)")
//        },
//                                         completionHandler: { image, error, cacheType, imageURL in
//                                            self.chatPersonImage.kf.indicator?.stopAnimatingView()
//        })
        
        self.receiverImageView?.layer.cornerRadius = (self.receiverImageView?.frame.size.width)! / 2
        self.receiverImageView?.clipsToBounds = true
        self.receiverImageView?.kf.indicatorType = .activity
        self.receiverImageView?.kf.indicator?.startAnimatingView()
        self.receiverImageView?.kf.setImage(with: URL(string: self.groupOrUserImageURL! as String),
                                         placeholder: UIImage.init(named: "profile_defalt_image"),
                                         options: [.transition(ImageTransition.fade(1))],
                                         progressBlock: { receivedSize, totalSize in
                                            //    print("\(receivedSize)/\(totalSize)")
        },
                                         completionHandler: { image, error, cacheType, imageURL in
                                            self.receiverImageView?.kf.indicator?.stopAnimatingView()
        })
        self.backImageView?.image = #imageLiteral(resourceName: "back_btn_off")
        self.receiverNameButton?.setTitle(self.receiverNameString, for: .normal)
        collectionView?.collectionViewLayout.incomingAvatarViewSize = CGSize(width:0.0, height:0.0)//
        collectionView?.collectionViewLayout.outgoingAvatarViewSize = CGSize(width:0.0, height:0.0)
        collectionView?.collectionViewLayout.springinessEnabled = false
        self.userNameButtonWidth.constant = self.view.frame.size.width - 240.0;

    }
}

//MARK: - Data Source
extension ActiveChatViewController {
    
    override func senderId() -> String {
        
        return (UIDevice.current.identifierForVendor?.uuidString)!
    }
    
    override func senderDisplayName() -> String {
        
        return (UIDevice.current.identifierForVendor?.uuidString)!
        
    }
    
    //MARK: Number of items in view
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    //MARK: Data on cell
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, messageDataForItemAt indexPath: IndexPath) -> JSQMessageData {
        
        let data = self.messages[indexPath.row]
        return data
    }
    //MARK: After deletion of message at index path
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, didDeleteMessageAt indexPath: IndexPath) {
        
        self.toDeleteMessageWith(indexPath:indexPath)
        
    }
   
    //MARK: To configure outgong or incomming bubble
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, messageBubbleImageDataForItemAt indexPath: IndexPath) -> JSQMessageBubbleImageDataSource? {
        
        return (messages[indexPath.item].senderId == senderId()) ? outgoingBubble : incomingBubble
        
    }
    
    //MARK: For Avatar Images
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, avatarImageDataForItemAt indexPath: IndexPath) -> JSQMessageAvatarImageDataSource? {
        
        return nil
        
    }
    
    //MARK: To show name above message
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        
        // Displaying names above messages
        //Mark: Removing Sender Display Name
        /**
         *  Example on showing or removing senderDisplayName based on user settings.
         *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
         */
        return nil
    }
    
    //MARK: To show timestamp above message
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForCellTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        
        /**
         *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
         *  The other label text delegate methods should follow a similar pattern.
         *
         *  Show a timestamp for every 3rd message
         */
        
        if(indexPath.item == 0){
            let message = self.messages[indexPath.item]
            return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
        }
        else{
            
            let currentMessage = self.messages[indexPath.item]
            let previousMessage = self.messages[indexPath.item - 1]
            
            if compareDates(currentDate: currentMessage.date, previousDate: previousMessage.date){
                return nil
            }else{
                let message = self.messages[indexPath.item]
                return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
            }
        }
        
        
        
    }
    
    func compareDates(currentDate:Date, previousDate:Date)->Bool{
        
        let calendar = Calendar.current
        
        let currentYear = calendar.component(.year, from: currentDate )
        let currentMonth = calendar.component(.month, from: currentDate)
        let currentDay = calendar.component(.day, from: currentDate)
        
        let previousYear = calendar.component(.year, from: previousDate)
        let previousMonth = calendar.component(.month, from: previousDate)
        let previousDay = calendar.component(.day, from: previousDate)
        
        if currentYear == previousYear && currentMonth == previousMonth && currentDay == previousDay{
            return true
        }
        else{
            return false
        }
        
    }
    
    
    
    //MARK: Height of cell to show timestamp
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellTopLabelAt indexPath: IndexPath) -> CGFloat {
        
        /**
         *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
         */
        
        /**
         *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
         *  The other label height delegate methods should follow similarly
         *
         *  Show a timestamp for every 3rd message
         */
        
        if(indexPath.item == 0){
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        else {
            let currentMessage = self.messages[indexPath.item]
            let previousMessage = self.messages[indexPath.item - 1]
            
            if compareDates(currentDate: currentMessage.date, previousDate: previousMessage.date){
                return 0.0
            }else{
                return kJSQMessagesCollectionViewCellLabelHeightDefault
            }
        }
        
    }
    
    //MARK:To show sender name
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForMessageBubbleTopLabelAt indexPath: IndexPath) -> CGFloat {
        
        /**
         *  Example on showing or removing senderDisplayName based on user settings.
         *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
         */
        
        /**
         *  iOS7-style sender name labels
         */
        let currentMessage = self.messages[indexPath.item]
        
        if currentMessage.senderId == self.senderId() {
            return 0.0
        }
        
        if indexPath.item - 1 > 0 {
            let previousMessage = self.messages[indexPath.item - 1]
            if previousMessage.senderId == currentMessage.senderId {
                return 0.0
            }
        }
        
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    
    //MARK: Responding to collection view tap events
    override func collectionView(_ collectionView: JSQMessagesCollectionView, didTapMessageBubbleAt indexPath: IndexPath) {
        
        //        let messageItem = collectionView.dataSource?.collectionView(collectionView, messageDataForItemAt: indexPath)
        
        self.inputToolbar.contentView?.textView?.resignFirstResponder()
        
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, didTapCellAt indexPath: IndexPath, touchLocation: CGPoint) {
        
        self.inputToolbar.contentView?.textView?.resignFirstResponder()
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, header headerView: JSQMessagesLoadEarlierHeaderView, didTapLoadEarlierMessagesButton sender: UIButton) {
        
        //        print("To respond load earlier message button")
        self.showLoadEarlierMessagesHeader = false
        let messageData = CouchbaseEvevnts.retriveData(document: self.document)
        let totalData = messageData["message"] as! [[String : String]]
        self.getMessageView(totalData: totalData, messageCount: 0)
    }
    
    func updateLastMessageAndTime(){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.init(abbreviation: "UTC") as TimeZone!
        
        if self.messages.count == 0{
            
            let strDate = dateFormatter.string(from: Date())
            CouchbaseEvevnts.updateDocumentForLastMessageAndTime(document: document, lastMessage: "No message", time: strDate)
            
            return
            
        }
        let lastMessageIndex = self.messages.count - 1
        let stringDate = dateFormatter.string(from: self.messages[lastMessageIndex].date)
        
        CouchbaseEvevnts.updateDocumentForLastMessageAndTime(document: document,
                                                             lastMessage: self.messages[lastMessageIndex].text,
                                                             time: stringDate)
    }
    
}


//MARK: - Toolbar
extension ActiveChatViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    override func didPressSend(_ button: UIButton, withMessageText text: String, senderId: String, senderDisplayName: String, date: Date) {
        let vc = networkRechabilityClass()
        
        if vc.connectedToNetwork() != true{
            let noInternetAlert = UIAlertController.init(title: "No Internet", message: "Please check your internet connection", preferredStyle: .alert)
            noInternetAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                // do something like...
                self.becomeFirstResponder()
            }))
            
            DispatchQueue.main.async {
                
                self.present(noInternetAlert, animated: true, completion: {
                    
                })
            }
        }
        else{
            self.messageId = NSDate.timeIntervalSinceReferenceDate
            let message = JSQMessage(senderId: senderId, senderDisplayName: senderDisplayName, date: date as Date, text: text, messageId: "\(messageId)")
            self.messages += [message]
            self.finishSendingMessage()
            
            let docId = document.documentID
            let base64String = self.base64Encoded(textMessage: text)
            
            if self.isSeller{
                
                ChatSocketIOClient.sharedInstance.sendMessage(message: base64String,fromUser: Utility.getUserID(), toUser:self.toUser, docId: docId, messageType: "0",postId: self.postId,seller: 1, messageId:self.messageId)
            }else{
                ChatSocketIOClient.sharedInstance.sendMessage(message: base64String,fromUser: Utility.getUserID(), toUser:self.toUser, docId: docId, messageType: "0",postId: self.postId,seller: 2, messageId:self.messageId)
            }
            
            if(self.isCreated == true ){
                OperationQueue.main.addOperation {() -> Void in
                    
                    let vc = CouchbaseEvevnts()
                    vc.updateDocument(document: self.document, messageArray: message, postId: self.postId)
                }
                
            }
            else{
                //                OperationQueue.main.addOperation {() -> Void in
                
                let vc = CouchbaseEvevnts()
                self.isCreated = vc.cerateDocument(document: self.document, receivingUser: "Server", sendingUser: self.fromUser, messageArray: message, postId: self.postId)
                //                }
                
            }
            OperationQueue.main.addOperation {() -> Void in
                
                let lastTime = self.getCurrentTimeInString()
                CouchbaseEvevnts.updateDocumentForLastMessageAndTime(document: self.document, lastMessage: text, time: lastTime)
            }
            
        }
    }
    
    
}

//MARK: Web services
extension ActiveChatViewController{
    
    func toClearAllChat(){
        
        var params = [String : AnyObject]()
        
        params["from"] = Utility.getUserID()  as AnyObject?
        params["to"] = self.toUser  as AnyObject?
        params["postId"] = self.postId  as AnyObject?
        Helper.showPI(_message: "Deleting ...")
        AFWrapper.requestPOSTURLForChat(serviceName: Constant.ServiceMethodNames.deleteMessage,
                                        params: params,
                                        success: { (response) in
                                            
                                            CouchbaseEvevnts.deleteChatOnly(document : self.document)
                                            self.messages.removeAll()
                                            self.collectionView?.reloadData()
                                            
        },
                                        failure: { (Error) in
//                                            print(Error)
        })
        
    }
    
    func toDeleteMessageWith(indexPath: IndexPath){
        
        let messageIdToRemove = self.messages[indexPath.row].messageId
        
        var params = [String : AnyObject]()
        params["from"] = Utility.getUserID()  as AnyObject?
        params["to"] = self.toUser  as AnyObject?
        params["postId"] = self.postId  as AnyObject?
        params["messageId"] = messageIdToRemove as AnyObject?
        
        //        Helper.showPI()
        AFWrapper.requestPOSTURLForChat(serviceName: Constant.ServiceMethodNames.deleteMessage,
                                        params: params,
                                        success: { (response) in
                                            
                                            self.messages.remove(at: indexPath.row)
                                            self.collectionView?.deleteItems(at: [indexPath])
                                            let vc = CouchbaseEvevnts()
                                            vc.deleteSingleMessage(document: self.document, messageId: messageIdToRemove)
                                            self.updateLastMessageAndTime()
                                            
        },
                                        failure: { (Error) in
//                                            print(Error)
        })
        
    }
    
}



//MARK: To Encode and Decode into Base64
extension ActiveChatViewController{
    
    func base64Encoded(textMessage: String) -> String {
        
        let plainData = textMessage.data(using: String.Encoding.utf8)
        let base64String = plainData?.base64EncodedString(options: .lineLength64Characters)
        return base64String!
    }
    
    func base64Decoded(bsae64String: String) -> String {
        
        let decodedData:NSData = NSData(base64Encoded: bsae64String, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)!
        let decodedString = NSString(data: decodedData as Data, encoding: String.Encoding.utf8.rawValue)
        return decodedString! as String
    }
    var safeTopAnchor: NSLayoutYAxisAnchor {
        if #available(iOS 11.0, *) {
            return view.safeAreaLayoutGuide.topAnchor
        } else {
            return topLayoutGuide.bottomAnchor
        }
    }
}

extension JSQMessagesInputToolbar {
    override open func didMoveToWindow() {
        super.didMoveToWindow()
        if #available(iOS 11.0, *) {
            if self.window?.safeAreaLayoutGuide != nil {
                self.bottomAnchor.constraintLessThanOrEqualToSystemSpacingBelow((self.window?.safeAreaLayoutGuide.bottomAnchor)!, multiplier: 1.0).isActive = true
            }
        }
    }
}
