//
//  AboutMeViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 28/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

protocol saveButtonAction {
    
    func saveButtonPressed(message:String)
}

class AboutMeViewController: UIViewController {
    
    var delegate:saveButtonAction! = nil
    @IBOutlet weak var textViewOutlet: UITextView!
    var textExistingData :String?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textViewOutlet.becomeFirstResponder()
        
        if let aboutMe = textExistingData{
            self.textViewOutlet.text = aboutMe
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        if !(self.delegate == nil)
        {
            self.delegate.saveButtonPressed(message: textViewOutlet.text)
            self.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func closeButtonAction(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
