//
//  EditProfileTableViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 28/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftyJSON

protocol ButtonActionsDelegates {
    
    func buttonPressedWithIndex(index : Int)
}

class EditProfileTableViewController: UITableViewController, saveButtonAction {
    
    @IBOutlet weak var farmNameOutlet: UILabel!
    @IBOutlet weak var phoneNumberOutlet: UILabel!
    @IBOutlet var tableViewOutlet: UITableView!
    @IBOutlet weak var imageIVewOutlet: UIImageView!
    @IBOutlet weak var emailIdOutlet: UITextField!
    @IBOutlet weak var birthDateOutlet: UILabel!
    @IBOutlet weak var genderOutlet: UILabel!
    @IBOutlet weak var firstNameOutlet: UITextField!
    @IBOutlet weak var loactionOutet: UITextField!
    @IBOutlet weak var aboutMeOutlet: UILabel!
    @IBOutlet weak var lastNameOutlet: UITextField!
    var ButtonActionsDelegates:ButtonActionsDelegates! = nil
    var profilePicUrl = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageIVewOutlet.image = #imageLiteral(resourceName: "profile_default")
        
        self.tableView.estimatedRowHeight = 70
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.endEditing(true)
        if let phoneNum = UserDefaults.standard.value(forKey: "phoneNum") as? String{
            self.phoneNumberOutlet.text = phoneNum
            UserDefaults.standard.removeObject(forKey: "phoneNum")
            UserDefaults.standard.synchronize()
            self.tableView.reloadData()
        }
        if let dictionary = UserDefaults.standard.value(forKey: "selectedValue") as? [String: AnyObject]
        {
            if let data = dictionary["options"] as? [Any]
            {
                if data.count == 0{
                    self.farmNameOutlet.text = " "
                }
                else
                {
                    if let farmData = data[0] as? [String: Any]
                    {
                        self.farmNameOutlet.text = farmData["farmName"] as? String
                    }
                }
            }
            UserDefaults.standard.removeObject(forKey: "selectedValue")
            UserDefaults.standard.synchronize()
            self.tableView.reloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    func setDataToCell(dataDict : [String:JSON])
    {
        var url = URL(string: "")
        if let urlStr = dataDict["profilePic"]?.string
        {
            url = URL(string: urlStr)
        }
        
        self.imageIVewOutlet.kf.indicatorType = .activity
        self.imageIVewOutlet.kf.indicator?.startAnimatingView()
        self.imageIVewOutlet.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "profile_default"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
            self.imageIVewOutlet.kf.indicator?.stopAnimatingView()
        })
        
        
        firstNameOutlet.text = (dataDict["fName"]?.stringValue)
        lastNameOutlet.text = (dataDict["lName"]?.stringValue)
        if let about = dataDict["about"]?.string
        {
            aboutMeOutlet.text = about
        }
        if let genderStr = dataDict["gender"]?.string
        {
            var gender = 0
            
            if genderStr.characters.count != 0
            {
                gender = Int(genderStr)!
            }
            switch gender {
            case 1:
                genderOutlet.text = "Male"
                break
            case 2 :
                genderOutlet.text = "Female"
                break
            case 3:
                genderOutlet.text = "Other"
                break
            default:
                genderOutlet.text = "Not Specified"
            }
        }
        if let bDate = dataDict["dob"]?.string
        {
            birthDateOutlet.text = bDate
        }
        if let email = dataDict["email"]?.string
        {
            emailIdOutlet.text = email
        }
        if let location = dataDict["location"]?.string
        {
            loactionOutet.text = location
        }
        if let farmName = dataDict["farmName"]?.string
        {
            farmNameOutlet.text = farmName
        }
        if let phone = dataDict["phone"]?.string
        {
            self.phoneNumberOutlet.text = phone
        }
        
        self.tableView.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func genderAction(_ sender: Any) {
        
        if((self.ButtonActionsDelegates) != nil)
        {
            self.ButtonActionsDelegates.buttonPressedWithIndex(index:1)
        }
    }
    
    @IBAction func cameraAction(_ sender: Any)
    {
        if((self.ButtonActionsDelegates) != nil)
        {
            self.ButtonActionsDelegates.buttonPressedWithIndex(index:0)
        }
    }
    
    @IBAction func birthDateAction(_ sender: Any) {
        if((self.ButtonActionsDelegates) != nil)
        {
            self.ButtonActionsDelegates.buttonPressedWithIndex(index:2)
        }
    }
    
    @IBAction func editButtonAction(_ sender: Any)
    {
        self.performSegue(withIdentifier: "About Me Segue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "About Me Segue"
        {
            let controller:AboutMeViewController = segue.destination as! AboutMeViewController
            controller.delegate = self
            if let about = self.aboutMeOutlet.text
            {
                controller.textExistingData = about
            }
        }
        else if segue.identifier == "phoneNumberSegue"{
            let navController = segue.destination as! UINavigationController
            let controller:PhoneNumberVerificationViewController = navController.childViewControllers.first as! PhoneNumberVerificationViewController
            controller.phoneNumberString = self.phoneNumberOutlet.text!
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func saveButtonPressed(message: String)
    {
        aboutMeOutlet.text = message
        self.tableView.reloadData()
    }
    // MARK: Keyboard Notifications
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tableView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight/4, 0)
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    @IBAction func phoneBtnAction(_ sender: Any)
    {
    }
    
    func saveButtonAction()
    {
        Helper.showPI(_message: "Saving...")
        var genderType = ""
        let gender:String = (genderOutlet.text?.capitalizingFirstLetter())!
        switch gender {
        case "Male":
            genderType = "1"
            break
        case "Female":
            genderType = "2"
            break
        case "Other":
            genderType = "3"
            break
        default:
            genderType = "0"
        }
        
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject,
                      "dob":birthDateOutlet.text! as AnyObject,
                      "email":emailIdOutlet.text! as AnyObject,
                      "fName":firstNameOutlet.text! as AnyObject,
                      "lName":lastNameOutlet.text! as AnyObject,
                      "gender":genderType as AnyObject,
                      "about":aboutMeOutlet.text! as AnyObject,
                      "work":"" as AnyObject,
                      "phone":self.phoneNumberOutlet.text! as AnyObject,
                      "profilePic":self.profilePicUrl as AnyObject,
                      "farmName":farmNameOutlet.text! as AnyObject,
                      "location":loactionOutet.text! as AnyObject]
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.editProfile,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                        
                                    case 0:
                                        
                                        var profileImage = [String : String]()
                                        profileImage["image"] = self.profilePicUrl;
                                        UserDefaults.standard.setValue(profileImage, forKey: "profileImage")
                                        
                                        self.showAlertWithDismissal("Message", message: response["Message"].string!)
                                        break
                                        
                                    default:
                                        
                                        self.showAlert("Error", message: response["Message"].string!)
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
        
    }
}

extension EditProfileTableViewController : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

