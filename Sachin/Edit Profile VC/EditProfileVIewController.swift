//
//  EditProfileVIewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 28/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import SwiftyJSON
import MobileCoreServices

class EditProfileVIewController: UIViewController,ButtonActionsDelegates {
    
    @IBOutlet var datePickeroutlet: UIDatePicker!
    let genderArray = ["Male","Female","Other"]
    var picker = UIImagePickerController()
    @IBOutlet var genderPickerOutlet: UIPickerView!
    @IBOutlet var popUpViewOutlet: UIView!
    @IBOutlet weak var bottomConstraints: NSLayoutConstraint!
    @IBOutlet weak var containerViewOutlet: UIView!
    var dataDict = [String : JSON]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideAnimationView()
        let editProfileTVC = self.childViewControllers.first as! EditProfileTableViewController
        editProfileTVC.setDataToCell(dataDict : dataDict)
        editProfileTVC.ButtonActionsDelegates = self
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func SaveButtonAction(_ sender: Any)
    {
        let editProfileTVC = self.childViewControllers.first as! EditProfileTableViewController
        editProfileTVC.saveButtonAction()
        
    }
    
    @IBAction func closeBtnAction(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func buttonPressedWithIndex(index : Int)
    {
        
        switch index {
        case 0:
            openCameraAction()
            break
        case 1:
            showAnimateView(index: 1)
            break
        case 2:
            showAnimateView(index: 2)
            break
        default:
            break
        }
    }
    
    @IBAction func cancelButtonAction(_ sender: Any)
    {
        hideAnimationView()
    }
    
    @IBAction func doneButtonAction(_ sender: Any)
    {
        let editProfileTVC = self.childViewControllers.first as! EditProfileTableViewController
        if self.genderPickerOutlet.isHidden
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM yyyy"
            let selectedDate = dateFormatter.string(from: self.datePickeroutlet.date)
            editProfileTVC.birthDateOutlet.text = selectedDate
        }
        else
        {
            let rowNum = self.genderPickerOutlet.selectedRow(inComponent: 0)
            editProfileTVC.genderOutlet.text = self.genderArray[rowNum]
        }
        hideAnimationView()
        
    }
    
    func showAnimateView(index : Int){
        
        if(index == 2)
        {
            self.genderPickerOutlet.isHidden = true
            self.datePickeroutlet.isHidden = false
        }
        else
        {
            self.datePickeroutlet.isHidden = true
            self.genderPickerOutlet.isHidden = false
        }
        UIView.animate(withDuration: 0.4, animations:
            {
                self.bottomConstraints.constant = self.view.frame.size.height - 250
                self.view.layoutIfNeeded()
        } , completion:nil)
    }
    
    func hideAnimationView(){
        
        self.datePickeroutlet.isHidden = true
        self.genderPickerOutlet.isHidden = true
        UIView.animate(withDuration: 0.4, animations:
            {
                self.bottomConstraints.constant = self.view.frame.size.height + 10
                self.view.layoutIfNeeded()
        } , completion:nil)
    }
    
    func openCameraAction(){
        
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        // Added actions
        picker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.mediaTypes = [kUTTypeImage as String]
            self.present(picker, animated: true, completion: nil)
        }
        else
        {
            self.showAlert("Warning", message: "You don't have camera")
        }
    }
    func openGallary()
    {
        picker.sourceType = .photoLibrary
        picker.mediaTypes = [kUTTypeImage as String]
        self.present(picker, animated: true, completion: nil)
    }
}

extension EditProfileVIewController : UIPickerViewDelegate, UIPickerViewDataSource
{
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 3
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderArray[row]
    }
}

extension EditProfileVIewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate, photoCropped{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.dismiss(animated: true, completion:{
                let shittyVC = ShittyImageCropVC(frame: (self.view.frame), image: image.fixOrientation(), aspectWidth: 3, aspectHeight: 2)
                self.present(shittyVC, animated: true, completion: nil)
                shittyVC.delegate = self
            })
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
//        print("picker cancel.")
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func resizeImage(_ image: UIImage,_ targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    func photoSaved(image: UIImage) {
        
        Helper.showPI(_message: "Uploading Image")
        let helperInstance = CloudinaryHelper.sharedInstance
        helperInstance.uploadImage(image:image.fixOrientation(), onCompletion: { (success, cloudinaryUrl) in
            
            if success
            {
                let editProfileTVC = self.childViewControllers.first as! EditProfileTableViewController
                editProfileTVC.profilePicUrl = cloudinaryUrl!
                editProfileTVC.imageIVewOutlet.image = image.fixOrientation()
                Helper.hidePI()
            }
            else
            {
                Helper.hidePI()
                self.showAlert("Error", message: "Image size is too large !!!")
                return
            }
        })
    }
}
