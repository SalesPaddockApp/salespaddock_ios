//
//  ShowDetailVC.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 23/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftyJSON


class ShowDetailVC: UIViewController {
    
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var numberOfFiltersOutlet: UILabel!
    @IBOutlet var gradiantViewOutlet: GradientView!
    @IBOutlet var navBackButtonOutlet: UIButton!
    @IBOutlet var navViewOutlet: UIView!
    @IBOutlet weak var tableViewOutlet: UITableView!
    var horseArray = [Any]()
    var postDictionary = [String:AnyObject]()
    var showId = String()
    var filterData = [AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewOutlet.estimatedRowHeight = 270
        tableViewOutlet.rowHeight = UITableViewAutomaticDimension
        
        if let filterValue = UserDefaults.standard.object(forKey:"FiltersData") as? [String:Any]{
            filterData = filterValue["filters"] as! [AnyObject]
            self.numberOfFiltersOutlet.text = "\(filterData.count)"
            self.getShowDetails(isFilteredApplied: 1, data: filterData)
        }
        else{
            filterData.removeAll()
            self.getShowDetails(isFilteredApplied: 0, data: filterData)
        }
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func dismissVIewAction(_ sender: Any)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func openFilters(_ sender: Any) {
        self.performSegue(withIdentifier: "filtersSegue", sender: filterData)
    }
    
    func getShowDetails(isFilteredApplied: Int, data: [AnyObject]) {
        
        Helper.showPI()
        
        var params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject,
                      "showId":postDictionary["showId"]!] as [String : AnyObject]
        if isFilteredApplied == 1
        {
            params["filters"] = data as AnyObject
        }
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getShowDetails,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                        
                                    case 0:
                                        
                                        let responseArray = (response["response"]).arrayObject
                                        if (responseArray != nil) {
                                            self.horseArray = responseArray! as [Any]
                                        }
                                        if let filterCount = response["countOfFilters"].int{
                                            if filterCount>0{
                                                self.numberOfFiltersOutlet.text = "\(filterCount)"
                                                self.colorView.isHidden = false
                                                self.colorView.backgroundColor = Color.init(hexString: "#0bb39dff")
                                                let filterData:[String:Any] = ["filters":response["filters"].arrayObject!]
                                                UserDefaults.standard.setValue(filterData, forKey: "FiltersData")
                                                self.filterData = response["filters"].arrayObject! as [AnyObject]
                                            }
                                            else{
                                                self.colorView.backgroundColor = Color.clear
                                                self.numberOfFiltersOutlet.text = ""
                                                self.colorView.isHidden = true
                                                UserDefaults.standard.setValue("", forKey: "FiltersData")
                                            }
                                            UserDefaults.standard.synchronize()
                                        }
                                        self.tableViewOutlet.reloadData()
                                        break
                                        
                                    default:
                                        
//                                        let errMsg = response["Message"].string
//                                        self.showAlert("Error", message: errMsg!)
                                        if let filterCount = response["countOfFilters"].int{
                                            if filterCount>0{
                                                self.numberOfFiltersOutlet.text = "\(filterCount)"
                                                self.colorView.isHidden = false
                                                self.colorView.backgroundColor = Color.init(hexString: "#0bb39dff")
                                                let filterData:[String:Any] = ["filters":response["filters"].arrayObject!]
                                                UserDefaults.standard.setValue(filterData, forKey: "FiltersData")
                                                self.filterData = response["filters"].arrayObject! as [AnyObject]
                                            }
                                            else{
                                                self.colorView.backgroundColor = Color.clear
                                                self.numberOfFiltersOutlet.text = ""
                                                self.colorView.isHidden = true
                                                UserDefaults.standard.setValue("", forKey: "FiltersData")
                                            }
                                            UserDefaults.standard.synchronize()
                                            self.horseArray.removeAll()
                                            self.tableViewOutlet.reloadData()
                                        }
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    Helper.hidePI()
//                                    self.showAlert("Error", message: Error.localizedDescription)
        })
        
    }
}

extension ShowDetailVC :UITableViewDelegate, UITableViewDataSource
{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView  == tableViewOutlet
        {
            let offsetY: CGFloat = scrollView.contentOffset.y
            
            if(offsetY > 200)
            {
                navBackButtonOutlet.setImage(Image.init(named: "back_btn_off"), for: .normal)
                self.gradiantViewOutlet.isHidden = true
                let alpha: CGFloat = min(1, 1 - ((64 + 200 - offsetY) / 200))
                navViewOutlet.backgroundColor = Color.white.withAlphaComponent(alpha)
            }
            else if (offsetY < 200)
            {
                navBackButtonOutlet.setImage(#imageLiteral(resourceName: "back_btn_wight_off"), for: .normal)
                self.gradiantViewOutlet.isHidden = false
                navViewOutlet.backgroundColor = UIColor.clear
            }
            else
            {
                navBackButtonOutlet.setImage(Image.init(named: "back_btn_wight_off"), for: .normal)
                self.gradiantViewOutlet.isHidden = false
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
            
        case 0:
            
            let cell = tableView.dequeueReusableCell(withIdentifier:"DetailsHeaderTableViewCell", for: indexPath) as? DetailsHeaderTableViewCell
            
            if (postDictionary["showName"] != nil)
            {
                cell?.showNameOutlet.text = postDictionary["showName"] as? String
            }
            
            if let imageURl = postDictionary["image"] as? String
            {
                let url = URL(string: imageURl)
                
                cell?.imageViewOutlet.kf.indicatorType = .activity
                cell?.imageViewOutlet.kf.indicator?.startAnimatingView()
                
                cell?.imageViewOutlet.kf.setImage(with: url,
                                                  placeholder: UIImage.init(named: "horse_default_image"),
                                                  options: [.transition(ImageTransition.fade(1))],
                                                  progressBlock: { receivedSize, totalSize in
                },
                                                  completionHandler: { image, error, cacheType, imageURL in
                                                    cell?.imageViewOutlet.kf.indicator?.stopAnimatingView()

                })
            }
            else if let imageURl = postDictionary["image"] as? URL
            {
                let url = imageURl
                
                cell?.imageViewOutlet.kf.indicatorType = .activity
                cell?.imageViewOutlet.kf.indicator?.startAnimatingView()
                
                cell?.imageViewOutlet.kf.setImage(with: url,
                                                  placeholder: UIImage.init(named: "horse_default_image"),
                                                  options: [.transition(ImageTransition.fade(1))],
                                                  progressBlock: { receivedSize, totalSize in
                },
                                                  completionHandler: { image, error, cacheType, imageURL in
                                                    cell?.imageViewOutlet.kf.indicator?.stopAnimatingView()
                })
            }
            
            cell?.descriptionTextViewOutlet.text = "\(postDictionary["Description"] as! String)"
            
            if let dateString = postDictionary["startDate"] as? String
            {
                cell?.heldOnDateOutlet.text = dateString
            }
            
            return cell!
        case 1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.userNameTabelViewCell, for: indexPath) as? UserNameTableViewCell
            return cell!
            
        default:
            
            let cell = tableView.dequeueReusableCell(withIdentifier:"HorseDetailsTableViewCell", for: indexPath) as? HorseDetailsTableViewCell
            
            cell?.loadCell(dictionary: horseArray[indexPath.row-2] as! [String: AnyObject])
            cell?.currentIndex = Int(indexPath.row-2)
            cell?.OpenUserDetailPageDelegate = self
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 + horseArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
            
        case 0: // Participating Shows
            
            break
            
        case 1: // Popular Horses
            
            break
            
            
        default: // Recently Viewed Details
            performSegue(withIdentifier: "horseDetailSegue", sender: horseArray[indexPath.row - 2] as! [String: AnyObject])
            break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let segueIdentifier:String = segue.identifier!
        
        switch segueIdentifier {
            
        case "horseDetailSegue":
            
            
            var dict = sender as! [String: AnyObject]
            let newdict = ["showId": postDictionary["showId"]!] as [String: AnyObject]
            
            for key in newdict.keys {
                dict[key] = newdict[key]
            }
            
            let HDVC = segue.destination as! HorseDetailViewController
            if let postId = dict["postId"] as? String, let showId = dict["showId"]  as? String{
                HDVC.postId = postId
                HDVC.showId = showId
            }
            break
            
        case "ShowDetailsToOwnerSegue":
            
            let HODVC = segue.destination as! HorseOwnerDetailViewController
            HODVC.postDetails = sender as! [String: AnyObject]
            
            break
            
        case "filtersSegue":
            
            let navController = segue.destination as! UINavigationController
            let controller =  navController.childViewControllers.first as! FiltersViewController
            controller.filterDataAppliedDelegate = self
            if let data = sender as? [AnyObject]{
                controller.selectedValues = data
            }
            break
            
        case "Open Profile Segue":
            let controller = segue.destination as! ProfilePageVC
            controller.isMyProfile = "0"
            let index = sender as! Int
            let dir = horseArray[index] as! [String: AnyObject]
            let userdetails =  dir["ownerDetail"] as! [String : AnyObject]
            controller.userId = userdetails["userId"] as! String
            break
            
        default:
            break
        }
    }
}

extension ShowDetailVC : filterDataAppliedDelegate,OpenUserDetailPageDelegate{
    
    func savedData(data: [AnyObject]) {
        self.getShowDetails(isFilteredApplied: 1, data: data)
    }
    
    func openUsersProfile(currentIndex: Int) {
        self.performSegue(withIdentifier: "Open Profile Segue", sender: currentIndex)
    }
}
