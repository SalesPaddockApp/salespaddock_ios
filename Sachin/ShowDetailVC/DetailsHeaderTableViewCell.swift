//
//  DetailsHeaderTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 23/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class DetailsHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var headerViewOutlet: UIView!
    @IBOutlet weak var showNameOutlet: UILabel!
    @IBOutlet weak var descriptionTextViewOutlet: UITextView!
    @IBOutlet weak var heldOnDateOutlet: UILabel!
    @IBOutlet weak var imageViewOutlet: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
