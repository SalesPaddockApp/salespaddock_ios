//
//  descriptionViewController.swift
//  salesPaddock
//
//  Created by Rahul Sharma on 13/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit

class descriptionViewController: UIViewController ,UITextViewDelegate{
    @IBOutlet weak var textViewOutlet: UITextView!    
    var prefId = String()
    var descriptionString = String()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.textViewOutlet.becomeFirstResponder()
        self.textViewOutlet.text = descriptionString
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    @IBAction func backBtnClicked(_ sender: AnyObject) {
       _ = navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        return true
    }

    func textViewShouldEndEditing(_ textView: UITextView) -> Bool
    {
       return true
    }
    
    @IBAction func saveButtonAction(_ sender: AnyObject) {
        
        var dictionary = [String:AnyObject]()
        let selectedObj = [self.textViewOutlet.text!]
        dictionary["options"] = selectedObj as AnyObject
        dictionary["pref_id"] = self.prefId as AnyObject
        UserDefaults.standard.set(dictionary, forKey: "selectedValue")
        UserDefaults.standard.synchronize()
        _ = navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
