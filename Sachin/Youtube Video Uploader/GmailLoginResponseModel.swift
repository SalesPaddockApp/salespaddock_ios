//
//  GmailLoginResponseModel.swift
//  GmailSwift
//
//  Created by Rahul Sharma on 14/11/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation
import GoogleSignIn

class GmailLoginResponseModel {
    
    var userId = "";                  // For client-side use only!
    var idToken = ""; // Safe to send to the server
    var fullName = "";
    var firstName = "";
    var lastName = "";
    var email = "";
    var accessToken = "";

    init(userDetailsFromGoogle:GIDGoogleUser) {
        
        if userDetailsFromGoogle.profile.hasImage {
            
            let dimension = round(thumbSize.width * UIScreen.main.scale)
            let imageURL = userDetailsFromGoogle.profile.imageURL(withDimension: UInt(dimension))
//            print("Image Url:",imageURL!)
        }
    
        
        // Perform any operations on signed in user here.
        userId = userDetailsFromGoogle.userID;                  // For client-side use only!
        idToken = userDetailsFromGoogle.authentication.idToken; // Safe to send to the server
        fullName = userDetailsFromGoogle.profile.name;
        if userDetailsFromGoogle.profile.givenName != nil{
            firstName = userDetailsFromGoogle.profile.givenName;
        }
        if userDetailsFromGoogle.profile.familyName != nil{
            lastName = userDetailsFromGoogle.profile.familyName;
        }
        email = userDetailsFromGoogle.profile.email;
        accessToken = userDetailsFromGoogle.authentication.accessToken
        
//        print("UserId:\(userId)\nFullName:\(fullName)\nFirstName:\(firstName)\nLastName:\(lastName)\n=Email:\(email)\n,Token-Id:\(idToken)\n")
 
    }
    
}
