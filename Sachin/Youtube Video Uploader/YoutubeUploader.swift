//
//  YoutubeUploader.swift
//  Sales Paddock
//
//  Created by Sachin Nautiyal on 22/12/2017.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import MobileCoreServices
import AssetsLibrary
import Alamofire

var videoURL:URL!

class YoutubeUploader {
    class func postVideoToYouTube(accessToken: String,videoUrl :URL, withName hName:String, andDescription description:String,  callback: @escaping (Bool, String?) -> Void){
        var horseName = hName
        if horseName.count == 0 {
            horseName = Utility.getUserFirstName()
        }
        let header = ["Authorization": "Bearer \(accessToken)"]
        let URLReq = try! URLRequest(url:URL.init(string: "https://www.googleapis.com/upload/youtube/v3/videos?part=snippet")!, method: .post, headers: header)
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                var tags = ""
                if horseName.count>0 {
                    tags = "['salespaddock', 'paddock', '\(horseName)', 'sales']"
                } else {
                    tags = "['salespaddock', 'paddock', 'sales']"
                    horseName = "Salespaddock"
                }
                multipartFormData.append("{'snippet':{'title' : '\(horseName)', 'description': '\(description) This video is uploaded from Salespaddock app. You can download it from https://itunes.apple.com/us/app/sales-paddock/id1164576952?mt=8 ', 'tags' : \(tags) }}".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "snippet", mimeType: "application/json")
                multipartFormData.append(videoUrl, withName: "binaryContent", fileName: "file.m4v", mimeType: "video/m4v")
        },
            with: URLReq,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
//                        print("response is ", response)
                        if((response.result.value) != nil) {
                            do {
                                guard let jsonResponse = try JSONSerialization.jsonObject(with: response.data!, options: []) as? [String : Any] else { return }
                                if let videoId = jsonResponse["id"] as? String {//Video Uploaded Successfully
                                    Helper.hidePI()
                                    //                                    Helper.alertVC(title: "Success!", message: "Video Link :- \(Helper.sharedInstance.getYoutubeLinkById(videoId: videoId))")
                                    callback(true,videoId)
                                } else {//Got some error from Google server
                                    if let errorResponse = jsonResponse["error"] as? [String:Any] {
                                        if let errorCode = errorResponse["code"] as? Int {
//                                            print("Error: \(errorCode) - \(String(describing: errorResponse["message"]))")
                                            callback(false,"\(errorCode)")
                                            return
                                        }
                                    }
                                    callback(false,nil)
                                }
                            } catch let jsonError {
//                                print("Response Data Error !!!",jsonError) // if there is any error in parsing then it is going to be print here.
                                Helper.hidePI()
                                callback(false,nil)
                            }
                        } else {
                            Helper.hidePI()
                            callback(false,nil)
                        }
                        }
                        .uploadProgress { progress in // main queue by default
//                            print("Upload Progress: \(progress.fractionCompleted * 100)")
                            
                    }
                    
                case .failure(let encodingError):
                    Helper.hidePI()
//                    print("failed", errno)
//                    print("ERROR RESPONSE: \(encodingError)")
                    callback(false,nil)
                    break
                }
        })
        
    }
}
