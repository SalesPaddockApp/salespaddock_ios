//
//  CreateChannelWebViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 03/04/18.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit

class CreateChannelWebViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    let channelCreateURL = "https://m.youtube.com/create_channel?chromeless=1&next=/channel_creation_done"
    let channelCreateDoenURL1 = "https://m.youtube.com/channel_creation_done"
    let channelCreateDoenURL2 = "https://m.youtube.com/channel_creation_done?reload"
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL.init(string: channelCreateURL)
        webView.loadRequest(URLRequest(url: url!))
        webView.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if let text = webView.request?.url?.absoluteString{
//            print(text)
            if text == channelCreateDoenURL1 || text == channelCreateDoenURL2{
                self.navigationController?.popViewController(animated: true)
            }else if text.contains(channelCreateDoenURL1){
                self.navigationController?.popViewController(animated: true)
            }
            
        }
    }

}
