//
//  GmailLoginHandler.swift
//  GmailSwift
//
//  Created by Apple on 05/04/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import GoogleSignIn

let thumbSize = CGSize(width: CGFloat(200), height: CGFloat(200))

protocol GmailLoginHandlerDelegate {
    
    func userDetailsFromGoogleLogin(userDetails:GmailLoginResponseModel)
    
    func errorFromGoogleNormalLogin(error:Error)
    
    func errorFromGoogleSilentLogin(error:Error)
    
    func gotErrorFromRefreshAccessToken(error:Error)
    
    func gotAccessToken(accessToken:String)
}



class GmailLoginHandler:NSObject,GIDSignInUIDelegate,GIDSignInDelegate {
    
    static var share:GmailLoginHandler?
    var delegate:GmailLoginHandlerDelegate? = nil

    class func sharedInstance() -> GmailLoginHandler {
        
        if (share == nil) {
            
            share = GmailLoginHandler.self()
        }
        return share!
    }

    override init() {
        
        super.init()
    }
    
    func checkAccessToken() {
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().scopes = ["https://www.googleapis.com/auth/plus.login",
                                                 "https://www.googleapis.com/auth/youtube",
                                                 "https://www.googleapis.com/auth/youtube.upload"]
        
        GIDSignIn.sharedInstance().loginHint = "Login Silently"
        
        //if GIDSignIn.sharedInstance().hasAuthInKeychain() {
            
            GIDSignIn.sharedInstance().signInSilently()
       // }
    }
    
    func login() {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().scopes = ["https://www.googleapis.com/auth/plus.login",
                                             "https://www.googleapis.com/auth/youtube",
                                             "https://www.googleapis.com/auth/youtube.upload"]
        
        GIDSignIn.sharedInstance().loginHint = "Normal Login"
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().signIn()
    }
    
    func getAccessToken() -> String? {
        
        if GIDSignIn.sharedInstance().currentUser != nil {
            
            return GIDSignIn.sharedInstance().currentUser.authentication.accessToken
        }
        return nil
    }
    
    func refreshAccessToken() {
        
        GIDSignIn.sharedInstance().currentUser.authentication.refreshTokens(handler: { (googleAuth, error) in
            if error != nil {
                self.delegate?.gotErrorFromRefreshAccessToken(error: error!)
            } else {
                self.delegate?.gotAccessToken(accessToken: (googleAuth?.accessToken)!)
            }
        })
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil
        {
            //Got Error
            Helper.hidePI()
            if signIn.loginHint == "Login Silently" {
                delegate?.errorFromGoogleSilentLogin(error: error)
            } else {
                delegate?.errorFromGoogleNormalLogin(error: error)
            }
        }
        else
        {
            //SuccessFully Logged IN
            //Return Back This Details
            Helper.hidePI()
            delegate?.userDetailsFromGoogleLogin(userDetails: GmailLoginResponseModel.init(userDetailsFromGoogle: user))
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
        
    }

}
