//
//  YoutubeURLVerifier.swift
//  Sales Paddock
//
//  Created by Sachin Nautiyal on 22/01/2018.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class YoutubeURLVerifier: NSObject {
    
    class func verifyURL(withID youtubeID : String, andAccessToken accessToken: String, completion : @escaping (String?) -> Void) {
        let apiKey = Constant.APIKeys.googleSKey
        let apiStr = "https://www.googleapis.com/youtube/v3/videos?key=\(apiKey)&part=snippet&id=\(youtubeID)"
        
        self.verifyURL(withURL: apiStr, andAccessToken: accessToken, success: { (response) in
//            print(response)
            if let thumbnailURL = response.dictionaryValue["items"]?.array?[0]["snippet"]["thumbnails"]["standard"]["url"].string {
                completion(thumbnailURL)
            }else if let thumbnailURL = response.dictionaryValue["items"]?.array?[0]["snippet"]["thumbnails"]["medium"]["url"].string {
                completion(thumbnailURL)
            }
        }) { (error) in
//            print(error)
            completion(nil)
        }
    }
    
    private class func verifyURL(withURL url : String, andAccessToken  accessToken: String, success:@escaping (JSON) -> Void, failure:@escaping (NSError) -> Void) {
        Alamofire.request(url, method:.get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: {
            response in
            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                success(resJson)
                Helper.hidePI()
            }
            if response.result.isFailure {
                let error : NSError = response.result.error! as NSError
                failure(error)
//                print(error.localizedDescription)
                Helper.hidePI()
            }
        })
    }
}

extension String {
    var youtubeID: String? {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: count)
        guard let result = regex?.firstMatch(in: self, options: [], range: range) else {
            return nil
        }
        return (self as NSString).substring(with: result.range)
    }
}
