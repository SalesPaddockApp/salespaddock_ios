//
//  ShowDetailsTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 04/10/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class ShowDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var showNameOutlet: UILabel!
    @IBOutlet weak var descriptionOutlet: UILabel!
    @IBOutlet weak var heldOnDateOutlet: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
