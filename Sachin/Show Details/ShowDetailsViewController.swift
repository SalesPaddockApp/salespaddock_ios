//
//  ShowDetailsViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 04/10/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher

class ShowDetailsViewController: UIViewController {
    
    @IBOutlet weak var titleLabelOutlet: UILabel!
    @IBOutlet weak var navigationViewOutlet: UIView!
    @IBOutlet weak var bottomConstraintOfTableViewOutlet: NSLayoutConstraint!
    @IBOutlet weak var backButtonOutlet: UIButton!
    @IBOutlet weak var tableViewOutlet: UITableView!
    @IBOutlet weak var horseImageOutlet: UIImageView!
    var showData = [String:JSON]()
    var postId = String()
    var isComingFromShowDetails = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: showData["image"]!.stringValue)
        
        self.horseImageOutlet.kf.indicatorType = .activity
        self.horseImageOutlet.kf.indicator?.startAnimatingView()
        
        horseImageOutlet.kf.setImage(with: url,
                                     placeholder: UIImage.init(named: "horse_default_image"),
                                     options: [.transition(ImageTransition.fade(1))],
                                     progressBlock: { receivedSize, totalSize in
        },
                                     completionHandler: { image, error, cacheType, imageURL in
                                        
                                        self.horseImageOutlet.kf.indicator?.stopAnimatingView()
        })
        tableViewOutlet.estimatedRowHeight = 150
        tableViewOutlet.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        /*
         if(isComingFromShowDetails)
         {
         self.navigationController?.setNavigationBarHidden(true, animated: true)
         self.bottomConstraintOfTableViewOutlet?.constant = 50
         }
         else{
         self.bottomConstraintOfTableViewOutlet?.constant = 0
         self.view.layoutIfNeeded()
         }
         */
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissViewAction(_ sender: AnyObject)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addHorseToShowAction(_ sender: AnyObject)
    {
        self.performSegue(withIdentifier: "getPostList", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "getPostList"
        {
            let controller = segue.destination as! AddHorsesInShowViewController
            controller.showId = showData["showId"]!.stringValue
        }
    }
}

extension ShowDetailsViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : ShowDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ShowDetailsWithLabel") as! ShowDetailsTableViewCell
        
        if(indexPath.row == 0){
            cell.showNameOutlet?.text = showData["showName"]!.stringValue
            return cell
        }
        else
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "ShowDetailsTableViewCell") as! ShowDetailsTableViewCell
            cell.descriptionOutlet?.text = showData["Description"]!.stringValue
            cell.heldOnDateOutlet?.text = showData["startDate"]!.stringValue
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func takeUserBack(){
        
        self.performSegue(withIdentifier: "present List Segue", sender: self)
        
    }
}

extension ShowDetailsViewController : UIScrollViewDelegate
{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView  == tableViewOutlet
        {
            let offsetY: CGFloat = scrollView.contentOffset.y
            
            if(offsetY > 100)
            {
                backButtonOutlet.setImage(Image.init(named: "back_btn_off"), for: .normal)
                let alpha: CGFloat = min(1, 1 - ((64 + 100 - offsetY) / 100))
                navigationViewOutlet.backgroundColor = Color.white.withAlphaComponent(alpha)
                self.titleLabelOutlet.isHidden = true
                if(alpha == 1)
                {
                    backButtonOutlet.setImage(Image.init(named: "back_btn_off"), for: .normal)
                    if let showName = showData["showName"]?.string{
                    self.titleLabelOutlet.text = showName
                        self.titleLabelOutlet.isHidden = false
                    }
                }
            }
            else if (offsetY < 100)
            {
                backButtonOutlet.setImage(Image.init(named: "back_btn_wight_off"), for: .normal)
                navigationViewOutlet.backgroundColor = Color.clear
                self.titleLabelOutlet.isHidden = true
            }
            else
            {
                backButtonOutlet.setImage(Image.init(named: "back_btn_wight_off"), for: .normal)
                self.titleLabelOutlet.isHidden = true
            }
        }
    }
    
}
