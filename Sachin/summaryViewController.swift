//
//  summaryViewController.swift
//  salesPaddock
//
//  Created by Rahul Sharma on 09/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher


class summaryViewController: UIViewController
{
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var rideAlertButtonOutlet: UIButton!
    @IBOutlet weak var viewOfCollectionViewOutlet: UIView!
    
    @IBOutlet weak var noSummaryLabelOutlet: UILabel!
    
    var grayBackgroundView = UIView()
    var popupView:rideAlertView?
    var popupViewDic:[String:rideAlertView] = [:]
    var constX:NSLayoutConstraint?
    var constY:NSLayoutConstraint?
    var window: UIWindow?
    var currentIndex :Int = 0
    var responseData  = [JSON]()
//    var visibleCurrentCell = 0
    /*
    var visibleCurrentCell: IndexPath? {
        for cell in self.collectionView.visibleCells {
            let indexPath = self.collectionView.indexPath(for: cell)
            return indexPath
        }
        return nil
    }
 */
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true);
    }
    
    @IBAction func sendRideAlertBtnClicked(_ sender: AnyObject)
    {
        if responseData.count>0{
            self.performSegue(withIdentifier: "Ride Alert Segue", sender: nil)
        }
        //if let index = visibleCurrentCell?.row{
        //    currentIndex = index
       // }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Ride Alert Segue"
        {
            let controller = segue.destination as! RideAlertViewController
            let dict = self.responseData[(currentIndex)]
            controller.responseData = dict.dictionaryValue
            
        }
    }

    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        getSummaryData()
        self.tabBarController?.navigationItem.hidesBackButton = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        let numberOfRows = collectionView.numberOfItems(inSection: 0)
        if numberOfRows>0{
            collectionView.scrollToItem(at: IndexPath(row: 0, section:0), at: UICollectionViewScrollPosition.right, animated: true)
            self.currentIndex = 0
        }
    }
}

extension summaryViewController : UIScrollViewDelegate, UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return responseData.count
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.findCenterIndex()
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageWidth: Float = Float(self.collectionView!.frame.width) - 35 //480 + 50
        // width + space
        let currentOffset: Float = Float(scrollView.contentOffset.x)
        let targetOffset: Float = Float(targetContentOffset.pointee.x)
        var newTargetOffset: Float = 0
        if targetOffset > currentOffset {
            newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth
        }
        else {
            newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth
        }
        if newTargetOffset < 0 {
            newTargetOffset = 0
        }
        else if (newTargetOffset > Float(scrollView.contentSize.width)){
            newTargetOffset = Float(Float(scrollView.contentSize.width))
        }
        
        targetContentOffset.pointee.x = CGFloat(currentOffset)
        scrollView.setContentOffset(CGPoint(x: CGFloat(newTargetOffset), y: scrollView.contentOffset.y), animated: true)
        self.findCenterIndex()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.CellIdentifiers.descriptionCollectionViewCell, for: indexPath) as! descriptionCollectionViewCell
        
        let dataDict = responseData[indexPath.row]
        cell.horseNameOutlet.text = dataDict["name"].stringValue
        cell.showNameOutlet.text = dataDict["showName"].stringValue
        if let views = dataDict["views"].int{
            cell.viewsLabelOutlet.text = String("\(views) Unique view(s)")
        }
        
        if let wishCounts = dataDict["wishCount"].int{
            cell.requestsOutlet.text = String("\(wishCounts) Save(s)")
        }
        
        let url = URL(string: dataDict["image"].stringValue)
        
        cell.imageOutlet.kf.indicatorType = .activity
        cell.imageOutlet.kf.indicator?.startAnimatingView()
        
        cell.imageOutlet.kf.setImage(with: url,
                                     placeholder: UIImage.init(named: "horse_default_image"),
                                     options: [.transition(ImageTransition.fade(1))],
                                     progressBlock: { receivedSize, totalSize in
            },
                                     completionHandler: { image, error, cacheType, imageURL in
                                        cell.imageOutlet.kf.indicator?.stopAnimatingView()
        })
        
        return cell
    }
    
    private func findCenterIndex() {
        let pageWidth: CGFloat = CGFloat(self.collectionView!.frame.width) - 35 //480 + 50
        currentIndex = Int(self.collectionView.contentOffset.x/pageWidth)
    }
    
    private func collectionView(_ collectionView: UITableView, willDisplayCell cell: UICollectionView, forRowAtIndexPath indexPath: IndexPath) {
        cell.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
        UIView.animate(withDuration: 0.2, animations: {
            cell.layer.transform = CATransform3DMakeScale(1,1,1)
        })
        UIView.animate(withDuration: 1, animations: {
            cell.layer.transform = CATransform3DMakeScale(2,2,2)
        })
        UIView.animate(withDuration: 0.2, animations: {
            cell.layer.transform = CATransform3DMakeScale(1,1,1)
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var width = self.collectionView.bounds.width
        width -= 25*2
        return CGSize(width: width, height: self.collectionView.bounds.height)
    }
}

extension  summaryViewController{
    func getSummaryData()
    {
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let userId = UserDefaults.standard.object(forKey: "user_id") as! String!
        
        let params:[String : AnyObject] = ["token":token! as AnyObject,"userId":userId! as AnyObject]
        Helper.showPI()

        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getSummaries, params: params as [String : AnyObject]?
            , success:
            { (response) in
                let errFlag = response["errCode"].numberValue
                Helper.hidePI()
                switch errFlag {
                case 0:
                    self.responseData = response["response"].arrayValue
                    self.collectionView.reloadData()
                    if self.responseData.count>0{
                        self.rideAlertButtonOutlet.isEnabled = true
                        self.viewOfCollectionViewOutlet.isHidden = false
                        self.noSummaryLabelOutlet.isHidden = true
                    }
                    else{
                        self.rideAlertButtonOutlet.isEnabled = false
                        self.viewOfCollectionViewOutlet.isHidden = true
                        self.noSummaryLabelOutlet.isHidden = false
                    }
                    break
                default:
                    let errMsg = response["Message"].stringValue
                    break
                }
            }, failure: { (Error) in
                Helper.hidePI()
        })
    }
}


