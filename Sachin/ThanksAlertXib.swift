//
//  thanksAlertXib.swift
//  salesPaddock
//
//  Created by Rahul Sharma on 12/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit

class ThanksAlertXib: UIView {

    static func loadViewFromNib(_ frame: CGRect) -> ThanksAlertXib {
        
        let view = Bundle(for: self).loadNibNamed("ThanksAlertXib", owner: nil, options: nil)?.first as! ThanksAlertXib
        view.frame = frame
        return view
    }
    
    @IBAction func panGestureAction(_ sender: AnyObject)
    {
        self.removeFromSuperview()
    }
}
