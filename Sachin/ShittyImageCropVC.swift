
import UIKit

protocol photoCropped {
    
    func photoSaved(image:UIImage)
}

class ShittyImageCropVC: UIViewController, UIScrollViewDelegate {
    
    var delegate:photoCropped! = nil
    var aspectW: CGFloat!
    var aspectH: CGFloat!
    var img: UIImage!
    var imageView: UIImageView!
    var scrollView: UIScrollView!
    var closeButton: UIButton!
    var cropButton: UIButton!
    
    var holeRect: CGRect!
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    init(frame: CGRect, image: UIImage, aspectWidth: CGFloat, aspectHeight: CGFloat) {
        super.init(nibName: nil, bundle: nil)
        aspectW = aspectWidth
        aspectH = aspectHeight
        img = image
        view.frame = frame
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView() {
        closeButton = UIButton(frame: CGRect(x: 40, y: view.frame.height - 40 - 90, width: 90, height: 90))
        closeButton.setImage(#imageLiteral(resourceName: "crop-back"), for: .normal)
        closeButton.addTarget(self, action: #selector(tappedClose), for: .touchUpInside)
        
        cropButton = UIButton(frame: CGRect(x: view.frame.width - 40 - 90, y: view.frame.height - 40 - 90, width: 90, height: 90))
        cropButton.setImage(#imageLiteral(resourceName: "crop-checkmark"), for: .normal)
        cropButton.addTarget(self, action: #selector(tappedCrop), for: .touchUpInside)
        
        view.backgroundColor = UIColor.gray
        
        // TODO: improve to handle super tall aspects (this one assumes full width)
        let holeWidth = view.frame.width
        let holeHeight = holeWidth * aspectH/aspectW
        holeRect = CGRect(x: 0, y: view.frame.height/2-holeHeight/2, width: holeWidth, height: holeHeight)
        
        imageView = UIImageView(image: img.fixOrientation())
        scrollView = UIScrollView(frame: view.bounds)
        scrollView.addSubview(imageView)
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.alwaysBounceHorizontal = true
        scrollView.alwaysBounceVertical = true
        scrollView.delegate = self
        view.addSubview(scrollView)
        
        let minZoom = max(holeWidth / imageView!.bounds.width, holeHeight / imageView!.bounds.height)
        scrollView.minimumZoomScale = minZoom
        scrollView.zoomScale = minZoom
        scrollView.maximumZoomScale = minZoom*4
        
        let viewFinder = hollowView(frame: view.frame, transparentRect: holeRect)
        view.addSubview(viewFinder)
        
        view.addSubview(closeButton)
        view.addSubview(cropButton)
    }
    
    // MARK: scrollView delegate
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let gapToTheHole = view.frame.height/2-holeRect.height/2
        scrollView.contentInset = UIEdgeInsetsMake(gapToTheHole, 0, gapToTheHole, 0)
    }
    
    // MARK: actions
    
    @objc func tappedClose() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func tappedCrop() {
        
        var imgX: CGFloat = 0
        if scrollView.contentOffset.x > 0 {
            imgX = scrollView.contentOffset.x / scrollView.zoomScale
        }
        
        let gapToTheHole = view.frame.height/2 - holeRect.height/2
        var imgY: CGFloat = 0
        if scrollView.contentOffset.y + gapToTheHole > 0 {
            imgY = (scrollView.contentOffset.y + gapToTheHole) / scrollView.zoomScale
        }
        
        let imgW = holeRect.width  / scrollView.zoomScale
        let imgH = holeRect.height  / scrollView.zoomScale
        
        let cropRect = CGRect(x: imgX, y: imgY, width: imgW, height: imgH)
        let imageRef = img.cgImage!.cropping(to: cropRect)
        let croppedImage = UIImage(cgImage: imageRef!)

        if (self.delegate != nil)
        {
            let watermark = croppedImage.waterMarkImagewithImage(backgroundImage: croppedImage, watermarkImage: #imageLiteral(resourceName: "Icon-App-60x60"))
            self.delegate!.photoSaved(image: watermark.fixOrientation())
        }
        self.dismiss(animated: true, completion: nil)
    }
}


// MARK: hollow view class

class hollowView: UIView {
    var transparentRect: CGRect!
    
    init(frame: CGRect, transparentRect: CGRect) {
        super.init(frame: frame)
        
        self.transparentRect = transparentRect
        self.isUserInteractionEnabled = false
        self.alpha = 0.5
        self.isOpaque = false
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        backgroundColor?.setFill()
        UIRectFill(rect)
        
        let holeRectIntersection = transparentRect.intersection( rect )
        
        UIColor.clear.setFill();
        UIRectFill(holeRectIntersection);
    }
}

extension UIImage
{
    func fixOrientation() -> UIImage
    {
        
        if self.imageOrientation == UIImageOrientation.up {
            return self
        }
        
        var transform = CGAffineTransform.identity
        
        switch self.imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: self.size.width, y: self.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi));
            
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: self.size.width, y: 0);
            transform = transform.rotated(by: CGFloat(Double.pi/2));
            
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: self.size.height);
            transform = transform.rotated(by: CGFloat(-Double.pi/2));
            
        case .up, .upMirrored:
            break
        }
        
        
        switch self.imageOrientation {
            
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
            
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: self.size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1);
            
        default:
            break;
        }
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx = CGContext(
            data: nil,
            width: Int(self.size.width),
            height: Int(self.size.height),
            bitsPerComponent: self.cgImage!.bitsPerComponent,
            bytesPerRow: 0,
            space: self.cgImage!.colorSpace!,
            bitmapInfo: UInt32(self.cgImage!.bitmapInfo.rawValue)
        )
        
        ctx!.concatenate(transform);
        
        switch self.imageOrientation {
            
        case .left, .leftMirrored, .right, .rightMirrored:
            // Grr...
            ctx?.draw(self.cgImage!, in: CGRect(x:0 ,y: 0 ,width: self.size.height ,height:self.size.width))
            
        default:
            ctx?.draw(self.cgImage!, in: CGRect(x:0 ,y: 0 ,width: self.size.width ,height:self.size.height))
            break;
        }
        
        // And now we just create a new UIImage from the drawing context
        let cgimg = ctx!.makeImage()
        
        let img = UIImage(cgImage: cgimg!)
        
        return img;
        
    }
}

