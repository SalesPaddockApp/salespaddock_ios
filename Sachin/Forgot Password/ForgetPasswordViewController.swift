//
//  ForgetPasswordViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 05/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: UIViewController {

    @IBOutlet weak var nxtBtnOutlet: UIButton!
    @IBOutlet weak var tableViewOutlet: UITableView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var nextButtonConstraintOutlet: NSLayoutConstraint!
    @IBOutlet weak var rightImageOutlet: UIImageView!
    @IBOutlet weak var emailTextFieldOutlet: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backButtonAction(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nxtButtonAction(_ sender: Any)
    {
        self.forgotEmail()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.navigationController!.hideNavigation();
        nxtBtnOutlet.setImage(UIImage.init(named:"next_btn_off"), for: .normal)
        nxtBtnOutlet.isEnabled = false

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.navigationController!.hideNavigation();
    }

    @objc func keyboardWillShow(_ notification:Notification) {
        
        var userInfo = (notification as NSNotification).userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var textFieldMaxY = emailTextFieldOutlet.frame.maxY
        var view = emailTextFieldOutlet.superview
        
        while view != self.view.superview {
            textFieldMaxY += view!.frame.minY
            view = view?.superview
        }
        
        let remainder = UIScreen.main.bounds.size.height - textFieldMaxY - keyboardFrame.size.height
        
        if remainder < 0 {
            
            let animationDurarion = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
            
            UIView.animate(withDuration: animationDurarion, animations: { () -> Void in
                self.tableViewOutlet.contentOffset.y = -remainder + 70
                self.view.layoutIfNeeded()
            })
        }
        
        self.nextButtonConstraintOutlet.constant = keyboardFrame.size.height + 10
        tableViewOutlet.contentSize = CGSize(width: contentView.frame.size.width, height: self.view.frame.size.height + keyboardFrame.height + 20)
    }
    
    
    @objc func keyboardWillHide(_ notification:Notification) {
        var userInfo = (notification as NSNotification).userInfo!
        let animationDurarion = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
        UIView.animate(withDuration: animationDurarion, animations: { () -> Void in
            self.nextButtonConstraintOutlet.constant = 30
            self.view.layoutIfNeeded()
        })
        tableViewOutlet.contentSize = contentView.frame.size
    }
    
    func forgotEmail() {
        Helper.showPI(_message: "Loading...")
        
        let params = ["email":self.emailTextFieldOutlet.text]
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.forgetPassword,
                                 params: params as [String : AnyObject]!,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                        
                                    case 0:
                                    
                                        self.view.endEditing(true)
                                        self.showAlerts("Check Your Email", message: "We sent an email to \(self.emailTextFieldOutlet.text!). Tap the link in that email to reset your password.")
                                        
                                        
                                        
                                    break
                                        
                                    default:
                                        self.view.endEditing(true)
                                        self.showAlert("Email address not exist", message: "No account exists for the requested email address")
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
    
    func showAlerts(_ title: String, message: String) {
        
        // create the alert
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            _ = self.navigationController?.popViewController(animated: true)
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
}

extension ForgetPasswordViewController :UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        var textFieldText:NSString = textField.text! as NSString
        textFieldText = textFieldText.replacingCharacters(in: range, with: string) as NSString
        
        let dummyTextField:UITextField = UITextField()
        dummyTextField.text = textFieldText as String
        
        if (dummyTextField.text?.isValidEmail())!
        {
            nxtBtnOutlet.setImage(UIImage.init(named:"next_btn_on"), for: .normal)
           self.nxtBtnOutlet.isEnabled = true
            self.rightImageOutlet.isHidden = false
        }
        else
        {
            nxtBtnOutlet.setImage(UIImage.init(named:"next_btn_off"), for: .normal)
            self.nxtBtnOutlet.isEnabled = false
            self.rightImageOutlet.isHidden = true
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return false
    }
}
