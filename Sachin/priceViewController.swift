//
//  priceViewController.swift
//  salesPaddock
//
//  Created by Rahul Sharma on 13/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit

class priceViewController: UIViewController {
    var prefId = String()

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var mainScrollViewOutlet: UIScrollView!
    @IBOutlet weak var sellingPriceTextFiledOutlet: UITextField!
    @IBOutlet weak var leasePriceTextFiledOutlet: UITextField!
    var activeTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Price Detail"
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backBtnClicked(_ sender: AnyObject) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    

    @IBAction func saveButtonAction(_ sender: AnyObject) {

            var dictionary = [String:AnyObject]()
        let arr = ["\(sellingPriceTextFiledOutlet.text!)"]
//            let selectedObj =  String("least \() ")
            dictionary["options"] = arr as AnyObject
            dictionary["pref_id"] = self.prefId as AnyObject
            UserDefaults.standard.set(dictionary, forKey: "selectedValue")
            UserDefaults.standard.synchronize()
       
        _ = navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension priceViewController : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    @objc func keyboardWillShow(_ notification:Notification) {
        
        var userInfo = (notification as NSNotification).userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var textFieldMaxY = activeTextField.frame.maxY
        var view = activeTextField.superview
        
        while view != self.view.superview {
            textFieldMaxY += view!.frame.minY
            view = view?.superview
        }
        let remainder = UIScreen.main.bounds.size.height - textFieldMaxY - keyboardFrame.size.height
        if remainder < 0 {
            
            let animationDurarion = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
            UIView.animate(withDuration: animationDurarion, animations: { () -> Void in
                
                self.mainScrollViewOutlet.contentOffset.y = -remainder + 70
                self.view.layoutIfNeeded()
            })
        }
        mainScrollViewOutlet.contentSize = CGSize(width: contentView.frame.size.width, height: contentView.frame.size.height + keyboardFrame.height + 20)
    }
    
    
    @objc func keyboardWillHide(_ notification:Notification) {
        
        var userInfo = (notification as NSNotification).userInfo!
        let animationDurarion = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
        UIView.animate(withDuration: animationDurarion, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
        mainScrollViewOutlet.contentSize = contentView.frame.size
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        activeTextField = textField
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        activeTextField = nil
    }
}
