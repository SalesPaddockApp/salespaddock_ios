//
//  FarmNameTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 21/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

protocol EditbuttonPressedDelegate{
    func editButtonPressedAction(index:Int)
}

class FarmNameTableViewCell: UITableViewCell {

    var EditbuttonPressedDelegate:EditbuttonPressedDelegate?
    var currentIndex:Int = 0
    @IBOutlet weak var farmNameOutlet: UILabel!
    @IBOutlet weak var farmAddressOutlet: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    @IBAction func editButtonAction(_ sender: Any)
    {
        if (self.EditbuttonPressedDelegate != nil)
        {
            self.EditbuttonPressedDelegate?.editButtonPressedAction(index: currentIndex)
        }
    }
    
}
