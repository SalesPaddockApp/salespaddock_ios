//
//  AddFarmLocationView.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 20/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps

protocol FarmNameValidationDelegate{
    
    func fieldsValidated(isValidated : Bool)
}

class AddFarmLocationView: UIView {
    
    var FarmNameValidationDelegate: FarmNameValidationDelegate! = nil
    @IBOutlet weak var streetTextFieldOutlet: UITextField!
    @IBOutlet weak var aptTextFieldOutlet: UITextField!
    @IBOutlet weak var cityTxtFieldOutlet: UITextField!
    @IBOutlet weak var stateTextFieldOutlet: UITextField!
    @IBOutlet weak var zipCodeTextFieldOutlet: UITextField!
    @IBOutlet weak var countryTextFieldOutlet: UITextField!
    @IBOutlet weak var farmNameTextFieldOutlet: UITextField!
    var gmsPlaceObj : GMSPlace!
    let countryNames = NSLocale.getAllCountryNames()
    @IBOutlet weak var bottomConstrainOutlet: NSLayoutConstraint!
    @IBOutlet weak var countryPickerView: UIPickerView!
    
    
    func updateData(locationData : GMSAutocompletePrediction)
    {
        if let placeId = locationData.placeID{
            let gmsPlaceClient = GMSPlacesClient()
            gmsPlaceClient.lookUpPlaceID(placeId, callback: { (gmsPlace, error) in
                if gmsPlace != nil{
                    self.gmsPlaceObj = gmsPlace!
                    let coordinates = CLLocationCoordinate2DMake((gmsPlace?.coordinate.latitude)!, (gmsPlace?.coordinate.longitude)!)
                    self.getDataFromCoordinates(coordinates: coordinates)
                }
                else{
                }
            })
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func getDataFromCoordinates(coordinates :  CLLocationCoordinate2D){
        let gmsGeoCoder = GMSGeocoder()
        gmsGeoCoder.reverseGeocodeCoordinate(coordinates, completionHandler: { (results, error) in
            if let addressResult:GMSAddress = results?.firstResult(){
                self.updateDataInFields(addressResult: addressResult)
            }
            else{
            }
        })
    }
    
    func getFarmData(farmAddObj : FarmAddress?) -> FarmAddress? {
        let timestamp = Date().ticks
        var farmDataObj : FarmAddress!
        if (gmsPlaceObj != nil)
        {
            farmDataObj = FarmAddress.init(latitude: "\(gmsPlaceObj!.coordinate.latitude)",
                longitude: "\(gmsPlaceObj!.coordinate.longitude)",
                street: streetTextFieldOutlet.text!,
                apartment: self.aptTextFieldOutlet.text!,
                city: self.cityTxtFieldOutlet.text!,
                state: self.stateTextFieldOutlet.text!,
                zipCode: self.zipCodeTextFieldOutlet.text!,
                country: self.countryTextFieldOutlet.text!,
                farmName: self.farmNameTextFieldOutlet.text!,
                farmId:"\(timestamp)",
                isFarmAlreadyAdded: "0",
                isLatLongAvailable: true)
        }
        else if let farmAddressObject = farmAddObj {
            if let lat = farmAddressObject.latitude {
                if lat.count>0 {
                    farmDataObj = FarmAddress.init(latitude: farmAddressObject.latitude!,
                                                   longitude: farmAddressObject.longitude!,
                                                   street: streetTextFieldOutlet.text!,
                                                   apartment: self.aptTextFieldOutlet.text!,
                                                   city: self.cityTxtFieldOutlet.text!,
                                                   state: self.stateTextFieldOutlet.text!,
                                                   zipCode: self.zipCodeTextFieldOutlet.text!,
                                                   country: self.countryTextFieldOutlet.text!,
                                                   farmName: self.farmNameTextFieldOutlet.text!,
                                                   farmId: farmAddressObject.farmId!,
                                                   isFarmAlreadyAdded: "1",
                                                   isLatLongAvailable: true)
                }
            }
        }
        return farmDataObj
    }
    
    func updateDataInFields(addressResult:GMSAddress){
        if let cityText = addressResult.locality{
            self.cityTxtFieldOutlet.text = cityText
        }
        
        if let state = addressResult.administrativeArea{
            self.stateTextFieldOutlet.text = state
        }
        
        if let zipCOde = addressResult.postalCode{
            self.zipCodeTextFieldOutlet.text = zipCOde
        }
        
        if let country = addressResult.country{
            self.countryTextFieldOutlet.text = country
        }
    }
    
    func updateDataFromModal(farmAdressObj : FarmAddress){
        self.cityTxtFieldOutlet.text = farmAdressObj.city
        self.stateTextFieldOutlet.text = farmAdressObj.state
        self.zipCodeTextFieldOutlet.text = farmAdressObj.zipCode
        self.countryTextFieldOutlet.text = farmAdressObj.country
        self.aptTextFieldOutlet.text = farmAdressObj.apartment
        self.countryTextFieldOutlet.text = farmAdressObj.country
        self.farmNameTextFieldOutlet.text = farmAdressObj.farmName
        self.streetTextFieldOutlet.text = farmAdressObj.street
    }
}

extension AddFarmLocationView : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var textFieldText:NSString = textField.text! as NSString
        textFieldText = textFieldText.replacingCharacters(in: range, with: string) as NSString
        
        let dummyTextField:UITextField = UITextField()
        dummyTextField.text = textFieldText as String
        
        if (farmNameTextFieldOutlet.text?.count)!>1 && (streetTextFieldOutlet.text?.count)!>1 && (cityTxtFieldOutlet.text?.count)!>1 && (stateTextFieldOutlet.text?.count)!>1 && (zipCodeTextFieldOutlet.text?.count)!>1 && (countryTextFieldOutlet.text?.count)!>1
        {
            if (self.FarmNameValidationDelegate != nil)
            {
                self.FarmNameValidationDelegate.fieldsValidated(isValidated: true)
            }
        }
        else{
            if (self.FarmNameValidationDelegate != nil)
            {
                self.FarmNameValidationDelegate.fieldsValidated(isValidated: false)
            }
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.countryTextFieldOutlet {
            self.endEditing(true)
            UIView.animate(withDuration: 0.4, animations:{
                self.bottomConstrainOutlet.constant = 0
                self.countryPickerView.reloadAllComponents()
                self.layoutIfNeeded()
            }, completion: nil)
            return false
        }else {
            UIView.animate(withDuration: 0.4, animations:{
                self.bottomConstrainOutlet.constant = -260
                self.layoutIfNeeded()
            }, completion: nil)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case farmNameTextFieldOutlet:
            if (farmNameTextFieldOutlet.text?.count)!>1 {
                streetTextFieldOutlet.becomeFirstResponder()
            }
            break
        case streetTextFieldOutlet:
            
            if (streetTextFieldOutlet.text?.count)!>1 {
                aptTextFieldOutlet.becomeFirstResponder()
            }
            break
            
            //        case aptTextFieldOutlet:
            //
            //            if (aptTextFieldOutlet.text?.characters.count)!>1
            //            {
            //                cityTxtFieldOutlet.becomeFirstResponder()
            //            }
            //            break
        //
        case cityTxtFieldOutlet:
            if (cityTxtFieldOutlet.text?.count)!>1
            {
                stateTextFieldOutlet.becomeFirstResponder()
            }
            break
            
        case stateTextFieldOutlet:
            if (stateTextFieldOutlet.text?.count)!>1
            {
                zipCodeTextFieldOutlet.becomeFirstResponder()
            }
            break
            
        case zipCodeTextFieldOutlet:
            if (zipCodeTextFieldOutlet.text?.count)!>1
            {
                countryTextFieldOutlet.becomeFirstResponder()
            }
            break
            
        case countryTextFieldOutlet:
            if (countryTextFieldOutlet.text?.count)!>1
            {
                self.endEditing(true)
            }
            break
            
        default:
            break
        }
        return true
    }
}

extension AddFarmLocationView : UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let value = countryPickerView.selectedRow(inComponent: 0)
        
        let countryName = countryNames[value]
        self.countryTextFieldOutlet.text = countryName
    }
}

extension AddFarmLocationView : UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countryNames.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countryNames[row]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
}
