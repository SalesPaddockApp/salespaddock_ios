//
//  MapView.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 16/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

protocol locationSelectedDelegate {
    func locationSelected(location : GMSAutocompletePrediction)
    func locationSelectedWithoutAnyValue()
    func textfieldAction()
}

class MapView: UIView {
    
    var locationSelectedDelegate:locationSelectedDelegate?
    @IBOutlet weak var reCenterButton: UIButton!
    @IBOutlet weak var addressTextFeild: UITextField!
    @IBOutlet weak var addressTableViewOutlet: UITableView!
    @IBOutlet weak var googlMapViewOutlet: GMSMapView!
    var placesClient = GMSPlacesClient()
    let locationManager  = LocationCoordinateManager.sharedInstance
    var placesResults = [GMSAutocompletePrediction]()
    var coordinates = CLLocationCoordinate2D(latitude: 0.00, longitude: 0.00)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        locationManager.LocationManagerDelegate = self
        placesClient = GMSPlacesClient.shared()
        googlMapViewOutlet.delegate = self
        if coordinates.latitude != 0.00 {
            self.updateToCoordinates(coordinates: coordinates)
            locationManager.stopUpdatingLocation()
        }else{
            locationManager.startUpdatingLocation()
        }
    }
    
    func maintainViewAfterSavingData(farmAddObj : FarmAddress){
        self.addressTextFeild.text = farmAddObj.fullAdress
        self.addressTableViewOutlet.isHidden = true
        if (googlMapViewOutlet.delegate != nil)
        {
            googlMapViewOutlet.delegate = self
        }
        let SavedCoordinate = CLLocationCoordinate2D(latitude: Double(farmAddObj.latitude!)!, longitude: Double(farmAddObj.longitude!)!)
        self.updateToCoordinates(coordinates: SavedCoordinate)
    }
    
    func reverseGeoCodeCoordinte(coordinate : CLLocationCoordinate2D){
        //        let geocoder = GMSGeocoder()
        self.coordinates = coordinate
        // geocoder.reverseGeocodeCoordinate(coordinate) { (response, error) in
        //                if let address = response?.firstResult(){
        //                    if let lines = address.lines{
        //                    self.addressTextFeild.text = lines.joined(separator: ", ")
        //                    }
        //                }
        //        }
    }
    
    func updateLocationToCurrent(){
        locationManager.startUpdatingLocation()
    }
    
    func updateToCoordinates(coordinates :CLLocationCoordinate2D)
    {
        self.locationManager.stopUpdatingLocation()
        self.googlMapViewOutlet.animate(to: GMSCameraPosition(target:coordinates, zoom: 15, bearing: 0, viewingAngle: 0))
    }
    
    func placeAutocomplete(placeName : String) {
        let filter = GMSAutocompleteFilter()
        filter.type = .geocode
        placesClient.autocompleteQuery(placeName, bounds: nil, filter: filter, callback: {(results, error) -> Void in
            if error != nil {
                return
            }
            if let results = results {
                
                self.placesResults = results
                self.addressTableViewOutlet.reloadData()
            }
        })
    }
}

extension MapView : UITextFieldDelegate
{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if (self.locationSelectedDelegate != nil) && ((textField.text?.count)!>0){
            
            self.locationSelectedDelegate?.textfieldAction()
            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var textFieldText:NSString = textField.text! as NSString
        textFieldText = textFieldText.replacingCharacters(in: range, with: string) as NSString
        
        let dummyTextField:UITextField = UITextField()
        dummyTextField.text = textFieldText as String
        
        if (dummyTextField.text?.count)!>0{
            self.placeAutocomplete(placeName :dummyTextField.text!)
            if (self.addressTableViewOutlet.delegate == nil) && (self.addressTableViewOutlet.dataSource == nil)
            {
                self.addressTableViewOutlet.delegate = self
                self.addressTableViewOutlet.dataSource = self
            }
            self.addressTableViewOutlet.isHidden = false
            self.reCenterButton.setImage(#imageLiteral(resourceName: "close_icon"), for: UIControlState.normal)
        }
        else
        {
            self.reCenterButton.setImage(#imageLiteral(resourceName: "navigation_icon"), for: UIControlState.normal)
            self.addressTableViewOutlet.delegate = nil
            self.addressTableViewOutlet.dataSource = nil
            self.addressTableViewOutlet.isHidden = true
        }
        return true
    }
}

//MARK:- GMSMapViewDelegate

extension MapView : GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
            coordinates = position.target
            reverseGeoCodeCoordinte(coordinate: position.target)
//            print("coordinates = \(position.target)")
    }
}

extension MapView : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressTableViewCell") as! AddressTableViewCell
        
        if (indexPath.row < placesResults.count) {
            let nameObj = placesResults[indexPath.row]
            cell.addressNameOutlet.attributedText = nameObj.attributedFullText
            return cell
        }
        else
        {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "Without Address Cell") as! AddressTableViewCell
            return cell1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if placesResults.count>5{
            return 5+1
        }
        else{
            return placesResults.count+1
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row < placesResults.count) {
            if (self.locationSelectedDelegate != nil){
                self.locationSelectedDelegate?.locationSelected(location: placesResults[indexPath.row])
            }
        }
        else{
            if (self.locationSelectedDelegate != nil){
                self.locationSelectedDelegate?.locationSelectedWithoutAnyValue()
            }
        }
    }
}

extension MapView : LocationManagerDelegate
{
    func didUpdateLocations(coordinates: CLLocationCoordinate2D)
    {
        googlMapViewOutlet.animate(to: GMSCameraPosition(target: coordinates, zoom: 15, bearing: 0, viewingAngle: 0))
        locationManager.stopUpdatingLocation()
    }
    
    func didChangeAuthorization() {
        googlMapViewOutlet.settings.myLocationButton = true
    }
}
