//
//  FarmNameListViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 15/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class FarmNameListViewController: UIViewController {
    
    @IBOutlet weak var tableViewOutlet: UITableView!
    var farmAddressObj = [FarmAddress]()
    var  farmAddresObj =  FarmAddress(latitude: "", longitude: "", street: "", apartment: "", city: "", state: "", zipCode: "", country: "", farmName: "", farmId: "", isFarmAlreadyAdded: "", isLatLongAvailable: false)
    var prefId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.removeObject(forKey: "selectedValue")
        UserDefaults.standard.synchronize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAdresses()
        tableViewOutlet.estimatedRowHeight = 270
        tableViewOutlet.rowHeight = UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelButtonAction(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addFarmAction(_ sender: Any)
    {
        self.performSegue(withIdentifier: "new farm segue", sender: nil)
    }
    
    @IBAction func saveButtonAction(_ sender: Any)
    {
        addDataWhenNothingIsThere()
    }
    
    
    func getAdresses()
    {
        var params = [String :AnyObject]()
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let userId = UserDefaults.standard.object(forKey: "user_id") as! String!
        params["userId"] = userId as AnyObject
        params["token"] = token as AnyObject
        Helper.showPI()
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getAllFarms, params: params, success:
            { (response) in
                Helper.hidePI()
                let errFlag = response["errCode"].numberValue
                switch errFlag {
                case 0:
                    if let reponseData = response["response"].arrayObject
                    {
                        self.farmAddressObj = self.farmAddresObj.addingDataToFarmAddress(addressDataArray: reponseData)
                        self.tableViewOutlet.reloadData()
                    }
                    break
                    
                default:
                    let errMsg = response["Message"].stringValue
//                    print(errMsg)
                    break
                }
        }, failure: { (Error) in
            Helper.hidePI()
        })
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "new farm segue"
        {
            if let farmAddObj = sender as? FarmAddress
            {
                let controller = segue.destination as! FarmLocationViewController
                controller.farmAddObj = farmAddObj
            }
        }
    }
    
    func deleteFarmListAPI(rowNum : Int){
        Helper.showPI(_message: "Deleting Farm")
        let indexpath = IndexPath(row: rowNum, section: 0)
        let farmAddObj = self.farmAddressObj[indexpath.row]
        var params = [String :AnyObject]()
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let userId = UserDefaults.standard.object(forKey: "user_id") as! String!
        params["userId"] = userId as AnyObject
        params["token"] = token as AnyObject
        params["farmID"] = farmAddObj.farmId as AnyObject
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.deleteFarm, params: params, success:
            { (response) in
                Helper.hidePI()
                let errFlag = response["errCode"].numberValue
                switch errFlag {
                case 0:
                    self.tableViewOutlet.beginUpdates()
                    self.tableViewOutlet.deleteRows(at: [indexpath], with: .fade)
                    self.farmAddressObj.remove(at: rowNum)
                    self.tableViewOutlet.endUpdates()
                    break
                    
                default:
                    let errMsg = response["Message"].stringValue
                    break
                }
        }, failure: { (Error) in
            Helper.hidePI()
        })
    }
}

extension FarmNameListViewController : EditbuttonPressedDelegate{
    func editButtonPressedAction(index: Int) {
        let farmAddObj = self.farmAddressObj[index]
        self.editFarmDetails(farmAddobj: farmAddObj)
    }
}

extension FarmNameListViewController : UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Farm Address Cell") as! FarmNameTableViewCell
        let farmAddObj = self.farmAddressObj[indexPath.row]
        cell.farmAddressOutlet.text = farmAddObj.fullAdress
        cell.farmNameOutlet.text = farmAddObj.farmName
        cell.currentIndex = Int(indexPath.row)
        cell.EditbuttonPressedDelegate = self
        return cell
    }
    
    override func setEditing(_ editing: Bool, animated: Bool)
    {
        super.setEditing(editing, animated: animated)
        self.tableViewOutlet.setEditing(editing, animated: animated)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.farmAddressObj.count
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
            if self.farmAddressObj.count>1{
                return true
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            self.deleteFarmListAPI(rowNum: indexPath.row)
            UserDefaults.standard.removeObject(forKey: "selectedValue")
            UserDefaults.standard.synchronize()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let farmAddObj = self.farmAddressObj[indexPath.row]
        addDataToPost(farmAddobj : farmAddObj)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func addDataWhenNothingIsThere(){
        if self.prefId.characters.count>1 {
            let farmAddObj = self.farmAddressObj[0]
            addDataToPost(farmAddobj : farmAddObj)
        }
        else {
            var dictionary = [String:AnyObject]()
            dictionary["options"] = [] as AnyObject
            dictionary["pref_id"] = self.prefId as AnyObject
            UserDefaults.standard.set(dictionary, forKey: "selectedValue")
            UserDefaults.standard.synchronize()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func addDataToPost(farmAddobj : FarmAddress){
        var dictionary = [String:AnyObject]()
        var params = [String:AnyObject]()
        params["lat"] = farmAddobj.latitude as AnyObject
        params["long"] = farmAddobj.longitude as AnyObject
        params["street"] = farmAddobj.street as AnyObject
        params["apt"] = farmAddobj.apartment as AnyObject
        params["city"] = farmAddobj.city as AnyObject
        params["state"] = farmAddobj.state as AnyObject
        params["zipCode"] = farmAddobj.zipCode as AnyObject
        params["country"] = farmAddobj.country as AnyObject
        params["farmName"] = farmAddobj.farmName as AnyObject
        params["farmID"] = farmAddobj.farmId as AnyObject
        dictionary["lat"] = farmAddobj.latitude as AnyObject
        dictionary["long"] = farmAddobj.longitude as AnyObject
        dictionary["fullAddress"] = farmAddobj.fullAdress as AnyObject
        
        dictionary["options"] = [params] as AnyObject
        dictionary["pref_id"] = self.prefId as AnyObject
        UserDefaults.standard.set(dictionary, forKey: "selectedValue")
        UserDefaults.standard.synchronize()
        self.dismiss(animated: true, completion: nil)
    }
    
    func editFarmDetails(farmAddobj : FarmAddress)
    {
        self.performSegue(withIdentifier: "new farm segue", sender: farmAddobj)
    }
}
