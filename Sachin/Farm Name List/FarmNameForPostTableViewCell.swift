//
//  FarmNameForPostTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 22/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class FarmNameForPostTableViewCell: UITableViewCell {

    @IBOutlet weak var farmNameOutlet: UILabel!
    @IBOutlet weak var farmAddressOutlet: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
