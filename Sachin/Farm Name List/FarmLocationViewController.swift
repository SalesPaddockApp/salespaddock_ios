//
//  FarmLocationViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 16/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import GooglePlaces

class FarmLocationViewController: UIViewController {
    
    @IBOutlet weak var pickerViewOutlet: UIPickerView!
    @IBOutlet weak var doneViewOutlet: UIView!
    @IBOutlet weak var farmLocationViewOutlet: AddFarmLocationView!
    @IBOutlet weak var saveButtonOutlet: UIButton!
    @IBOutlet weak var tableViewOutlet: UITableView!
    @IBOutlet weak var mapViewOutlet: MapView!
    var isMapViewVisible : Bool = true
    var isComingFromPostList :Bool = false
    var farmAddObj : FarmAddress?
    var prefId = "58ad97cdc288860c2844571d"
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isComingFromPostList{
            self.navigationController?.hideNavigation()
        }
        
        self.farmLocationViewOutlet.FarmNameValidationDelegate = self
        self.mapViewOutlet.locationSelectedDelegate = self
        self.viewReplaced(boolValue: false, isDoneButtonAvailable: false)
        if let farmAddObj = self.farmAddObj {
            let coordinates : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: Double(farmAddObj.latitude!)!, longitude: Double(farmAddObj.longitude!)!)
            mapViewOutlet.coordinates = coordinates
            self.mapViewOutlet.addressTextFeild.text = farmAddObj.fullAdress
            mapViewOutlet.coordinates = coordinates
            if (self.mapViewOutlet.addressTextFeild.text?.count)!>0{
                mapViewOutlet.reCenterButton.setImage(#imageLiteral(resourceName: "close_icon"), for: UIControlState.normal)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.mapViewOutlet.updateToCoordinates(coordinates: coordinates)
            }
            self.textfieldAction()
        }
    }
    
    func viewReplaced(boolValue : Bool, isDoneButtonAvailable : Bool){
        view.endEditing(true)
        self.saveButtonOutlet.isHidden = !boolValue
        self.mapViewOutlet.isHidden = boolValue
        self.farmLocationViewOutlet.isHidden = !boolValue
        if isDoneButtonAvailable{
            self.doneViewOutlet.isHidden = false
        }
        else
        {
            self.doneViewOutlet.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- Buttons action
    @IBAction func reCenterButtonAction(_ sender: Any)
    {
        if (mapViewOutlet.addressTextFeild.text?.count)!>0{
            mapViewOutlet.addressTextFeild.text = ""
            mapViewOutlet.reCenterButton.setImage(#imageLiteral(resourceName: "navigation_icon"), for: UIControlState.normal)
            mapViewOutlet.addressTableViewOutlet.delegate = nil
            mapViewOutlet.addressTableViewOutlet.dataSource = nil
            mapViewOutlet.addressTableViewOutlet.isHidden = true
        }
        else if (mapViewOutlet.addressTextFeild.text?.count) == 0{
            mapViewOutlet.updateLocationToCurrent()
            mapViewOutlet.reCenterButton.setImage(#imageLiteral(resourceName: "navigation_icon"), for: UIControlState.normal)
        }
    }
    
    @IBAction func countryPickerDonButtonAction(_ sender: Any) {
        UIView.animate(withDuration: 0.4, animations:{
            self.farmLocationViewOutlet.bottomConstrainOutlet.constant = -260
            self.farmLocationViewOutlet.layoutIfNeeded()
        }, completion: nil)
    }
    
    @IBAction func doneButtonAction(_ sender: Any)
    {
        self.updateDataToServer()
    }
    
    @IBAction func backButtonAction(_ sender: Any)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonAction(_ sender: Any)
    {
        self.view.endEditing(true)
        if let farmAddressObj = farmLocationViewOutlet.getFarmData(farmAddObj: farmAddObj) {
            mapViewOutlet.maintainViewAfterSavingData(farmAddObj: farmAddressObj)
            self.saveButtonOutlet.isHidden = true
            self.mapViewOutlet.isHidden = false
            self.farmLocationViewOutlet.isHidden = true
            self.doneViewOutlet.isHidden = false
            self.mapViewOutlet.alpha = 1
        }
    }
    
    func updateDataToServer(){
        Helper.showPI()
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let userId = UserDefaults.standard.object(forKey: "user_id") as! String!
        if let farmAddressObj = farmLocationViewOutlet.getFarmData(farmAddObj : farmAddObj) {
            var timestamp = "\(Date().ticks)"
            if let farmId = farmAddressObj.farmId {
                timestamp = farmId
            }
            var params = [String :AnyObject]()
            params["isUpdated"] = 0 as AnyObject
            params["lat"] = "\(self.mapViewOutlet.coordinates.latitude )"as AnyObject
            params["long"] = "\(self.mapViewOutlet.coordinates.longitude)" as AnyObject
            params["street"] = farmAddressObj.street as AnyObject
            params["apt"] = farmAddressObj.apartment as AnyObject
            params["city"] = farmAddressObj.city as AnyObject
            params["state"] = farmAddressObj.state as AnyObject
            params["zipCode"] = farmAddressObj.zipCode as AnyObject
            params["country"] = farmAddressObj.country as AnyObject
            params["farmName"] = farmAddressObj.farmName as AnyObject
            params["isUpdated"] = farmAddressObj.isFarmAlreadyAdded as AnyObject
            params["userId"] = userId as AnyObject
            params["token"] = token as AnyObject
            params["farmID"] = timestamp as AnyObject
            
            AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.addOrUpdateFarm, params: params, success: { response in
                
                var dictionary = [String:AnyObject]()
                
                dictionary["options"] = [params] as AnyObject
                dictionary["pref_id"] = self.prefId as AnyObject
                dictionary["lat"] = farmAddressObj.latitude as AnyObject
                dictionary["long"] = farmAddressObj.longitude as AnyObject
                dictionary["fullAddress"] = farmAddressObj.fullAdress as AnyObject
                UserDefaults.standard.set(dictionary, forKey: "selectedValue")
                UserDefaults.standard.synchronize()
                if self.isComingFromPostList{
                    _ = self.navigationController?.popViewController(animated: true)
                }else{
                    self.dismiss(animated: true, completion: nil)
                }
            }) { (error) in
            }
        }
    }
}

extension FarmLocationViewController : FarmNameValidationDelegate, locationSelectedDelegate{
    
    func fieldsValidated(isValidated: Bool) {
        if isValidated{
            saveButtonOutlet.isEnabled = true
            self.saveButtonOutlet.isUserInteractionEnabled = true
            saveButtonOutlet.setTitleColor(UIColor.init(hexString: "#484848FF"), for: UIControlState.normal)
        }
        else{
            saveButtonOutlet.isEnabled = false
            self.saveButtonOutlet.isUserInteractionEnabled = false
            saveButtonOutlet.setTitleColor(UIColor.init(hexString: "#888B8DFF"), for: UIControlState.normal)
        }
    }
    
    func locationSelected(location: GMSAutocompletePrediction) {
        farmLocationViewOutlet.updateData(locationData: location)
        self.isMapViewVisible = false
        self.saveButtonOutlet.isHidden = false
        self.mapViewOutlet.alpha = 1
        UIView.animate(withDuration: 0.4, animations:{
            self.mapViewOutlet.alpha = 0
        }, completion: {
            finished in
            self.viewReplaced(boolValue: true, isDoneButtonAvailable: false)
        })
    }
    
    func locationSelectedWithoutAnyValue() {
        self.isMapViewVisible = false
        self.saveButtonOutlet.isHidden = false
        self.mapViewOutlet.alpha = 1
        UIView.animate(withDuration: 0.4, animations:{
            self.mapViewOutlet.alpha = 0
        }, completion: {
            finished in
            self.viewReplaced(boolValue: true, isDoneButtonAvailable: false)
        })
    }
    
    func textfieldAction() {
        self.isMapViewVisible = false
        self.saveButtonOutlet.isHidden = false
        fieldsValidated(isValidated: true)
        self.mapViewOutlet.alpha = 1
        if let farmAddObj = farmAddObj {
            farmLocationViewOutlet.updateDataFromModal(farmAdressObj: farmAddObj)
        }
        UIView.animate(withDuration: 0.4, animations:{
            self.mapViewOutlet.alpha = 0
        }, completion: {
            finished in
            self.viewReplaced(boolValue: true, isDoneButtonAvailable: false)
        })
    }
}

