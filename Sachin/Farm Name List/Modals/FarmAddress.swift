//
//  FarmAddress.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 21/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class FarmAddress: NSObject {
    
    var latitude : String?
    var longitude : String?
    var street : String?
    var apartment : String?
    var city : String?
    var state : String?
    var zipCode : String?
    var country : String?
    var farmName : String?
    var isFarmAlreadyAdded : String?
    var isLatLongAvailable :Bool?
    var farmId : String?
    var fullAdress : String?
    
    init(latitude : String, longitude : String, street : String, apartment : String, city : String, state : String, zipCode : String, country : String, farmName : String, farmId: String, isFarmAlreadyAdded : String, isLatLongAvailable : Bool) {
        self.latitude = latitude
        self.longitude = longitude
        self.street = street
        self.apartment = apartment
        self.city = city
        self.state = state
        self.zipCode = zipCode
        self.country = country
        self.farmName = farmName
        self.isFarmAlreadyAdded = isFarmAlreadyAdded
        self.isLatLongAvailable = isLatLongAvailable
        self.farmId = farmId
        self.fullAdress = farmName+" "+street+" "+apartment+" "+city+" "+state+" "+country
    }
    
    class func farmAddressObj() -> FarmAddress{
     let farmAddObj = FarmAddress(latitude: "", longitude: "", street: "", apartment: "", city: "", state: "", zipCode: "", country: "", farmName: "", farmId: "", isFarmAlreadyAdded: "", isLatLongAvailable:false)
        return farmAddObj
    }
    
    func addingDataToFarmAddress(addressDataArray : [Any]) -> [FarmAddress]
    {
        var farmAddresses = [FarmAddress]()
        for address in addressDataArray
        {
            if let addressDict = address as? [String: Any]{
                
                if let lat = addressDict["lat"], let long = addressDict["long"], let state = addressDict["state"] as? String, let farmID = addressDict["farmID"] as? String, let country = addressDict["country"] as? String, let zipCode = addressDict["zipCode"] as? String, let city = addressDict["city"] as? String, let street = addressDict["street"] as? String, let farmName = addressDict["farmName"] as? String, let apt = addressDict["apt"] as? String
                {
                    let farmAddObj = FarmAddress(latitude: "\(lat)", longitude: "\(long)", street: street, apartment: apt, city: city, state: state, zipCode: zipCode, country: country, farmName: farmName, farmId: farmID, isFarmAlreadyAdded: "1", isLatLongAvailable: true)
                    farmAddresses.append(farmAddObj)
                }
            }
        }
        return farmAddresses
    }
    
    func getDictionaryDataFromFarmAddressObj(farmAddObj : FarmAddress) -> [String :String]{
        var params = [String :String]()
        params["lat"] = farmAddObj.latitude
        params["long"] = farmAddObj.longitude
        params["state"] = farmAddObj.state
        params["farmID"] = farmAddObj.farmId
        params["country"] = farmAddObj.country
        params["zipCode"] = farmAddObj.zipCode
        params["city"] = farmAddObj.city
        params["street"] = farmAddObj.street
        params["farmName"] = farmAddObj.farmName
        params["apt"] = farmAddObj.apartment
        
        return params
    }
    
}
