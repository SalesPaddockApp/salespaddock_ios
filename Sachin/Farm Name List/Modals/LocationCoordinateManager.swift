//
//  LocationCoordinateManager.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 24/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase
import FirebaseInstanceID

protocol LocationManagerDelegate {
    func didChangeAuthorization()
    func didUpdateLocations(coordinates:CLLocationCoordinate2D)
}

class LocationCoordinateManager: NSObject {
    
    let locationManager = CLLocationManager()
    var currentCoordinates = CLLocationCoordinate2D()
    var LocationManagerDelegate : LocationManagerDelegate? = nil
    static let sharedInstance: LocationCoordinateManager = {
        
        let instance = LocationCoordinateManager()
        instance.locationManager.delegate = instance
        instance.locationManager.requestWhenInUseAuthorization()
        
        return instance
    }()
    
    override init() {
        super.init()
    }
    
    func startUpdatingLocation(){
        locationManager.startUpdatingLocation()
    }
    
    func stopUpdatingLocation(){
        locationManager.stopUpdatingLocation()
    }
    
    func updateLocation(){
        if let userID = UserDefaults.standard.object(forKey: "user_id") as? String{
            if userID.count>0 {
                let deviceid = UIDevice.current.identifierForVendor?.uuidString
                guard let pushToken = FIRInstanceID.instanceID().token() else { return }
                if let token = UserDefaults.standard.object(forKey: "session_token") as? String{
                    var params = [String : AnyObject]()
                    params["userId"] = userID as AnyObject
                    params["lat"] = "\(currentCoordinates.latitude)" as AnyObject
                    params["long"] = "\(currentCoordinates.longitude)" as AnyObject
                    params["deviceId"] = deviceid as AnyObject
                    params["token"] = token as AnyObject
                    params["pushToken"] = pushToken as AnyObject
                    AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.updateLocation, params: params, success:
                        { (response) in
                            let errFlag = response["errCode"].numberValue
                            switch errFlag {
                            case 0:
                                UserDefaults.standard.set(response["totalCount"].intValue, forKey: "Unread Notification Count")
                                UserDefaults.standard.synchronize()
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "badgeCountUpdate"), object: nil)
                                
                            default:
                                _ = response["Message"].stringValue
                            }
                    }, failure: { (Error) in
                    })
                }
            }
        }
    }
    
    func checkLocationStatus() -> Bool {
        var isLocationenabled = true
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied {
            isLocationenabled = true
        }else{
            isLocationenabled = false
            let alert = UIAlertController(title: "Location Services Disabled!", message: "Please enable Location Based Services for better results! We promise to keep your location private", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { action in
                if let url = URL(string: "App-Prefs:root=LOCATION_SERVICES") {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, completionHandler: .none)
                    }
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            if let navController = UIApplication.shared.windows.first?.rootViewController as? UINavigationController{
                DispatchQueue.main.async {
                    navController.visibleViewController?.present(alert, animated: true, completion: nil)
                }
            }
            else if let viewController = UIApplication.shared.windows.first?.rootViewController {
                DispatchQueue.main.async {
                    viewController.present(alert, animated: true, completion: nil)
                }
            }
        }
        return isLocationenabled
    }
}


extension LocationCoordinateManager : CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.LocationManagerDelegate?.didChangeAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            currentCoordinates = location.coordinate
            self.LocationManagerDelegate?.didUpdateLocations(coordinates: location.coordinate)
            locationManager.stopUpdatingLocation()
        }
    }
}
