//
//  FarmListName.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 25/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class FarmListName: NSObject {
    
    var farmNameOutlet : String
    var isSelected : Bool
    
    init(farmNameOutlet : String,isSelected : Bool) {
        self.farmNameOutlet = farmNameOutlet
        self.isSelected = isSelected
    }
}
