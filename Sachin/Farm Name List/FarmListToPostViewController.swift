//
//  FarmListToPostViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 22/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import SwiftyJSON

class FarmListToPostViewController: UIViewController {
    var farmAddressObj = [FarmAddress]()
    var farmAddresObj = FarmAddress.farmAddressObj()
    var responseData = [JSON]()
    var imageId = String()
    var postId = String()
    var selectedValuesOfList = [AnyObject]()
    var selectedValues = [AnyObject]()
    var selectedValueArray = [FarmAddress]()
    
    @IBOutlet weak var tableViewOutlet: UITableView!
    let FarmPrefId = "58ad97cdc288860c2844571d"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewOutlet.estimatedRowHeight = 70
        self.tableViewOutlet.rowHeight = UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getFarmNames()
        self.navigationController?.showNavigation()
        self.selectedValueArray.removeAll()
    }
    
    @IBAction func saveButtonAction(_ sender: Any)
    {
        if selectedValueArray.isEmpty{
            //error will come here
            return
        }
        self.performSegue(withIdentifier: "Segue To Create Post", sender: nil)
    }
    
    @IBAction func addFarmAction(_ sender: Any)
    {
        self.performSegue(withIdentifier: "Add new farm Segue", sender: nil)
    }
    
    func getFarmNames(){
        var params = [String :AnyObject]()
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let userId = UserDefaults.standard.object(forKey: "user_id") as! String!
        params["userId"] = userId as AnyObject
        params["token"] = token as AnyObject
        Helper.showPI()

        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getAllFarms, params: params, success:
            { (response) in
                Helper.hidePI()
                let errFlag = response["errCode"].numberValue
                switch errFlag {
                case 0:
                    if let reponseData = response["response"].arrayObject
                    {
                        self.farmAddressObj = self.farmAddresObj.addingDataToFarmAddress(addressDataArray: reponseData)
                        self.tableViewOutlet.reloadData()
                    }
                    break
                    
                default:
                    let errMsg = response["Message"].stringValue
                    break
                }
        }, failure: { (Error) in
            Helper.hidePI()
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Segue To Create Post"{
            let controller = segue.destination as! breedCategoryViewController
            var dictionary = [String:AnyObject]()
            let dataObj = farmAddresObj.getDictionaryDataFromFarmAddressObj(farmAddObj: selectedValueArray[0])
            dictionary["options"] = [dataObj] as AnyObject
            dictionary["pref_id"] = FarmPrefId as AnyObject
            self.selectedValues.append(dictionary as AnyObject!)
            controller.selectedValuesOfList = self.selectedValues as Array!
            controller.responseData = self.responseData
            controller.imageId = self.imageId
            controller.lat = dataObj["lat"]!
            controller.long = dataObj["long"]!
            controller.prductId = self.postId
        }
        else if segue.identifier == "Add new farm Segue"
        {
            let controller = segue.destination as! FarmLocationViewController
            controller.isComingFromPostList = true
        }
    }
}

extension FarmListToPostViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FarmNameForPostTableViewCell", for: indexPath) as! FarmNameForPostTableViewCell
        let farmNameObj = self.farmAddressObj[indexPath.row]
        cell.farmNameOutlet.text = farmNameObj.farmName
        cell.farmAddressOutlet.text = farmNameObj.fullAdress
        if self.selectedValueArray.contains(farmNameObj)
        {
            cell.backgroundColor = UIColor.init(hexString: "#E7E7E7FF")
        }
        else{
            cell.backgroundColor = UIColor.white
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.farmAddressObj.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let cell = tableView.cellForRow(at: indexPath) as! FarmNameForPostTableViewCell
        let farmNameObj = self.farmAddressObj[indexPath.row]
        
        if let previousValue = self.selectedValueArray.first
        {
            let rowIndex = self.farmAddressObj.index(of:(previousValue))
            let previousCell = tableView.cellForRow(at: IndexPath.init(row: rowIndex!, section: 0)) as! FarmNameForPostTableViewCell
            previousCell.backgroundColor = UIColor.white
            self.selectedValueArray.removeAll()
        }
        self.selectedValueArray.append(farmNameObj)
        cell.backgroundColor = UIColor.init(hexString: "#E7E7E7FF")
    }
}
