//
//  RideAlertViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 10/01/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import SwiftyJSON

class RideAlertViewController: UIViewController,UITextViewDelegate {
    
    @IBOutlet var lengthOutlet: UILabel!
    var responseData = [String : JSON]()
    @IBOutlet weak var msgTextViewOutlet: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lengthOutlet.text = "200 characters left"
        msgTextViewOutlet.becomeFirstResponder()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButtonAction(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func confirmAndMessageAction(_ sender: Any)
    {
        sendRideAlert()
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count
        lengthOutlet.text = "\(200 - numberOfChars) characters left"
        return numberOfChars < 200
    }
    
    func sendRideAlert(){
        
        Helper.showPI()
        
        if(self.msgTextViewOutlet.text.characters.count == 0){
            self.showAlert("Error", message: "Please add some message here")
        }
        
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let userId = UserDefaults.standard.object(forKey: "user_id") as! String!
        
        let dictionary = responseData
        let postId = dictionary["postId"]?.stringValue
        let showId = dictionary["showId"]?.stringValue
        let message = self.msgTextViewOutlet.text
        
        let params:[String : AnyObject] = ["token":token! as AnyObject,"userId":userId! as AnyObject, "postId":postId as AnyObject,"showId":showId as AnyObject, "mess":message! as AnyObject]
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.sendAlerts, params: params as [String : AnyObject]?
            , success:
            { (response) in
                let errFlag = response["errCode"].numberValue
                Helper.hidePI()
                switch errFlag {
                case 0:
                    self.showAlertWithDismissal("Success", message: "Alert sent successfully")
                    break
                default:
                    break
                }
        }, failure: { (Error) in
            Helper.hidePI()
        })
    }
    
}
