//
//  EnterVerificationCodeViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 07/01/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class EnterVerificationCodeViewController: UIViewController {
    
    struct Constants {
        static let totalTime = 30.0
        static let elapsingTime = 1.0
    }
    
    @IBOutlet weak var confirmationCodeOutlet: UITextField!
    @IBOutlet weak var resendButtonOutlet: UIButton!
    
    var timer:Timer!
    var counrtyCode : String!
    var userEnteredNumber : String!
    var remainingTime:Double!
    var phoneNumber = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.activateTimer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmButtonAction(_ sender: Any) {
        self.confirmCode()
    }
    
    @IBAction func resendButtonAction(_ sender: Any) {
        self.activateTimer()
        self.requestOTP()
    }
    
    func requestOTP(){
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let userId = UserDefaults.standard.object(forKey: "user_id") as! String!
        let params:[String : AnyObject] = ["token":token! as AnyObject,"userId":userId! as AnyObject,"phone":self.phoneNumber as AnyObject]
        Helper.showPI()
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.sendOTP, params: params as [String : AnyObject]?
            , success:
            
            { (response) in
                
                Helper.hidePI()
                
                let errFlag = response["errCode"].numberValue
                switch errFlag {
                case 0:
                    Helper.hidePI()
                    break
                    
                default:
                    let errMsg = response["Message"].stringValue
//                    print(errMsg)
                    break
                }
        }, failure: { (Error) in
            Helper.hidePI()
        })
    }
    
    func activateTimer()
    {
        remainingTime = Constants.totalTime
        if (timer != nil)
        {
            timer.invalidate()
        }
        timer = Timer.scheduledTimer(timeInterval: Constants.elapsingTime, target: self, selector: #selector(self.countDown), userInfo: nil, repeats: true)
    }
    
    @objc func countDown()
    {
        if remainingTime > 0{
            remainingTime = remainingTime - Constants.elapsingTime
            self.resendButtonOutlet.isEnabled = false
            self.resendButtonOutlet.setTitle("resend code in 00:\(Int(remainingTime))", for: .normal)
            self.resendButtonOutlet.setTitleColor(UIColor.lightGray, for: .normal)
        }
        else {
            timer.invalidate()
            self.resendButtonOutlet.isEnabled = true
            self.resendButtonOutlet.setTitle("resend code", for: .normal)
            var color:UIColor!
            color = UIColor(red: 0/255, green: 90/255, blue: 255/255, alpha: 1.0)
            self.resendButtonOutlet.setTitleColor(color, for: .normal)
        }
    }
    
    
    func confirmCode(){
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let userId = UserDefaults.standard.object(forKey: "user_id") as! String!
        let params:[String : AnyObject] = ["token":token! as AnyObject,"userId":userId! as AnyObject,"otp":"\(self.confirmationCodeOutlet.text!)" as AnyObject,
                                           "phone":phoneNumber as AnyObject]
        Helper.showPI()
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.validateNumber, params: params as [String : AnyObject]?
            , success:
            
            { (response) in
                
                Helper.hidePI()
                
                let errFlag = response["errCode"].numberValue
                switch errFlag {
                case 0:
                    self.performSegue(withIdentifier: "number verified Segue", sender: nil)
                    break
                    
                default:
                    let errMsg = response["Message"].stringValue
                    self.showAlert("Error!", message: errMsg)
                    break
                }
        }, failure: { (Error) in
            Helper.hidePI()
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "number verified Segue"
        {
            let controler = segue.destination as! AddedNumberViewController
            controler.phoneNum = self.phoneNumber
        }
    }
}

extension EnterVerificationCodeViewController : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var textFieldText:NSString = textField.text! as NSString
        textFieldText = textFieldText.replacingCharacters(in: range, with: string) as NSString
        return textFieldText.length<=4
    }
}
