//
//  PhoneNumberVerificationViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 07/01/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class PhoneNumberVerificationViewController: UIViewController {

    @IBOutlet weak var addPhoneNumberVieWOUTLET: UIView!
    var phoneNumberString = String()
    @IBOutlet weak var editViewOutlet: UIView!
    @IBOutlet weak var phoneNumberOutlet: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        if (phoneNumberString.characters.count == 0) {
            self.addPhoneNumberVieWOUTLET.isHidden = false
            self.editViewOutlet.isHidden = true
        }
        else{
            self.addPhoneNumberVieWOUTLET.isHidden = true
            self.editViewOutlet.isHidden = false
            self.phoneNumberOutlet.text = phoneNumberString
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
