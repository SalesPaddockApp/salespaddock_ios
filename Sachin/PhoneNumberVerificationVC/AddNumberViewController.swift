//
//  AddNumberViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 07/01/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class AddNumberViewController: UIViewController {

    @IBOutlet weak var countryCodeOutlet: UILabel!
    @IBOutlet weak var numberOutlet: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            
            countryCodeOutlet.text = VNHCountryPicker.dialCode(code: countryCode).dialCode
        }
        self.numberOutlet.becomeFirstResponder()
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendButtonAction(_ sender: Any) {
        
        self.sendVerificationCode()
    }
    
    @IBAction func countryPickerAction(_ sender: Any) {
        self.performSegue(withIdentifier: "country Picker Segue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "country Picker Segue"
        {
            let controller = segue.destination as! VNHCountryPicker
            controller.delegate = self
        }
        else if segue.identifier == "confirmation Screen Segue"
        {
            //confirmation Screen Segue
            let controller = segue.destination as! EnterVerificationCodeViewController
            controller.phoneNumber = "\(self.countryCodeOutlet.text!)\(self.numberOutlet.text!)"
        }
        
    }
    
    
    func sendVerificationCode(){
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let userId = UserDefaults.standard.object(forKey: "user_id") as! String!
        let params:[String : AnyObject] = ["token":token! as AnyObject,"userId":userId! as AnyObject,"phone":"\(self.countryCodeOutlet.text!)\(self.numberOutlet.text!)" as AnyObject]
        Helper.showPI()
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.sendOTP, params: params as [String : AnyObject]?
            , success:
            
            { (response) in
                
                Helper.hidePI()
                
                let errFlag = response["errCode"].numberValue
                switch errFlag {
                case 0:
                    self.performSegue(withIdentifier: "confirmation Screen Segue", sender: nil)
                    break
                    
                default:
                    let errMsg = response["Message"].stringValue
                    break
                }
        }, failure: { (Error) in
            Helper.hidePI()
        })
    }
}

extension AddNumberViewController : VNHCountryPickerDelegate{
    
    func didPickedCountry(country: VNHCounty) {
        self.countryCodeOutlet.text = country.dialCode
    }
}
