//
//  AddedNumberViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 09/01/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class AddedNumberViewController: UIViewController {

    var phoneNum = String()
    @IBOutlet weak var phoneNumberOutlet: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.phoneNumberOutlet.text = phoneNum
        UserDefaults.standard.set(phoneNum, forKey: "phoneNum")
        UserDefaults.standard.synchronize()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
