//
//  DisciplineName.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 13/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class DisciplineName: NSObject {
    
    var nameOfDiscipline : String
    var isSelected : Bool
    
    init(nameOfDiscipline : String,isSelected : Bool) {
        self.nameOfDiscipline = nameOfDiscipline
        self.isSelected = isSelected
    }
    
    class func getSelectedCount(disciplines : [DisciplineName]) -> [String]{
        var selectedDisiplines = [String]()
        for discipline in disciplines{
            if discipline.isSelected
            {
                selectedDisiplines.append(discipline.nameOfDiscipline)
            }
        }
        return selectedDisiplines
    }
    
    class func getAllDisciplineList(disciplines:[DisciplineName]) -> [String]{
        var selectedDisiplines = [String]()
        for discipline in disciplines{
            selectedDisiplines.append(discipline.nameOfDiscipline)
        }
        return selectedDisiplines
    }
    
}
