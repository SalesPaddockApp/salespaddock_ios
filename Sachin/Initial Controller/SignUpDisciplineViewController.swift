//
//  SignUpDisciplineViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 13/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SignUpDisciplineViewController: UIViewController {
    
    @IBOutlet weak var nxtBtnOutlet: UIButton!
    var disciplineNames = [DisciplineName]()
    var disciplineId = String()
    @IBOutlet weak var collectionViewOutlet: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.getDisciplineLists()
    }
    
    @IBAction func skipButtonAction(_ sender: Any)
    {
        let disciplineList = DisciplineName.getAllDisciplineList(disciplines: self.disciplineNames)
        applyForDesciplines(disciplineList: disciplineList)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backButtonAction(_ sender: Any)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonAction(_ sender: Any)
    {
        let disciplines = DisciplineName.getSelectedCount(disciplines: self.disciplineNames)
        if disciplines.count == 0{
            self.showAlert("Error", message: "Please select atleast one discipline")
            return
        }
        applyForDesciplines(disciplineList : disciplines)
    }
    
    func getDisciplineLists(){
        Helper.showPI()
        self.disciplineNames.removeAll()
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getAppliedDescipline, params: nil, success:
            { (response) in
                Helper.hidePI()
                let errFlag = response["errCode"].numberValue
                switch errFlag {
                case 0:
                    
                    if let reponseData = response["response"].dictionaryObject
                    {
                        if let id  = reponseData["id"] as? String{
                            self.disciplineId = id
                        }
                        if let values = reponseData["values"] as? [String]
                        {
                            for string in values {
                                let disciplineName = DisciplineName(nameOfDiscipline : string,isSelected : false)
                                self.disciplineNames.append(disciplineName)
                            }
                        }
                    }
                    self.collectionViewOutlet.reloadData()
                    break
                    
                default:
                    let errMsg = response["Message"].stringValue
                    break
                }
        }, failure: { (Error) in
            Helper.hidePI()
        })
    }
    
    func applyForDesciplines(disciplineList : [String])
    {
        var dictionary = [String:AnyObject]()
        dictionary["values"] = disciplineList as AnyObject
        dictionary["pref_id"] = self.disciplineId as AnyObject
        let filterData:[String:Any] = ["filters":[dictionary]]
        UserDefaults.standard.setValue(filterData, forKey: "FiltersData")
        UserDefaults.standard.synchronize()
        self.performSegue(withIdentifier: "logIn To Tab Bar Segue", sender: nil)
    }
}

extension SignUpDisciplineViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DisciplineNameCollectionViewCell", for: indexPath) as! DisciplineNameCollectionViewCell
        let name : DisciplineName = disciplineNames[indexPath.row]
        if name.isSelected
        {
            cell.viewIsSelected()
        }
        else{
            cell.viewUnselected()
        }
        cell.disciplineNameOutlet.text = name.nameOfDiscipline
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return disciplineNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let disciplineObj = disciplineNames[indexPath.row]
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width-30, height: 50))
        label.text = disciplineObj.nameOfDiscipline
        let width = label.intrinsicContentSize.width
        return CGSize(width: width+50, height: 36)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let nameObj : DisciplineName = disciplineNames[indexPath.row]
        nameObj.isSelected = !nameObj.isSelected
        let cell = collectionView.cellForItem(at: indexPath) as! DisciplineNameCollectionViewCell
        if nameObj.isSelected
        {
            collectionView.performBatchUpdates({
                cell.viewIsSelected() }, completion: nil)
        }
        else{
            collectionView.performBatchUpdates({
                cell.viewUnselected() }, completion: nil)
        }
        disciplineNames[indexPath.row] = nameObj
        
        let disciplines = DisciplineName.getSelectedCount(disciplines: self.disciplineNames)
        if disciplines.count == 0{
            nxtBtnOutlet.isEnabled = false
            nxtBtnOutlet.setImage(UIImage.init(named:"next_btn_off"), for: .normal)
        }
        else{
            nxtBtnOutlet.isEnabled = true
            nxtBtnOutlet.setImage(UIImage.init(named:"next_btn_on"), for: .normal)
        }
    }
}
