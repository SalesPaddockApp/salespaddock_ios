//
//  DisciplineNameCollectionViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 13/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class DisciplineNameCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var disciplineNameOutlet: UILabel!
    @IBOutlet weak var backgroundViewOutlet: UIView!
    @IBOutlet weak var innerViewOutlet: UIView!
    @IBOutlet weak var iconImageOutlet: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func viewIsSelected()
    {
        self.disciplineNameOutlet.textColor = .white
        self.backgroundViewOutlet.backgroundColor = UIColor.white
        self.innerViewOutlet.backgroundColor = UIColor.init(hexString: "#0bb39dff")
        self.iconImageOutlet.image = #imageLiteral(resourceName: "close_btn")
    }
    
    func viewUnselected(){
        self.disciplineNameOutlet.textColor = UIColor.init(hexString: "#484848ff")
        self.backgroundViewOutlet.backgroundColor = UIColor.init(hexString: "#484848ff")
        self.innerViewOutlet.backgroundColor = UIColor.white
        self.iconImageOutlet.image = #imageLiteral(resourceName: "right")
    }
}
