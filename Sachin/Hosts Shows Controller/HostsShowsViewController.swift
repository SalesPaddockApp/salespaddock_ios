//
//  HostsShowsViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 01/10/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import SwiftyJSON

class HostsShowsViewController: UIViewController {
    var showsData = [JSON]()
    var index = 0
    var isComingFromShowDetails = false
    var postId = String()
    
    @IBOutlet weak var tableViewOutlet: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        let height = (view.frame.size.width - 40)*67/100
        self.tableViewOutlet.estimatedRowHeight = 60
        self.tableViewOutlet.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(isComingFromShowDetails) {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
        else {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getHostsShows()
        index = 0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "show Details"{
            let controller = segue.destination as! ShowDetailsViewController
            controller.showData = (sender as! JSON).dictionaryValue
            controller.isComingFromShowDetails = isComingFromShowDetails
            controller.postId = self.postId
        }
    }
}

extension HostsShowsViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : HostsShowTableViewCell = tableView.dequeueReusableCell(withIdentifier: "HostsShowTableViewCell") as! HostsShowTableViewCell
        let data = (showsData[indexPath.row]).dictionaryValue
        cell.delegate = self
        
        cell.showNameOutlet?.text = data["key"]!.stringValue
        
        cell.collectionViewData = (data["data"]?.arrayValue)! as [AnyObject]
        cell.collectionViewOutlet.reloadData()
        index = index+1
        return cell;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return showsData.count
    }
    
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return UITableViewAutomaticDimension
    //    }
    
    func getHostsShows(){
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let userId = UserDefaults.standard.object(forKey: "user_id") as! String!
        let params = ["token":token!,"userId":userId!]
        
        Helper.showPI()
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getAllHostShow, params: params as [String : AnyObject]?, success: { (response) in
            Helper.hidePI()
            
            let errFlag = response["errCode"].numberValue
            switch errFlag {
            case 0:
                self.showsData = response["response"].arrayValue as [JSON]
                self.tableViewOutlet.reloadData()
                break
            default:
                let errMsg = response["Message"].string
                break
            }
        }, failure: { (Error) in
            Helper.hidePI()
        })
    }
}

extension HostsShowsViewController : collectionViewCellTaped {
    
    func cellTappedWithDetails(data: JSON) {
        
        let showData = data
        self.performSegue(withIdentifier: "show Details", sender: showData)
    }
}
