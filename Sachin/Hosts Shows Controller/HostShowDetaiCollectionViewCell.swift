//
//  HostShowDetaiCollectionViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 01/10/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class HostShowDetaiCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var hostsShowNameOutlet: UILabel!
    @IBOutlet weak var hostShowImageOutlet: UIImageView!
}
