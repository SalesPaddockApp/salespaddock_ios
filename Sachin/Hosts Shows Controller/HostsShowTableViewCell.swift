//
//  HostsShowTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 01/10/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher

protocol collectionViewCellTaped {
    
    func cellTappedWithDetails(data : JSON)
}

class HostsShowTableViewCell: UITableViewCell {

    var collectionViewData = [AnyObject]()
    @IBOutlet weak var showNameOutlet: UILabel!
    @IBOutlet weak var collectionViewOutlet: UICollectionView!
    var delegate:collectionViewCellTaped! = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.collectionViewOutlet.delegate = self
        self.collectionViewOutlet.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension HostsShowTableViewCell : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:HostShowDetaiCollectionViewCell = collectionView .dequeueReusableCell(withReuseIdentifier: "HostShowDetaiCollectionViewCell", for: indexPath) as! HostShowDetaiCollectionViewCell
        let dataDict = collectionViewData[indexPath.row] as! JSON
        cell.hostsShowNameOutlet.text = dataDict["showName"].stringValue
        let url = URL(string: dataDict["image"].stringValue)

        cell.hostShowImageOutlet.kf.indicatorType = .activity
        cell.hostShowImageOutlet.kf.indicator?.startAnimatingView()
        cell.hostShowImageOutlet.kf.setImage(with: url,
                                              placeholder: UIImage.init(named: "horse_default_image"),
                                              options: [.transition(ImageTransition.fade(1))],
                                              progressBlock: { receivedSize, totalSize in
            },
                                              completionHandler: { image, error, cacheType, imageURL in
                                                cell.hostShowImageOutlet.kf.indicator?.stopAnimatingView()
        })

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionViewData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        var frameOfCell = self.frame
        frameOfCell.size.width -= 25*2 // Padding
        frameOfCell.size.height = self.frame.size.height-92
        
        return frameOfCell.size
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageWidth: Float = Float(self.collectionViewOutlet!.frame.width) - 40 //480 + 50
        // width + space
        let currentOffset: Float = Float(scrollView.contentOffset.x)
        let targetOffset: Float = Float(targetContentOffset.pointee.x)
        var newTargetOffset: Float = 0
        if targetOffset > currentOffset {
            newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth
        }
        else {
            newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth
        }
        if newTargetOffset < 0 {
            newTargetOffset = 0
        }
        else if (newTargetOffset > Float(scrollView.contentSize.width)){
            newTargetOffset = Float(Float(scrollView.contentSize.width))
        }
        
        targetContentOffset.pointee.x = CGFloat(currentOffset)
        scrollView.setContentOffset(CGPoint(x: CGFloat(newTargetOffset), y: scrollView.contentOffset.y), animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if (delegate != nil) {
            delegate.cellTappedWithDetails(data: collectionViewData[indexPath.row] as! JSON)
        }
    }
    
}
