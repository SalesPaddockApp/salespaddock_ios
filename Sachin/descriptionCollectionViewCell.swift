//
//  descriptionCollectionViewCell.swift
//  salesPaddock
//
//  Created by Rahul Sharma on 09/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit

class descriptionCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var viewsLabelOutlet: UILabel!
    @IBOutlet weak var showNameOutlet: UILabel!
    @IBOutlet weak var imageOutlet: UIImageView!
    @IBOutlet weak var horseNameOutlet: UILabel!
    @IBOutlet weak var requestsOutlet: UILabel!
    
}
