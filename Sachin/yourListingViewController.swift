//
//  yourListingViewController.swift
//  salesPaddock
//
//  Created by Rahul Sharma on 09/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher

class yourListingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var listing = [JSON]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //        self.navigationItem.setHidesBackButton(true, animated:true);
        // Do any additional setup after loading the view.
        self.tabBarController?.navigationItem.hidesBackButton = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.navigationItem.hidesBackButton = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getLists()
        self.tabBarController?.navigationItem.hidesBackButton = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listing.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell:yourListingViewCell = self.tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.yourListingViewControllerCell) as! yourListingViewCell!
        
        let dataDict = listing[indexPath.row].dictionaryValue
        if (dataDict["isCompleted"]!.boolValue){
            cell.statusOfList.text = String("Listed")
            cell.statusOfList.textColor = UIColor(hexString: "#5ece3bff")
        }
        else{
            cell.statusOfList.text = String("\(dataDict["steps"]!.stringValue) steps")
            cell.statusOfList.textColor = UIColor(hexString: "#d7a63cff")
        }
        
        cell.horseNameOutlet.text = String(dataDict["name"]!.stringValue)
        if let isActivated = dataDict["isActivated"]?.boolValue
        {
            if isActivated{
               cell.pendingAdminApprovalOutlet.isHidden = true
            }
            else
            {
                cell.pendingAdminApprovalOutlet.isHidden = false
            }
        }
        
        let imageDict = dataDict["image"]?.dictionaryValue
        let imageArray = imageDict?["options"]?.arrayValue
        if(!(imageArray?.isEmpty)!){
            if  let dataDictionary = imageArray?[0].dictionaryValue
            {
                if(dataDictionary.isEmpty)
                {
                    return cell
                }
                var url = URL(string: (imageArray?[0].stringValue)!)
                
                if(dataDictionary["type"]?.stringValue == "0")
                {
                    let urlStr = (dataDictionary["imageUrl"]?.stringValue)!
                    url = URL(string: urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                }
                else
                {
                    let urlStr = (dataDictionary["thumbnailUrl"]?.stringValue)!
                    url = URL(string: urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                }
                cell.imageViewOutlet.kf.indicatorType = .activity
                cell.imageViewOutlet.kf.indicator?.startAnimatingView()
                cell.imageViewOutlet?.kf.setImage(with: url,
                                                  placeholder: UIImage.init(named: "horse_default_image"),
                                                  options: [.transition(ImageTransition.fade(1))],
                                                  progressBlock: { receivedSize, totalSize in
                },
                                                  completionHandler: { image, error, cacheType, imageURL in
                                                    cell.imageViewOutlet.kf.indicator?.stopAnimatingView()
                })
            }
        }
        else{
            cell.imageViewOutlet.image = #imageLiteral(resourceName: "horse_default_image")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            let dataDict = self.listing[indexPath.row].dictionaryValue
            self.deletePost(postId: (dataDict["postId"]?.stringValue)!)
        }
        return [delete]
    }
   
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "direct Segue", sender: indexPath)
    }
    
    @IBAction func addnewListingBtnClicked(_ sender: AnyObject)
    {
        self.performSegue(withIdentifier: "addNewListingsegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "direct Segue"
        {
            let controller = segue.destination as! breedCategoryViewController
            let indexPath = sender as! IndexPath
            let dataDict = listing[indexPath.row].dictionaryValue
            
            let imageDict = dataDict["image"]
            let imageArray = imageDict?["options"].arrayValue
            if(!(imageArray?.isEmpty)!){
                controller.imageUrl = (imageArray?[0].stringValue)!
            }
            controller.imageId = (imageDict?["id"].stringValue)!
            controller.responseData = dataDict["data"]!.arrayValue
            controller.isComingFromList = true
            controller.postId = dataDict["postId"]!.stringValue
            controller.imageData = dataDict["image"]!.dictionaryValue

            if let isActivated = dataDict["isActivated"]?.boolValue
            {
                if isActivated{
                    controller.isAdminApproved = true
                }
                else{
                    controller.isAdminApproved = false
                }
            }

            var totalCountSteps = 0
            var completedCount = 0
            if let totalCount = dataDict["totalCount"]?.number
            {
                totalCountSteps = Int(totalCount)
            }
            if let steps = dataDict["steps"]?.number
            {
                completedCount = Int(steps)
            }
            controller.totalNumberOfSteps = totalCountSteps
            controller.numberofsteps = completedCount
        }
    }
    @IBAction func addMoreActn(_ sender: Any)
    {
        self.performSegue(withIdentifier: "addNewListingsegue", sender: self)
    }
    @IBAction func addMoreAction(_ sender: Any)
    {
        self.performSegue(withIdentifier: "addNewListingsegue", sender: self)
    }
}

extension yourListingViewController{
    
    func getLists(){
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let userId = UserDefaults.standard.object(forKey: "user_id") as! String!
        let params:[String : AnyObject] = ["token":token! as AnyObject,"userId":userId! as AnyObject]
        Helper.showPI()
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getAllPostsHost, params: params as [String : AnyObject]?
            , success:
            { (response) in
                
                Helper.hidePI()
                
                let errFlag = response["errCode"].numberValue
                switch errFlag {
                case 0:
                    self.listing = response["response"]["result"].arrayValue
                    self.tableView.reloadData()
                    break
                default:
                    let errMsg = response["Message"].stringValue
                    break
                }
        }, failure: { (Error) in
            Helper.hidePI()
        })
    }
    
    func deletePost(postId : String)
    {
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let params:[String : AnyObject] = ["token":token! as AnyObject,"postId":postId as AnyObject]
        Helper.showPI()
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.deletePost, params: params as [String : AnyObject]?
            , success:
            { (response) in
                Helper.hidePI()
                let errFlag = response["errCode"].numberValue
                switch errFlag {
                case 0:
                    self.getLists()
                    break
                default:
                    let errMsg = response["Message"].stringValue
                    break
                }
        }, failure: { (Error) in
            Helper.hidePI()
        })
    }
}
