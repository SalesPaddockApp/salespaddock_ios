//
//  breedViewController.swift
//  salesPaddock
//
//  Created by Rahul Sharma on 13/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class breedViewController: UIViewController ,UITableViewDelegate ,UITableViewDataSource{
    
    @IBOutlet weak var titleLabelOutlet: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    var breedNames = [JSON]()
    var prefId = String()
    var titleStr = String()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
//        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
//        navigationController?.hidesBarsOnSwipe = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.titleLabelOutlet.text = "Choose your horse's \(titleStr)"
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return breedNames.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // create a new cell if needed or reuse an old one
        let cell = self.tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.breedTableViewCell) as! breedTableViewCell!
        cell?.breedNameOutlet.text = self.breedNames[(indexPath as NSIndexPath).row].stringValue
        return cell!
    }
  
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let selectedIndex = indexPath.row
        let selectedObj:Array = [self.breedNames[selectedIndex].stringValue]
        var dictionary = [String:AnyObject]()
        dictionary["options"] = selectedObj as AnyObject
        dictionary["pref_id"] = self.prefId as AnyObject
        UserDefaults.standard.set(dictionary, forKey: "selectedValue")
        UserDefaults.standard.synchronize()
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func backBtnClicked(_ sender: AnyObject) {
        _ = navigationController?.popViewController(animated: true)
    }
}
