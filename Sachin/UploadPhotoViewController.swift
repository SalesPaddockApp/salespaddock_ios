//
//  UploadPhotoViewController.swift
//  salesPaddock
//
//  Created by Rahul Sharma on 09/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit
import MobileCoreServices
import Kingfisher
import AVFoundation
import AVKit
import SwiftyJSON
import GoogleSignIn
import youtube_ios_player_helper

protocol photoUploaded {
    func photoUploaded(data : [String:AnyObject])
}


class UploadPhotoViewController: UIViewController
{
    @IBOutlet weak var editButtonOutlet: UIButton!
    @IBOutlet weak var collectionVIewOutlet: UICollectionView!
    fileprivate var longPressGesture: UILongPressGestureRecognizer!
    var delegate:photoUploaded! = nil
    var isEditingEnabeld = false
    var picker = UIImagePickerController()
    var mediaArray = [AnyObject]()
    var jsonValueArray = [JSON]()
    var imagePicked = UIImage()
    var images = [NSString]()
    var imageId = String()
    var imageUrl = String()
    var thumbnailUrl = String()
    var type = String()
    var postName = ""
    var postDescription = ""
    var videoURL:URL!
    let gmailLoginHandler = GmailLoginHandler.sharedInstance()
    var controller = UIImagePickerController()
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addPhotosAndVideosToMediaArray()
        self.collectionVIewOutlet.reloadData()
        gmailLoginHandler.delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        gmailLoginHandler.checkAccessToken()
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(UploadPhotoViewController.handleLongGesture(_:)))
        self.collectionVIewOutlet.addGestureRecognizer(longPressGesture)
    }
    
    @objc func handleLongGesture(_ gesture: UILongPressGestureRecognizer) {
        switch(gesture.state) {
        case UIGestureRecognizerState.began:
            guard let selectedIndexPath = self.collectionVIewOutlet.indexPathForItem(at: gesture.location(in: self.collectionVIewOutlet)) else {
                break
            }
            collectionVIewOutlet.beginInteractiveMovementForItem(at: selectedIndexPath)
        case UIGestureRecognizerState.changed:
            collectionVIewOutlet.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case UIGestureRecognizerState.ended:
            collectionVIewOutlet.endInteractiveMovement()
        default:
            collectionVIewOutlet.cancelInteractiveMovement()
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func openPickerOptions() {
        let alert:UIAlertController=UIAlertController(title: "Choose Image and Video", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let YoutubeURLAction = UIAlertAction(title: "Add Existing Youtube Videos", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.addYoutubeURL()
        }
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        // Added actions
        picker.delegate = self
        alert.addAction(YoutubeURLAction)
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func addYoutubeURL() {
        let alertController = UIAlertController(title: "Enter Youtube URL", message: "Paste your youtube URL here.", preferredStyle: .alert)
        alertController.addTextField(configurationHandler: {(_ textField: UITextField) -> Void in
            textField.placeholder = "for ex:- https://www.youtube.com/watch?v=AaG7Q-LkVtU"
        })
        let confirmAction = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
//            print("entered URL \(String(describing: alertController.textFields?[0].text))")
            Helper.showPI(_message: "Uploading...")
            //compare the current password and do action here
            if let text = alertController.textFields?[0].text {
                if let accessToken = self.gmailLoginHandler.getAccessToken(),let youtubeId = text.youtubeID {
                    YoutubeURLVerifier.verifyURL(withID: youtubeId, andAccessToken: accessToken, completion: { thumbnail in
                        Helper.hidePI()
                        if let thumbnail = thumbnail {
                            var data = [String : Any]()
                            data["thumbnailUrl"] = thumbnail
                            data["imageUrl"] = youtubeId
                            data["type"] = "2" // Used 2 instead of 1 for uploading videos.
                            self.mediaArray.append(data as AnyObject)
                            self.collectionVIewOutlet.reloadData()
                            
                        } else {
                            self.showAlert("Error!", message: "Your URL is not valid. Please try with a valid URL")
                        }
                    })
                }
                else {
                    self.showAlert("Error!", message: "Your URL is not valid. Please try with a valid URL")
                    Helper.hidePI()
                }
            }
        })
        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
//            print("Canelled")
        })
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- Buttons Action
    @IBAction func addphotoBtnClicked(_ sender: AnyObject)
    {
        if gmailLoginHandler.getAccessToken() != nil {
            openPickerOptions()
        } else {
            gmailLoginHandler.login()
        }
    }
    
    func addPhotosAndVideosToMediaArray() {
        for jsonObj in jsonValueArray {
            let dict = jsonObj.dictionaryValue
            var dataDict = [String: AnyObject]()
            if(!dict.isEmpty) {
                if dict["type"]?.stringValue == "0" {
                    dataDict["type"] = dict["type"]?.stringValue as AnyObject
                    dataDict["imageUrl"] = dict["imageUrl"]?.stringValue as AnyObject
                }
                else {
                    dataDict["type"] = dict["type"]?.stringValue as AnyObject
                    dataDict["thumbnailUrl"] = dict["thumbnailUrl"]?.stringValue as AnyObject
                    dataDict["imageUrl"] = dict["imageUrl"]?.stringValue as AnyObject
                }
                mediaArray.append(dataDict as AnyObject)
            }
        }
    }
    
    @IBAction func backButtonActn(_ sender: Any)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
            self.present(picker, animated: true, completion: nil)
        }
        else {
            self.showAlert("Warning", message: "You don't have camera")
        }
    }
    func openGallary()
    {
        picker.sourceType = .photoLibrary
        picker.mediaTypes = [kUTTypeMovie as String,kUTTypeImage as String]
        self.present(picker, animated: true, completion: nil)
    }
    
    //MARK:UIImagePickerControllerDelegate
    
    @IBAction func editButtonAction(_ sender: Any)
    {
        self.editButtonOutlet.isSelected = !self.editButtonOutlet.isSelected
        if let editButton = sender as? UIButton{
            if editButton.isSelected
            {
                self.isEditingEnabeld = true
            }
            else{
                self.isEditingEnabeld = false
            }
            self.collectionVIewOutlet.reloadData()
        }
    }
    
    
    @IBAction func saveButtonAction(_ sender: AnyObject) {
        
        var dictionary = [String:AnyObject]()
        if(mediaArray.count == 0){
            self.showAlert("Error", message: "Please add a Photo first")
            return
        }
        
        
        dictionary["options"] = mediaArray as AnyObject
        dictionary["pref_id"] = self.imageId as AnyObject
        UserDefaults.standard.set(dictionary, forKey: "selectedValue")
        UserDefaults.standard.synchronize()
        
        if (self.delegate != nil) {
            if(self.mediaArray.count>0)
            {
                let imageData = self.mediaArray[0]
                self.delegate.photoUploaded(data:imageData as! [String : AnyObject])
            }
        }
        _ = navigationController?.popViewController(animated: true)
    }
    
}

extension UploadPhotoViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate, photoCropped{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var data = [String : String]()
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.dismiss(animated: true, completion:{
                let shittyVC = ShittyImageCropVC(frame: (self.navigationController?.view.frame)!, image: image.fixOrientation(), aspectWidth: 3, aspectHeight: 2)
                self.navigationController?.present(shittyVC, animated: true, completion: nil)
                shittyVC.delegate = self
            })
        }
        else {
            if let video = info["UIImagePickerControllerMediaURL"] as? NSURL{
                Helper.showPI(_message: "Uploading Video")
                let image = thumbnail(sourceURL: video)
                let helperInstance = CloudinaryHelper.sharedInstance
                helperInstance.uploadImage(image:self.resizeImage(image,CGSize.init(width: self.view.frame.size.width*3,height: self.view.frame.size.width*2)), onCompletion: {
                    (success, cloudinaryUrl) in
                    if success {
                        if let thumbnailURL = cloudinaryUrl{
                            data["thumbnailUrl"] = thumbnailURL
                            if let accessToken = self.gmailLoginHandler.getAccessToken() {
                                YoutubeUploader.postVideoToYouTube(accessToken: accessToken, videoUrl: video as URL, withName: self.postName, andDescription: self.postDescription, callback: { (isUploaded, url) in
                                    Helper.hidePI()
                                    if isUploaded {
                                        if let url = url {
                                            let videoUrl = url
                                            data["imageUrl"] = videoUrl
                                            // 2 for youtube and 1 for cloudinary.
                                            data["type"] = "2" // Used 2 instead of 1 for uploading videos.
                                            self.mediaArray.append(data as AnyObject)
                                            self.collectionVIewOutlet.reloadData()
                                        }
                                    }else{
                                        if url != nil, let statusCode = url{
                                            switch statusCode{
                                            case "401":
                                                self.showAlert("Error", message: "Please create a youtube channel before uploading any video on your connected google account.")
//                                                self.performSegue(withIdentifier: "createChannelWebView", sender: nil)
                                                break
                                            default:
                                                let gmailLoginHandler = GmailLoginHandler.sharedInstance()
                                                gmailLoginHandler.refreshAccessToken()
                                                break
                                            }
                                        }
                                    }
                                })
                            }
                        }
                    }
                    else
                    {
                        Helper.hidePI()
                        self.showAlert("Error", message: "Video size is too large, Please upload less then 1 minute video !!!")
                        return
                    }
                })
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func resizeImage(_ image: UIImage,_ targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    func photoSaved(image: UIImage) {
        Helper.showPI(_message: "Uploading Image")
        var data = [String : String]()
        let helperInstance = CloudinaryHelper.sharedInstance
        helperInstance.uploadImage(image:image.fixOrientation(), onCompletion: { (success, cloudinaryUrl) in
            
            if success
            {
                data["imageUrl"] = cloudinaryUrl!
                data["type"] = "0"
                Helper.hidePI()
                self.mediaArray.append(data as AnyObject)
                self.collectionVIewOutlet.reloadData()
            }
            else
            {
                Helper.hidePI()
                self.showAlert("Error", message: "Image size is too large !!!")
                return
            }
        })
    }
    
    func thumbnail(sourceURL:NSURL) -> UIImage {
        let asset = AVAsset(url: sourceURL as URL)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        let time = CMTime(seconds: 5, preferredTimescale: 1)
        
        do {
            let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            return UIImage(named: "\(sourceURL)")!
        }
    }
    
    @objc func deleteButtonAction(with sender: UIButton) {
        let i : Int = sender.tag
        mediaArray.remove(at: i)
        collectionVIewOutlet.reloadData()
    }
}

extension UploadPhotoViewController:videoPlayeDelegate
{
    func playVideoWithURL(videoUrl: URL, withytView ytView:YTPlayerView) {
        ytView.isHidden = false
        let player = AVPlayer(url: videoUrl as URL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func playVideo(withID videoID : String, withView ytView : YTPlayerView) {
        let dict = [
            "playsinline" : 1,
            "showinfo" : 0,
            "controls" : 1,
            "modestbranding": 0,
            "enablejsapi":1,
            "autohide":2,
            "rel":0,
            "origin" : "http://www.youtube.com",
            ] as [String : Any]
        let result = ytView.load(withVideoId: videoID, playerVars: dict)
        ytView.webView?.allowsInlineMediaPlayback = false
//        print("Result = \(result)")
        ytView.delegate = self
    }
}

extension UploadPhotoViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let temp = mediaArray.remove(at: sourceIndexPath.item)
        mediaArray.insert(temp, at: destinationIndexPath.item)
        collectionVIewOutlet.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.row == 0 {
            let wid = self.view.frame.size.width
            let higt = wid * 0.678
            return CGSize(width: wid - 20, height: higt - 10)
        }
        else {
            let wid = self.view.frame.size.width/2-20
            let higt = wid * 0.678
            return CGSize(width: wid, height: higt)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell :ImageCollectionViewCell = collectionView .dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        let data = mediaArray[indexPath.row] as! [String : AnyObject]
        cell.videoPlayeDelegate = self
        if self.isEditingEnabeld{
            cell.deleteButtonOutlet.isHidden = false
            cell.wobble()
        }
        else{
            cell.deleteButtonOutlet.isHidden = true
        }
        cell.deleteButtonOutlet.tag = indexPath.row
        cell.deleteButtonOutlet.addTarget(self, action: #selector(UploadPhotoViewController.deleteButtonAction(with:)), for: .touchUpInside)
        var url = URL(string: "\([indexPath.row])")
        if(data["type"] as! String) == "1" {
            if let urlstr = data["thumbnailUrl"] as? String {
                url = URL(string: urlstr)
            }
            if let videoUrl = data["imageUrl"] as? String {
                cell.videoUrl = videoUrl
                cell.videoPlayBtnOutlet.isHidden = false
                if let youTubeId = data["youTubeId"] as? String {
                    cell.ytPlayerViewOutlet.isHidden = false
                    cell.videoPlayBtnOutlet.isHidden = true
                    self.playVideo(withID: youTubeId, withView : cell.ytPlayerViewOutlet)
                } else {
                    cell.ytPlayerViewOutlet.isHidden = true
                    cell.videoPlayBtnOutlet.isHidden = false
                }
            }
        } else if (data["type"] as! String) == "2" {
            if let urlstr = data["thumbnailUrl"] as? String {
                url = URL(string: urlstr)
            }
            if let videoUrl = data["imageUrl"] as? String {
                cell.videoUrl = videoUrl
                cell.videoPlayBtnOutlet.isHidden = false
                cell.ytPlayerViewOutlet.isHidden = false
                cell.videoPlayBtnOutlet.isHidden = true
                self.playVideo(withID: videoUrl, withView : cell.ytPlayerViewOutlet)
            }
        }
        else {
            cell.ytPlayerViewOutlet.isHidden = true
            cell.videoPlayBtnOutlet.isHidden = true
            let urlstr = data["imageUrl"] as! String
            url = URL(string:urlstr)
        }
        cell.image.kf.indicatorType = .activity
        cell.image.kf.indicator?.startAnimatingView()
        cell.image.kf.setImage(with: url,
                               placeholder: UIImage.init(named: "horse_default_image"),
                               options: [.transition(ImageTransition.fade(1))],
                               progressBlock: { receivedSize, totalSize in
        },
                               completionHandler: { image, error, cacheType, imageURL in
                                cell.image.kf.indicator?.stopAnimatingView()
        })
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return mediaArray.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}

extension UploadPhotoViewController : GmailLoginHandlerDelegate{
    
    func userDetailsFromGoogleLogin(userDetails: GmailLoginResponseModel) {
        //Hide Progress Bar
        
    }
    
    
    func gotAccessToken(accessToken: String) {
    }
    
    
    func errorFromGoogleNormalLogin(error: Error) {
        //Hide Progress Bar
        Helper.hidePI()
//        print("error",error.localizedDescription)
        //        Helper..alertVC(title: "Error!", message: error.localizedDescription)
    }
    
    func errorFromGoogleSilentLogin(error: Error) {
        //Hide Progress Bar
        Helper.hidePI()
//        print("error",error.localizedDescription)
    }
    
    func gotErrorFromRefreshAccessToken(error: Error) {
        //Hide Progress Bar
        Helper.hidePI()
//        print("error",error.localizedDescription)
    }
}

extension UploadPhotoViewController : YTPlayerViewDelegate {
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        
    }
    
    func playerView(_ playerView: YTPlayerView, didPlayTime playTime: Float) {
        
    }
    
    func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
        
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo quality: YTPlaybackQuality) {
        
    }
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        playerView.webView?.allowsInlineMediaPlayback = false
    }
}

extension UploadPhotoViewController : GIDSignInUIDelegate  {
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.navigationController?.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.navigationController?.dismiss(animated: true, completion:nil)
        self.openPickerOptions()
    }
}

