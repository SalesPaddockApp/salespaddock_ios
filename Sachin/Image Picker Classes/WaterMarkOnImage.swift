//
//  WaterMarkOnImage.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 10/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class WaterMarkOnImage: NSObject {

}

extension UIImage{
    
    enum WaterMarkCorner{
        case TopLeft
        case TopRight
        case BottomLeft
        case BottomRight
    }
    
    func waterMarkImagewithImage(backgroundImage:UIImage , watermarkImage: UIImage)-> UIImage{
        
        UIGraphicsBeginImageContextWithOptions(backgroundImage.size, false, 0.0)
        backgroundImage.draw(in: CGRect(x: 0, y: 0, width: backgroundImage.size.width, height:backgroundImage.size.height))
        watermarkImage.draw(in: CGRect(x: backgroundImage.size.width - backgroundImage.size.width/10, y: backgroundImage.size.height - backgroundImage.size.width/10, width: backgroundImage.size.width/10, height: backgroundImage.size.width/10))
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
}

