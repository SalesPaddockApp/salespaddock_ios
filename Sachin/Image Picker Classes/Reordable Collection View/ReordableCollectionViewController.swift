//
//  ReordableCollectionViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 03/01/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

private let reuseIdentifier = "ImageCollectionViewCell"

class ReordableCollectionViewController: UICollectionViewController {

    var dataArray = [AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

       // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return dataArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell :ImageCollectionViewCell = collectionView .dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ImageCollectionViewCell
        let data = dataArray[indexPath.row] as! [String : AnyObject]
        var url = URL(string: "\([indexPath.row])")
        cell.wobble()
        if(data["type"] as! String) == "1" || (data["type"] as! String) == "2"
        {
            let urlstr = data["thumbnailUrl"] as! String
            url = URL(string: urlstr)
            cell.videoPlayBtnOutlet.isHidden = false
        }
        else
        {
            cell.videoPlayBtnOutlet.isHidden = true
            let urlstr = data["imageUrl"] as! String
            url = URL(string:urlstr)
        }
        
        cell.image.kf.indicatorType = .activity
        cell.image.kf.indicator?.startAnimatingView()
        
        cell.image.kf.setImage(with: url,
                               placeholder: UIImage.init(named: "horse_default_image"),
                               options: [.transition(ImageTransition.fade(1))],
                               progressBlock: { receivedSize, totalSize in
        },
                               completionHandler: { image, error, cacheType, imageURL in
                                cell.image.kf.indicator?.stopAnimatingView()
        })
        
        return cell
    }
}
extension ReordableCollectionViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.row == 0
        {
            let wid = self.collectionView?.frame.size.width
            let higt = wid! * 0.678
            return CGSize(width: wid! - 30, height: higt - 30)
        }
        return CGSize(width: self.view.frame.size.width/2 - 35, height: self.view.frame.size.width/2 - 70)
    }
}
