//
//  ImageCollectionViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 07/10/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

protocol videoPlayeDelegate {
    
    func playVideoWithURL(videoUrl : URL, withytView : YTPlayerView)
}

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var deleteButtonOutlet: UIButton!
    @IBOutlet weak var videoPlayBtnOutlet: UIButton!
    @IBOutlet weak var image: UIImageView!
    var videoUrl : String?
    var videoPlayeDelegate:videoPlayeDelegate! = nil
    var imageArray = [AnyObject]()
    @IBOutlet weak var ytPlayerViewOutlet: YTPlayerView!
    
    let animationRotateDegres: CGFloat = 0.5
    let animationTranslateX: CGFloat = 1.0
    let animationTranslateY: CGFloat = 1.0
    let count: Int = 1
    
    func wobble() {
        let leftOrRight: CGFloat = (count % 2 == 0 ? 1 : -1)
        let rightOrLeft: CGFloat = (count % 2 == 0 ? -1 : 1)
        let leftWobble: CGAffineTransform = CGAffineTransform(rotationAngle: degreesToRadians(x: animationRotateDegres * leftOrRight))
        let rightWobble: CGAffineTransform = CGAffineTransform(rotationAngle: degreesToRadians(x: animationRotateDegres * rightOrLeft))
        let moveTransform: CGAffineTransform = leftWobble.translatedBy(x: -animationTranslateX, y: -animationTranslateY)
        let conCatTransform: CGAffineTransform = leftWobble.concatenating(moveTransform)
        
        transform = rightWobble // starting point
        
        UIView.animate(withDuration: 0.1, delay: 0.08, options: [.allowUserInteraction, .autoreverse, .repeat], animations: { () -> Void in
            self.transform = conCatTransform
        }, completion: nil)
    }
    
    func degreesToRadians(x: CGFloat) -> CGFloat {
        return .pi * x/180.0
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func videoPlayBtnAction(_ sender: Any)
    {
        if(self.videoPlayeDelegate != nil)
        {
            guard let videoUrl = videoUrl else { return }
                if let videoURL = URL(string: videoUrl){
                    self.videoPlayeDelegate.playVideoWithURL(videoUrl: videoURL, withytView: self.ytPlayerViewOutlet)
                }
        }
    }
}
