//
//  CalendarViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 28/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class CalendarViewController: UIViewController {
    
    @IBOutlet weak var CalenarpickerViewOutlet: UIPickerView!
    @IBOutlet weak var currentMonthOutlet: UILabel!
    @IBOutlet weak var titleOutlet: UILabel!
    var currentMonth : String = ""
    var monthArray : [String]!
    var yearsArray : [String]!
    var currentYear : String = ""
    var prefId = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        monthArray = ["January","February","March","April","May","June","July","August","September","October","November","December"];
        yearsArray = ["1980","1981","1982","1983","1984","1985","1986","1987","1988","1989","1990","1991","1992","1993","1994","1995","1996","1997","1998","1999","2000","2001","2002","2003","2004","2005","2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017","2018"]
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.showNavigation()
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonAction(_ sender: AnyObject) {
      
        if(!currentMonth.isEmpty && !currentYear.isEmpty)
        {
            var dictionary = [String:AnyObject]()
            let selectedObj =  [currentMonthOutlet.text!]
            dictionary["options"] = selectedObj as AnyObject
            dictionary["pref_id"] = self.prefId as AnyObject
            UserDefaults.standard.set(dictionary, forKey: "selectedValue")
            UserDefaults.standard.synchronize()
        }
        else
        {
            // error comes here
        }
        _ = navigationController?.popViewController(animated: true)
    }
}

extension CalendarViewController : UIPickerViewDataSource, UIPickerViewDelegate
{
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0{
            return monthArray.count
        }
        else{
            return yearsArray.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0{
            return monthArray[row]
        }
        else{
            return yearsArray[row]
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0
        {
            currentMonth = monthArray[row] as String!
        }
        else{
            currentYear = yearsArray[row] as String!
        }
        currentMonthOutlet.textColor = UIColor.init(hexString: "#484848ff")
        currentMonthOutlet.text = "\(currentMonth), \(currentYear)"
    }
}
