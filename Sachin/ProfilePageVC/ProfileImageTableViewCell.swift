//
//  ProfileImageTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 27/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class ProfileImageTableViewCell: UITableViewCell {

    @IBOutlet weak var nameOutlet: UILabel!
    @IBOutlet weak var placeOutlet: UILabel!
    
    @IBOutlet weak var editButtonOutlet: UIView!
    @IBOutlet weak var imageViewOUTLET: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
