//
//  PofilePageHorsesTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 10/01/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher

protocol HorseSelectedInProfileDelegate {
    func horseSelectedAtIndex(index:Int)
}

class PofilePageHorsesTableViewCell: UITableViewCell {

    var numberOfHorses = [JSON]()
    var HorseSelectedInProfileDelegate:HorseSelectedInProfileDelegate! = nil
    @IBOutlet weak var collectionViewOutlet: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension PofilePageHorsesTableViewCell : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfilePageHorsesCollectionViewCell", for: indexPath) as! ProfilePageHorsesCollectionViewCell
        
        let dataDict = numberOfHorses[indexPath.row] 
        cell.horseNameOutlet.text = dataDict["name"].stringValue
        let imageDict = dataDict["image"].dictionaryValue
        let imageArray = imageDict["options"]?.arrayValue
        let imaegValueAtFirstindex = imageArray?[0].dictionaryValue
        var urlStr = String()
        if(imaegValueAtFirstindex?["type"] == "0")
        {
            if let imageURL = imaegValueAtFirstindex?["imageUrl"]?.stringValue{
//                urlStr = (imaegValueAtFirstindex?["imageUrl"]?.stringValue)!
                urlStr = imageURL
            }
        }
        else{
            if let imageURL = imaegValueAtFirstindex?["thumbnailUrl"]?.stringValue{
                //                urlStr = (imaegValueAtFirstindex?["thumbnailUrl"]?.stringValue)!
                urlStr = imageURL
            }
            
        }
        
        let url = URL(string:urlStr)
        
        cell.horseImageOutlet.kf.indicatorType = .activity
        cell.horseImageOutlet.kf.indicator?.startAnimatingView()
        
        cell.horseImageOutlet.kf.setImage(with: url,
                                             placeholder: UIImage.init(named: "horse_default_image"),
                                             options: [.transition(ImageTransition.fade(1))],
                                             progressBlock: { receivedSize, totalSize in
        },
                                             completionHandler: { image, error, cacheType, imageURL in
                                                cell.horseImageOutlet.kf.indicator?.stopAnimatingView()
        })

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfHorses.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (HorseSelectedInProfileDelegate != nil){
            self.HorseSelectedInProfileDelegate.horseSelectedAtIndex(index: indexPath.row)
        }
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        var frameOfCell = self.frame
        frameOfCell.size.width -= 25*2 // Padding
        frameOfCell.size.height = self.frame.size.height-90
        
        return frameOfCell.size
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageWidth: Float = Float(self.collectionViewOutlet!.frame.width) - 35 //480 + 50
        // width + space
        let currentOffset: Float = Float(scrollView.contentOffset.x)
        let targetOffset: Float = Float(targetContentOffset.pointee.x)
        var newTargetOffset: Float = 0
        if targetOffset > currentOffset {
            newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth
        }
        else {
            newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth
        }
        if newTargetOffset < 0 {
            newTargetOffset = 0
        }
        else if (newTargetOffset > Float(scrollView.contentSize.width)){
            newTargetOffset = Float(Float(scrollView.contentSize.width))
        }
        
        targetContentOffset.pointee.x = CGFloat(currentOffset)
        scrollView.setContentOffset(CGPoint(x: CGFloat(newTargetOffset), y: scrollView.contentOffset.y), animated: true)
    }
}
