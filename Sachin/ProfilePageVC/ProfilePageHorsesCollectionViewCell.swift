//
//  ProfilePageHorsesCollectionViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 10/01/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ProfilePageHorsesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var priceOutlet: UILabel!
    @IBOutlet weak var horseImageOutlet: UIImageView!
    @IBOutlet weak var horseNameOutlet: UILabel!
}
