//
//  ProfilePageVC.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 27/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftyJSON

class ProfilePageVC: UIViewController {
    
    @IBOutlet var saperatorViewOutlet: UIView!
    @IBOutlet var navigationBackButtonOutlet: UIButton!
    @IBOutlet var navigationBarOutlet: UIView!
    var profileData = [String : Any]()
    var isMyProfile = "1" //0 = for others profile //1 = for own profile
    var responseDictionary = [String : Any]()
    var numberOfRowsAndDataForUser = [Any]()
    var userHorsesArray = [JSON]()
    var aboutMe = String()
    var userId = String()
    @IBOutlet weak var tableVIewOutlet: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        tableVIewOutlet.estimatedRowHeight = 270
        tableVIewOutlet.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getProfileDetails()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func editButtonAction(_ sender: Any)
    {
        self.performSegue(withIdentifier: "edit button segue", sender: responseDictionary)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "edit button segue"
        {
            let controller = segue.destination as! EditProfileVIewController
            controller.dataDict = self.responseDictionary as! [String : JSON]
        }
        else if segue.identifier == "Horse Details Segue"
        {
            let controller = segue.destination as! HorseDetailViewController
            let index = sender as! Int
            let data = userHorsesArray[index].dictionaryObject! as [String : AnyObject]
            if let postId = data["postId"] as? String{
                controller.postId = postId
            }
            controller.comingFromProfile = true
            
        }
    }
    
    func getProfileDetails() {
        
        Helper.showPI()
        
        var params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject,
                      "isMyProfile":isMyProfile as AnyObject]
        
        if let userID = profileData["userId"] as? String{
            params["userId"] = userID as AnyObject
        }
        if self.userId.characters.count>0{
            params["userId"] = self.userId as AnyObject
        }
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.viewProfile,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                        
                                    case 0:
                                        
                                        self.responseDictionary = response["userProfile"].dictionaryValue
                                        self.makeDataArrayFromResponse(dataDict: response["userProfile"].dictionaryValue)
                                        if let userPostDetails = response["userPostDetails"].array
                                        {
                                            self.userHorsesArray = userPostDetails
                                        }
                                        self.tableVIewOutlet.reloadData()
                                        let res = response["userProfile"].dictionaryObject
                                        var profileImage = [String : String]()
                                        profileImage["image"] = res?["profilePic"] as? String
                                        UserDefaults.standard.setValue(profileImage, forKey: "profileImage")
                                        
                                        
                                        break
                                        
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Error", message: errMsg!)
                                        
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
    @IBAction func navBackAction(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func makeDataArrayFromResponse(dataDict : [String:JSON])
    {
        numberOfRowsAndDataForUser.removeAll()
        if let fName = dataDict["fName"]?.stringValue
        {
            profileData["fName"] = fName
        }
        if let lName = dataDict["lName"]?.stringValue
        {
            profileData["lName"] = lName
        }
        if let location = dataDict["location"]?.stringValue
        {
            profileData["location"] = location
        }
        if let profilePic = dataDict["profilePic"]?.stringValue
        {
            profileData["profilePic"] = profilePic
        }
        if let about = dataDict["about"]?.stringValue
        {
            var dataAny = [String:Any]()
            dataAny["about"] = about
            numberOfRowsAndDataForUser.append(dataAny)
        }
        //        if let school = dataDict["school"]?.stringValue
        //        {
        //            var dataAny = [String:Any]()
        //            dataAny["school"] = school
        //            numberOfRowsAndDataForUser.append(dataAny)
        //        }
        //        if let work = dataDict["work"]?.stringValue
        //        {
        //            var dataAny = [String:Any]()
        //            dataAny["work"] = work
        //            numberOfRowsAndDataForUser.append(dataAny)
        //        }
        if let languages = dataDict["languages"]?.stringValue
        {
            var dataAny = [String:Any]()
            dataAny["languages"] = languages
            numberOfRowsAndDataForUser.append(dataAny)
        }
        if let farmName = dataDict["farmName"]?.stringValue
        {
            var dataAny = [String:Any]()
            dataAny["farmName"] = farmName
            numberOfRowsAndDataForUser.append(dataAny)
        }
        
        self.tableVIewOutlet.reloadData()
        //  if let memberSince = dataDict["work"] as? String
        //  {
        //      numberOfRowsAndDataForUser.append(work)
        //  }
    }
}

extension ProfilePageVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = (indexPath as NSIndexPath).row
        switch index {
        case (0):
            let cell = tableView.dequeueReusableCell(withIdentifier:"ProfileImageTableViewCell", for: indexPath) as? ProfileImageTableViewCell
            
            let url = URL(string:profileData["profilePic"] as! String)
            
            cell?.imageViewOUTLET.kf.indicatorType = .activity
            cell?.imageViewOUTLET.kf.indicator?.startAnimatingView()
            cell?.imageViewOUTLET.kf.setImage(with:url, placeholder: #imageLiteral(resourceName: "profile_default"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil,
                                              completionHandler: { image, error, cacheType, imageURL in
                cell?.imageViewOUTLET.kf.indicator?.stopAnimatingView()
            })
            
            if isMyProfile == "0"{
                cell?.editButtonOutlet.isHidden = true
            }
            else{
                if let fname = profileData["fName"] as? String, let lname = profileData["lName"] as? String
                {
                    Utility.setUserFirstName(firstName: fname)
                    Utility.setUserLastName(lastName: lname)
                }
            }
            
            var fullName = String()
            if let name = profileData["fName"] as? String
            {
                fullName = name
                
            }
            if let lname = profileData["lName"] as? String
            {
                fullName.append(" \(lname)")
            }
            
            cell?.nameOutlet.text = fullName
            
            if let location = profileData["location"] as? String
            {
                if location.characters.count > 1
                {
                    cell?.placeOutlet.text = location
                }
            }
            
            return cell!
            
        default :
            
            if userHorsesArray.count>0
            {
                let rowsNum = self.tableVIewOutlet.numberOfRows(inSection: 0)
                if indexPath.row == rowsNum-1{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PofilePageHorsesTableViewCell", for: indexPath) as? PofilePageHorsesTableViewCell
                    cell?.numberOfHorses = self.userHorsesArray
                    cell?.collectionViewOutlet.reloadData()
                    cell?.HorseSelectedInProfileDelegate = self
                    return cell!
                }
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier:"ProfileTitlesTableViewCell", for: indexPath) as? ProfileTitlesTableViewCell
            
            let data = numberOfRowsAndDataForUser[indexPath.row-1] as! [String : String]
            
            if let about = data["about"]
            {
                cell?.titleOutlet.text = about
                if (about.count == 0) {
                    cell?.titleOutlet.text = nil
                }
                cell?.labelOutlet.text = ""
            }
            
            if let about = data["farmName"]
            {
                if about.count > 1
                {
                    if isMyProfile == "1"{
                        cell?.titleOutlet.text = "Location"
                        cell?.labelOutlet.text = about
                    }else {
                        cell?.titleOutlet.text = "Farm Name"
                        cell?.labelOutlet.text = about
                    }
                    
                }
                else{
                    cell?.titleOutlet.text = nil
                    cell?.labelOutlet.text = ""
                }
            }
            
            if let about = data["work"]
            {
                cell?.titleOutlet.text = "Work"
                cell?.labelOutlet.text = about
            }
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if numberOfRowsAndDataForUser.count == 0
        {
            return 0
        }
        if userHorsesArray.count>0{
            
            return numberOfRowsAndDataForUser.count+2
        }
        return numberOfRowsAndDataForUser.count+1
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView  == tableVIewOutlet
        {
            let offsetY: CGFloat = scrollView.contentOffset.y
            if offsetY > 50 {
                let alpha: CGFloat = min(1, 1 - ((50 + 64 - offsetY) / 64))
                navigationBarOutlet.backgroundColor = Color.white.withAlphaComponent(alpha)
                if(alpha == 1)
                {
                    navigationBackButtonOutlet.setImage(Image.init(named: "back_btn_off"), for: .normal)
                    self.saperatorViewOutlet.isHidden = false
                }
            }
            else
            {
                navigationBackButtonOutlet.setImage(Image.init(named: "back_btn_wight_off"), for: .normal)
                self.saperatorViewOutlet.isHidden = true
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView  == tableVIewOutlet
        {
            let offsetY: CGFloat = scrollView.contentOffset.y
            if offsetY > 50 {
                let alpha: CGFloat = min(1, 1 - ((50 + 64 - offsetY) / 64))
                navigationBarOutlet.backgroundColor = Color.white.withAlphaComponent(alpha)
                if(alpha == 1)
                {
                    navigationBackButtonOutlet.setImage(Image.init(named: "back_btn_off"), for: .normal)
                    self.saperatorViewOutlet.isHidden = false
                }
            }
            else
            {
                navigationBackButtonOutlet.setImage(Image.init(named: "back_btn_wight_off"), for: .normal)
                self.saperatorViewOutlet.isHidden = true
            }
        }
    }
}

extension ProfilePageVC : HorseSelectedInProfileDelegate{
    
    func horseSelectedAtIndex(index:Int)
    {
        self.performSegue(withIdentifier: "Horse Details Segue", sender: index)
    }
}
