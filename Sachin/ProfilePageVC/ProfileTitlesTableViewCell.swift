//
//  ProfileTitlesTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 27/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class ProfileTitlesTableViewCell: UITableViewCell {

    @IBOutlet weak var titleOutlet: UITextView!
    @IBOutlet weak var labelOutlet: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
