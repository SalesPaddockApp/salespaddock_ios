//
//  disciplineViewController.swift
//  salesPaddock
//
//  Created by Rahul Sharma on 13/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class disciplineViewController: UIViewController ,UITableViewDataSource ,UITableViewDelegate
{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabelOutlet: UILabel!
    
    var selectedArray=[String]()
    var disciplines = [String]()
    var responseData = [JSON]()
    var selectedValues = [AnyObject]()
    var onCreateData = [JSON]()
    var prefId = String()
    var imageId = String()
    var postId = String()
    var titleStr = String()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.titleLabelOutlet.text = "Choose your horse's \(titleStr)"
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return disciplines.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:disciplineTableViewCell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.disciplineTableViewCell) as! disciplineTableViewCell
        cell.disciplineLbl.text = self.disciplines[indexPath.row]
        let disciplineStr = self.disciplines[indexPath.row]
        cell.indexOutlet.text = String(disciplineStr[disciplineStr.startIndex])
        
        cell.checkBoxBtn.addTarget(self, action:#selector(disciplineViewController.tickClicked(_:)), for: .touchUpInside)
        
        cell.checkBoxBtn.tag = indexPath.row
        if !selectedArray.isEmpty{
            if selectedArray.contains(self.disciplines[(indexPath as NSIndexPath).row]){
                
                cell.checkBoxBtn.setImage(#imageLiteral(resourceName: "tick"), for: .normal)
            }
            else
            {
                cell.checkBoxBtn.setImage(#imageLiteral(resourceName: "tick_box"), for: .normal)
            }
        }
        else
        {
            cell.checkBoxBtn.setImage(#imageLiteral(resourceName: "tick_box"), for: .normal)
        }
        return cell
    }
    
    @objc func tickClicked(_ sender: UIButton!)
    {
        var value = sender.tag;
        if !selectedArray.isEmpty{
            if selectedArray.contains(self.disciplines[value]){
                value = selectedArray.index(of: self.disciplines[value])!
                selectedArray.remove(at:value)
            }
            else
            {
                selectedArray.append(self.disciplines[value])
            }
        }
        else
        {
            selectedArray.append(self.disciplines[value])
        }
        tableView.reloadData()
    }
    
    @IBAction func backBtnClicked(_ sender: AnyObject)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveBtnClicked(_ sender: AnyObject)
    {
        if selectedArray.isEmpty{
            //error will come here
            return
        }
        if(prefId.isEmpty)
        {
            UserDefaults.standard.removeObject(forKey: "selectedValue")
            UserDefaults.standard.synchronize()
            self.performSegue(withIdentifier: "Segue to Farm List", sender: self)
        }
        else{
            
            var dictionary = [String:AnyObject]()
            dictionary["options"] = selectedArray as AnyObject
            dictionary["pref_id"] = prefId as AnyObject
            UserDefaults.standard.setValue(dictionary, forKey: "selectedValue")
            UserDefaults.standard.synchronize()
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Segue to Farm List"{
            let controller = segue.destination as! FarmListToPostViewController
            var dictionary = [String:AnyObject]()
            dictionary["options"] = selectedArray as AnyObject
            if(!self.onCreateData.isEmpty){
                dictionary["pref_id"] = self.onCreateData[1]["id"].stringValue as AnyObject
            }
            else{
                dictionary["pref_id"] = prefId as AnyObject
            }
            self.selectedValues.append(dictionary as AnyObject!)
            controller.selectedValues = self.selectedValues as Array!
            controller.responseData = self.responseData
            controller.imageId = self.imageId
            controller.postId = self.postId
        }
    }
}
