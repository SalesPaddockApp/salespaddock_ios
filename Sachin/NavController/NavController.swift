//
//  NavController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 16/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class NavController: UINavigationController,UINavigationBarDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        if (self.delegate == nil){
        self.delegate = self
        }
        self.interactivePopGestureRecognizer?.delegate = self
        ChatSocketIOClient.sharedInstance.sendHeartbeatForUser(status: "1")
        
        // Do any additional setup after loading the view.
    }

    deinit {
        self.delegate = nil
        self.interactivePopGestureRecognizer?.delegate = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
        if self.responds(to: #selector(getter: interactivePopGestureRecognizer))
        {
            self.interactivePopGestureRecognizer?.isEnabled = false
        }
    }
    
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        if navigationController .responds(to: #selector(getter:interactivePopGestureRecognizer)){
            navigationController.interactivePopGestureRecognizer?.isEnabled = true
        }
        
        if navigationController.viewControllers.count == 1
        {
            navigationController.interactivePopGestureRecognizer?.isEnabled = false
        }
    }

   // override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     //   let controller = segue.destination as! RAMAnimatedTabBarController
        //controller.selectedIndex = 0
    //}
}
