//
//  NavViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 12/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class NavViewController: UINavigationController,UINavigationBarDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate {
    var isComingFromPush = false
    var isPushRideAlert = false
    var isMyProfile = ""
    var userId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (self.delegate == nil){
            self.delegate = self
        }
        self.timerAction()
        self.interactivePopGestureRecognizer?.delegate = self
    }
    
    deinit {
        self.delegate = nil
        self.interactivePopGestureRecognizer?.delegate = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isPushRideAlert
        {
            self.performSegue(withIdentifier: "profile Page Segue", sender: nil)
            self.isPushRideAlert = false
        }
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
        if self.responds(to: #selector(getter: interactivePopGestureRecognizer))
        {
            self.interactivePopGestureRecognizer?.isEnabled = false
        }
    }
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        if navigationController .responds(to: #selector(getter:interactivePopGestureRecognizer)){
            navigationController.interactivePopGestureRecognizer?.isEnabled = true
        }
        
        if navigationController.viewControllers.count == 1
        {
            navigationController.interactivePopGestureRecognizer?.isEnabled = false
        }
    }
    
    func timerAction() {
        
        // Check for Sezsion Presence
        let sessionToken = UserDefaults.standard.string(forKey: Constant.UD_NAME.SESSION_TOKEN)
        
        // If Session is Present go to Tab Bar Controller
        // On Start up
        if ((sessionToken) != nil) {
            performSegue(withIdentifier: "SplashToTabBarVC", sender: nil)
        }
        else {
            // Else to controlller Where We can Login or Registered
            performSegue(withIdentifier: "splashToHelpSegue", sender: nil)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SplashToTabBarVC"
        {
            let controller = segue.destination as! RAMAnimatedTabBarController
            controller.selectedIndex = 0
            if isComingFromPush
            {
                controller.setSelectIndex(from: 0, to: 3)
            }
        }
        else if segue.identifier == "profile Page Segue"
        {
            let controller = segue.destination as! ProfilePageVC
            controller.isMyProfile = isMyProfile
            controller.userId = userId
        }
    }
}
