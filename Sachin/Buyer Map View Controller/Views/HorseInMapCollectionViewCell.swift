//
//  HorseInMapCollectionViewCell.swift
//  Sales Paddock
//
//  Created by Sachin Nautiyal on 31/01/2018.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class HorseInMapCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageViewOutlet: UIImageView!
    @IBOutlet weak var titleLabelOutlet: UILabel!
    @IBOutlet weak var favImageOutlet: UIImageView!
    var horseData : Horse! {
        didSet {
            self.titleLabelOutlet.text = horseData.name
            if horseData.isFavorite {
                self.favImageOutlet.isHidden = false
            } else {
                self.favImageOutlet.isHidden = true
            }
        }
    }
    
    func setImage(withHorseData horseData : Horse) {
        if let imageURL = URL(string : horseData.image) {
            self.imageViewOutlet.kf.indicatorType = .activity
            self.imageViewOutlet.kf.indicator?.startAnimatingView()
            self.imageViewOutlet.kf.setImage(with: imageURL, placeholder: #imageLiteral(resourceName: "horse_default_image"), options: [.transition(ImageTransition.fade(1))], progressBlock:nil , completionHandler: { (image, error, _, _) in
                self.imageViewOutlet.kf.indicator?.stopAnimatingView()
            })
        }
    }
}
