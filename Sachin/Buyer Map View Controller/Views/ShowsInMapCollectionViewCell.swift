//
//  ShowsInMapCollectionViewCell.swift
//  Sales Paddock
//
//  Created by Sachin Nautiyal on 31/01/2018.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class ShowsInMapCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var showNameOutlet: UILabel!
    @IBOutlet weak var showImageViewOutlet: UIImageView!
    
    var showsData : Show! {
        didSet {
            self.showNameOutlet.text = showsData.name
        }
    }
    
    func setImage(withShowData showData : Show) {
        if let imageURL = URL(string : showData.image) {
            self.showImageViewOutlet.kf.indicatorType = .activity
            self.showImageViewOutlet.kf.indicator?.startAnimatingView()
            self.showImageViewOutlet.kf.setImage(with: imageURL, placeholder: #imageLiteral(resourceName: "horse_default_image"), options: [.transition(ImageTransition.fade(1))], progressBlock:nil , completionHandler: { (image, error, _, _) in
                self.showImageViewOutlet.kf.indicator?.stopAnimatingView()
            })
        }
    }
}
