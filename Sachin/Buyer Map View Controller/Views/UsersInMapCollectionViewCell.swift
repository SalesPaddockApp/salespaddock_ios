//
//  UsersInMapCollectionViewCell.swift
//  Sales Paddock
//
//  Created by Sachin Nautiyal on 31/01/2018.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class UsersInMapCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var userImageOutlet: UIImageView!
    @IBOutlet weak var userNameOutlet: UILabel!
    @IBOutlet weak var userLocationOutlet: UILabel!
    
    var userData : User! {
        didSet {
            self.userNameOutlet.text = userData.name
            self.userLocationOutlet.text = userData.place
        }
    }
    
    func setImage(withUserData userData : User) {
        if let imageURL = URL(string : userData.image) {
            self.userImageOutlet.kf.indicatorType = .activity
            self.userImageOutlet.kf.indicator?.startAnimatingView()
            self.userImageOutlet.kf.setImage(with: imageURL, placeholder: #imageLiteral(resourceName: "profile_defalt_image"), options: [.transition(ImageTransition.fade(1))], progressBlock:nil , completionHandler: { (image, error, _, _) in
                self.userImageOutlet.kf.indicator?.stopAnimatingView()
            })
        }else{
            self.userImageOutlet.image = #imageLiteral(resourceName: "host_profile_image")
        }
    }
}
