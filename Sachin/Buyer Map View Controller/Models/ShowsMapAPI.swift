//
//  ShowsMapAPI.swift
//  Sales Paddock
//
//  Created by Sachin Nautiyal on 06/02/2018.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Show {
    var id : String!
    var name : String!
    var endDate : String!
    var startDate : String!
    var venue : String!
    var image : String!
    var description : String!
    var lat : String!
    var long : String!
}

struct Horse {
    var name : String!
    var image : String!
    var lat : String!
    var long : String!
    var id : String!
    var isFavorite : Bool!
}

struct User {
    var name : String!
    var lat : String!
    var long : String!
    var id : String!
    var image : String!
    var totalActivePost: String!
    var place : String!
    var userID : String!
}

class ShowsMapAPI {
    
    struct MapAPIConstant {
        static let getAroundYouPosts = "GetAroundYouPosts"
        static let getAroundYouShows = "GetAroundYouShows"
        static let getAroundYouUsers = "GetAroundYouUsers"
    }
    
    let currentCoordinates = LocationCoordinateManager.sharedInstance.currentCoordinates
    
    private func getAroundYouData(withAPIName apiName: String, andLat lat: String, andLong long : String, radious:String, andPageIndex:String, responseCompletion : @escaping ([JSON]?) -> Void) {
        
        guard let userID = UserDefaults.standard.object(forKey: "user_id") as? String else { return }
        if userID.count>0 {
            guard let token = UserDefaults.standard.object(forKey: "session_token") as? String else { return }
            var params = [String : AnyObject]()
            params["userId"] = userID as AnyObject
            params["lat"] = lat as AnyObject
            params["long"] = long as AnyObject
            params["token"] = token as AnyObject
            params["redious"] = radious as AnyObject
            params["pageNo"] = andPageIndex as AnyObject
//            print("param:\(params)")
            AFWrapper.requestPOSTURL(serviceName: apiName, params: params , success: { (response) in
                let errFlag = response["errCode"].numberValue
                switch errFlag {
                case 0:
                    responseCompletion(response["response"].array)
                    break
                    
                default:
                    break
                }
            }, failure : { (error) in
//                print(error.localizedDescription)
            })
        }
    }
    
    func getAroundYouPosts(withLat lat : String, andLong long : String, radious: String, pageIndex: String,  completionHandler :@escaping ([Horse]?) -> Void) {
        let serviceName = MapAPIConstant.getAroundYouPosts
        self.getAroundYouData(withAPIName: serviceName, andLat: lat, andLong: long, radious: radious, andPageIndex: pageIndex ) { (responseData) in
            if let responseArray = responseData {
                var horseData = [Horse]()
//                print("Response count :\(responseArray.count)")
                for (_, responseObject) in responseArray.enumerated() {
                    if let postName = responseObject["name"].string,
                        let latitude = responseObject["location"]["latitude"].double,
                        let longitude = responseObject["location"]["longitude"].double,
                        let postId = responseObject["_id"].string,
                        let postImage = responseObject["image"].string,
                        let isMyfavoritPost = responseObject["isMyfavoritPost"].bool {
                        let postData = Horse(name: postName, image: postImage, lat: "\(latitude)", long: "\(longitude)", id: postId, isFavorite : isMyfavoritPost)
                        horseData.append(postData)
                    }
                }
                completionHandler(horseData)
            } else {
//                print("Got Error")
                completionHandler(nil)
            }
        }
    }
    
    func getAroundYouShows(withLat lat : String, andLong long : String, radious: String, pageIndex: String, completionHandler :@escaping ([Show]?) -> Void) {
        let serviceName = MapAPIConstant.getAroundYouShows
        self.getAroundYouData(withAPIName: serviceName, andLat: lat, andLong: long, radious: radious, andPageIndex: pageIndex) { (responseData) in
            if let responseArray = responseData {
                var showsData = [Show]()
                for (_, responseObject) in responseArray.enumerated() {
                    if let showId = responseObject["_id"].string,
                        let showName = responseObject["showName"].string,
                        let endDate = responseObject["endDate"].string,
                        let venue = responseObject["placeName"].string,
                        let image = responseObject["imgPath"].string,
                        let description = responseObject["Description"].string,
                        let startdate = responseObject["startDate"].string,
                        let lat = responseObject["lat"].double,
                        let long = responseObject["lng"].double {
                        let showData = Show(id: showId, name: showName, endDate: endDate, startDate: startdate, venue: venue, image: image, description: description, lat: "\(lat)", long: "\(long)")
                        showsData.append(showData)
                    }
                }
                completionHandler(showsData)
            } else {
//                print("Got Error")
                completionHandler(nil)
            }
        }
    }
    
    func getAroundYouUsers(withLat lat : String, andLong long : String, radious: String, pageIndex: String, completionHandler : @escaping ([User]?) -> Void) {
        let serviceName = MapAPIConstant.getAroundYouUsers
        self.getAroundYouData(withAPIName: serviceName, andLat: lat, andLong: long, radious: radious, andPageIndex: pageIndex) { (responseData) in
            if let responseArray = responseData {
                var usersData = [User]()
                for (_, responseObject) in responseArray.enumerated() {
                    if let postName = responseObject["name"].string,
                        let latitude = responseObject["location"]["latitude"].double,
                        let longitude = responseObject["location"]["longitude"].double,
                        let postId = responseObject["_id"].string,
                        let placeName = responseObject["placeName"].string,
                        let userID = responseObject["userId"].string,
                        let numberOfPosts = responseObject["totalActivePost"].int,
                        let postImage = responseObject["image"].string {
                        let postData = User(name: postName, lat: "\(latitude)", long: "\(longitude)", id: postId, image: postImage, totalActivePost: "\(numberOfPosts)", place: placeName, userID: userID)
                        usersData.append(postData)
                    }
                }
                completionHandler(usersData)
            } else {
//                print("Got Error")
                completionHandler(nil)
            }
        }
    }
}
