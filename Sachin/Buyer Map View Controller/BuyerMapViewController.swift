//
//  BuyerMapViewController.swift
//  Sales Paddock
//
//  Created by Sachin Nautiyal on 31/01/2018.
//  Copyright © 2018 3Embed. All rights reserved.
//

import UIKit
import MapKit
import Kingfisher

class BuyerMapViewController: UIViewController {
    
    struct Constants {
        static let horseCellName = "HorseInMapCollectionViewCell"
        static let showCellName = "ShowsInMapCollectionViewCell"
        static let userCellName = "UsersInMapCollectionViewCell"
        static let annotationViewIdentifier = "annotationViewIdentifier"
        
    }
    
    enum SegueConstants : String  {
        case showDetailsSegue = "showDetailsSegue"
        case horseDetailsSegue = "horseDetailsSegue"
        case userDetailsSegue = "userDetailsSegue"
    }
    
    @IBOutlet weak var gradientViewOutlet: GradientView!
    @IBOutlet weak var mapViewOutlet: MKMapView!
    @IBOutlet weak var horseCollectionViewOutlet: UICollectionView!
    @IBOutlet weak var usersCollectionView: UICollectionView!
    @IBOutlet weak var horseAndShowsCollectionViewOutlet: UIView!
    @IBOutlet weak var usersCollectionViewOutlet: UIView!
    @IBOutlet weak var crossButtonoutlet: UIButton!
    
    var currentPage : Int!
    let apiModalObject = ShowsMapAPI()
    var collectionViewData: [Any] = []
    var previousTag :Int!
    private var mapChangedFromUserInteraction = false
    private var mapViewInitialized = true
    var lastAnnotation : MKAnnotation?
    var annotationArray : [MKAnnotation]?
    
    var span : MKCoordinateSpan?
    var pageIndex : Int = 0
    var currentRadious : Double = 250.0
    var currentCenterCoordinates : CLLocationCoordinate2D?
    var centerAnnotationView = MKPinAnnotationView()
    var centerAnnotation = MKPointAnnotation()
    var isDoneWithPagging : Bool = false
    
    let paggingNumber = 100
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        span = MKCoordinateSpan(latitudeDelta: 10, longitudeDelta: 10)
        self.currentCenterCoordinates = LocationCoordinateManager.sharedInstance.currentCoordinates
        self.centerAnnotation.coordinate = self.mapViewOutlet.centerCoordinate
        self.centerAnnotationView = MKPinAnnotationView(annotation: self.centerAnnotation, reuseIdentifier: "Center location")
        //        self.setupCollectionViews(withType: currentPage, andCoordinates: self.currentCenterCoordinates!, radious: "250" )
    }
    
    private func setupCollectionViews(withType currentType: Int,andCoordinates coordinates: CLLocationCoordinate2D, radious: String) {
        let lat = "\(coordinates.latitude)"
        let long = "\(coordinates.longitude)"
        switch currentType {
        case 0:
            self.enableUsersCollectionView()
            getHorses(withLat: lat, andLong: long, currentRadious: radious )
        case 1:
            self.enableUsersCollectionView()
            getShows(withLat: lat, andLong: long, currentRadious: radious )
        case 2:
            self.enableHorseAndShowsCollectionView()
            getUsers(withLat: lat, andLong: long, currentRadious: radious )
        default:
            //            print("Not Valid")
            break
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.pageIndex = 0;
        self.setupCollectionViews(withType: currentPage, andCoordinates: self.currentCenterCoordinates!, radious: "\(self.currentRadious)" )
        self.makeHighlightedCrossButton()
        self.makeCenterAnnotaion()
    }
    
    /// To make center annotation view on map
    func makeCenterAnnotaion(){
        let deadlineTime = DispatchTime.now() + 1.0
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            self.mapViewOutlet.addSubview(self.centerAnnotationView)
            self.moveMapAnnotationToCoordinate(coordinate: self.mapViewOutlet.centerCoordinate)
        }
    }
    
    /// To make cross button highlighted
    func makeHighlightedCrossButton(){
        self.crossButtonoutlet.layer.shadowOffset = CGSize.init(width: 2, height: 2)
        self.crossButtonoutlet.layer.shadowRadius = 2.0
        self.crossButtonoutlet.layer.shadowOpacity = 1.0
    }
    
    private func setCenterLocation(withCoordinates  coordinates: CLLocationCoordinate2D) {
        //        let region = MKCoordinateRegionMakeWithDistance(coordinates, self.spanLattitude, self.spanLongitude)
        let region = MKCoordinateRegionMake(coordinates, span!)
        DispatchQueue.main.async {
            let adjustRegion = self.mapViewOutlet.regionThatFits(region)
            self.mapViewOutlet.setRegion(adjustRegion, animated: true)
            self.mapViewOutlet.showsUserLocation = true
        }
    }
    
    private func getUsers(withLat lat:String, andLong long : String, currentRadious : String) {
        apiModalObject.getAroundYouUsers(withLat: lat, andLong: long, radious: currentRadious, pageIndex: "\(pageIndex)", completionHandler: { (users) in
            if let users = users {
                if self.pageIndex == 0{
                    self.mapViewOutlet.removeAnnotations(self.mapViewOutlet.annotations)
                    self.collectionViewData = users
                }else{
                    if users.count == 0{
                        self.isDoneWithPagging = true
                    }
                    var usersArr = self.collectionViewData as! [User]
                    usersArr.append(contentsOf: users)
                    self.collectionViewData = usersArr
                }
                self.addDatIntoMap(withData: users)
                self.usersCollectionView.reloadData()
            }
        })
    }
    
    private func getHorses(withLat lat:String, andLong long : String, currentRadious : String) {
        apiModalObject.getAroundYouPosts(withLat: lat, andLong: long, radious: currentRadious, pageIndex: "\(pageIndex)", completionHandler: { (horses) in
            if let horses = horses {
                if self.pageIndex == 0{
                    self.mapViewOutlet.removeAnnotations(self.mapViewOutlet.annotations)
                    self.collectionViewData = horses
                }else{
                    if horses.count == 0{
                        self.isDoneWithPagging = true
                    }
                    var horseArr = self.collectionViewData as! [Horse]
                    horseArr.append(contentsOf: horses)
                    self.collectionViewData = horseArr
                }
                self.addDatIntoMap(withData: horses)
                self.horseCollectionViewOutlet.reloadData()
            }
        })
    }
    
    private func getShows(withLat lat:String, andLong long : String, currentRadious : String) {
        apiModalObject.getAroundYouShows(withLat: lat, andLong: long, radious: currentRadious, pageIndex: "\(pageIndex)", completionHandler: { (shows) in
            if let shows = shows {
                if self.pageIndex == 0{
                    self.mapViewOutlet.removeAnnotations(self.mapViewOutlet.annotations)
                    self.collectionViewData = shows
                }else{
                    if shows.count == 0{
                        self.isDoneWithPagging = true
                    }
                    var showsArr = self.collectionViewData as! [Show]
                    showsArr.append(contentsOf: shows)
                    self.collectionViewData = showsArr
                }
                self.addDatIntoMap(withData: shows)
                self.horseCollectionViewOutlet.reloadData()
            }
        })
    }
    
    private func addDatIntoMap(withData data : [Any]) {
        if let horseData = data as? [Horse] {
            for (index, horse) in horseData.enumerated() {
                if let lat = horse.lat, let long = horse.long, let id = horse.id{
                    if index == 0{
                        self.createAnnotation(forLat: lat, andLong: long, withId: id, isFirstAnnotation : mapViewInitialized)
                    } else {
                        self.createAnnotation(forLat: lat, andLong: long, withId: id, isFirstAnnotation : false)
                    }
                }
            }
        } else if let showData = data as? [Show] {
            for (index, show) in showData.enumerated() {
                if let lat = show.lat, let long = show.long, let id = show.id {
                    if index == 0{
                        self.createAnnotation(forLat: lat, andLong: long, withId: id, isFirstAnnotation : mapViewInitialized)
                    } else {
                        self.createAnnotation(forLat: lat, andLong: long, withId: id, isFirstAnnotation : false)
                    }
                }
            }
        } else if let userData = data as? [User] {
            for (index, user) in userData.enumerated() {
                if let lat = user.lat, let long = user.long, let id = user.id {
                    if index == 0{
                        self.createAnnotation(forLat: lat, andLong: long, withId: id, isFirstAnnotation : mapViewInitialized)
                    } else {
                        self.createAnnotation(forLat: lat, andLong: long, withId: id, isFirstAnnotation : false)
                    }
                }
            }
        }
        
        
    }
    
    private func enableHorseAndShowsCollectionView() {
        ///Disabling Horse collection view
        self.horseAndShowsCollectionViewOutlet.isHidden = true
        self.horseCollectionViewOutlet.delegate = nil
        self.horseCollectionViewOutlet.dataSource = nil
        if #available(iOS 10.0, *) {
            self.horseCollectionViewOutlet.prefetchDataSource = nil
        }
        
        ///Enabling Users collection view
        self.usersCollectionViewOutlet.isHidden = false
        self.usersCollectionView.dataSource = self
        self.usersCollectionView.delegate = self
        if #available(iOS 10.0, *) {
            self.usersCollectionView.prefetchDataSource = self
        }
    }
    
    private func enableUsersCollectionView() {
        ///Enabling horse collection view
        self.horseAndShowsCollectionViewOutlet.isHidden = false
        self.horseCollectionViewOutlet.delegate = self
        self.horseCollectionViewOutlet.dataSource = self
        if #available(iOS 10.0, *) {
            self.horseCollectionViewOutlet.prefetchDataSource = self
        }
        
        ///Disabling Users collection view
        self.usersCollectionViewOutlet.isHidden = true
        self.usersCollectionView.dataSource = nil
        self.usersCollectionView.delegate = nil
        if #available(iOS 10.0, *) {
            self.usersCollectionView.prefetchDataSource = nil
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Buttons action
    
    @IBAction func closeButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func currentLocation(_ sender: Any) {
        let region =  MKCoordinateRegion(center: self.mapViewOutlet.userLocation.coordinate, span: self.span!)
        self.mapViewOutlet.setRegion(region, animated: true)
        let deadlineTime = DispatchTime.now() + 1.0
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            self.getPostsserviceCall(mapView: self.mapViewOutlet)
        }
    }
}

extension BuyerMapViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let horseData = collectionViewData[indexPath.row] as? Horse {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.horseCellName, for: indexPath) as! HorseInMapCollectionViewCell
            cell.horseData = horseData
            cell.imageViewOutlet.kf.indicatorType = .activity
            return cell
        }
        else if let showData = collectionViewData[indexPath.row] as? Show {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.showCellName, for: indexPath) as! ShowsInMapCollectionViewCell
            cell.showsData = showData
            cell.showImageViewOutlet.kf.indicatorType = .activity
            return cell
        }
        else if let userData = collectionViewData[indexPath.row] as? User {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.userCellName, for: indexPath) as! UsersInMapCollectionViewCell
            cell.userData = userData
            cell.userImageOutlet.kf.indicatorType = .activity
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            assert(false, "Unexpected element kind")
        case UICollectionElementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HorseInMapCollectionReusableView", for: indexPath) as! HorseInMapCollectionReusableView
            footerView.activityIndicator.startAnimating()
            return footerView
        default:
            assert(false, "Unexpected element kind")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForFooterInSection section: Int) -> CGSize{
        if self.collectionViewData.count < paggingNumber || self.isDoneWithPagging{
            return CGSize(width: 0, height: 0)
        }
        return CGSize(width: 60, height: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        //Pagging check
        if self.collectionViewData.count >= paggingNumber && indexPath.item == (self.collectionViewData.count - 1) && !self.isDoneWithPagging{
            pageIndex = pageIndex + 1
            self.setupCollectionViews(withType: currentPage, andCoordinates: self.currentCenterCoordinates!, radious: "\(self.currentRadious)")
        }
        //Setting image in collection cell
        if let cell = cell as? HorseInMapCollectionViewCell {
            if let horseData = collectionViewData[indexPath.row] as? Horse {
                cell.setImage(withHorseData: horseData)
            }
        } else if let cell = cell as? ShowsInMapCollectionViewCell {
            if let showData = collectionViewData[indexPath.row] as? Show {
                cell.setImage(withShowData: showData)
            }
        } else if let cell = cell as? UsersInMapCollectionViewCell {
            if let userData = collectionViewData[indexPath.row] as? User {
                cell.setImage(withUserData: userData)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.horseCollectionViewOutlet {
            return CGSize(width: 214, height: 184)
        } else if collectionView == self.usersCollectionView {
            return CGSize(width: 260, height: 65)
        } else {
            return CGSize(width: 0, height: 0)
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        var pageWidth : Float = 0
        if scrollView == self.usersCollectionView {
            pageWidth =  260+22 // PagingWidth = CellWidth + Cell Space
        } else if scrollView == self.horseCollectionViewOutlet {
            pageWidth =  214+22 // PagingWidth = CellWidth + Cell Space
        }
        let currentOffset: Float = Float(scrollView.contentOffset.x)
        let targetOffset: Float = Float(targetContentOffset.pointee.x)
        var newTargetOffset: Float = 0
        if targetOffset > currentOffset {
            newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth
        }
        else {
            newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth
        }
        if newTargetOffset < 0 {
            newTargetOffset = 0
        }
        else if (newTargetOffset > Float(scrollView.contentSize.width)){
            newTargetOffset = Float(Float(scrollView.contentSize.width))
        }
        
        targetContentOffset.pointee.x = CGFloat(currentOffset)
        scrollView.setContentOffset(CGPoint(x: CGFloat(newTargetOffset), y: scrollView.contentOffset.y), animated: true)
        if scrollView == self.usersCollectionView {
            let cIndex = Int((newTargetOffset+50)/282) //Cell size + spacing
            //            print(scrollView.contentOffset.x,scrollView.contentSize.width)
            if scrollView.contentOffset.x+374 > scrollView.contentSize.width {
                self.changeSelectedAnnotation(withIndex: self.collectionViewData.count-1)
            } else {
                self.changeSelectedAnnotation(withIndex: cIndex)
            }
        } else if scrollView == self.horseCollectionViewOutlet {
            let cIndex = Int((newTargetOffset+50)/236)
            if scrollView.contentOffset.x+374 > scrollView.contentSize.width {
                self.changeSelectedAnnotation(withIndex: self.collectionViewData.count-1)
            } else {
                self.changeSelectedAnnotation(withIndex: cIndex)
            }
        }
        self.view.layoutIfNeeded()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectionViewData.count
    }
    
    private func createAnnotation(forLat latitude: String, andLong longitude : String,withId dataID: String, isFirstAnnotation : Bool) {
        let annotation = MKPointAnnotation()
        let latitude = Double(latitude)
        let longitude = Double(longitude)
        annotation.title = dataID
        annotation.coordinate = CLLocationCoordinate2DMake(latitude!, longitude!)
        self.mapViewOutlet.addAnnotation(annotation)
        self.annotationArray?.append(annotation)
        if isFirstAnnotation {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.setCenterLocation(withCoordinates:CLLocationCoordinate2DMake(latitude!, longitude!))
                self.setAnnotationSelected(forDataID: dataID)
            }
        }
    }
    
    private func changeSelectedAnnotation(withIndex currentRow : Int) {
        switch self.currentPage {
        case 0: // For Horses
            if let data = self.collectionViewData[currentRow] as? Horse {
                self.setAnnotationSelected(forDataID: data.id)
            }
            
        case 1: // For Shows
            if let data = self.collectionViewData[currentRow] as? Show {
                self.setAnnotationSelected(forDataID: data.id)
            }
            
        case 2: // For Users
            if let data = self.collectionViewData[currentRow] as? User {
                self.setAnnotationSelected(forDataID: data.id)
            }
        default:
            //            print("Not Valid")
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let data = self.collectionViewData[indexPath.row] as? Show {
            self.performSegue(withIdentifier: SegueConstants.showDetailsSegue.rawValue, sender: data)
        } else if let data = self.collectionViewData[indexPath.row] as? Horse {
            self.performSegue(withIdentifier: SegueConstants.horseDetailsSegue.rawValue, sender: data)
        } else if let data = self.collectionViewData[indexPath.row] as? User {
            self.performSegue(withIdentifier: SegueConstants.userDetailsSegue.rawValue, sender: data)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case SegueConstants.horseDetailsSegue.rawValue:
            if let controller = segue.destination as? HorseDetailViewController, let horseData = sender as? Horse {
                controller.postId = horseData.id
            }
        case SegueConstants.showDetailsSegue.rawValue:
            if let controller = segue.destination as? ShowDetailVC, let showData = sender as? Show {
                var postDict : [String : AnyObject] = [String : AnyObject]()
                postDict["showId"] = showData.id as AnyObject
                postDict["showName"] = showData.name as AnyObject
                postDict["endDate"] = showData.endDate as AnyObject
                postDict["startDate"] = showData.startDate as AnyObject
                postDict["venue"] = showData.venue as AnyObject
                postDict["image"] = showData.image as AnyObject
                postDict["Description"] = showData.description as AnyObject
                controller.postDictionary = postDict
            }
        case SegueConstants.userDetailsSegue.rawValue:
            if let controller = segue.destination as? ProfilePageVC, let userData = sender as? User {
                controller.userId = userData.userID
                controller.isMyProfile = "0"
            }
        default:
            break
        }
    }
    
}

extension BuyerMapViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        let urls = indexPaths.flatMap {
            URL(string:self.urlStringReturn(withRow: $0.row))
        }
        ImagePrefetcher(urls: urls).start()
    }
    
    func urlStringReturn(withRow row: Int) -> String {
        var urlStr = ""
        if let horseData = collectionViewData[row] as? Horse {
            urlStr =  horseData.image
        } else if let showData = collectionViewData[row] as? Show {
            urlStr = showData.image
        } else if let userData = collectionViewData[row] as? User {
            urlStr = userData.image
        } else {
            urlStr = ""
        }
        return urlStr
    }
}

extension BuyerMapViewController : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView : MKAnnotationView!
        if let annotationview = mapViewOutlet.dequeueReusableAnnotationView(withIdentifier: Constants.annotationViewIdentifier) {
            annotationView = annotationview
        } else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: Constants.annotationViewIdentifier)
        }
        annotationView.annotation = annotation
        guard let title = annotation.title as? String else { return nil }
        guard let index = self.getAnnotationIndex(forDataID: title) else { return nil }
        switch self.currentPage {
        case 0: // For Horses
            if let horseData = self.collectionViewData[index] as? Horse {
                if horseData.isFavorite {
                    annotationView.image = #imageLiteral(resourceName: "heart_icon_unselected")
                } else {
                    annotationView.image = #imageLiteral(resourceName: "posts_off")
                }
            }
        case 1: // For Shows
            annotationView.image = #imageLiteral(resourceName: "posts_off")
        case 2: // For Users
            annotationView.image = #imageLiteral(resourceName: "user_posts_off")
            if let userData = self.collectionViewData[index] as? User {
                guard let count = userData.totalActivePost else { return nil }
                annotationView.image = UIImage(named: "user_posts_off")?.textToImage(drawText: count, isSelected:false)
            }
        default:
            //            print("Not Valid")
            break
        }
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        self.mapViewOutlet.deselectAnnotation(self.lastAnnotation, animated: true)
        self.makeAnnotationSelected(withAnnotationView : view)
        self.lastAnnotation = view.annotation
        //        if let coordinates = view.annotation?.coordinate {
        //            self.setCenterLocation(withCoordinates:coordinates)
        //        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        guard let annotation = view.annotation else { return }
        guard let title = annotation.title as? String else { return }
        guard let index = self.getAnnotationIndex(forDataID: title) else { return }
        switch self.currentPage {
        case 0: // For Horses
            if let horseData = self.collectionViewData[index] as? Horse {
                if horseData.isFavorite {
                    view.image = #imageLiteral(resourceName: "heart_icon_unselected")
                } else {
                    view.image = #imageLiteral(resourceName: "posts_off")
                }
            }
        case 1: // For Shows
            view.image = #imageLiteral(resourceName: "posts_off")
        case 2: // For Users
            view.image = #imageLiteral(resourceName: "user_posts_off")
            guard let userData = self.collectionViewData[index] as? User else { return }
            guard let count = userData.totalActivePost else { return }
            view.image = UIImage(named: "user_posts_off")?.textToImage(drawText: count, isSelected:false)
            
        default:
            //            print("Not Valid")
            break
        }
    }
    
    private func setAnnotationSelected(forDataID dataID : String) {
        let annotations = self.mapViewOutlet.annotations
        if let index = annotations.index(where: {($0.title as? String) == dataID}) {
            self.mapViewOutlet.selectAnnotation(annotations[index], animated: true)
            //            let coordinates = annotations[index].coordinate
            //            self.setCenterLocation(withCoordinates:coordinates)
        }
    }
    
    private func getAnnotationIndex(forDataID dataID : String) -> Int?  {
        switch self.currentPage {
        case 0: // For Horses
            let horseData = self.collectionViewData as! [Horse]
            if let index = horseData.index(where: {$0.id == dataID}) {
                return index
            }
        case 1: // For Shows
            let showsData = self.collectionViewData as! [Show]
            if let index = showsData.index(where: {$0.id == dataID}) {
                return index
            }
            
        case 2: // For Users
            let usersData = self.collectionViewData as! [User]
            if let index = usersData.index(where: {$0.id == dataID}) {
                return index
            }
        default:
            break
        }
        return nil
    }
    
    private func makeAnnotationSelected(withAnnotationView view: MKAnnotationView) {
        if let title = view.annotation?.title as? String {
            if let index = self.getAnnotationIndex(forDataID: title) {
                self.getData(forIndex: index)
                switch self.currentPage {
                case 0: // For Horses
                    if let horseData = self.collectionViewData[index] as? Horse {
                        if horseData.isFavorite {
                            view.image = #imageLiteral(resourceName: "heart_icon_selected")
                        } else {
                            view.image = #imageLiteral(resourceName: "posts_on")
                        }
                    }
                case 1: // For Shows
                    view.image = #imageLiteral(resourceName: "posts_on")
                case 2: // For Users
                    if let userData = self.collectionViewData[index] as? User {
                        if let count = userData.totalActivePost {
                            view.image = UIImage(named: "user_posts_on")?.textToImage(drawText: count, isSelected:true)
                        }
                    }
                default:
                    //                    print("Not Valid")
                    break
                }
            }
        }
    }
    
    func getData(forIndex index : Int) {
        switch self.currentPage {
        case 0: // For Horses
            self.horseCollectionViewOutlet.scrollToItem(at: IndexPath(row: index, section: 0), at: .left, animated: true)
        case 1: // For Shows
            self.horseCollectionViewOutlet.scrollToItem(at: IndexPath(row: index, section: 0), at: .left, animated: true)
        case 2: // For Users
            self.usersCollectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: .left, animated: true)
        default:
            //            print("Not Valid")
            break
        }
    }
    
    private func mapViewRegionDidChangeFromUserInteraction() -> Bool {
        let view = self.mapViewOutlet.subviews[0]
        if let gestureRecognizers = view.gestureRecognizers {
            for recognizer in gestureRecognizers {
                if( recognizer.state == .began || recognizer.state == .ended ) {
                    return true
                }
            }
        }
        return false
    }
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        mapChangedFromUserInteraction = mapViewRegionDidChangeFromUserInteraction()
        //        currentLocationAnnotation.coordinate = mapView.centerCoordinate
        mapViewInitialized = false
        if (mapChangedFromUserInteraction) {
            //             user is changing map region
            let deadlineTime = DispatchTime.now() + 1.0
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                self.getPostsserviceCall(mapView: mapView)
            }
            //            _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.serviceCall(mapView:)), userInfo: nil, repeats: false)
        }
        span = MKCoordinateSpan(latitudeDelta: mapView.region.span.latitudeDelta, longitudeDelta: mapView.region.span.longitudeDelta)
    }
    
    // You can include this method for more accuracy.
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        span = MKCoordinateSpan(latitudeDelta: mapView.region.span.latitudeDelta, longitudeDelta: mapView.region.span.longitudeDelta)
    }
    
    
    /// To get horses and user at center of map in map readios which is visible in map
    ///
    /// - Parameter mapView: mapView on which we get horses and users
    @objc func getPostsserviceCall(mapView: MKMapView){
        mapViewInitialized = false
        let currentCoordinates = self.mapViewOutlet.centerCoordinate
        mapViewOutlet.removeAnnotations(self.mapViewOutlet.annotations)
        self.annotationArray?.removeAll()
        let rad = self.getRadious()
        var radiou = (rad - (rad / 10)) / 1000
        if radiou <= 5{
            radiou = 5
        }
        self.pageIndex = 0;
        self.currentRadious = radiou
        self.currentCenterCoordinates = currentCoordinates
        self.isDoneWithPagging = false
        self.setupCollectionViews(withType: currentPage, andCoordinates: currentCoordinates, radious: "\(radiou)")
        span = MKCoordinateSpan(latitudeDelta: mapView.region.span.latitudeDelta, longitudeDelta: mapView.region.span.longitudeDelta)
    }
    
    
    /// To Get center of mapView
    ///
    /// - Returns: Center Coordinate map
    func topCenterCoordinate() -> CLLocationCoordinate2D {
        return self.mapViewOutlet.convert(CGPoint(x: self.view.frame.size.width / 2.0, y: 0), toCoordinateFrom: self.view)
    }
    
    
    /// To get current redious of map
    ///
    /// - Returns: Radious of map
    func getRadious() -> Double {
        let centerLocation = CLLocation(latitude: self.mapViewOutlet.centerCoordinate.latitude, longitude: self.mapViewOutlet.centerCoordinate.longitude)
        let topCenterCoordinate = self.mapViewOutlet.convert(CGPoint(x: 0, y: self.view.frame.size.height / 2.0), toCoordinateFrom: self.view)
        let topCenterLocation = CLLocation(latitude: topCenterCoordinate.latitude, longitude: topCenterCoordinate.longitude)
        return centerLocation.distance(from: topCenterLocation)
    }
    
    // These are the constants need to offset distance between the lower left corner of
    // the annotaion view and the head of the pin
    //    #define PIN_WIDTH_OFFSET 7.75
    //    #define PIN_HEIGHT_OFFSET 5
    func moveMapAnnotationToCoordinate(coordinate: CLLocationCoordinate2D){
        let mapViewPoint = self.mapViewOutlet.convert(coordinate, toPointTo: self.mapViewOutlet)
        // Offset the view from to account for distance from the lower left corner to the pin head
        let xoffset = (self.centerAnnotationView.bounds.midX) - 7.75
        let yoffset = -(self.centerAnnotationView.bounds.midY) + 5.0
        self.centerAnnotationView.center = CGPoint(x: mapViewPoint.x + xoffset, y: mapViewPoint.y + yoffset)
    }
    
}


