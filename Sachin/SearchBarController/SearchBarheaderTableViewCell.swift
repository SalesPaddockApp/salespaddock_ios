//
//  SearchBarheaderTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 17/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SearchBarheaderTableViewCell: UITableViewCell {
    @IBOutlet weak var headerTitleOutlet: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
