//
//  SearchbarTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 16/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SearchbarTableViewCell: UITableViewCell {

    @IBOutlet weak var searchItemLabelOutlet: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
