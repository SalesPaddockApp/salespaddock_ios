//
//  SearchResults.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 17/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import SwiftyJSON

enum SearchResultType {
    case users
    case shows
    case horses
}

class Search : NSObject {
    var searchResultId : String?
    var searchResultName : String?
    var searchResultType : SearchResultType
    var searchData : [String : Any]?
    
    init(searchResultId : String,searchResultName : String, searchResultType : SearchResultType ) {
        self.searchResultId = searchResultId
        self.searchResultName = searchResultName
        self.searchResultType = searchResultType
    }
}

class SearchResults: NSObject {
    
    var searchCategoryName  : String
    var searchResults : [Search]
    
    init(searchCategoryName : String,searchResults : [Search] ) {
        self.searchCategoryName = searchCategoryName
        self.searchResults = searchResults
    }
    
    class func getSearchData(forString searchString : String,callback :@escaping ([SearchResults]) -> Void){
        let token = UserDefaults.standard.object(forKey: "session_token") as! String
        var params = [String :AnyObject]()
        params["token"] = token as AnyObject
        params["searchParam"] = searchString as AnyObject
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.searchPosts, params: params, success: { response in
//            print(response)
            let errFlag = response["errCode"].numberValue
            
            switch errFlag {
            case 0:
                var results : [SearchResults] = []
                if let responseData = response["response"].dictionaryObject{
                    if let users = responseData["users"] as? [Any] {
                        var userObj : [Search] = []
                        for user in users {
                            guard let userData = user as? [String: Any] else { return }
                            if let name = userData["name"] as? [String : Any] {
                                if let fName = name["fName"] as? String, let lName = name["lName"] as? String {
                                    guard let userID = userData["userId"] as? String else { return }
                                    let search = Search(searchResultId: userID, searchResultName: "\(fName) \(lName)", searchResultType: .users)
                                    userObj.append(search)
                                }
                            }
                        }
                        let usersObj = SearchResults(searchCategoryName: "Sellers", searchResults: userObj)
                        results.append(usersObj)
                    }
                    if let horses = responseData["horse"] as? [Any] {
                        var horseObj : [Search] = []
                        for horse in horses {
                            guard let horseData = horse as? [String: Any] else { return }
                            if let name = horseData["name"] as? String {
                                guard let postID = horseData["postId"] as? String else { return }
                                let search = Search(searchResultId: postID, searchResultName: "\(name)", searchResultType: .horses)
                                horseObj.append(search)
                            }
                        }
                        let horsesObj = SearchResults(searchCategoryName: "Horse", searchResults: horseObj)
                        results.append(horsesObj)
                    }
                    if let shows = responseData["Shows"] as? [Any] {
                        var showsObj : [Search] = []
                        for show in shows {
                            guard let showData = show as? [String: Any] else { return }
                            if let name = showData["showName"] as? String {
                                guard let showID = showData["showId"] as? String else { return }
                                let search = Search(searchResultId: showID, searchResultName: "\(name)", searchResultType: .shows)
                                search.searchData = showData
                                showsObj.append(search)
                            }
                        }
                        let horsesObj = SearchResults(searchCategoryName: "Shows", searchResults: showsObj)
                        results.append(horsesObj)
                    }
                }
                callback(results)
            default :
                break
            }
        })
        { (error) in
//            print(error.localizedDescription)
        }
    }
}
