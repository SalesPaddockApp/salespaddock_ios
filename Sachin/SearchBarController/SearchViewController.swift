//
//  SearchViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 16/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    struct Constants {
        static let searchBarTableViewCell = "SearchbarTableViewCell"
        static let searchBarheaderTableViewCell = "SearchBarheaderTableViewCell"
        static let cellHeight:CGFloat = 45.0
        static let toShowDetailsControllerSegue = "toShowDetailsControllerSegue"
        static let toUserProfileControllerSegue = "toUserProfileControllerSegue"
        static let toHorseDetailsControllerSegue = "toHorseDetailsControllerSegue"
    }
    
    @IBOutlet weak var noDataAvailablelabelOutlet: UILabel!
    @IBOutlet weak var searchBarOutlet: UITextField!
    @IBOutlet weak var tableViewOutlet: UITableView!
    
    var searchResults : [SearchResults] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getSearchData(withSearchText: "")
        self.searchBarOutlet.becomeFirstResponder()
    }
    
    func getSearchData(withSearchText text : String ) {
        self.noDataAvailablelabelOutlet.isHidden = true
        SearchResults.getSearchData(forString: text, callback: { (results) in
            self.searchResults = results
            var totalResults:[Search] = []
            for result in results {
                totalResults.append(contentsOf: result.searchResults)
            }
            if (totalResults.count == 0) {
                self.noDataAvailablelabelOutlet.isHidden = false
            }else {
                self.noDataAvailablelabelOutlet.isHidden = true
            }
            self.tableViewOutlet.reloadData()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let resultData = sender as? Search {
            
            if segue.identifier == Constants.toHorseDetailsControllerSegue { // segue to horse details
                let controller = segue.destination as! HorseDetailViewController
                controller.postId = resultData.searchResultId!
            }
                
            else if segue.identifier == Constants.toUserProfileControllerSegue { // segue to user profile
                let controller = segue.destination as! ProfilePageVC
                controller.isMyProfile = "0"
                if let userId = resultData.searchResultId{
                    controller.userId = userId
                }
            }
                
            else if segue.identifier == Constants.toShowDetailsControllerSegue { // segue to show details
                let SDVC = segue.destination as! ShowDetailVC
                if let searchData = resultData.searchData {
                    SDVC.postDictionary = searchData as [String : AnyObject]
                }
            }
        }
    }
}

extension SearchViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        getSearchData(withSearchText: newString)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

extension SearchViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let searchResult = self.searchResults[indexPath.section]
        let resultsArray = searchResult.searchResults
        let result = resultsArray[indexPath.row]
        switch result.searchResultType {
        case .horses:
            self.performSegue(withIdentifier: Constants.toHorseDetailsControllerSegue, sender: result)
        case .shows:
            self.performSegue(withIdentifier: Constants.toShowDetailsControllerSegue, sender: result)
        case .users:
            self.performSegue(withIdentifier: Constants.toUserProfileControllerSegue, sender: result)
        }
    }
}

extension SearchViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.searchBarTableViewCell) as! SearchbarTableViewCell
        let searchResult = self.searchResults[indexPath.section]
        let resultsArray = searchResult.searchResults
        let result = resultsArray[indexPath.row]
        cell.searchItemLabelOutlet.text = result.searchResultName
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.searchResults.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let searchResult = self.searchResults[section]
        return searchResult.searchResults.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.cellHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.searchBarheaderTableViewCell) as! SearchBarheaderTableViewCell
        let searchResult = self.searchResults[section]
        let categoryName = searchResult.searchCategoryName
        cell.headerTitleOutlet.text = categoryName
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let searchResult = self.searchResults[section]
        if (searchResult.searchResults.count == 0) {
            return 0
        }
        return Constants.cellHeight
    }
}


