//
//  disciplineTableViewCell.swift
//  salesPaddock
//
//  Created by Rahul Sharma on 13/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit

class disciplineTableViewCell: UITableViewCell
{
    @IBOutlet weak var indexOutlet: UILabel!
    @IBOutlet weak var checkBoxBtn: UIButton!
    @IBOutlet weak var disciplineLbl: UILabel!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
