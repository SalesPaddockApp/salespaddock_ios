//
//  listSelectBreedViewController.swift
//  salesPaddock
//
//  Created by Rahul Sharma on 12/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class listSelectBreedViewController: UIViewController, UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var titleLabelOutlet: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var responseData = [JSON]()
    var SelectedAsset = [String : String]()
    var breedNames = [String]()
    var onCreateData = [JSON]()
    var selectedValues = [AnyObject]()
    var imageId = String()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        getPrefernces()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return breedNames.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.listSelectBreedTableViewCell, for: indexPath) as! listSelectBreedTableViewCell
        
        let breedName = self.breedNames[indexPath.row]
        cell.listSelectedLbl.text = breedName
        cell.indexOutlet.text = String(breedName[breedName.startIndex])
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.performSegue(withIdentifier: "disciplineControllerSegue", sender: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    @IBAction func backBtnClicked(_ sender: AnyObject)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "disciplineControllerSegue"{
            let disciplineController = segue.destination as! disciplineViewController
            let index = sender as! IndexPath
            let selectedIndex = index.row
            let selectedObj = self.breedNames[selectedIndex]
            var dictionary = [String:AnyObject]()
            disciplineController.postId = self.SelectedAsset["_id"]!
            dictionary["options"] = [selectedObj] as AnyObject?
            dictionary["pref_id"] = self.onCreateData[0]["id"].stringValue as AnyObject
            self.selectedValues.append(dictionary as AnyObject)
            disciplineController.onCreateData = self.onCreateData
            
            if let disciplines = self.onCreateData[1]["values"].arrayObject as? [String]{
                disciplineController.disciplines = disciplines.sorted()
            }
            disciplineController.responseData = self.responseData
            disciplineController.title = self.onCreateData[1]["title"].stringValue
            disciplineController.titleStr = self.onCreateData[1]["title"].stringValue
            disciplineController.selectedValues = self.selectedValues
            disciplineController.imageId = self.imageId
        }
    }
}

extension listSelectBreedViewController{
    
    func getPrefernces(){
        let keys = [String](SelectedAsset.keys)
        let values = [String](SelectedAsset.values)
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let params = ["token":token!,keys[0]:values[0],keys[1]:values[1],keys[2]:values[2]] as [String: String]
        
        
        Helper.showPI()

        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getPreferences, params: params as [String : AnyObject]?, success: { (response) in
            let errFlag = response["errCode"].numberValue
            Helper.hidePI()

            switch errFlag {
            case 0:
                self.responseData = response["response"]["data"].arrayValue
                let onCreateData = response["response"]["onCreate"].arrayValue.sorted{ $0["prirority"].intValue < $1["prirority"].intValue }
                self.onCreateData = onCreateData
                if let breedNameArray = onCreateData[0]["values"].arrayObject{
                    self.breedNames =  breedNameArray as! [String]
                    self.breedNames = self.breedNames.sorted()
                }
                self.title = onCreateData[0]["title"].stringValue
                self.titleLabelOutlet.text = "Choose your horse's \(onCreateData[0]["title"].stringValue)"
                self.tableView.reloadData()
                self.imageId = response["response"]["image"]["id"].stringValue
                break
            default:
                let errMsg = response["Message"].string
                break
            }
            }, failure: { (Error) in
                Helper.hidePI()
        })
    }
}
