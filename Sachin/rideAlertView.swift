//
//  rideAlertView.swift
//  salesPaddock
//
//  Created by Rahul Sharma on 09/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class rideAlertView: UIView
{
    @IBOutlet fileprivate var contentView:UIView?
    @IBOutlet weak var textViewOutlet: UITextView!
    
    var responseData = [JSON]()
    var currentIndex = Int()
    
    // other outlets
    static func loadViewFromNib(_ frame: CGRect) -> rideAlertView {
        
        let view = Bundle(for: self).loadNibNamed("rideAlertView", owner: nil, options: nil)?.first as! rideAlertView
        view.frame = frame
        return view
    }
    
    @IBAction func backButtonAction(_ sender: AnyObject)
    {
        self.removeFromSuperview()
    }
    
    func dismiss(_ text: String) {
        
        self.removeFromSuperview()
    }
    
    @IBAction func clickBtnClicked(_ sender: AnyObject) {
        
        self.sendRideAlert()
    }
    
    @IBAction func dismissViewAction(_ sender: AnyObject)
    {
        self.removeFromSuperview()
    }
}

extension rideAlertView : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func sendRideAlert(){
        
        Helper.showPI()
        
        if(self.textViewOutlet.text.characters.count == 0){
            
            self.window?.rootViewController?.showAlert("Error", message: "Please add some message here")

        }

        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let userId = UserDefaults.standard.object(forKey: "user_id") as! String!
        
        let dictionary = responseData[currentIndex]
        let postId = dictionary["postId"].stringValue
        let showId = dictionary["showId"].stringValue
        let message = self.textViewOutlet.text
        
        let params:[String : AnyObject] = ["token":token! as AnyObject,"userId":userId! as AnyObject, "postId":postId as AnyObject,"showId":showId as AnyObject, "mess":message! as AnyObject]
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.sendAlerts, params: params as [String : AnyObject]?
            , success:
            { (response) in
                let errFlag = response["errCode"].numberValue
                Helper.hidePI()
                switch errFlag {
                case 0:
                    self.window?.rootViewController?.showAlert("Success", message: "Alert sent successfully")
                    self.removeFromSuperview()
                    break
                default:
                    break
                }
            }, failure: { (Error) in
                Helper.hidePI()
        })
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Please enter the message you want to send as a ride to all the people who have viewed your horse at the show"
        {
            textView.text = ""
            textView.textColor = UIColor.black
        }
        textView.becomeFirstResponder()
    }
 
    func textViewDidEndEditing(_ textView: UITextView) {
        
         if textView.text == ""
         {
            textView.text = "Please enter the message you want to send as a ride to all the people who have viewed your horse at the show"
            textView.textColor = UIColor.lightGray
        }
        textView.resignFirstResponder()
    }
}
