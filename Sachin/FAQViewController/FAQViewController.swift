//
//  FAQViewController.swift
//  Sales Paddock
//
//  Created by Govind on 1/6/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import SwiftyJSON

class FAQViewController: UIViewController {
    
    var dataArray = [JSON]()
    @IBOutlet var faqTableViewOutlet: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        faqTableViewOutlet.estimatedRowHeight = 270
        faqTableViewOutlet.rowHeight = UITableViewAutomaticDimension
        
        self.getFAQs()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: Any)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func getFAQs(){
        
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let params:[String : AnyObject] = ["token":token! as AnyObject]
        Helper.showPI()
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getFAQ, params: params as [String : AnyObject]?
            , success:
            
            { (response) in
                
                Helper.hidePI()
                
                let errFlag = response["errCode"].numberValue
                switch errFlag {
                case 0:
                    self.dataArray = response["data"].arrayValue
                    self.faqTableViewOutlet.reloadData()
                    break
                default:
                    let errMsg = response["Message"].stringValue
                    break
                }
        }, failure: { (Error) in
            Helper.hidePI()
        })
    }
}

extension FAQViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FAQTableViewCell") as! FAQTableViewCell
        
        let dataDict = dataArray[indexPath.row].dictionaryValue
        cell.titleLabelOutlet.text = dataDict["question"]?.stringValue
        
        /*
         
         self.textLbl.attributedText = [[NSAttributedString alloc] initWithData:    [@"html-string" dataUsingEncoding:NSUnicodeStringEncoding]
         options:@{     NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType
         } documentAttributes:nil error:nil];
         
         */
        let text =  dataDict["answer"]?.stringValue
        cell.descriptionOutlet.attributedText = text?.html2AttributedString
        
        return cell
    }
}
extension String {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8),
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
//            print("error: ", error)
            return nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

