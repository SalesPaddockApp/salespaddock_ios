//
//  FAQTableViewCell.swift
//  Sales Paddock
//
//  Created by Govind on 1/6/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class FAQTableViewCell: UITableViewCell {

    @IBOutlet var descriptionOutlet: UITextView!
    @IBOutlet var titleLabelOutlet: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
