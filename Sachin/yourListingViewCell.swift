//
//  yourListingViewCell.swift
//  salesPaddock
//
//  Created by Rahul Sharma on 09/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit

class yourListingViewCell: UITableViewCell {

    @IBOutlet weak var pendingAdminApprovalOutlet: UILabel!
    @IBOutlet weak var imageViewOutlet: UIImageView!
    @IBOutlet weak var listTypeOutlet: UILabel!
    @IBOutlet weak var statusOfList: UILabel!
    @IBOutlet weak var horseNameOutlet: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
