//
//  FilterRangeTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 06/01/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import MARKRangeSlider
import SwiftyJSON

protocol filterRangeDelegates {
    func rangeForSliderWithValuesChanged()
}

class FilterRangeTableViewCell: UITableViewCell {
    
    var filterRangeDelegates :filterRangeDelegates! = nil
    var dataDict = [String : JSON]()
    var maxValue = Double()
    @IBOutlet var rangeViewOutlet: UIView!
    @IBOutlet var maxRangeOutlet: UILabel!
    @IBOutlet var minRangeOutlet: UILabel!
    @IBOutlet var titleOutlet: UILabel!
    var slider : MARKRangeSlider!
    var isResetButtonPressed : Bool!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setRangeView(min: Float,max:Float)
    {
        self.titleOutlet.text = dataDict["preferenceTitle"]?.stringValue
        if let existingSlider = self.viewWithTag(2) as? MARKRangeSlider{ //added a check for not getting repeated.
            slider = existingSlider

            if isResetButtonPressed{
                self.minRangeOutlet.text = "\(6.0)"
                self.maxRangeOutlet.text = "\(20.0)"
                self.slider.setLeftValue(6.0, rightValue: 20.0)
            }else{
                self.minRangeOutlet.text = "\(min)"
                self.maxRangeOutlet.text = "\(max)"
                self.slider.setLeftValue(CGFloat(min), rightValue: CGFloat(max))
            }
        }
        else{
            slider = MARKRangeSlider(frame: CGRect(x: 0, y: 15, width: UIScreen.main.bounds.size.width-50, height: self.rangeViewOutlet.frame.size.height))
            slider.tag = 2
            slider.rangeImage = self.getImageWithColor(color: UIColor(red: 0.039, green: 0.706, blue: 0.616, alpha: 1.0))
            slider.setMinValue(6.0, maxValue: 20.0)
            self.slider.setLeftValue(6.0, rightValue: 20.0)
            if max == 0 {
                self.minRangeOutlet.text = "\(6.0)"
                self.maxRangeOutlet.text = "\(20.0)"
            }
            else{
                slider.setLeftValue(CGFloat(min), rightValue: CGFloat(max))
                self.minRangeOutlet.text = "\(min)"
                self.maxRangeOutlet.text = "\(max)"
            }
            slider.minimumDistance = 2
            
            slider.addTarget(self, action: #selector(sliderMoved(slider:)), for: UIControlEvents.valueChanged)
            slider.sizeToFit()
            
            rangeViewOutlet.addSubview(slider)
        }
    }
    
    @objc func sliderMoved(slider : MARKRangeSlider)
    {
        let intMinVlaue = Int(slider.leftValue)
        let decimalMinimumValue = Float(slider.leftValue) - Float(intMinVlaue)
//        print("decimal Min value:\(decimalMinimumValue)")
        self.minRangeOutlet.text = String(format: "%.1f",Float(intMinVlaue)+decimalMinimumValue/2.9)

        let intMaxVlaue = Int(slider.rightValue)
        let decimalMaximumValue = Float(slider.rightValue) - Float(intMaxVlaue)
//        print("decimal Min value:\(decimalMaximumValue)")
        self.maxRangeOutlet.text = String(format: "%.1f",Float(intMaxVlaue)+decimalMaximumValue/2.9)
        
        UserDefaults.standard.removeObject(forKey: "selectedFilters")
        UserDefaults.standard.synchronize()
        var dictionary = [String:AnyObject]()
        let leftValue = String(format: "%.1f",Float(intMinVlaue)+decimalMinimumValue/2.9)
        let rightValue = String(format: "%.1f",Float(intMaxVlaue)+decimalMaximumValue/2.9)
        let name = dataDict["preferenceTitle"]?.stringValue
        dictionary["values"] = ["\(leftValue)","\(rightValue)"] as AnyObject
        dictionary["name"] = name! as AnyObject
        dictionary["pref_id"] = dataDict["_id"]?.stringValue as AnyObject
//        print("dictionary:\(dictionary)")
        UserDefaults.standard.set(dictionary, forKey: "selectedFilters")
        UserDefaults.standard.synchronize()
        if (self.filterRangeDelegates != nil){
            self.filterRangeDelegates.rangeForSliderWithValuesChanged()
        }
    }
    
    func getImageWithColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 15, width: UIScreen.main.bounds.size.width-50, height: self.rangeViewOutlet.frame.size.height)
        UIGraphicsBeginImageContextWithOptions(CGSize(width:UIScreen.main.bounds.size.width-50, height: self.rangeViewOutlet.frame.size.height), false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
