//
//  FiltersTableViewCell.swift
//  Sales Paddock
//
//  Created by Govind on 1/6/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol filterButtonActionDelegate {
    func filterButtonPressed(dataDictionary : [String: JSON])
}

class FiltersTableViewCell: UITableViewCell {

    var dataDict = [String : JSON]()
    @IBOutlet var detailsOutlet: UILabel!
    @IBOutlet var titleOutlet: UILabel!
    var filterButtonActionDelegate:filterButtonActionDelegate! = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
  
    @IBAction func changeBtnActn(_ sender: Any) {
        if (self.filterButtonActionDelegate != nil)
        {
            self.filterButtonActionDelegate.filterButtonPressed(dataDictionary: dataDict)
        }
    }

}
