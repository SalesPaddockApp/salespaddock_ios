//
//  FilterOptionTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 07/01/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class FilterOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var keyNameOutlet: UILabel!
    @IBOutlet weak var checkBoxImageViewOutlet: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
