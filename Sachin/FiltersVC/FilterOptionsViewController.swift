//
//  FilterOptionsViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 07/01/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import SwiftyJSON

class FilterOptionsViewController: UIViewController {

    @IBOutlet weak var titleOutlet: UILabel!
    @IBOutlet weak var tableVIewOutlet: UITableView!
    var titleOutletText = String()
    var dataDictionary = [String : JSON]()
    var prefID = String()
    var selectedValuesArray = [String]()
    var optionsArray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.titleOutlet.text = titleOutletText
        if let options = dataDictionary["optionsValue"]?.arrayObject
        {
            let optionsArr = options as! [String]
//            self.optionsArray = optionsArr.sorted()
            self.optionsArray = optionsArr
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveButtonAction(_ sender: Any)
    {
        var dictionary = [String:AnyObject]()
        let name = dataDictionary["preferenceTitle"]?.stringValue
        dictionary["values"] = selectedValuesArray as AnyObject
        dictionary["pref_id"] = self.prefID as AnyObject
        dictionary["name"] = name! as AnyObject
        UserDefaults.standard.set(dictionary, forKey: "selectedFilters")
        UserDefaults.standard.synchronize()
        _ = navigationController?.popViewController(animated: true)
    }

    @IBAction func backButtonAction(_ sender: Any)
    {
        _ = navigationController?.popViewController(animated: true)
    }
}

extension FilterOptionsViewController : UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterOptionTableViewCell") as! FilterOptionTableViewCell
        let key = self.optionsArray[indexPath.row]
        cell.keyNameOutlet.text = key
        
        if selectedValuesArray.contains(key)
        {
            cell.checkBoxImageViewOutlet.image = #imageLiteral(resourceName: "tick")
        }else
        {
            cell.checkBoxImageViewOutlet.image = #imageLiteral(resourceName: "tick_box")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.optionsArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath) as! FilterOptionTableViewCell
        let selectedString = self.optionsArray[indexPath.row]
        if selectedValuesArray.contains(selectedString)
        {
            cell.checkBoxImageViewOutlet.image = #imageLiteral(resourceName: "tick_box")
            let index = selectedValuesArray.index(of: selectedString)
            selectedValuesArray.remove(at: index!)
        }else
        {
            selectedValuesArray.append(selectedString)
            cell.checkBoxImageViewOutlet.image = #imageLiteral(resourceName: "tick")
        }
    }
}
