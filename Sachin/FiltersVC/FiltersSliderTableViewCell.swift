//
//  FiltersSliderTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 29/03/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

protocol fliterSliderDelegate {
    func filterValueChanged()
}

class FiltersSliderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var currentValueLabelOutlet: UILabel!
    @IBOutlet weak var silderRangeOutlet: UISlider!
    @IBOutlet weak var titleLabelOutlet: UILabel!
    var dataDict = [String : JSON]()
    var range = 20000
    var fliterSliderDelegate : fliterSliderDelegate! = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let locationManagerObj = LocationCoordinateManager.sharedInstance
        let status = locationManagerObj.checkLocationStatus()
        self.silderRangeOutlet.isUserInteractionEnabled = status
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func sliderMovedAction(_ sender: Any)
    {
        self.currentValueLabelOutlet.text = "\(Int(self.silderRangeOutlet.value)) miles"
        
        UserDefaults.standard.removeObject(forKey: "selectedFilters")
        UserDefaults.standard.synchronize()
        var dictionary = [String:AnyObject]()
        let upperVlaue = Int(self.silderRangeOutlet.value)
        let name = dataDict["preferenceTitle"]?.stringValue
        dictionary["values"] = ["\(upperVlaue)"] as AnyObject
        dictionary["name"] = name! as AnyObject
        dictionary["pref_id"] = dataDict["_id"]?.stringValue as AnyObject
        UserDefaults.standard.set(dictionary, forKey: "selectedFilters")
        UserDefaults.standard.synchronize()
        self.fliterSliderDelegate.filterValueChanged()
    }
}
