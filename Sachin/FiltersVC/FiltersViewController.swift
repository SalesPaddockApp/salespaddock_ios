//
//  FiltersViewController.swift
//  Sales Paddock
//
//  Created by Govind on 1/6/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol filterDataAppliedDelegate {
    func savedData(data: [AnyObject])
}

class FiltersViewController: UIViewController {
    
    var numberOfFilters = [JSON]()
    var selectedValues = [AnyObject]()
    var filterDataAppliedDelegate:filterDataAppliedDelegate! = nil
    var isResetButtonPressed = false
    @IBOutlet var filtersTableViewOutlet: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        filtersTableViewOutlet.estimatedRowHeight = 130
        filtersTableViewOutlet.rowHeight = UITableViewAutomaticDimension
        self.getFilters()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addValues()
        self.filtersTableViewOutlet.reloadData()
    }
    
    func addValues(){
        if((UserDefaults.standard.value(forKey: "selectedFilters")) != nil){
            let selectedValue = UserDefaults.standard.value(forKey: "selectedFilters") as! [String: AnyObject]
            
            var searchDataSource = [AnyObject]()
            let searchString = selectedValue["pref_id"] as! String
            let predicate = NSPredicate(format: "self contains %@",searchString)
            
            searchDataSource = selectedValues.filter { predicate.evaluate(with: $0)
            }
            if(searchDataSource.isEmpty)
            {
                selectedValues.append(selectedValue as AnyObject)
            }
            else{
                let indexOfPerson = selectedValues.index{$0 === searchDataSource[0]}
                if let array = selectedValue["values"] as? [String]{
                    if array.isEmpty{
                        selectedValues.remove(at: indexOfPerson!)
                    }
                    else{
                        selectedValues[indexOfPerson!] = selectedValue as AnyObject
                    }
                }
            }
        }
        UserDefaults.standard.removeObject(forKey: "selectedFilters")
        UserDefaults.standard.synchronize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // 1 & 2 type of prefernce (check box)
    // 3 range
    
    func getFilters()
    {
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let params:[String : AnyObject] = ["token":token! as AnyObject]
        Helper.showPI()
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getFilters, params: params as [String : AnyObject]?
            , success:
            
            { (response) in
                
                Helper.hidePI()
                
                let errFlag = response["errCode"].numberValue
                switch errFlag {
                case 0:
                    self.numberOfFilters = response["data"].arrayValue
                    DispatchQueue.main.async {
                        self.filtersTableViewOutlet.reloadData()
                    }
                    break
                default:
                    let errMsg = response["Message"].stringValue
                    break
                }
        }, failure: { (Error) in
            Helper.hidePI()
        })
    }
    
    @IBAction func applyFiltersAction(_ sender: Any)
    {
        let filterData:[String:Any] = ["filters":self.selectedValues]
        UserDefaults.standard.setValue(filterData, forKey: "FiltersData")
        UserDefaults.standard.synchronize()
        if (self.filterDataAppliedDelegate != nil)
        {
            self.filterDataAppliedDelegate.savedData(data: self.selectedValues)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func resetAllAction(_ sender: Any)
    {
        selectedValues.removeAll()
        UserDefaults.standard.removeObject(forKey: "selectedValue")
        UserDefaults.standard.synchronize()
        self.isResetButtonPressed = true
        self.filtersTableViewOutlet.reloadData()
    }
}

extension FiltersViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FiltersTableViewCell") as! FiltersTableViewCell
        
        let dataDict = numberOfFilters[indexPath.row].dictionaryValue
        let prefType = dataDict["typeOfPreference"]?.stringValue
        switch prefType! {
        case "1":
            cell.titleOutlet.text = dataDict["preferenceTitle"]?.stringValue
            cell.detailsOutlet.text = ""
            var searchDataSource = [AnyObject]()
            let searchString = (dataDict["_id"]!.stringValue)
            let predicate = NSPredicate(format: "self contains %@",searchString)
            searchDataSource = self.selectedValues.filter {
                predicate.evaluate(with: $0)
            }
            if (searchDataSource.count > 0){
                let dataArray:[String] = (searchDataSource[0] as! [String : AnyObject])["values"] as! [String]
                if(dataArray.count >= 2){
                    let str = dataArray.joined(separator: ", ")
                    cell.detailsOutlet.text = str
                }
                else{
                    if dataArray.count != 0
                    {
                        cell.detailsOutlet.text = dataArray[0]
                    }
                }
            }
            cell.dataDict = dataDict
            cell.filterButtonActionDelegate = self
            return cell
            
        case "2":
            cell.detailsOutlet.text = ""
            var searchDataSource = [AnyObject]()
            let searchString = (dataDict["_id"]!.stringValue)
            let predicate = NSPredicate(format: "self contains %@",searchString)
            searchDataSource = self.selectedValues.filter {
                predicate.evaluate(with: $0)
            }
            if (searchDataSource.count > 0){
                if let dataDictionary = searchDataSource[0] as? [String : AnyObject]{
                    if let dataArray = dataDictionary["values"] as? [String]{
                        if(dataArray.count >= 2){
                            let str = dataArray.joined(separator: ", ")
                            cell.detailsOutlet.text = str
                        }
                        else if dataArray.count == 0
                        {
                            cell.detailTextLabel?.text = ""
                        }
                        else{
                            cell.detailsOutlet.text = dataArray[0]
                        }
                    }
                    else if let dataArray = dataDictionary["values"] as? [Any]{
                        let values = dataArray[0] as! [String]
                        if(values.count >= 2){
                            let str = values.joined(separator: ", ")
                            cell.detailsOutlet.text = str
                        }
                        else if values.count == 0
                        {
                            cell.detailTextLabel?.text = ""
                        }
                        else{
                            cell.detailsOutlet.text = values[0]
                        }
                    }
                }
            }
            cell.titleOutlet.text = dataDict["preferenceTitle"]?.stringValue
            cell.dataDict = dataDict
            cell.filterButtonActionDelegate = self
            return cell
            
        case "7":
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FiltersSliderTableViewCell") as! FiltersSliderTableViewCell
            cell.fliterSliderDelegate = self
            cell.dataDict = dataDict
            cell.silderRangeOutlet.maximumValue = 20000
            cell.currentValueLabelOutlet.text = "\(20000) miles"
            cell.silderRangeOutlet.value = 20000
            if let name = dataDict["preferenceTitle"]?.string
            {
                cell.titleLabelOutlet.text = name
            }
            
            var searchDataSource = [AnyObject]()
            let searchString = (dataDict["_id"]!.stringValue)
            let predicate = NSPredicate(format: "self contains %@",searchString)
            searchDataSource = self.selectedValues.filter {
                predicate.evaluate(with: $0)
            }
            if (searchDataSource.count > 0){
                let dataArray:[String] = (searchDataSource[0] as! [String : AnyObject])["values"] as! [String]
                if !dataArray.isEmpty
                {
                    cell.currentValueLabelOutlet.text = "\(dataArray[0]) miles"
                    cell.silderRangeOutlet.value = Float(dataArray[0])!
                }
            }
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterRangeTableViewCell") as! FilterRangeTableViewCell
            
            var minimum = Float()
            var maximum = Float()
            if let optionArray = dataDict["optionsValue"]?.arrayValue{
                cell.dataDict = dataDict
                cell.filterRangeDelegates = self
                cell.isResetButtonPressed = self.isResetButtonPressed
                self.isResetButtonPressed = false
                if !optionArray.isEmpty{
                    if let minValue = optionArray[0].string
                    {
                        if let min = Float(minValue){
                            minimum = min
                        }
                    }
                    if let maxValue = optionArray[1].string
                    {
                        if let max = Float(maxValue){
                            maximum = max
                        }
                    }
                    cell.setRangeView(min: minimum, max: maximum)
                }else{
                    cell.setRangeView(min: 6.0, max: 20.0)
                }
            }
            
            var searchDataSource = [AnyObject]()
            let searchString = (dataDict["_id"]!.stringValue)
            let predicate = NSPredicate(format: "self contains %@",searchString)
            searchDataSource = self.selectedValues.filter {
                predicate.evaluate(with: $0)
            }
            if (searchDataSource.count > 0){
                let dataArray:[String] = (searchDataSource[0] as! [String : AnyObject])["values"] as! [String]
                if !dataArray.isEmpty
                {
                    if let minValue = Float(dataArray[0]), let maxValue = Float(dataArray[1]){
                        
                        cell.setRangeView(min: minValue, max:maxValue)
                    }
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfFilters.count
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "filter options segue"
        {
            var searchDataSource = [AnyObject]()
            let data = sender as! [String : JSON]
            let controler = segue.destination as! FilterOptionsViewController
            controler.dataDictionary = data
            let titleOutletText = data["preferenceTitle"]?.stringValue
            controler.titleOutletText = titleOutletText!
            controler.prefID = (data["_id"]?.stringValue)!
            
            let searchString = (data["_id"]!.stringValue)
            let predicate = NSPredicate(format: "self contains %@",searchString)
            searchDataSource = self.selectedValues.filter {
                predicate.evaluate(with: $0)
            }
            if (searchDataSource.count > 0){
                if let dataDictionary = searchDataSource[0] as? [String : AnyObject]{
                    if let dataArray = dataDictionary["values"] as? [String]{
                        controler.selectedValuesArray = dataArray
                    }
                    else if let dataArray = dataDictionary["values"] as? [Any]{
                        let values = dataArray[0] as! [String]
                        controler.selectedValuesArray = values
                    }
                }
            }
        }
    }
}

extension FiltersViewController : filterButtonActionDelegate,filterRangeDelegates, fliterSliderDelegate{
    
    func filterButtonPressed(dataDictionary: [String : JSON]) {
        
        self.performSegue(withIdentifier: "filter options segue", sender: dataDictionary)
    }
    
    func rangeForSliderWithValuesChanged() {
        addValues()
    }
    
    func filterValueChanged(){
        addValues()
    }
}
