//
//  Locale+Names.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 07/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import UIKit

extension NSLocale {
    
    struct Local {
        let countryIDentifier: String
        let countryName: String
    }
    
    static func locales() -> [Local] {
        var locales = [Local]()
        
        for code in NSLocale.isoCountryCodes as [String] {
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            let local = Local(countryIDentifier: id, countryName: name)
            locales.append(local)
        }
        return locales
    }
    
    static func getAllCountryNames() -> [String] {
        
        let path = Bundle.main.path(forResource: "CallingCodes", ofType: "plist")
        let array = NSArray(contentsOfFile: path!) as! [[String: String]]
        
        var temp: [String]! = []
        for dict in array {
            temp.append(dict["name"]!)
        }
        return temp as Array
    }
}
