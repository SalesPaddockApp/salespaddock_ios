//
//  Date + Epoc Time Stamp.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 21/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation
import UIKit

extension Date {
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}
