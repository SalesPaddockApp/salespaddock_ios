//
//  TransitionCustomSegue.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 19/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class TransitionCustomSegue: UIStoryboardSegue
{
    override func perform() {
        scale()
    }
    
    func scale(){
        let toViewController = self.destination
        let fromViewController = self.source
        
        let contanierView = fromViewController.view.superview
        let originalCenter = fromViewController.view.center
        toViewController.view.alpha = 0
        toViewController.view.transform = CGAffineTransform(scaleX: 1, y: 0.3)
        toViewController.view.center = originalCenter
        
        contanierView?.addSubview(toViewController.view)
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            toViewController.view.transform = CGAffineTransform.identity
            toViewController.view.alpha = 1
        }) { success in
            //            fromViewController.performSegue(withIdentifier: "custom", sender: nil)
            fromViewController.present(toViewController, animated: false, completion: nil)
        }
    }
}

class UnwindScaleSegue : UIStoryboardSegue
{
    override func perform() {
        scale()
    }
    
    func scale(){
        let toViewController = self.destination
        let fromViewController = self.source
        
        fromViewController.view.superview?.insertSubview(toViewController.view, at: 0)
        fromViewController.view.alpha = 1
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            fromViewController.view.transform = CGAffineTransform(scaleX: 1, y: 0.3)
            fromViewController.view.alpha = 0
            
        }) { success in
            //            fromViewController.performSegue(withIdentifier: "custom", sender: nil)
            fromViewController.dismiss(animated: false, completion: nil)
        }
    }
}
