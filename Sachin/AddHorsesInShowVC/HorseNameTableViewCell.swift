//
//  HorseNameTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 28/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class HorseNameTableViewCell: UITableViewCell {

    @IBOutlet weak var pendingHorseApprovalLabelOutlet: UILabel!
    @IBOutlet weak var horseImgeOutlet: UIImageView!
    @IBOutlet weak var horseNameOutlet: UILabel!
    @IBOutlet weak var checkBoxOutlet: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
