//
//  AddHorsesInShowViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 28/12/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher

class AddHorsesInShowViewController: UIViewController {
    
    @IBOutlet weak var addHorseShowTableVIewOutlet: UITableView!
    var selectedHorseArray = [String]()
    var horseListArray = [JSON]()
    var showId = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        getPostDetails()
        // Do any additional setup after loading the view.
        
        addHorseShowTableVIewOutlet.estimatedRowHeight = 270
        addHorseShowTableVIewOutlet.rowHeight = UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getPostDetails(){
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let userId = UserDefaults.standard.object(forKey: "user_id") as! String!
        let params:[String : AnyObject] = ["token":token! as AnyObject,"userId":userId! as AnyObject,"showId":showId as AnyObject]
        Helper.showPI()
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getAllMyListedPosts, params: params as [String : AnyObject]?
            , success:
            
            { (response) in
                
                Helper.hidePI()
                
                let errFlag = response["errCode"].numberValue
                switch errFlag {
                case 0:
                    
                    self.horseListArray = response["response"]["result"].arrayValue
                    self.addHorseShowTableVIewOutlet.reloadData()
                    break
                    
                default:
                    let errMsg = response["Message"].string
                    break
                }
        }, failure: { (Error) in
            
            Helper.hidePI()
        })
    }
    
    @IBAction func dismissViewAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddHorsesInShowViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1
        {
            return self.horseListArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HorseNameTableViewCell") as! HorseNameTableViewCell
        if indexPath.section == 1
        {
            let dict = self.horseListArray[indexPath.row].dictionaryValue
            let imageDict = dict["image"]?["options"].arrayValue
            let type = imageDict?[0]["type"].stringValue
            var urlStr = ""
            if type == "0"
            {
                urlStr = (imageDict?[0]["imageUrl"].stringValue)!
            }
            else
            {
                urlStr = (imageDict?[0]["thumbnailUrl"].stringValue)!
            }
            
            cell.horseNameOutlet.text = dict["name"]?.stringValue
            let url = URL(string:urlStr)
            
            if let isPending = dict["isPending"]?.stringValue{
                if isPending == "0"{
                    cell.pendingHorseApprovalLabelOutlet.isHidden = true
                }
                else{
                    cell.pendingHorseApprovalLabelOutlet.isHidden = false
                }
            }
            
            let addedToShow = dict["isAddedOnShow"]?.stringValue
            if addedToShow == "0"
            {
                cell.checkBoxOutlet.image = #imageLiteral(resourceName: "tick_box")
            }
            else{
                cell.checkBoxOutlet.image = #imageLiteral(resourceName: "tick")
            }
            
            cell.horseImgeOutlet.kf.indicatorType = .activity
            cell.horseImgeOutlet.kf.indicator?.startAnimatingView()
            cell.horseImgeOutlet.kf.setImage(with: url!, placeholder: #imageLiteral(resourceName: "horse_default_image"), options:  [.transition(ImageTransition.fade(1))], progressBlock: nil,
                                             completionHandler: { image, error, cacheType, imageURL in
                cell.horseImgeOutlet.kf.indicator?.stopAnimatingView()
            })
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 1
        {
            let dict = self.horseListArray[indexPath.row].dictionaryValue
            let postId = dict["postId"]?.stringValue
            let addedToShow = dict["isAddedOnShow"]?.stringValue
            if addedToShow == "0"
            {
                addHorseToShow(showId: showId, postId: postId!)
            }
            else
            {
                removeHorseFromShow(showId: showId, postId: postId!)
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func addHorseToShow(showId: String, postId:String){
        
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let userId = UserDefaults.standard.object(forKey: "user_id") as! String!
        let params:[String : AnyObject] = ["token":token! as AnyObject,"userId":userId! as AnyObject,"postId": postId as AnyObject,"showId":showId as AnyObject]
        Helper.showPI()
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.addToShow, params: params as [String : AnyObject]?
            , success:
            
            { (response) in
                
                Helper.hidePI()
                
                let errFlag = response["errCode"].numberValue
                switch errFlag {
                case 0:
                    self.getPostDetails()
                    self.showAlert("", message:response["Message"].stringValue)
                    break
                    
                default:
                    let errMsg = response["Message"].string
                    break
                }
        }, failure: { (Error) in
            
            Helper.hidePI()
        })
    }
    
    func removeHorseFromShow(showId: String, postId:String)
    {
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let userId = UserDefaults.standard.object(forKey: "user_id") as! String!
        let params:[String : AnyObject] = ["token":token! as AnyObject,"userId":userId! as AnyObject,"postId": postId as AnyObject,"showId":showId as AnyObject]
        Helper.showPI()
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.deletePostFromShow, params: params as [String : AnyObject]?
            , success:
            
            { (response) in
                
                Helper.hidePI()
                
                let errFlag = response["errCode"].numberValue
                switch errFlag {
                case 0:
                    self.getPostDetails()
                    self.showAlert("", message:response["Message"].stringValue)
                    break
                    
                default:
                    let errMsg = response["Message"].string
                    break
                }
        }, failure: { (Error) in
            
            Helper.hidePI()
        })
    }
    
}
