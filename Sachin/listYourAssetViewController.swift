//
//  listYourAssetViewController.swift
//  salesPaddock
//
//  Created by Rahul Sharma on 12/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher

class listYourAssetViewController: UIViewController ,UITableViewDataSource ,UITableViewDelegate
{
    @IBOutlet weak var tableView: UITableView!
    var numberOfLists = [JSON]()
    //    let ImageList = ["horse_icon","clothes_icon","tack_icon"]
    override func viewDidLoad()
        
    {
        super.viewDidLoad()
        self.title = "List your asset"
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let params:[String : String] = ["token":token!]
        Helper.showPI()
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getProductTypes, params: params as [String : AnyObject]?
            , success:
            { (response) in
                Helper.hidePI()
                let errFlag = response["errCode"].numberValue
                switch errFlag {
                case 0:
                    self.numberOfLists = response["response"]["data"].arrayValue
                    
                    self.tableView.reloadData()
                    break
                default:
                    let errMsg = response["Message"].string
                    break
                }
        }, failure: { (Error) in
            Helper.hidePI()
        })
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return numberOfLists.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.listYourAssetTableViewCell, for: indexPath) as! listYourAssetTableViewCell
        let data = self.numberOfLists[(indexPath as NSIndexPath).row].dictionaryValue
        if let url:String = data["image"]?.string
        {
            let urlNew:String = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let imageUrl = URL(string:urlNew)
            cell.imageLbl.kf.setImage(with: imageUrl)
        }
        cell.nameLbl.text = data["name"]!.stringValue
        return cell
    }
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.performSegue(withIdentifier: "listSelectBreedsegue", sender: indexPath)
        
        let data = self.numberOfLists[(indexPath as NSIndexPath).row].dictionaryValue
        let productType:String = data["name"]!.string!
        UserDefaults.standard.set(productType, forKey: "productType")
        UserDefaults.standard.synchronize()
        
        tableView .deselectRow(at: indexPath, animated: true)
    }
    @IBAction func backBtnClicked(_ sender: AnyObject) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "listSelectBreedsegue"{
            let indexpath = sender as! IndexPath
            let listBreed = segue.destination as! listSelectBreedViewController
            let data = self.numberOfLists[(indexpath as NSIndexPath).row].dictionaryObject
            listBreed.SelectedAsset = data as! [String : String]
        }
    }
}
