//
//  WishlistHorseTableViewCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 03/03/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class WishlistHorseTableViewCell: UITableViewCell {

    @IBOutlet weak var horseImageOutlet: UIImageView!
    @IBOutlet weak var horseTypeOutlet: UILabel!
    @IBOutlet weak var horseNameOutelt: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
