//
//  WishListViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 03/03/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class WishListViewController: UIViewController {
    
    let horseDataObj = HorseData(postPrice: "", postID: "", postName: "", postImageUrl: "", userImage: "", userId: "", ownerName: "")
    var horseDataArray = [HorseData]()
    @IBOutlet weak var tableViewOutlet: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableViewOutlet.estimatedRowHeight = 270
        self.tableViewOutlet.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getWishLists()
    }
    
    @IBAction func backButtonAction(_ sender: Any)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func getWishLists() {
        Helper.showPI()
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject]
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getWishList,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    switch errFlag {
                                        
                                    case 0:
                                        if let responseArray = (response["response"]["result"]).arrayObject
                                        {
                                            self.horseDataArray = self.horseDataObj.fetchDataFromAPIForPost(postData: responseArray)
                                        }
                                        self.tableViewOutlet.reloadData()
                                        break
                                        
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Error", message: errMsg!)
                                        
                                        break
                                    }
        },
                                 failure: { (Error) in
                                    Helper.hidePI()
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
}

extension WishListViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WishlistHorseTableViewCell", for: indexPath) as! WishlistHorseTableViewCell
        
        let postData = horseDataArray[indexPath.row]
        cell.horseImageOutlet.kf.indicatorType = .activity
        cell.horseImageOutlet.kf.indicator?.startAnimatingView()
        
        cell.horseImageOutlet.kf.setImage(with: postData.postImageUrl!,
                                          placeholder: UIImage.init(named: "horse_default_image"),
                                          options: [.transition(ImageTransition.fade(1))],
                                          progressBlock: { receivedSize, totalSize in
        },
                                          completionHandler: { image, error, cacheType, imageURL in
                                            cell.horseImageOutlet.kf.indicator?.stopAnimatingView()
        })
        cell.horseNameOutelt.text = postData.postName!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return horseDataArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "horse Details Segue", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "horse Details Segue"
        {
            let controller = segue.destination as! HorseDetailViewController
            if let rowNum = sender as? Int{
                let horseDataObj = self.horseDataArray[rowNum]
                controller.postId = horseDataObj.postID!
            }
        }
    }
}
