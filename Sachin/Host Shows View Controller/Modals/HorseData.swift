//
//  HorseData.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 25/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class HorseData: NSObject {
    
    var postPrice : String?
    var postImageUrl : URL?
    var postID : String?
    var postName :String?
    var userImage : URL?
    var userId : String?
    var ownerName : Any?
    
    init(postPrice : String, postID : String, postName : String, postImageUrl : String, userImage : String, userId : String, ownerName : Any) {
        self.postPrice = postPrice
        self.postImageUrl = URL(string:postImageUrl)
        self.postID = postID
        self.postName = postName
        if let url = URL(string:userImage){
            self.userImage = url
        }
        self.ownerName = ownerName
        self.userId = userId
    }
    
    /// It will fetch data from response object and convert it to the HorseData Object.
    ///
    /// - Parameter postData: response data
    /// - Returns: array of HorseData Object
    
    func fetchDataFromAPIForPost(postData : [Any]) -> [HorseData]{
        var postDataArray = [HorseData]()
        for postDetails in postData
        {
            if let postDict = postDetails as? [String: Any]{
                var postPrice = ""
                var id = ""
                var postTempName = ""
                var imageUrl = ""
                var userImage = ""
                var userIda = ""
                var postObject : HorseData
                if let priceDetails = postDict["priceDetails"] as? String{
                    postPrice = priceDetails
                }
                if let postId = postDict["postId"] as? String{
                    id = postId
                }
                if let userPicUrl = postDict["userImage"] as? String{
                    userImage = userPicUrl
                }
                if let name = postDict["name"] as? String{
                    postTempName = name
                }
                if let userIdentity = postDict["userId"] as? String{
                    userIda = userIdentity
                }
                if let imageArray = postDict["image"] as? [Any]
                {
                    if !imageArray.isEmpty{
                        if let imageObj = imageArray[0] as? [String:String]{
                            if imageObj["type"] == "0"{
                                imageUrl = imageObj["imageUrl"]!
                            }
                            else{
                                imageUrl = imageObj["thumbnailUrl"]!
                            }
                        }
                    }
                }
                
                 if (postDict["userName"] != nil)
                 {
                 postObject = HorseData(postPrice: postPrice, postID: id, postName: postTempName, postImageUrl: "", userImage : userImage, userId: userIda, ownerName: postDict["userName"]! )
                }
                else
                 {
                    postObject = HorseData(postPrice: postPrice, postID: id, postName: postTempName, postImageUrl: "", userImage : userImage, userId: userIda, ownerName: [])
                }
                postObject.postImageUrl = URL(string:imageUrl)
                postDataArray.append(postObject)
            }
        }
        return postDataArray
    }
}
