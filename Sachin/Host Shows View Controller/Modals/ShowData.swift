//
//  ShowData.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 25/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ShowData: NSObject {
    
    var showId : String?
    var showImage : URL?
    var showName : String?
    var showEndDate : String?
    var showStartDate : String?
    var showVenue : String?
    var isUpcomingShow : Bool
    var showDescription : String?
    
    init(showId : String, showName : String,isUpcomingShow : Bool, showImage : String, showEndDate : String, showStartDate : String, showVenue : String, showDescription : String) {
        self.showId = showId
        self.showImage = URL(string:showImage)
        self.showName = showName
        self.showEndDate = showEndDate
        self.showStartDate = showStartDate
        self.showVenue = showVenue
        self.showDescription = showDescription
        self.isUpcomingShow = isUpcomingShow
    }
    
    func fetchDataFromAPIForShowDetails(showData : [Any]) -> [ShowData]{
        
        var showDataArray = [ShowData]()
        for showDetails in showData
        {
            if let showDict = showDetails as? [String: Any]{
                
                if let showName = showDict["showName"] as? String,
                    let showId = showDict["showId"] as? String,
                    let image = showDict["image"] as? String,
                    let startDate = showDict["startDate"] as? String,
                    let endDate = showDict["endDate"] as? String,
                    let showVenue = showDict["venue"] as? String,
                    let isUpcmingShow = showDict["isUpcommingShow"] as? Bool,
                    let showDesc = showDict["Description"] as? String{
                    let showObj = ShowData(showId: showId, showName: showName, isUpcomingShow: isUpcmingShow, showImage: image, showEndDate: endDate, showStartDate: startDate, showVenue: showVenue, showDescription: showDesc)
                    showDataArray.append(showObj)
                }
            }
        }
        return showDataArray
    }
}
