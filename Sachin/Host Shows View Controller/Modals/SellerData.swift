//
//  SellerData.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 08/08/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SellerData: NSObject {

    var username : String?
    var profilePic : String?
    var userID  :String?
    var location : String?
    
    class func fetchDataFromAPIForShowDetails(SellerDataArray : [Any]) -> [SellerData]? {
        
        var sellerDataArray : [SellerData] = []
        for sellerDetails in SellerDataArray
        {
            let sellerData = SellerData()
            if let sellerDict = sellerDetails as? [String: Any]{
                if let sellerID = sellerDict["userId"] as? String {
                    sellerData.userID = sellerID
                }
                if let location = sellerDict["location"] as? String {
                    sellerData.location = location
                }
                if let name = sellerDict["name"] as? [String:Any] {
                    guard let fname = name["fName"] as? String, let lName = name["lName"] as? String else { return nil}
                    let nameStr = "\(fname) \(lName)"
                    sellerData.username = nameStr
                }
                if let profilePic = sellerDict["profilePic"] as? String {
                    sellerData.profilePic = profilePic
                }
            }
            sellerDataArray.append(sellerData)
        }
        return sellerDataArray
    }
}
