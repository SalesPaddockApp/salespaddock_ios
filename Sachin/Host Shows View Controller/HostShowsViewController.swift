//
//  HostShowsViewController.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 15/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//Show Detail Segue
//

import UIKit
import SwiftyJSON
import Kingfisher

class HostShowsViewController: UIViewController {
    
    @IBOutlet weak var filterViewOutlet: UIView!
    @IBOutlet weak var scrollViewOutlet: UIScrollView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var numberOfFiltersOutlet: UILabel!
    @IBOutlet weak var aroundViewOutlet: AroundYouView!
    @IBOutlet weak var pastShowsViewOutlet: PastShowsView!
    @IBOutlet weak var activeShowsViewOutlet: ActiveShowsView!
    @IBOutlet weak var headerViewOutlet: HeaderView!
    var filterData = [AnyObject]()
    var currentPageIndex = 0
    var horseDataObj = HorseData(postPrice: "", postID: "", postName: "", postImageUrl: "", userImage: "", userId: "", ownerName: "")
    var showObj = ShowData(showId: "", showName: "", isUpcomingShow: false, showImage: "", showEndDate: "", showStartDate: "", showVenue: "", showDescription: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let locationManagerObj = LocationCoordinateManager.sharedInstance
        _ = locationManagerObj.checkLocationStatus()
        
        aroundViewOutlet.hostShowViewControllerObj = self
        pastShowsViewOutlet.hostShowViewControllerObj = self
        activeShowsViewOutlet.hostShowViewControllerObj = self
        
        if (UserDefaults.standard.object(forKey:"FiltersData") as? [String:Any]) == nil{
            self.getShowDataForBuyer(isFilteredApplied: 0, data: filterData)
        }
        else
        {
            if self.filterData.count>0 {
                self.aroundViewOutlet.currentPageNumber = 0
                self.aroundViewOutlet.aroundYouHorseData.removeAll()
                self.aroundViewOutlet.AroundYouTableView.reloadData()
                self.getShowDataForBuyer(isFilteredApplied: 1, data: filterData)
            }
            else if let userData = UserDefaults.standard.object(forKey:"FiltersData") as? [String:Any]{
                self.filterData = userData["filters"] as! [AnyObject]
                self.getShowDataForBuyer(isFilteredApplied: 1, data: filterData)
            }
            else {
                self.getShowDataForBuyer(isFilteredApplied: 0, data: filterData)
            }
            filterData.removeAll()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func aroundYouButtonAction(_ sender: Any) {
        headerViewOutlet.aroundYouButtonPressed()
        self.scroll(toPage: 0, animated: true)
        self.filterViewOutlet.isHidden = false
    }
    
    @IBAction func activeShowsAction(_ sender: Any) {
        headerViewOutlet.activeShowsButtonPressed()
        self.scroll(toPage: 1, animated: true)
        self.filterViewOutlet.isHidden = false
    }
    
    @IBAction func pastShowsAction(_ sender: Any) { //now its Sellers
        headerViewOutlet.pastShowButtonPressed()
        self.scroll(toPage: 2, animated: true)
        self.filterViewOutlet.isHidden = true
    }
    @IBAction func filterButtonAction(_ sender: Any) {
        self.performSegue(withIdentifier: "Open Filters Segue", sender: filterData)
    }
    
    @IBAction func mapButtonAction(_ sender: UIButton) {
//        print(currentPageIndex)
        self.performSegue(withIdentifier: "mapSegue", sender: currentPageIndex)
    }
    
    func getShowDataForBuyer(isFilteredApplied: Int, data: [AnyObject]){
        
        var params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject]
        Helper.showPI()
        if isFilteredApplied == 1 && data.count>0
        {
            params["filters"] = data as AnyObject
        }
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getShowForUsers,
                                 params: params,
                                 success: { (response) in
                                    
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                    case 0:
                                        if let responseData = response["response"].dictionaryObject{
                                            if let aroundYou = responseData["Around You"] as? [String :Any]{
                                                self.filterData.removeAll()
                                                if let filterArray = aroundYou["filters"] as? [Any]{
                                                    
                                                    if filterArray.count>0{
                                                        self.numberOfFiltersOutlet.text = "\(filterArray.count)"
                                                        self.colorView.isHidden = false
                                                        self.colorView.backgroundColor = Color.init(hexString: "#0bb39dff")
                                                        let filteredData:[String:Any] = ["filters":filterArray]
                                                        UserDefaults.standard.setValue(filteredData, forKey: "FiltersData")
                                                        self.filterData = filterArray as [AnyObject]
                                                        UserDefaults.standard.synchronize()
                                                    }
                                                    else{
                                                        self.colorView.backgroundColor = Color.clear
                                                        self.numberOfFiltersOutlet.text = ""
                                                        self.colorView.isHidden = true
                                                        UserDefaults.standard.setValue("", forKey: "FiltersData")
                                                        self.filterData.removeAll()
                                                    }
                                                }
                                                else if let filterDataObj = aroundYou["filters"] as? [String : Any]
                                                {
                                                    self.numberOfFiltersOutlet.text = "\(1)"
                                                    self.colorView.isHidden = false
                                                    self.colorView.backgroundColor = Color.init(hexString: "#0bb39dff")
                                                    let filteredData:[String:Any] = ["filters":[filterDataObj]]
                                                    UserDefaults.standard.setValue(filteredData, forKey: "FiltersData")
                                                    self.filterData = [filterDataObj as AnyObject] as [AnyObject]
                                                    UserDefaults.standard.synchronize()
                                                }
                                                else{
                                                    self.colorView.backgroundColor = Color.clear
                                                    self.numberOfFiltersOutlet.text = ""
                                                    self.colorView.isHidden = true
                                                    UserDefaults.standard.setValue("", forKey: "FiltersData")
                                                    self.filterData.removeAll()
                                                }
                                                
                                                self.aroundViewOutlet.aroundYouHorseData.removeAll()
                                                if let aroundYouData = aroundYou["response"] as? [Any]{
                                                    self.aroundViewOutlet.aroundYouHorseData = self.horseDataObj.fetchDataFromAPIForPost(postData: aroundYouData)
                                                    self.aroundViewOutlet.noHorseFoundLabelOutlet.isHidden = true
                                                }else{
                                                    self.aroundViewOutlet.noHorseFoundLabelOutlet.isHidden = false
                                                }
                                                DispatchQueue.main.async {
                                                    self.aroundViewOutlet.AroundYouTableView.reloadData()
                                                }
                                            }
                                            if let activeShowsData = responseData["Active Shows"] as? [Any]{
                                                self.activeShowsViewOutlet.activeShowData = self.showObj.fetchDataFromAPIForShowDetails(showData: activeShowsData)
                                                DispatchQueue.main.async {
                                                    self.activeShowsViewOutlet.activeShowsTableView.reloadData()
                                                }
                                            }
                                            if let sellerData = responseData["Seller"] as? [Any]{
                                                if let sellerData = SellerData.fetchDataFromAPIForShowDetails(SellerDataArray: sellerData) {
                                                    self.pastShowsViewOutlet.sellerData = sellerData
                                                }
                                                DispatchQueue.main.async {
                                                    self.pastShowsViewOutlet.pastShowsTableView.reloadData()
                                                }
                                            }
                                        }
                                        break
                                    default:
                                        
                                        let errMsg = response["Message"].string
                                        self.showAlert("Error", message: errMsg!)
                                        if let filterCount = response["countOfFilters"].int{
                                            if filterCount>0{
                                                self.numberOfFiltersOutlet.text = "\(filterCount)"
                                                self.colorView.isHidden = false
                                                self.colorView.backgroundColor = Color.init(hexString: "#0bb39dff")
                                                let filterData:[String:Any] = ["filters":response["filters"].arrayObject!]
                                                UserDefaults.standard.setValue(filterData, forKey: "FiltersData")
                                                self.filterData = response["filters"].arrayObject! as [AnyObject]
                                            }
                                            else{
                                                self.colorView.backgroundColor = Color.clear
                                                self.numberOfFiltersOutlet.text = ""
                                                self.colorView.isHidden = true
                                                UserDefaults.standard.setValue("", forKey: "FiltersData")
                                                self.filterData.removeAll()
                                            }
                                            UserDefaults.standard.synchronize()
                                            self.aroundViewOutlet.aroundYouHorseData.removeAll()
                                            DispatchQueue.main.async {
                                                self.aroundViewOutlet.AroundYouTableView.reloadData()
                                            }
                                            break
                                        }
                                    }},
                                 failure: { (Error) in
                                    Helper.hidePI()
                                    self.showAlert("Error", message: Error.localizedDescription)
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let identifier = segue.identifier!
        switch identifier {
        case "Open Horse Detail Segue":
            let controller = segue.destination as! HorseDetailViewController
            if let rowNum = sender as? Int{
                let horseDataObj = self.aroundViewOutlet.aroundYouHorseData[rowNum]
                controller.postId = horseDataObj.postID!
            }
            break;
            
        case "sellerProfileSegue":
            if let rowNum = sender as? Int {
                let sellerData = self.pastShowsViewOutlet.sellerData[rowNum]
                let controller = segue.destination as! ProfilePageVC
                controller.isMyProfile = "0"
                if let userId = sellerData.userID {
                    controller.userId = userId
                }
            }
            break;
            
        case "Active Show Detail Segue":
            if let rowNum = sender as? Int{
                let SDVC = segue.destination as! ShowDetailVC
                let showDataObj = self.activeShowsViewOutlet.activeShowData[rowNum]
                var postDict = [String:AnyObject]()
                postDict["showId"] = showDataObj.showId as AnyObject
                postDict["showName"] = showDataObj.showName as AnyObject
                postDict["endDate"] = showDataObj.showEndDate as AnyObject
                postDict["startDate"] = showDataObj.showStartDate as AnyObject
                postDict["venue"] = showDataObj.showVenue as AnyObject
                postDict["image"] = showDataObj.showImage as AnyObject
                postDict["Description"] = showDataObj.showDescription as AnyObject
                SDVC.postDictionary = postDict
            }
            break;
            
        case "Open Filters Segue":
            let navController = segue.destination as! UINavigationController
            let controller =  navController.childViewControllers.first as! FiltersViewController
            controller.filterDataAppliedDelegate = self
            if let data = sender as? [AnyObject]{
                controller.selectedValues = data
            }
            break
            
        case "open profile Segue":
            let controller = segue.destination as! ProfilePageVC
            controller.isMyProfile = "0"
            if let userId = sender as? String {
                controller.userId = userId
            }
            break
            
        case "mapSegue":
            let navController = segue.destination as! UINavigationController
            let controller = navController.childViewControllers[0] as! BuyerMapViewController
            controller.currentPage = self.currentPageIndex
            break
            
        default :
            break
        }
    }
}

extension HostShowsViewController : HorseCellClickedDelegate, ActiveShowCellClickedDelegate, PastShowCellClickedDelegate,UITabBarControllerDelegate{
    
    func cellClickedAtIndex(row: Int) {
        self.performSegue(withIdentifier: "Open Horse Detail Segue", sender: row)
    }
    
    func pastShowClickedAtIndex(row: Int) {
        self.performSegue(withIdentifier: "sellerProfileSegue", sender: row)
    }
    
    func activeShowClickedAtIndex(row: Int) {
        self.performSegue(withIdentifier: "Active Show Detail Segue", sender: row)
    }
    
    func profileButtonClickedwith(userId: String) {
        self.performSegue(withIdentifier: "open profile Segue", sender: userId)
    }
}

extension HostShowsViewController : filterDataAppliedDelegate {
    
    func savedData(data: [AnyObject]) {
        if let filterValue = UserDefaults.standard.object(forKey:"FiltersData") as? [String:Any]{
            filterData = filterValue["filters"] as! [AnyObject]
            self.numberOfFiltersOutlet.text = "\(filterData.count)"
            self.getShowDataForBuyer(isFilteredApplied: 1, data: filterData)
            self.aroundViewOutlet.currentPageNumber = 0
            self.aroundViewOutlet.aroundYouHorseData.removeAll()
            self.aroundViewOutlet.AroundYouTableView.reloadData()
        }
        else{
            if self.filterData.count>0 {
                self.aroundViewOutlet.currentPageNumber = 0
                self.aroundViewOutlet.aroundYouHorseData.removeAll()
                self.aroundViewOutlet.AroundYouTableView.reloadData()
                self.getShowDataForBuyer(isFilteredApplied: 0, data: filterData)
            }
            filterData.removeAll()
        }
    }
}

extension HostShowsViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNo = Int(round(self.scrollViewOutlet.contentOffset.x / self.scrollViewOutlet.frame.size.width))
        self.currentPageIndex = pageNo
        switch pageNo {
        case 0:
            headerViewOutlet.aroundYouButtonPressed()
            self.filterViewOutlet.isHidden = false
        case 1:
            headerViewOutlet.activeShowsButtonPressed()
            self.filterViewOutlet.isHidden = false
        case 2:
            headerViewOutlet.pastShowButtonPressed()
            self.filterViewOutlet.isHidden = true
        default:
            headerViewOutlet.aroundYouButtonPressed()
            self.filterViewOutlet.isHidden = false
        }
    }
    
    func scroll(toPage page: Int, animated: Bool) {
        var frame: CGRect = self.scrollViewOutlet.frame
        frame.origin.x = frame.size.width * CGFloat(page)
        self.currentPageIndex = page
        frame.origin.y = 0;
        self.scrollViewOutlet.scrollRectToVisible(frame, animated: animated)
    }
}
