//
//  PastShowsView.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 25/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

protocol PastShowCellClickedDelegate {
    func pastShowClickedAtIndex(row : Int)
}

class PastShowsView: UIView {
    
    let spinner = UIActivityIndicatorView(activityIndicatorStyle :.gray)
    
    var currentPageNumber  = Int()
    var PastShowCellClickedDelegate:PastShowCellClickedDelegate?
    var hostShowViewControllerObj:HostShowsViewController!
    {
        didSet{
            self.PastShowCellClickedDelegate = hostShowViewControllerObj
        }
    }
    var sellerData = [SellerData]()
    var filterData = [AnyObject]()
    
    @IBOutlet weak var pastShowsTableView: UITableView!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        let spinner = UIActivityIndicatorView(activityIndicatorStyle :.gray)
        refreshControl.addTarget(self, action: #selector(PastShowsView.handleRefresh(refreshControl:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //        self.pastShowsTableView.estimatedRowHeight = 270
        //        self.pastShowsTableView.rowHeight = UITableViewAutomaticDimension
        self.pastShowsTableView.addSubview(self.refreshControl)
        self.spinner.frame = CGRect(x: 0, y: 0, width: self.pastShowsTableView.frame.width, height: 60)
        self.pastShowsTableView.tableFooterView = self.spinner
        if let filterValue = UserDefaults.standard.object(forKey:"FiltersData") as? [String:Any]{
            filterData = filterValue["filters"] as! [AnyObject]
        }
        else
        {
            filterData.removeAll()
        }
    }
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        if let filterValue = UserDefaults.standard.object(forKey:"FiltersData") as? [String:Any]{
            filterData = filterValue["filters"] as! [AnyObject]
            //            self.numberOfFiltersOutlet.text = "\(filterData.count)"
            self.getUpcomingShows(currentIndex:0, isFilteredApplied: 1, data: filterData)
            self.currentPageNumber = 0
        }
        else{
            filterData.removeAll()
            self.getUpcomingShows(currentIndex:0, isFilteredApplied: 0, data: filterData)
            self.currentPageNumber = 0
        }
    }
    
    func getUpcomingShows(currentIndex : Int, isFilteredApplied: Int, data: [AnyObject]){
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject,
                      "pageNum": "\(currentPageNumber)" as AnyObject]
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getSellers,
                                 params: params,
                                 success: { (response) in
                                    
                                    self.refreshControl.endRefreshing()
                                    self.spinner.stopAnimating()
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                    case 0:
                                        if let responseData = response["response"].arrayObject{
                                            if currentIndex == 0{
                                                self.sellerData.removeAll()
                                            }
                                            if let sellerArray = SellerData.fetchDataFromAPIForShowDetails(SellerDataArray: responseData) {
                                                self.sellerData.append(contentsOf: sellerArray)
                                                self.pastShowsTableView.reloadData()
                                            }
                                            if let filterCount = response["countOfFilters"].int{
                                                if filterCount>0{
                                                    let filterData:[String:Any] = ["filters":response["filters"].arrayObject!]
                                                    UserDefaults.standard.setValue(filterData, forKey: "FiltersData")
                                                    self.filterData = response["filters"].arrayObject! as [AnyObject]
                                                }
                                                else{
                                                    UserDefaults.standard.setValue("", forKey: "FiltersData")
                                                }
                                                UserDefaults.standard.synchronize()
                                            }
                                        }
                                        break
                                    default:
                                        if let filterCount = response["countOfFilters"].int{
                                            if filterCount>0{
                                                let filterData:[String:Any] = ["filters":response["filters"].arrayObject!]
                                                UserDefaults.standard.setValue(filterData, forKey: "FiltersData")
                                                self.filterData = response["filters"].arrayObject! as [AnyObject]
                                            }
                                            else{
                                                UserDefaults.standard.setValue("", forKey: "FiltersData")
                                            }
                                            UserDefaults.standard.synchronize()
                                            self.sellerData.removeAll()
                                            DispatchQueue.main.async {
                                                self.pastShowsTableView.reloadData()
                                            }
                                            
                                            let errMsg = response["Message"].string
                                            self.window?.rootViewController?.navigationController?.visibleViewController?.showAlert("Error", message: errMsg!)
                                            break
                                        }}},
                                 failure: { (Error) in
                                    Helper.hidePI()
                                    self.refreshControl.endRefreshing()
                                    self.spinner.stopAnimating()
                                    self.window?.rootViewController?.navigationController?.visibleViewController?.showAlert("Error", message: Error.localizedDescription)
        })
    }
}

extension PastShowsView : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PastShowsCell", for: indexPath) as! PastShowsCell
        let sellerDetails = sellerData[indexPath.row]
        cell.userImageOutlet.image = #imageLiteral(resourceName: "profile_defalt_image")
        cell.userImageOutlet.kf.indicatorType = .activity
        cell.userImageOutlet.kf.indicator?.startAnimatingView()
        if let name = sellerDetails.username {
            cell.userNameLabelOutlet.text = name
        }
        else {
            cell.userNameLabelOutlet.text = ""
        }
        if let location = sellerDetails.location {
            cell.userLocationOutlet.text = location
        }
        else {
            cell.userLocationOutlet.text = ""
        }
        guard let profilePic = sellerDetails.profilePic else { return UITableViewCell() }
        cell.userImageOutlet.kf.setImage(with: URL(string :profilePic),
                                         placeholder: #imageLiteral(resourceName: "profile_defalt_image"),
                                         options: [.transition(ImageTransition.fade(1))],
                                         progressBlock: { receivedSize, totalSize in
        },
                                         completionHandler: { image, error, cacheType, imageURL in
                                            cell.userImageOutlet.kf.indicator?.stopAnimatingView()
        })
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sellerData.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if (self.PastShowCellClickedDelegate != nil){
            self.PastShowCellClickedDelegate?.pastShowClickedAtIndex(row: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == self.sellerData.count - 1) {
            self.spinner.startAnimating()
            currentPageNumber = currentPageNumber+1
            if let filterValue = UserDefaults.standard.object(forKey:"FiltersData") as? [String:Any]{
                filterData = filterValue["filters"] as! [AnyObject]
                self.getUpcomingShows(currentIndex:currentPageNumber, isFilteredApplied: 1, data: filterData)
            }
            else{
                filterData.removeAll()
                self.getUpcomingShows(currentIndex:currentPageNumber, isFilteredApplied: 0, data: filterData)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
}
