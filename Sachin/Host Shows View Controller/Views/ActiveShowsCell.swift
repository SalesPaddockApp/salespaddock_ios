//
//  ActiveShowsCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 25/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ActiveShowsCell: UITableViewCell {
    
    @IBOutlet weak var showNameOutlet: UILabel!
    @IBOutlet weak var upcomingViewOutlet: UIView!
    @IBOutlet weak var showImageOutlet: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
