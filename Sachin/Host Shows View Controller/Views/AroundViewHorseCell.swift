//
//  AroundViewHorseCell.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 25/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class AroundViewHorseCell: UITableViewCell {
    
    @IBOutlet weak var profilePicButtonOutlet: UIButton!
    @IBOutlet weak var profilePicImageViewOutlet: UIImageView!
    @IBOutlet weak var postNameOutlet: UILabel!
    @IBOutlet weak var postImageOutlet: UIImageView!
    var horseData : HorseData!
    {
        didSet{
            self.setEssentialValues()
        }
    }
    
    func setEssentialValues()
    {
        if let postName = horseData.postName{
            self.postNameOutlet.text = postName
        }
        if let postImageUrl = horseData.postImageUrl
        {
            self.postImageOutlet.kf.indicatorType = .activity
            self.postImageOutlet.kf.indicator?.startAnimatingView()
            self.postImageOutlet.kf.setImage(with: postImageUrl, placeholder: #imageLiteral(resourceName: "horse_default_image"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                self.postImageOutlet.kf.indicator?.stopAnimatingView()
            })
            
        }
        else {
            self.postImageOutlet.image = #imageLiteral(resourceName: "horse_default_image")
        }
        if let imageUrl =  horseData.userImage{
            self.profilePicImageViewOutlet.kf.indicatorType = .activity
            self.profilePicImageViewOutlet.kf.indicator?.startAnimatingView()
            self.profilePicImageViewOutlet.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "horse_default_image"), options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                self.profilePicImageViewOutlet.kf.indicator?.stopAnimatingView()
            })
        }
        else
        {
            self.profilePicImageViewOutlet.image = #imageLiteral(resourceName: "profile_defalt_image")
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.setEssentialValues()
    }
    
    var userId = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
