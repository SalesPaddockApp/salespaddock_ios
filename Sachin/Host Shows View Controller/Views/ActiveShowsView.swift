//
//  ActiveShowsView.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 25/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

protocol ActiveShowCellClickedDelegate {
    func activeShowClickedAtIndex(row : Int)
}

class ActiveShowsView: UIView {
    
    let showsObj = ShowData(showId: "", showName: "",isUpcomingShow: false, showImage: "", showEndDate: "", showStartDate: "", showVenue: "", showDescription: "")
    let spinner = UIActivityIndicatorView(activityIndicatorStyle :.gray)

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ActiveShowsView.handleRefresh(refreshControl:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()

    var currentPageNumber  = Int()
    var ActiveShowCellClickedDelegate:ActiveShowCellClickedDelegate?
    var activeShowData = [ShowData]()
    var filterData = [AnyObject]()
    var hostShowViewControllerObj:HostShowsViewController!{
        didSet{
            self.ActiveShowCellClickedDelegate = hostShowViewControllerObj
        }
    }
    
    @IBOutlet weak var activeShowsTableView: UITableView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.activeShowsTableView.estimatedRowHeight = 270
        self.activeShowsTableView.rowHeight = UITableViewAutomaticDimension
        
        self.activeShowsTableView.addSubview(self.refreshControl)
        self.spinner.frame = CGRect(x: 0, y: 0, width: self.activeShowsTableView.frame.width, height: 60)
        self.activeShowsTableView.tableFooterView = self.spinner;
        
        if let filterValue = UserDefaults.standard.object(forKey:"FiltersData") as? [String:Any]{
            filterData = filterValue["filters"] as! [AnyObject]
        }
        else
        {
            filterData.removeAll()
        }
    }
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        if let filterValue = UserDefaults.standard.object(forKey:"FiltersData") as? [String:Any]{
            filterData = filterValue["filters"] as! [AnyObject]
            //            self.numberOfFiltersOutlet.text = "\(filterData.count)"
            self.getActiveShows(currentIndex:0, isFilteredApplied: 1, data: filterData)
            self.currentPageNumber = 0
        }
        else{
            filterData.removeAll()
            self.getActiveShows(currentIndex:0, isFilteredApplied: 0, data: filterData)
            self.currentPageNumber = 0
        }
    }
    
    func getActiveShows(currentIndex : Int, isFilteredApplied: Int, data: [AnyObject]){
        
        let params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject,
                      "pageNum": "\(currentIndex)" as AnyObject]
        //        Helper.showPI()
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.activeShows,
                                 params: params,
                                 success: { (response) in
                                    
                                    self.refreshControl.endRefreshing()
                                    self.spinner.stopAnimating()
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                    case 0:
                                        
                                        if let responseData = response["response"].arrayObject{
                                            if currentIndex == 0{
                                                self.activeShowData.removeAll()
                                            }
                                            let showArray = self.showsObj.fetchDataFromAPIForShowDetails(showData: responseData)
                                            self.activeShowData.append(contentsOf: showArray)
                                            self.activeShowsTableView.reloadData()
                                            if let filterCount = response["countOfFilters"].int{
                                                if filterCount>0{
                                                    let filterData:[String:Any] = ["filters":response["filters"].arrayObject!]
                                                    UserDefaults.standard.setValue(filterData, forKey: "FiltersData")
                                                    self.filterData = response["filters"].arrayObject! as [AnyObject]
                                                }
                                                else{
                                                    UserDefaults.standard.setValue("", forKey: "FiltersData")
                                                }
                                                UserDefaults.standard.synchronize()
                                            }
                                        }
                                        
                                        break
                                        
                                    default:
                                        if let filterCount = response["countOfFilters"].int{
                                            if filterCount>0{
                                                let filterData:[String:Any] = ["filters":response["filters"].arrayObject!]
                                                UserDefaults.standard.setValue(filterData, forKey: "FiltersData")
                                                self.filterData = response["filters"].arrayObject! as [AnyObject]
                                            }
                                            else{
                                                UserDefaults.standard.setValue("", forKey: "FiltersData")
                                            }
                                            UserDefaults.standard.synchronize()
                                            self.activeShowData.removeAll()
                                            DispatchQueue.main.async {
                                                self.activeShowsTableView.reloadData()
                                            }
                                            let errMsg = response["Message"].string
                                            self.window?.rootViewController?.navigationController?.visibleViewController?.showAlert("Error", message: errMsg!)
                                            break
                                        }}},
                                 failure: { (Error) in
                                    self.refreshControl.endRefreshing()
                                    self.spinner.stopAnimating()
                                    Helper.hidePI()
                                    self.window?.rootViewController?.navigationController?.visibleViewController?.showAlert("Error", message: Error.localizedDescription)
        })
    }
}

extension ActiveShowsView : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ActiveShowsCell", for: indexPath) as! ActiveShowsCell
        
        cell.upcomingViewOutlet.isHidden = true
        let showData = activeShowData[indexPath.row]
        cell.showImageOutlet.kf.indicatorType = .activity
        cell.showImageOutlet.kf.indicator?.startAnimatingView()
        cell.showImageOutlet.kf.setImage(with: showData.showImage!,
                                         placeholder: UIImage.init(named: "horse_default_image"),
                                         options: [.transition(ImageTransition.fade(1))],
                                         progressBlock: { receivedSize, totalSize in
        },
                                         completionHandler: { image, error, cacheType, imageURL in
                                            cell.showImageOutlet.kf.indicator?.stopAnimatingView()
        })
        cell.showNameOutlet.text = showData.showName!
        cell.upcomingViewOutlet.isHidden = !showData.isUpcomingShow
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activeShowData.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if (self.ActiveShowCellClickedDelegate != nil){
            self.ActiveShowCellClickedDelegate?.activeShowClickedAtIndex(row: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == self.activeShowData.count - 1) {
            self.spinner.startAnimating()
            currentPageNumber = currentPageNumber+1
            if let filterValue = UserDefaults.standard.object(forKey:"FiltersData") as? [String:Any]{
                filterData = filterValue["filters"] as! [AnyObject]
                self.getActiveShows(currentIndex:currentPageNumber, isFilteredApplied: 1, data: filterData)
            }
            else{
                filterData.removeAll()
                self.getActiveShows(currentIndex:currentPageNumber, isFilteredApplied: 0, data: filterData)
            }
        }
    }
}
