//
//  HeaderView.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 25/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

protocol ShowsButtonPresedDelegate {
    func showsButtonPresedWithIndex(index :Int)
}

class HeaderView: UIView {
    var ShowsButtonPresedDelegate : ShowsButtonPresedDelegate? = nil
    @IBOutlet weak var pastShowsViewOutlet: UIView!
    @IBOutlet weak var activeShowsOutlet: UIView!
    @IBOutlet weak var aroundYouViewOutlet: UIView!
    @IBOutlet weak var aroundYouButtonOutlet: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func aroundYouButtonPressed(){
        self.activeShowsOutlet.backgroundColor = UIColor.init(hexString: "#E7E7E7FF")
        self.pastShowsViewOutlet.backgroundColor = UIColor.init(hexString: "#E7E7E7FF")
        self.aroundYouViewOutlet.backgroundColor = UIColor.init(hexString: "#484848FF")
    }
    
    func pastShowButtonPressed(){
        self.aroundYouViewOutlet.backgroundColor = UIColor.init(hexString: "#E7E7E7FF")
        self.activeShowsOutlet.backgroundColor = UIColor.init(hexString: "#E7E7E7FF")
        self.pastShowsViewOutlet.backgroundColor = UIColor.init(hexString: "#484848FF")
    }
    
    func activeShowsButtonPressed(){
        self.aroundYouViewOutlet.backgroundColor = UIColor.init(hexString: "#E7E7E7FF")
        self.pastShowsViewOutlet.backgroundColor = UIColor.init(hexString: "#E7E7E7FF")
        self.activeShowsOutlet.backgroundColor = UIColor.init(hexString: "#484848FF")

    }
}
