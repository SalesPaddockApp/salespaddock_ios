//
//  AroundYouView.swift
//  Sales Paddock
//
//  Created by Rahul Sharma on 25/02/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

protocol HorseCellClickedDelegate {
    func cellClickedAtIndex(row : Int)
    func profileButtonClickedwith(userId : String)
}

class AroundYouView: UIView {
    
    @IBOutlet weak var noHorseFoundLabelOutlet: UILabel!
    var currentPageNumber  = Int()
    var filterData = [AnyObject]()
    var hostShowViewControllerObj:HostShowsViewController!{
        didSet{
            self.hostShowViewControllerObj.tabBarController?.delegate = self
            self.HorseCellClickedDelegate = hostShowViewControllerObj
        }
    }
    
    let horseObj = HorseData(postPrice: "", postID: "", postName: "", postImageUrl: "", userImage: "", userId: "", ownerName: "")
    let spinner = UIActivityIndicatorView(activityIndicatorStyle :.gray)
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(AroundYouView.handleRefresh(refreshControl:)), for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    
    var HorseCellClickedDelegate:HorseCellClickedDelegate?
    var aroundYouHorseData = [HorseData]()
    @IBOutlet weak var AroundYouTableView: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.AroundYouTableView.estimatedRowHeight = 270
        self.AroundYouTableView.rowHeight = UITableViewAutomaticDimension
        self.AroundYouTableView.addSubview(self.refreshControl)
        self.spinner.frame = CGRect(x: 0, y: 0, width: self.AroundYouTableView.frame.width, height: 60)
        self.AroundYouTableView.tableFooterView = self.spinner
        
    }
    func getAroundYou(currentIndex : Int, isFilteredApplied: Int, data: [AnyObject]){
        var params = ["userId":Utility.getUserID() as AnyObject,
                      "token" :Utility.getSessionToken() as AnyObject,
                      "pageNum": "\(currentIndex)" as AnyObject]
        
        
        if isFilteredApplied == 1 && data.count>0
        {
            params["filters"] = data as AnyObject
        }
        //        Helper.showPI()
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.getAroundYou,
                                 params: params,
                                 success: { (response) in
                                    
                                    self.refreshControl.endRefreshing()
                                    self.spinner.stopAnimating()
                                    Helper.hidePI()
                                    let errFlag = response["errCode"].numberValue
                                    
                                    switch errFlag {
                                    case 0:
                                        if let responseData = response["response"].arrayObject{
                                            if currentIndex == 0{
                                                self.aroundYouHorseData.removeAll()
                                            }
                                            let horsesArray = self.horseObj.fetchDataFromAPIForPost(postData: responseData)
                                            self.aroundYouHorseData.append(contentsOf: horsesArray)
                                            self.AroundYouTableView.reloadData()
                                            if let filterCount = response["countOfFilters"].int{
                                                if filterCount>0{
                                                    let filterData:[String:Any] = ["filters":response["filters"].arrayObject!]
                                                    UserDefaults.standard.setValue(filterData, forKey: "FiltersData")
                                                    self.filterData = response["filters"].arrayObject! as [AnyObject]
                                                }
                                                else{
                                                    UserDefaults.standard.setValue("", forKey: "FiltersData")
                                                }
                                                UserDefaults.standard.synchronize()
                                            }
                                        }
                                        break
                                    default:
                                        if let filterCount = response["countOfFilters"].int{
                                            if filterCount>0{
                                                let filterData:[String:Any] = ["filters":response["filters"].arrayObject!]
                                                UserDefaults.standard.setValue(filterData, forKey: "FiltersData")
                                                self.filterData = response["filters"].arrayObject! as [AnyObject]
                                            }
                                            else{
                                                UserDefaults.standard.setValue("", forKey: "FiltersData")
                                            }
                                            UserDefaults.standard.synchronize()
                                            self.aroundYouHorseData.removeAll()
                                            self.AroundYouTableView.reloadData()
                                            let errMsg = response["Message"].string
                                            self.window?.rootViewController?.navigationController?.visibleViewController?.showAlert("Error", message: errMsg!)
                                            break
                                        }}},
                                 failure: { (Error) in
                                    self.refreshControl.endRefreshing()
                                    self.spinner.stopAnimating()
                                    Helper.hidePI()
                                    self.window?.rootViewController?.navigationController?.visibleViewController?.showAlert("Error", message: Error.localizedDescription)
        })
    }
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        if let filterValue = UserDefaults.standard.object(forKey:"FiltersData") as? [String:Any]{
            filterData = filterValue["filters"] as! [AnyObject]
            //            self.numberOfFiltersOutlet.text = "\(filterData.count)"
            self.getAroundYou(currentIndex:0, isFilteredApplied: 1, data: filterData)
            self.currentPageNumber = 0
        }
        else{
            filterData.removeAll()
            self.currentPageNumber = 0
            self.getAroundYou(currentIndex:0, isFilteredApplied: 0, data: filterData)
        }
    }
}

extension AroundYouView : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AroundViewHorseCell", for: indexPath) as! AroundViewHorseCell        
        cell.horseData = aroundYouHorseData[indexPath.row]
        cell.userId.removeAll()
        cell.profilePicButtonOutlet.tag = indexPath.row
        cell.profilePicButtonOutlet.addTarget(self, action: #selector(profileButtonPressed(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aroundYouHorseData.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if (self.HorseCellClickedDelegate != nil){
            self.HorseCellClickedDelegate?.cellClickedAtIndex(row: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == self.aroundYouHorseData.count - 1) {
            self.spinner.startAnimating()
            if let filterValue = UserDefaults.standard.object(forKey:"FiltersData") as? [String:Any]{
                filterData = filterValue["filters"] as! [AnyObject]
                self.getAroundYou(currentIndex:currentPageNumber, isFilteredApplied: 1, data: filterData)
                currentPageNumber = currentPageNumber+1
            }
            else{
                filterData.removeAll()
                currentPageNumber = currentPageNumber+1
                self.getAroundYou(currentIndex:currentPageNumber, isFilteredApplied: 0, data: filterData)
            }
        }
    }
    
    @objc func profileButtonPressed(_ sender: Any) {
        if (self.HorseCellClickedDelegate != nil){
            
            if let button = sender as? UIButton
            {
                let index = button.tag
//                print(index)
                let postData = aroundYouHorseData[index]
                if (postData.userId != nil){
                    self.HorseCellClickedDelegate?.profileButtonClickedwith(userId: postData.userId!)
                }
            }
        }
    }
}


extension AroundYouView : UITabBarControllerDelegate{
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool
    {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            self.AroundYouTableView.setContentOffset(CGPoint.zero, animated: true)
        }
        return true
    }
}
