//
//  listYourAssetTableViewCell.swift
//  salesPaddock
//
//  Created by Rahul Sharma on 12/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit

class listYourAssetTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var imageLbl: UIImageView!

    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
