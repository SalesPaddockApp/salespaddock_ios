//
//  breedCategoryViewController.swift
//  salesPaddock
//
//  Created by Rahul Sharma on 09/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//
//farmAddressController

import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher

class breedCategoryViewController: UIViewController ,UITableViewDataSource , UITableViewDelegate
{
    
    @IBOutlet weak var headerViewOutlet: UIView!
    @IBOutlet weak var numberOfStepsOutlet: UIView!
    @IBOutlet weak var checkBoxOutlet: UIImageView!
    @IBOutlet weak var horseImageOutlet: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var lat = String()
    var long = String()
    var responseData = [JSON]()
    var sortedData = [JSON]()
    var selectedValuesOfList = [AnyObject]()
    var isComingFromList = Bool()
    var postId = String()
    var imageId = String()
    var imageData = [String:JSON]()
    var imageUrl = String()
    var prductId = String()
    var numberofsteps = 0
    var totalNumberOfSteps = 0
    var isAdminApproved : Bool = false
    
    
    var window: UIWindow?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        self.tableView.estimatedRowHeight = 70
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        if isComingFromList{
            addDataIfThereIsSelectedValues()
            addPhotosToMediaArray()
            if(!imageUrl.isEmpty){
                var dictionary = [String:AnyObject]()
                let array = [imageUrl]
                dictionary["options"] = array as AnyObject
                dictionary["pref_id"] = self.imageData["id"]!.stringValue as AnyObject
                UserDefaults.standard.set(dictionary, forKey: "selectedValue")
                UserDefaults.standard.synchronize()
            }
        }
        else
        {
            self.createPost()
        }
        self.addPhoto()
        self.navigationController?.white()
        self.updateNumberOfStepsInView(numberOfSteps: numberofsteps, totalCount: totalNumberOfSteps)
    }
    
    func addPhotosToMediaArray()
    {
        var jsonValueArray = [JSON]()
        if(self.imageId.isEmpty && !imageData.isEmpty){
            self.imageId = (self.imageData["id"]?.stringValue)!
        }
        if(!imageData.isEmpty){
            
            if let imageId = self.imageData["id"]?.stringValue
            {
                self.imageId = imageId
            }
            
            let imageArray = imageData["options"]?.arrayValue
            jsonValueArray = imageArray! as [JSON]
        }
        if jsonValueArray.count == 0
        {
            return
        }
        var mediaArray = [AnyObject]()
        for jsonObj in jsonValueArray
        {
            let dict = jsonObj.dictionaryValue
            var dataDict = [String: AnyObject]()
            if(!dict.isEmpty)
            {
                if dict["type"]?.stringValue == "0"
                {
                    dataDict["type"] = dict["type"]?.stringValue as AnyObject
                    dataDict["imageUrl"] = dict["imageUrl"]?.stringValue as AnyObject
                }
                else
                {
                    dataDict["type"] = dict["type"]?.stringValue as AnyObject
                    dataDict["thumbnailUrl"] = dict["thumbnailUrl"]?.stringValue as AnyObject
                    dataDict["imageUrl"] = dict["imageUrl"]?.stringValue as AnyObject
                }
                mediaArray.append(dataDict as AnyObject)
            }
        }
        var dictionary = [String:AnyObject]()
        if(mediaArray.count == 0){
            return
        }
        dictionary["options"] = mediaArray as AnyObject
        dictionary["pref_id"] = self.imageId as AnyObject
        UserDefaults.standard.set(dictionary, forKey: "selectedValue")
        UserDefaults.standard.synchronize()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        selectedValuesOfList = selectedValuesOfList.flatMap {$0} //for removing repeated values
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.addValueToSelectedArray()
    }
    
    func addValueToSelectedArray(){
        
        sortedData = responseData.sorted { $0["prirority"].intValue < $1["prirority"].intValue }
        self.navigationController?.white()
        
        if((UserDefaults.standard.value(forKey: "selectedValue")) != nil){
            guard let selectedValue = UserDefaults.standard.value(forKey: "selectedValue") as? [String: AnyObject] else { return }
            if let options = selectedValue["options"] as? [AnyObject]
            {
                if !options.isEmpty{
                    if let values = options[0] as? [String: AnyObject]
                    {
                        if let lat = values["lat"] as? String,let long = values["long"] as? String
                        {
                            self.lat = lat
                            self.long = long
                        }
                    }
                }
            }
            var searchDataSource = [AnyObject]()
            let searchString = selectedValue["pref_id"] as! String
            let predicate = NSPredicate(format: "self contains %@",searchString)
            
            searchDataSource = selectedValuesOfList.filter { predicate.evaluate(with: $0)
            }
            if(searchDataSource.isEmpty)
            {
                selectedValuesOfList.append(selectedValue as AnyObject)
                self.updateData(isComingFromPhotos: true, saveButtonPressed: false)
            }
            else{
                let indexOfPerson = selectedValuesOfList.index{$0 === searchDataSource[0]}
                selectedValuesOfList[indexOfPerson!] = selectedValue as AnyObject
                self.updateData(isComingFromPhotos: true, saveButtonPressed: false)
            }
        }
        UserDefaults.standard.removeObject(forKey: "selectedValue")
        UserDefaults.standard.synchronize()
        
        DispatchQueue.main.async { [unowned self] in
            self.tableView.reloadData()
        }
    }
    
    func addPhoto(){
        if(!imageData.isEmpty ){
            let imageArray = imageData["options"]?.arrayValue
            if(!(imageArray?.isEmpty)!){
                
                if  let dataDictionary = imageArray?[0].dictionaryValue
                {
                    if(dataDictionary.isEmpty)
                    {
                        return
                    }
                    var url = URL(string: (imageArray?[0].stringValue)!)
                    
                    if(dataDictionary["type"]?.stringValue == "0")
                    {
                        url = URL(string:(dataDictionary["imageUrl"]?.stringValue)!)
                    }
                    else
                    {
                        url = URL(string:(dataDictionary["thumbnailUrl"]?.stringValue)!)
                    }
                    loadImage(url:url!)
                }
            }
        }
    }
    
    func loadImage(url : URL){
        
        self.checkBoxOutlet.image = #imageLiteral(resourceName: "tick_box")
        
        self.horseImageOutlet.kf.indicatorType = .activity
        self.horseImageOutlet.kf.indicator?.startAnimatingView()
        
        horseImageOutlet.kf.setImage(with: url,
                                     placeholder: UIImage.init(named: "horse_default_image"),
                                     options: [.transition(ImageTransition.fade(1))],
                                     progressBlock: { receivedSize, totalSize in
        },
                                     completionHandler: { image, error, cacheType, imageURL in
                                        self.checkBoxOutlet.image = #imageLiteral(resourceName: "tick")
                                        self.horseImageOutlet.kf.indicator?.stopAnimatingView()
                                        
        })
    }
    
    @IBAction func uploadPhotoBtnClicked(_ sender: AnyObject)
    {
        Helper.showPI()
        self.performSegue(withIdentifier: "uploadPhotoSegue", sender: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        Helper.hidePI()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedData.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:breedCategoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifiers.breedCategoryTableViewCell) as! breedCategoryTableViewCell
        cell.subCategoryLbl.text = ""
        let object = self.sortedData[indexPath.row].dictionary
        cell.titleLbl.text = object?["title"]?.stringValue
        var searchDataSource = [AnyObject]()
        let searchString = (object!["id"]!.stringValue)
        let predicate = NSPredicate(format: "self contains %@",searchString)
        searchDataSource = self.selectedValuesOfList.filter {
            predicate.evaluate(with: $0)
        }
        if (searchDataSource.count > 0){
            if let dataArray:[AnyObject] = (searchDataSource[0] as! [String : AnyObject])["options"] as? [AnyObject]{
                if !dataArray.isEmpty {
                    if let dataDict = dataArray[0] as? [String:AnyObject] {
                        if let farmName = dataDict["farmName"] {
                            cell.subCategoryLbl.text = "\(farmName)"
                        }
                        if let lat = dataDict["lat"] as? String, let long = dataDict["long"] as? String {
                            self.lat = lat
                            self.long = long
                        }
                    }
                    else {
                        let dataStringArray = dataArray as! [String]
                        if(dataArray.count >= 2) {
                            let str = dataStringArray.joined(separator: ", ")
                            cell.subCategoryLbl.text = str
                        }
                        else {
                            cell.subCategoryLbl.text = dataStringArray[0]
                        }
                        cell.checkBoxOutlet.image = #imageLiteral(resourceName: "tick")
                    }
                } else {
                    cell.checkBoxOutlet.image = #imageLiteral(resourceName: "tick_box")
                }
                cell.checkBoxOutlet.image = #imageLiteral(resourceName: "tick")
            }
        }
        else
        {
            cell.checkBoxOutlet.image = #imageLiteral(resourceName: "tick_box")
        }
        
        if (object!["id"]!.stringValue == "599573b67046ff742b53b2d0") // adding NA by default for Sell Price
        {
            cell.checkBoxOutlet.image = UIImage(named:"tick")
            if (cell.subCategoryLbl.text?.count)!==0{
                cell.subCategoryLbl.text = "NA"
                var dictionary = [String:AnyObject]()
                let arr = ["NA"]
                dictionary["options"] = arr as AnyObject
                dictionary["pref_id"] = "599573b67046ff742b53b2d0" as AnyObject
                UserDefaults.standard.set(dictionary, forKey: "selectedValue")
                UserDefaults.standard.synchronize()
                self.addValueToSelectedArray()
            }
        }
        
        if (object!["id"]!.stringValue == "57efa28700eba35a3ed379ce")
        {
            cell.checkBoxOutlet.image = UIImage(named:"tick")
            if (cell.subCategoryLbl.text?.count)!==0{
                cell.subCategoryLbl.text = "Contact for Price"
                var dictionary = [String:AnyObject]()
                let arr = ["Contact for Price"]
                dictionary["options"] = arr as AnyObject
                dictionary["pref_id"] = "57efa28700eba35a3ed379ce" as AnyObject
                UserDefaults.standard.set(dictionary, forKey: "selectedValue")
                UserDefaults.standard.synchronize()
                self.addValueToSelectedArray()
            }
        }
        if (object!["id"]!.stringValue == "57efa18600eba31340d379cc")
        {
            cell.checkBoxOutlet.image = UIImage(named:"tick")
            if (cell.subCategoryLbl.text?.count)!==0{
                cell.subCategoryLbl.text = "None"
                var dictionary = [String:AnyObject]()
                let arr = ["None"]
                dictionary["options"] = arr as AnyObject
                dictionary["pref_id"] = "57efa18600eba31340d379cc" as AnyObject
                UserDefaults.standard.set(dictionary, forKey: "selectedValue")
                UserDefaults.standard.synchronize()
                self.addValueToSelectedArray()
            }
        }
        if (object!["id"]!.stringValue == "58651cb4deaae5636131609b")
        {
            cell.checkBoxOutlet.image = UIImage(named:"tick")
            if (cell.subCategoryLbl.text?.count)!==0{
                cell.subCategoryLbl.text = "Not applicable"
                var dictionary = [String:AnyObject]()
                let arr = ["Not applicable"]
                dictionary["options"] = arr as AnyObject
                dictionary["pref_id"] = "58651cb4deaae5636131609b" as AnyObject
                UserDefaults.standard.set(dictionary, forKey: "selectedValue")
                UserDefaults.standard.synchronize()
                self.addValueToSelectedArray()
            }
        }
        return cell
    }
    
    func createPost(){
        
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let userId = UserDefaults.standard.object(forKey: "user_id") as! String!
        let productType = UserDefaults.standard.object(forKey: "productType") as! String!
        var params:[String : AnyObject] = ["token":token! as AnyObject,"userId":userId! as AnyObject,"values":selectedValuesOfList as AnyObject,"productType":productType! as AnyObject,"_id":self.prductId as AnyObject]
        if self.lat.count>0 {
            params["lat"] = self.lat as AnyObject
            params["long"] = self.long as AnyObject
        }
        Helper.showPI()
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.createPost, params: params as [String : AnyObject]?
            , success:
            
            { (response) in
                
                Helper.hidePI()
                
                let errFlag = response["errCode"].numberValue
                switch errFlag {
                case 0:
                    self.postId = response["response"]["postId"].stringValue
                    self.updateNumberOfStepsInView(numberOfSteps: 2, totalCount: response["response"]["totalCount"].number as! Int)
                    self.isComingFromList = true
                    
                default:
                    _ = response["Message"].stringValue
                    break
                }
        }, failure: { (Error) in
            Helper.hidePI()
        })
    }
    
    func updateData(isComingFromPhotos : Bool, saveButtonPressed: Bool){
        
        let token = UserDefaults.standard.object(forKey: "session_token") as! String!
        let userId = UserDefaults.standard.object(forKey: "user_id") as! String!
        var params:[String : AnyObject] = ["token":token! as AnyObject,"userId":userId! as AnyObject,"values":selectedValuesOfList as AnyObject,"postId":self.postId as AnyObject,"_id":self.prductId as AnyObject]
        if self.lat.count>0 {
            params["lat"] = self.lat as AnyObject
            params["long"] = self.long as AnyObject
        }
        
        Helper.showPI()
        
        AFWrapper.requestPOSTURL(serviceName: Constant.ServiceMethodNames.updatePost, params: params as [String : AnyObject]?
            , success:
            
            { (response) in
                
                Helper.hidePI()
                
                let errFlag = response["errCode"].numberValue
//                print(response)
                switch errFlag {
                case 0:
                    var totalCountSteps = 0
                    var completedCount = 0
                    if let totalCount = response["response"]["totalCount"].number
                    {
                        totalCountSteps = totalCount.intValue
                    }
                    if let steps = response["response"]["steps"].number
                    {
                        completedCount = steps.intValue
                    }
                    self.updateNumberOfStepsInView(numberOfSteps: completedCount, totalCount: totalCountSteps)
                    
                    if let imageDataArray = response["response"]["image"].dictionary
                    {
                        self.imageData = imageDataArray
                    }
                    if !isComingFromPhotos
                    {
                        let postData = response["response"].dictionary
                        
                        if let isPostCompleted = postData?["isPostCompleted"]?.boolValue
                        {
                            if isPostCompleted{
                                if self.isAdminApproved{
                                    let alert = UIAlertController(title: "Success !!", message: "Your listing has been updated.", preferredStyle: UIAlertControllerStyle.alert) //msg will changed
                                    
                                    // add an action (button)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                                        self.postId = response["postId"].stringValue
                                        self.takeUserBackToListingPage()
                                    }))
                                    // show the alert
                                    self.present(alert, animated: true, completion: nil)
                                }
                                else{
                                    let alert = UIAlertController(title: "Success !!", message: "Your listing is complete and waiting for admin approval", preferredStyle: UIAlertControllerStyle.alert)
                                    
                                    // add an action (button)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                                        self.postId = response["postId"].stringValue
                                        self.takeUserBackToListingPage()
                                    }))
                                    // show the alert
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                            else{
                                
                                let alert = UIAlertController(title: "Success !!", message: "Listing saved. Please come back and complete so Admin can approve.", preferredStyle: UIAlertControllerStyle.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                                    self.postId = response["postId"].stringValue
                                    self.takeUserBackToListingPage()
                                }))
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                        else{
                            let alert = UIAlertController(title: "Success !!", message: "Your Horse is Saved", preferredStyle: UIAlertControllerStyle.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                                self.postId = response["postId"].stringValue
                                self.takeUserBackToListingPage()
                            }))
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                default:
                    _ = response["Message"].string
                    break
                }
        }, failure: { (Error) in
            Helper.hidePI()
        })
    }
    
    @IBAction func saveBtnClicked(_ sender: AnyObject)
    {
        if isComingFromList {
            self.updateData(isComingFromPhotos: false, saveButtonPressed:true)
            return
        }
        self.createPost()
    }
    
    // method to run when table view cell is tapped
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var type = Int()
        
        let object = sortedData[indexPath.row].dictionary
        type = (object?["type"]?.numberValue.intValue)!
        switch type
        {
        case 1:
            self.performSegue(withIdentifier: "selectBreedSegue", sender: indexPath)
            break
            
        case 2: self.performSegue(withIdentifier: "disciplineSegue", sender: indexPath)
            break
            
        case 3: self.performSegue(withIdentifier: "priceSegue", sender: indexPath)
            break
            
        case 4: self.performSegue(withIdentifier: "descriptionSegue", sender: indexPath)
            break
            
        case 5:
            self.performSegue(withIdentifier: "calendar segue", sender: indexPath)
            break
        case 7:
            self.performSegue(withIdentifier: "Open Farm Name Lists", sender: indexPath)
        default:
            break
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    /*
     @IBAction func addToShowActionButton(_ sender: AnyObject)
     {
     openShowController()
     }
     
     func openShowController(){
     
     if(postId.isEmpty)
     {
     self.saveData(isDataSavedBefore: true)
     return
     }
     
     self.performSegue(withIdentifier: "show Shows Segue", sender: self)
     
     }
     */
    
    func takeUserBackToListingPage(){
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of : RAMAnimatedTabBarController.self)
            {
                _ = self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                break
            }
        }
    }
    
    @IBAction func backBtnClicked(_ sender: AnyObject) {
        
        self.takeUserBackToListingPage()
    }
    
    @IBAction func click(_ sender: UITapGestureRecognizer)
    {
    }
    
    func getName() -> String {
        var name = ""
        for data in sortedData {
            if (data["id"].stringValue == "57efa0aa00eba37a3dd379cd") { //Name
                if let pName = data.dictionaryValue["options"]?[0].stringValue {
                    name = pName
                }
            }
        }
        return name
    }
    
    func getDescription() -> String {
        var description = ""
        for data in sortedData {
            if (data["id"].stringValue == "57efa19900eba35a3ed379cd") { //description
                if let pDesc = data.dictionaryValue["options"]?[0].stringValue {
                    description = pDesc
                }
            }
        }
        return description
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "uploadPhotoSegue")
        {
            let controller = segue.destination as! UploadPhotoViewController
            controller.delegate = self
            controller.imageId = self.imageId
            if(self.imageId.isEmpty && !imageData.isEmpty) {
                self.imageId = (self.imageData["id"]?.stringValue)!
            }
            if(!imageData.isEmpty) {
                if let imageId = self.imageData["id"]?.stringValue
                {
                    self.imageId = imageId
                }
                let imageArray = imageData["options"]?.arrayValue
                controller.jsonValueArray = imageArray!
            }
            controller.postName = self.getName()
            controller.postDescription = self.getDescription()
        }
        else if(segue.identifier == "show Shows Segue")
        {
            let controller:HostsShowsViewController = segue.destination as! HostsShowsViewController
            controller.isComingFromShowDetails = true
            controller.postId = self.postId
        }
        else {
            var object = [String:JSON]()
            var type = Int()
            let indexPath = sender as! IndexPath
            let currentIndex = indexPath.row
            var controller = UIViewController()
            object = sortedData[currentIndex].dictionary!
            type = (object["type"]?.numberValue.intValue)!
            switch type
            {
            case 1:
                let controll = segue.destination as! breedViewController
                controll.title = object["title"]!.stringValue
                controll.titleStr = object["title"]!.stringValue
                controll.breedNames = self.sortedData[currentIndex]["values"].arrayValue
                controll.prefId = self.sortedData[currentIndex]["id"].stringValue
                
            case 2:
                let controll = segue.destination as! disciplineViewController
                controll.title = object["title"]!.stringValue
                controll.titleStr = object["title"]!.stringValue
                if let disciplines = self.sortedData[currentIndex]["values"].arrayObject as? [String]{
                    //                    controll.disciplines = disciplines.sorted()
                    controll.disciplines = disciplines
                }
                controll.prefId = self.sortedData[currentIndex]["id"].stringValue
                
            case 3:
                let controll = segue.destination as! priceViewController
                controll.title = object["title"]!.stringValue
                controll.prefId = self.sortedData[currentIndex]["id"].stringValue
                
            case 4:
                let controll = segue.destination as! descriptionViewController
                controll.title = object["title"]!.stringValue
                if let data = object["options"]?.arrayValue
                {
                    if (data.count > 0){
                        controll.descriptionString = data[0].stringValue
                    }
                }
                
                let searchString = object["id"]?.stringValue
                let predicate = NSPredicate(format: "self contains %@",searchString!)
                let dataArray = selectedValuesOfList.filter {predicate.evaluate(with: $0)}
                if(!dataArray.isEmpty)
                {
                    if let options = dataArray[0] as? [String:Any]{
                        if let optionValues = options["options"] as? [String]
                        {
                            controll.descriptionString = optionValues[0]
                        }
                    }
                }
                controll.prefId = self.sortedData[currentIndex]["id"].stringValue
                
            case 5:
                let controll = segue.destination as! CalendarViewController
                controll.title = object["title"]!.stringValue
                controll.prefId = self.sortedData[currentIndex]["id"].stringValue
                break
                
            case 7:
                let navControler = segue.destination as! UINavigationController
                let controller = navControler.childViewControllers.first as! FarmNameListViewController
                controller.prefId = self.sortedData[currentIndex]["id"].stringValue
                break
                
            default:
                controller = segue.destination
                controller.title = object["title"]!.stringValue
            }
        }
    }
    
    func updateNumberOfStepsInView(numberOfSteps : Int, totalCount : Int)
    {
        if (totalCount == 0 || numberOfSteps == 0)
        {
            return
        }
        var xValue = 0
        let widtH = Int((self.view?.frame.size.width)!)/totalCount
        let widthOfView = widtH
        for index in 1...totalCount
        {
            let newWidth = xValue + widthOfView
            let progressView = UIView.init(frame: CGRect(x:CGFloat(xValue), y:1.0, width: CGFloat(widthOfView - 2), height: self.numberOfStepsOutlet.frame.size.height-2))
            xValue = newWidth + 2
            if index <= numberOfSteps
            {
                progressView.backgroundColor = Color.init(hexString: "#0ab49dff")
            }
            else
            {
                progressView.backgroundColor = Color.white
            }
            self.numberOfStepsOutlet.addSubview(progressView)
        }
        let incompleteCount = totalCount - numberOfSteps
        
        self.title = "\(incompleteCount) Step(s) to List"
        if (numberOfSteps == totalCount)
        {
            self.title = "Listing Complete"
        }
    }
}

extension breedCategoryViewController : photoUploaded {
    
    func addDataIfThereIsSelectedValues(){
        
        for dictionary in self.responseData {
            if (!dictionary["options"].isEmpty){
                
                var dict = [String:AnyObject]()
                var options = [AnyObject]()
                for str in dictionary["options"].arrayObject! {
                    options.append(str as AnyObject)
                }
                dict["options"] = options as AnyObject
                dict["pref_id"] = (dictionary["id"].stringValue) as AnyObject
                
                selectedValuesOfList.append(dict as AnyObject)
            }
        }
    }
    
    func photoUploaded(data: [String:AnyObject]) {
        let imageDict = data
        if(imageDict["type"]?.isEqual("0"))!
        {
            if let urlStr = imageDict["imageUrl"] as! String?
            {
                let url = URL(string:urlStr)
                
                loadImage(url:url!)
            }
        }
        else
        {
            let url = URL(string:imageDict["thumbnailUrl"] as! String)
            loadImage(url:url!)
        }
    }
}
