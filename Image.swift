//
//  Image.swift
//  Sales Paddock
//
//  Created by Sachin Nautiyal on 12/02/2018.
//  Copyright © 2018 3Embed. All rights reserved.
//

import Foundation

extension UIImage {
    
    func textToImage(drawText text: String, isSelected: Bool ) -> UIImage? {
        
        // Setup the font specific variables'
        var textColor : UIColor!
        if isSelected {
        textColor = UIColor.white
        } else {
        textColor = UIColor.init(hexString: "#484848FF")!
        }
        let textFont: UIFont = UIFont(name: "Helvetica Bold", size: 30)!
        
        //Setup the image context using the passed image.
        UIGraphicsBeginImageContext(self.size)
        
        //Setups up the font attributes that will be later used to dictate how the text should be drawn
        let textFontAttributes = [NSAttributedStringKey.foregroundColor: textColor,
                                  NSAttributedStringKey.font : textFont] as [NSAttributedStringKey : Any]
        
        //Put the image into a rectangle as large as the original image.
        self.draw(in: CGRect(x:0, y:0, width:self.size.width, height: self.size.height))
        
        // Creating a point within the space that is as bit as the image.
        if text.count == 1 {
        let rect: CGRect = CGRect(x:12, y:4, width:self.size.width, height:self.size.height)
            text.draw(in: rect, withAttributes: textFontAttributes)
        } else {
            let rect: CGRect = CGRect(x:4, y:4, width:self.size.width, height:self.size.height)
            text.draw(in: rect, withAttributes: textFontAttributes)
        }
        
        //Now Draw the text into an image.
        
        // Create a new image out of the images we have created
        let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        
        // End the context now that we have the image we need
        UIGraphicsEndImageContext()
        
        //And pass it back up to the caller.
        return newImage
        
    }
}
