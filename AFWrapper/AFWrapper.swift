//
//  AFWrapper.swift
//  Sales Paddock
//
//  Created by 3Embed on 15/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Firebase
import GoogleSignIn

class AFWrapper: NSObject {
    
    class func requestGETURL(serviceName: String, success:@escaping (JSON) -> Void, failure:@escaping (NSError) -> Void) {
        
        let strURL = Constant.URLs.baseURL + serviceName
        let headers = ["authorization":"Basic \("horse:123456".enCodeToBase64())"] as [String: String]
        Alamofire.request(strURL, method:.get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: {
            response in
            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                success(resJson)
                Helper.hidePI()
            }
            if response.result.isFailure {
                let error : NSError = response.result.error! as NSError
                failure(error)
//                print(error.localizedDescription)
                Helper.hidePI()
            }
        })
    }
    
    class func requestPOSTURL(serviceName : String, params : [String : AnyObject]?, success:@escaping (JSON) -> Void, failure:@escaping (NSError) -> Void) {
        
        if ReachabilityCheck().isNetworkAvailable == false {
            Helper.hidePI()
            return
        }
        
        let strURL = Constant.URLs.baseURL + serviceName
        let headers = ["authorization":"Basic \("horse:123456".enCodeToBase64())"] as [String: String]
        
        Alamofire.request(strURL, method:.post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: {
            response in
            Helper.hidePI()
            if response.result.isSuccess {
                //                print("\(strURL) \n\n params = \(String(describing: params)) \n\(response)")
                let resJson = JSON(response.result.value!)
                if (resJson["errCode"].intValue == 1 && resJson["errNum"].intValue == 133)
                {
                    if (UserDefaults.standard.object(forKey: "session_token") as? String) != nil{
                        
                        AFWrapper.showAlertForSessionExpired(message: "Your session has expired, because you just logged in from another device.")
                    }
                    return
                }
                success(resJson)
            }
            if response.result.isFailure {
                Helper.hidePI()
                let error : NSError = response.result.error! as NSError
                failure(error)
                print(error.localizedDescription)
            }
        })
    }
    
    class func requestPOSTURLForChat(serviceName : String, params : [String : AnyObject]?, success:@escaping (JSON) -> Void, failure:@escaping (NSError) -> Void) {
        
        
        if ReachabilityCheck().isNetworkAvailable == false {
            Helper.hidePI()
            return
        }
        
        let strURL = Constant.URLs.socketBaseURL + serviceName
        //        let headers = ["authorization":"Basic \("horse:123456".enCodeToBase64())"] as [String: String]
        
        Alamofire.request(strURL, method:.post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: {
            response in
            Helper.hidePI()
            if response.result.isSuccess {
                //                print("\(strURL) \n\n params = \(String(describing: params)) \n\(response)")
                let resJson = JSON(response.result.value!)
                if (resJson["errCode"].intValue == 1 && resJson["errNum"].intValue == 133)
                {
                    if (UserDefaults.standard.object(forKey: "session_token") as? String) != nil{
                        AFWrapper.showAlertForSessionExpired(message: "Your session has expired, because you just logged in from another device.")
                    }
                    return
                }
                success(resJson)
            }
            if response.result.isFailure {
                Helper.hidePI()
                let error : NSError = response.result.error! as NSError
                failure(error)
//                print(error.localizedDescription)
            }
        })
    }
    
    class func requestGETURLWithParams(serviceName: String,params:[String : AnyObject]?, success:@escaping (JSON) -> Void, failure:@escaping (NSError) -> Void) {
        
        let strURL = Constant.URLs.baseURL + serviceName
        let headers = ["authorization":"Basic \("horse:123456".enCodeToBase64())"] as [String: String]
        Alamofire.request(strURL, method:.get, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: {
            response in
            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                success(resJson)
                Helper.hidePI()
            }
            if response.result.isFailure {
                let error : NSError = response.result.error! as NSError
                failure(error)
//                print(error.localizedDescription)
                Helper.hidePI()
            }
        })
    }
    
    class func dataFromPlaces(serviceUrl: String, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
        
        Alamofire.request(serviceUrl, encoding: JSONEncoding.default).responseJSON(completionHandler: {
            response in
            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                success(resJson)
                Helper.hidePI()
            }
            if response.result.isFailure {
                let error : Error = response.result.error! as Error
                failure(error)
                Helper.hidePI()
            }
        })
    }
    
    class func showAlertForSessionExpired(message : String){
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let navControllerObj = storyboard.instantiateViewController(withIdentifier: "NavViewController") as! NavViewController
            let appDelegateObj = UIApplication.shared.delegate! as! AppDelegate
            if (appDelegateObj.window!.rootViewController as? NavViewController) != nil
            {
                DispatchQueue.main.async {
                    Utility.removeSessionToken()
                    appDelegateObj.window!.rootViewController = navControllerObj
                }
            }
            else
            {
                DispatchQueue.main.async {
                    Utility.removeSessionToken()
                    appDelegateObj.window!.rootViewController = navControllerObj
                }
            }
        }))
        let appDelegateObj = UIApplication.shared.delegate! as! AppDelegate
        // show the alert
        var controller = appDelegateObj.window!.rootViewController
        if (appDelegateObj.window!.rootViewController as? UINavigationController) != nil
        {
            let navControllr = appDelegateObj.window!.rootViewController as! UINavigationController
            controller = navControllr.visibleViewController
        }
        else
        {
            controller = appDelegateObj.window!.rootViewController
        }
        controller?.present(alert, animated: true, completion: nil)
        
        let batchCountArray = [String]()
        UserDefaults.standard.set(batchCountArray, forKey: "batchCount")
        NotificationCenter.default.post(name:Notification.Name(rawValue:"badgeCount"),
                                        object: nil,
                                        userInfo: ["count":batchCountArray.count])
        AFWrapper.deleteUserData()
    }
    
    /// To delete userdefaults and setting timestamp to zero to get all chat when user again login
    class func deleteUserData(){
        
        UserDefaults.standard.removeObject(forKey: "selectedFilters")
        let timestamp : Int = 0
        UserDefaults.standard.set(timestamp, forKey: "timeStamp")
        AFWrapper.deleteAllDocuments()
        // Unsubscrib to group channel
        FIRMessaging.messaging().unsubscribe(fromTopic: "adminMessage")
        GIDSignIn.sharedInstance().signOut()
    }
    
    /// TO delete all chat document
    class func deleteAllDocuments(){
        
        var result : CBLQueryEnumerator
        let database = CBObject.sharedInstance.database
        let query = database?.createAllDocumentsQuery()
        query?.allDocsMode =  CBLAllDocsMode.allDocs
        query?.descending = true
        do{
            result = try query!.run()
            for i in 0..<result.count{
                let cbe = CouchbaseEvevnts()
                cbe.deleteDocument(document: (database?.document(withID: (result.row(at: i)).documentID!)!)!)
            }
        }
        catch{
        }
    }
}

