//
//  UIButton+SpringAnimation.swift
//  Sales Paddock
//
//  Created by 3Embed on 16/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

extension UIButton {
    
    func springAnimation() -> Void {
        self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        
        UIView.animate(withDuration: 2.0,
                                   delay: 0,
                                   usingSpringWithDamping: 0.20,
                                   initialSpringVelocity: 5.00,
                                   options: UIViewAnimationOptions.allowUserInteraction,
                                   animations: {
                                    self.transform = CGAffineTransform.identity
            }, completion: nil)
    }
}
